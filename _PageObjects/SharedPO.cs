﻿using System;
using OpenQA.Selenium;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;
using FluentAssertions;
using System.Diagnostics;
using Framework.Tests.Steps;
using Portosis.UITests.Utils;
using System.Linq;
using SpecFlowTeste.Core.Utils;
using System.Collections.ObjectModel;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Reflection;
using System.Threading;

namespace UiTestSiteCP._PageObjects
{
    public class SharedPO : BasePagePO
    {

        public SharedPO(IWebDriver driver) : base(driver)
        { }
        public IWebElement TxtDescricaoProposta => FindElement(By.Id("ContentPlaceHolder1_lbDescricaoProposta"));

        /*Botão Gravar */
        public IWebElement BtnGravarSimulacao => FindElement(By.Id("ContentPlaceHolder1_btnGravar"));
        public IWebElement BtnGravarPreAnalise => FindElement(By.Id("BtnGravar"));
        public IWebElement BtnGravarIncProposta => FindElement(By.Id("ContentPlaceHolder1_BtnGravarp"));

        /*Botao Atualizar*/
        public IWebElement BtnAtualizar => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_BtnAtualizar' or @id='BtnAtualizar']"));

        /*Botão Avançar*/
        public IWebElement btnAvancarSimulacao => FindElement(By.Id("ContentPlaceHolder1_btnAvancar"));
        public IWebElement BtnAvancarIncProposta => FindElement(By.Id("ContentPlaceHolder1_BtnAvancardas"));
        public IWebElement BtnEfetivarProposta => FindElement(By.Id("ajustarIDEfetiavacao"));
        public IWebElement TxtMsgPreEfetivacao => FindElement(By.XPath("//*[@class='box_mensagem'][contains(.,'" + msgElement + "')]"));//FindElement(By.ClassName("box_mensagem"));
        public IWebElement BtnConfirmacaoMensagem => FindElement(By.XPath("//*[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-dialog-buttons' and (contains(@style,'display: block') and (contains(.,'" + msgElement + "')))]//*[@class='ui-button-text' and ((contains(text(),'Ok')) or (contains(text(),'Sim')))]"));

        /*Botão Cancelar Proposta*/
        public IWebElement btnCancelarProposta => FindElement(By.Id("ContentPlaceHolder1_btnCancelaProposta"));

        /*Campos para recalculo PMT*/
        public IWebElement txtValorOperacao => driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbUnVlrCreditoPrin' or @id='ContentPlaceHolder1_Simulacao1_lblValorResultSimula']"));
        public IWebElement txtValorParcela => driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbUnVlrParcela' or @id='ContentPlaceHolder1_Simulacao1_lblParcelaResultSimula']"));
        public IWebElement inputValorPrestacao => FindElement(By.Id("ContentPlaceHolder1_txtValorPrestacao"));

        /*PRÉ ANÁLISE E INCLUSAO DE PROPOSTA - SESSAO PROPOSTA CADASTRADA*/
        public IWebElement txtPrazo => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblSimulaPrazo"));
        public IWebElement txtPlano => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblSimulaPlano"));
        public IWebElement txtProduto => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblSimulaProduto"));
        public IWebElement txtSeguro => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblSimulaSeguro"));

        public static int EtapaAtualBD { get; set; }
        public static string EtapaAtualFront { get; set; }

        public static decimal _vlParcelaPMT { get; set; }
        public static decimal _vlOperacaoPMT { get; set; }
        public static string msgElement { get; set; }
        public static string descSeguro { get; set; }

        public void AtualizarStatusProposta(int situacao, string etapa)
        {
            EtapaAtualBD = Convert.ToInt32(etapa.Substring(0, 1));
            PageDownRodape();

            if (EtapaAtualBD == 2)
            {
                Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, situacao);
            }
            else
            {
                Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, situacao);
            }
            BtnAtualizar.Click();
        }
        public void AtualizarStatusNaBaseProposta(int situacao)
        {

            Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, situacao);
        }

        public void AlterarPMTPreenchendoValorParcela()
        {
            PageDownRodape();
            new ModalNovaSimulacaoPO(Configs.Driver).ArmazenarValoresReCalcularPMT(txtValorOperacao, txtValorParcela);
            if (ModalNovaSimulacaoPO._vlParcelaPMT != 0)
                _vlParcelaPMT = ModalNovaSimulacaoPO._vlParcelaPMT;
            inputValorPrestacao.ClickSendKeys(_vlParcelaPMT.ToString("F") + Keys.Tab);
            ReportSteps.AddScreenshot();
        }
        public void ValidarSituacaoProposta(string situacaoProp)
        {
            try
            {
                driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Houve alterações nos contratos refinanciados da proposta.']")); // 1) é obrigatório.')]"));            
                driver.FindElement(By.ClassName("ui-button-text")).Click();
            }
            catch { }
            PageUpCabecalho();
            //TxtDescricaoProposta.Text.Should().ContainAll(situacaoProp);
            ReportSteps.AddScreenshot();
        }
        public void ArmazenarNumeroProposta()
        {
            PropPCPO.NroProposta = TxtDescricaoProposta.Text.Substring(10, 10);
        }

        public void ValidarMensagem(string msg, bool bugTela = false)
        {// esta achando 02 componentes de botõa em tela

            msgElement = msg;
            TxtMsgPreEfetivacao.Text.Should().ContainAll(msg);
            ReportSteps.AddScreenshot();

            if (bugTela)
            {
                Configs.Driver.Scripts().ExecuteScript($"document.querySelector('.ui-button-text').click()");
                driver.FindElement(By.XPath("//*[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-dialog-buttons' and (contains(@style,'display: block') and (contains(.,'" + msgElement + "')))]//*[@class='ui-button-text' and ((contains(text(),'Ok')) or (contains(text(),'Sim')))]")).Click();
            }
            else
            {
                BtnConfirmacaoMensagem.Click();
            }
            msgElement = null;

        }

        public void ClicarCancelarProposta()
        {
            btnCancelarProposta.Click();

            try
            {
                Thread.Sleep(500);
                driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Situação atual da proposta não permite cancelamento!')]"));
                driver.FindElement(By.ClassName("ui-button-text")).Click();
                if (SharedPO.EtapaAtualBD == 2)
                {
                    Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, 31);
                }
                else
                {
                    Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, 21);
                }
                BtnAvancarIncProposta.Click();
            }
            catch { }
        }
        public void ValidaRedirecionamentoPagina(string etapaProp)
        {
            PageUpCabecalho();
            string sitProposta = TxtDescricaoProposta.Text.Substring(34, 2);
            var etapa = etapaProp.Substring(0, 1);
            ReportSteps.AddScreenshot();
            FindElement(By.XPath("//div[@class='s" + etapa + "']//span[@class='active']")).Should().NotBeNull();

        }
        public void ValidarValorParcelaPMT()
        {
            if (ModalNovaSimulacaoPO._vlOperacaoPMT != 0)
                _vlParcelaPMT = ModalNovaSimulacaoPO._vlParcelaPMT;

            PageDownRodape();
            var operacaoMin = Math.Floor((_vlParcelaPMT - (_vlParcelaPMT * Configs.PercentualQuebraPMT)) * 100) / 100;
            var operacaoMax = Math.Round((_vlParcelaPMT + (_vlParcelaPMT * Configs.PercentualQuebraPMT)), 2);
            var valorParceGerada = decimal.Round(Convert.ToDecimal(txtValorParcela.Text), 2, MidpointRounding.AwayFromZero);
            ReportSteps.AddScreenshot();
            (ValidarValorEntreMaxEMin(valorParceGerada, operacaoMin, operacaoMax)).Should().BeTrue();
        }
        public void ValidarValorOperacaoPMT()
        {
            if (ModalNovaSimulacaoPO._vlOperacaoPMT != 0)
                _vlOperacaoPMT = ModalNovaSimulacaoPO._vlOperacaoPMT;

            PageDownRodape();
            var operacaoMin = Math.Floor((_vlOperacaoPMT - (_vlOperacaoPMT * Configs.PercentualQuebraPMT)) * 100) / 100;
            var operacaoMax = Math.Round((_vlOperacaoPMT + (_vlOperacaoPMT * Configs.PercentualQuebraPMT)), 2);
            var valorOperacaoGerado = decimal.Round(Convert.ToDecimal(txtValorOperacao.Text), 2, MidpointRounding.AwayFromZero);
            // ReportSteps.AddScreenshot();
            (ValidarValorEntreMaxEMin(valorOperacaoGerado, operacaoMin, operacaoMax)).Should().BeTrue();

        }
        public bool ValidarSituacaoPropostaBase(string situacao)
        {
            int sit = Convert.ToInt32(situacao.Substring(0, 2));
            var PropBase = Repositories.ClientesRepositorySicred.BuscarDadosProposta(PropPCPO.NroProposta);
            var situacaoAnterior = Convert.ToInt32(PropBase.SituacaoAnterior);
            var situacaoAtual = Convert.ToInt32(PropBase.SITUACAO);
            if (situacaoAnterior == sit || situacaoAtual == sit)
            {
                return true;
            }
            return false;
        }

        public void AtualizarSeguro(string seguro)
        {
            descSeguro = seguro.Trim();
            switch (descSeguro)
            {
                case "BÁSICO":
                    if (PropPCPO.Produto != Configs.CodCPFatura && PropPCPO.Produto != Configs.CodMoto)
                        PropPCPO.Seguro = "14";
                    break;
                case "COMPLETO":
                    if (PropPCPO.Produto != Configs.CodMoto)
                        PropPCPO.Seguro = "15";
                    break;
                case "SEM SEGURO":
                    PropPCPO.Seguro = "0";
                    break;
                default:
                    break;
            }
            try
            {
                // Atualizar Elemento Combo da Página de Simulação
                driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_uc_ddlTipoSeguro_ddlTipoSeguro']/option[contains(text(),'" + descSeguro + "')]")).Click();
            }
            catch
            {
                // Atualizar Elemento Combo da Página de Nova Simulação
                driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_Simulacao1_uc_ddlTipoSeguro_ddlTipoSeguro']/option[contains(text(),'" + descSeguro + "')]")).Click();
                //*[@id="ContentPlaceHolder1_Simulacao1_uc_ddlTipoSeguro_ddlTipoSeguro"]/option[contains(text(),'B')]
            }
        }

        public void ValidarPrazoAlterado()
        {
            MoveToElement(txtPrazo);
            txtPrazo.Text.Should().Contain(Convert.ToString(Convert.ToInt32(PropPCPO.Prazo)));
            ReportSteps.AddScreenshot();
        }

        public void ValidarPlanoAlterado()
        {
            MoveToElement(txtPlano);
            txtPlano.Text.Should().Contain(Convert.ToString(Convert.ToInt32(PropPCPO.Plano)));
            ReportSteps.AddScreenshot();
        }

        public void ValidarProdutoAlterado()
        {
            MoveToElement(txtProduto);
            txtProduto.Text.Should().Contain(Convert.ToString(Convert.ToInt32(PropPCPO.Produto)));
            ReportSteps.AddScreenshot();
        }

        public void ValidarSeguroAlterado()
        {
            MoveToElement(txtSeguro);
            txtSeguro.Text.ToUpper().Should().Contain(SharedPO.descSeguro);
            ReportSteps.AddScreenshot();
        }

        public void RealizarRefreshPagina()
        {
            driver.Navigate().Refresh();
        }





    }
}


//public void AtualizarStatusProposta(string etapa)
//{
//    EtapaAtualBD = Convert.ToInt32(etapa.Substring(0, 1));
//    string sitProposta = TxtDescricaoProposta.Text.Substring(34, 2);
//    var etapaBD = Convert.ToInt32(etapa.Substring(0, 1));
//    Stopwatch time = new Stopwatch();
//    time.Start();
//    PageDownRodape();
//    do
//    {
//        if (EtapaAtualBD == 2)
//        {

//            BtnAtualizar.Click();
//            Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, 31);
//        }
//        else
//        {

//            BtnAtualizar.Click();
//            Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, 21);
//        }
//        BtnAtualizar.Click();
//        sitProposta = TxtDescricaoProposta.Text.Substring(34, 2);
//    } while (sitProposta != "21" && sitProposta != "31");
//    time.Stop();
//    if ((sitProposta != "31" && sitProposta != "21"))
//    {
//        Console.WriteLine(sitProposta + "NDA ");
//        return false;
//    }
//    return true;
//}



//case "05": // ANÁLISE AUTOMÁTICA
//    Console.WriteLine("situacaoPro: " + sitProposta);
//    BtnAtualizar.Click();
//    break;
//case "02": // ANÁLISE MANUAL
//    Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(TxtDescricaoProposta.Text.Substring(10, 10), etapaBD);
//    Console.WriteLine("situacaoPro: " + sitProposta + " set na base situacao - nrProposta: " + TxtDescricaoProposta.Text.Substring(10, 10));
//    BtnAtualizar.Click();
//    break;
//case "22": // PROPOSTA EM VERIFICAÇÃO PELA TI                           
//    Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(TxtDescricaoProposta.Text.Substring(10, 10), etapaBD);
//    Console.WriteLine("situacaoPro: " + sitProposta + " set na base situacao - nrProposta:" + TxtDescricaoProposta.Text.Substring(10, 10));
//    BtnAtualizar.Click();
//    break;
//case "07": // PROPOSTA REPROVADA AUTOMATICAMENTE
//    Console.WriteLine("situacaoPro: " + sitProposta);
//    // throw new NotImplementedException("Situação 07 (PROPOSTA REPROVADA AUTOMATICAMENTE)");
//    break;
//case "10": // PROPOSTA PENDENTE
//    Console.WriteLine("situacaoPro: " + sitProposta);
//    // throw new NotImplementedException("Situação 10 (PROPOSTA PENDENTE)");
//    break;




//public void GravarEtapa(string etapa)
//{
//    PageDownRodape();
//    switch (etapa)
//    {
//        case "Simulação":
//            BtnGravarSimulacao.Click();
//            return true;
//        case "Pré Análise":
//            BtnGravarPreAnalise.Click();
//            return true;
//        case "Inclusão de Proposta":
//            BtnGravarIncProposta.Click();
//            return true;
//        default:
//            return true;
//    }
//    Thread.Sleep(2000);
//}

//public void AvancarEtapa(string etapa)
//{
//    PageDownRodape();
//    switch (etapa)
//    {
//        case "Simulação":
//            BtnAvancarSimulacao.Click();
//            return true;
//        case "Inclusão de Proposta":
//            BtnAvancarIncProposta.Click();
//            return true;
//        default:
//            return true;
//    }
//    Thread.Sleep(2000);
//}