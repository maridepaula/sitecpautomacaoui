﻿using FluentAssertions;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace SpecFlowTeste._PageObjects
{
    class ConsolidadoDoClientePO : BasePagePO
    {
        public ConsolidadoDoClientePO(IWebDriver driver) : base(driver)
        { }

        public IWebElement inputCPF => FindElement(By.Id("ContentPlaceHolder1_PickListCliente1_txtCPF"));
        public IWebElement btnLimpar => FindElement(By.Id("ContentPlaceHolder1_HyperLink1"));
        public IWebElement btnConsultar => FindElement(By.Id("ContentPlaceHolder1_Consultar"));
        public IList<IWebElement> GridDadosDosContratos => new List<IWebElement>(FindElements(By.CssSelector("[class='ConsolidadoCliente'] [id='divContrato']")));
        //static IList listaContratos = new List<dynamic>();
        public IWebElement txtNomeClienteGrid => FindElement(By.Id("ContentPlaceHolder1_nomeCliente"));
        public IWebElement txtCPFClienteGrid => FindElement(By.Id("ContentPlaceHolder1_cpfCliente"));

        private static string CPFClienteConsolidadoBase { get; set; }

        public void InformarCPFConsolidado(string CPF)
        {
            inputCPF.ClickSendKeysOnChange(CPF, false);
        }

        public void SelecionarLimpar()
        {
            btnLimpar.Click();
        }

        public void SelecionarConsultar()
        {
            btnConsultar.Click();
        }

        public void InformarCPFConsolidadoSemContratoEfetivado(string Produto)
        {
            var prod = Regex.Replace(Produto, "[^0-9]+", string.Empty);
            var ClienteBase = Repositories.ClientesRepositorySicred.BuscaClienteSemContratoEfetivado(prod);
            InformarCPFConsolidado(ClienteBase.CLIENTE.Substring(0, 11));
        }

        public void InformarCPFConsolidadoComContratoEfetivado(string Produto)
        {
            var prod = Regex.Replace(Produto, "[^0-9]+", string.Empty);
            CPFClienteConsolidadoBase = (Repositories.ClientesRepositorySicred.BuscaCPFClienteComContratoEfetivado(prod)).Substring(0, 11);
            // listaContratos = Repositories.ClientesRepositorySicred.BuscaCPFClienteComContratoEfetivado(prod);
            InformarCPFConsolidado(CPFClienteConsolidadoBase);
        }

        public void ValidarDadosDoContratoConsultadoNaGripPrincipal()
        {
            var ContratosBase = Repositories.PlanosRepositorySicred.ObterDadosDosContratosPorCPF(CPFClienteConsolidadoBase+"1", 20);
            foreach (var element in GridDadosDosContratos)
            {
                foreach (var contrato in ContratosBase)
                {
                    if ( element.Text.Contains(contrato.CONTRATO.ToString()))
                    {
                        txtNomeClienteGrid.Text.Should().ContainAll(contrato.NOME.ToString());                      
                        txtCPFClienteGrid.Text.Should().ContainAll(string.Format(@"{0:000\.###\.###-##}", Convert.ToInt64(contrato.CPF)));
                        element.Text.Should().ContainAll(contrato.CONTRATO.ToString());
                        element.Text.Should().ContainAll((($"{Convert.ToDecimal(contrato.VALOROPERACAO):C}").Replace("R$", "")).Trim());
                        element.Text.Should().ContainAll((($"{Convert.ToDecimal(contrato.VALORLIBERADO):C}").Replace("R$", "")).Trim());
                        element.Text.Should().ContainAll((($"{Convert.ToDecimal(contrato.PARCELA):C}").Replace("R$", "")).Trim());
                        element.Text.Should().ContainAll(contrato.PRAZO.ToString());
                        element.Text.Should().ContainAll(contrato.PRODUTO.ToString());
                        element.Text.Should().ContainAll(contrato.LOJISTA.ToString());
                        element.Text.Should().ContainAll(contrato.LOJA.ToString());
                        break;
                    }
                }
            }
            CPFClienteConsolidadoBase = null;

        }


    }
}
