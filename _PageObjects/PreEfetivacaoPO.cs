﻿using FluentAssertions;
using Framework.Tests.Steps;
using OpenQA.Selenium;
using Portosis.UITests.Utils;
using System;
using System.Threading;
using UiTestSiteCP.Core.Utils;


namespace UiTestSiteCP._PageObjects
{
    public class PreEfetivacaoPO : BasePagePO
    {
        public PreEfetivacaoPO(IWebDriver driver) : base(driver)
        { }
        /*INCLUSÃO DE PROPOSTA - Dados dos Cheques Pré Datados*/
        public IWebElement btnCompletarCheques => FindElement(By.Id("ContentPlaceHolder1_btCompletarCheques"));
        public IWebElement inputCMC7 => FindElement(By.Id("ctl00_ContentPlaceHolder1_rptCheques_ctl00_txtCMC7"));


        /* PRE EFETIVACAO - DADOS LIBERACAO DE CREDITO */
        public IWebElement cboTipoPessoa => FindElement(By.CssSelector("[id='ContentPlaceHolder1_ddlTipoPessoa']>[value='"+ Lib.TipoPessoa + "']"));
        public IWebElement cboFormaLiberacao => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dplblFormaLibera']>[value='" + Lib.FormaLib + "']"));
        public IWebElement inputCnpjProprietario => FindElement(By.Id("ContentPlaceHolder1_txtCpfCnpj"));
        public IWebElement inputBeneficiario => FindElement(By.Id("ContentPlaceHolder1_txtBeneficiario"));
        public IWebElement cboBancoBenef => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dropBanco1']>[value='"+ Lib.Banco + "']"));
        public IWebElement inputAgenciaBenef => FindElement(By.Id("ContentPlaceHolder1_txtCodAgencia"));
        public IWebElement cboTipoContaBancariaBenef => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dropTipoContaBancaria1']>[value='"+ Lib.TipoContaBancaria + "']"));
        public IWebElement inputContaBenef => FindElement(By.Id("ContentPlaceHolder1_txtConta1"));               
        public IWebElement txtValorLiberacao => FindElement(By.Id("ContentPlaceHolder1_txtValorLibera"));
        public IWebElement sessaoDadosLiberacao => FindElement(By.ClassName("dadosLiberacao"));


        /* CP FATURA - PRE EFETIVACAO - DADOS LIBERACAO DE CREDITO PREENCHIDOS */        
        public IWebElement txtCboFormaLiberacao => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dplblFormaLibera']>[selected='selected']"));
        public IWebElement txtCnpjProprietario => FindElement(By.Id("ContentPlaceHolder1_txtCpfCnpj"));
        public IWebElement txtBeneficiario => FindElement(By.Id("ContentPlaceHolder1_txtBeneficiario"));
        public IWebElement txtCboBancoBenef => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dropBanco1']>[selected='selected']"));
        public IWebElement txtAgenciaBenef => FindElement(By.Id("ContentPlaceHolder1_txtCodAgencia"));
        public IWebElement txtCboTipoContaBancariaBenef => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dropTipoContaBancaria1']>[selected='selected']"));
        public IWebElement txtContaBenef => FindElement(By.Id("ContentPlaceHolder1_txtConta1"));        





        /*PRE EFETIVACAO - PRE EFETIVAR*/
        public IWebElement btnPreEfetivar => FindElement(By.Id("ContentPlaceHolder1_BtnAvancar"));

        /*PRE EFETIVACAO - EFETIVAR*/
        public IWebElement btnEfetivar => FindElement(By.Id("ContentPlaceHolder1_BtnAvancar"));
        /*PRE EFETIVACAO  - CARNE*/
        public IWebElement inputEmail => FindElement(By.Id("ContentPlaceHolder1_txtEmail"));
        public IWebElement checkEmail => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_chbForma']//*[contains(@value,'1')]"));
        //        public IWebElement checkCorreios => FindElement(By.Id("ContentPlaceHolder1_chbForma_1"));
        public IWebElement checkCorreios => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_chbForma']//*[contains(@value,'2') or @id='ContentPlaceHolder1_chbForma_1']"));
        //        public IWebElement checkLoja => FindElement(By.XPath("//*[@id = 'ContentPlaceHolder1_chbForma_2"));
        public IWebElement checkLoja => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_chbForma']//*[contains(@value,'3') or @id='ContentPlaceHolder1_chbForma_2']"));
        public IWebElement cboBanco => FindElement(By.Id("ContentPlaceHolder1_lbDadosClienteCodBanco"));

        public IWebElement TxtDescricaoProposta => FindElement(By.Id("ContentPlaceHolder1_lbDescricaoProposta"));

      
        public void PreEfetivarProposta()
        {
            PageDownRodape();
            btnPreEfetivar.Click();
        }

        public void PreencherChequesPreDatados()
        {
            MoveToElement(inputCMC7);
            string banco = PropCli.CodBanco;

            switch (PropCli.CodBanco)
            {
                case "0001": //Banco do Brasil
                    inputCMC7.ClickSendKeysOnChange("001461280108500805606000639911");
                    break;
                case "0104": //CEF
                    inputCMC7.ClickSendKeysOnChange("151007850180000875700103352803");
                    break;
                case "0237": //BRADESCO
                    inputCMC7.ClickSendKeysOnChange("237207580100007105877500587348");
                    break;
                case "0341": //ITAÚ
                    inputCMC7.ClickSendKeysOnChange("341441404520000285721281591210");
                    break;
                case "0033": //SANTANDER
                    inputCMC7.ClickSendKeysOnChange("033024530185923255200010646461");
                    break;
                case "0041": //BANRISUL
                    inputCMC7.ClickSendKeysOnChange("041055540103092485735853772032");
                    break;
                default:
                    break;
            }

            PropCheq.CMC7 = inputCMC7.GetAttribute("value").Replace(" ", ""); // todo
            btnCompletarCheques.Click();
            ReportSteps.AddScreenshot();
        }

        public void PreencherChequesPreDatadosRefin()
        {
            MoveToElement(inputCMC7);

            switch (cboBanco.Text)
            {
                case "0001": //Banco do Brasil
                    inputCMC7.ClickSendKeysOnChange("001461280108500805606000639911");
                    break;
                case "0104": //CEF
                    inputCMC7.ClickSendKeysOnChange("151007850180000875700103352803");
                    break;
                case "0237": //BRADESCO
                    inputCMC7.ClickSendKeysOnChange("237207580100007105877500587348");
                    break;
                case "0341": //ITAÚ
                    inputCMC7.ClickSendKeysOnChange("341441404520000285721281591210");
                    break;
                case "0033": //SANTANDER
                    inputCMC7.ClickSendKeysOnChange("033024530185923255200010646461");
                    break;
                case "0041": //BANRISUL
                    inputCMC7.ClickSendKeysOnChange("041055540103092485735853772032");
                    break;
                default:
                    break;
            }

            inputCMC7.GetAttribute("value").Replace(" ", "");
            btnCompletarCheques.Click();
            ReportSteps.AddScreenshot();
        }      


        public void SelecionarTipoEnvio(string tipoEnvio)
        {
            switch (tipoEnvio.ToUpper())
            {
                case "EMAIL":
                    if (!checkEmail.Selected)
                        checkEmail.Click();
                    if (string.IsNullOrEmpty(PropCli.InfoEmail))
                    {
                        inputEmail.Text.Should().ContainAll(PropCli.InfoEmail);
                    }
                    else
                    {
                        FormaEnvioCarne.Email = PropCli.InfoEmail;
                    }
                    FormaEnvioCarne.Forma = "E-mail";
                    break;
                case "CORREIOS":
                    checkCorreios.Click();
                    FormaEnvioCarne.Forma = "Correios";
                    FormaEnvioCarne.Email = "";

                    if (PropPCPO.Produto != Configs.CodMoto)
                    {
                        if (checkEmail.Selected)
                            checkEmail.Click();
                    }
                    break;
                case "LOJA":
                    FormaEnvioCarne.Forma = "Loja";
                    FormaEnvioCarne.Email = "";
                    checkLoja.Click();
                    if (PropPCPO.Produto != Configs.CodMoto)
                    {
                        if (checkEmail.Selected)
                            checkEmail.Click();
                    }
                    break;
                //case "TODOS": //implementar futuramente
                //    if (string.IsNullOrEmpty(PropCli.InfoEmail))
                //    {
                //        inputEmail.Text.Should().ContainAll(PropCli.InfoEmail);
                //        FormaEnvioCarne.Email = PropCli.InfoEmail;
                //    }
                //    checkCorreios.Click();
                //    checkLoja.Click();
                //    break;
                default:
                    break;
            }
            Thread.Sleep(1000);

        }

        public void SelecionarTipoEnvioRefin(string tipoEnvio)
        {
            //Foi necessário replicar o método, pois para Refin não estamos preenchendo na tela e alimentando no objeto desde o início conforme o Financiamento
            switch (tipoEnvio.ToUpper())
            {
                case "EMAIL":
                    if (!checkEmail.Selected)
                    {                                       
                        checkEmail.Click();
                        inputEmail.ClickSendKeysOnChange(PropCli.InfoEmail);
                    }
                    if (string.IsNullOrEmpty(PropCli.InfoEmail))
                    {
                        inputEmail.Text.Should().ContainAll(PropCli.InfoEmail);
                    }                    
                    break;
                case "CORREIOS":
                    checkCorreios.Click();
                    FormaEnvioCarne.Forma = "Correios";
                    FormaEnvioCarne.Email = "";
                    if (PropPCPO.Produto != Configs.CodMoto)
                    {
                        if (checkEmail.Selected)
                            checkEmail.Click();
                    }
                    break;
                case "LOJA":
                    FormaEnvioCarne.Forma = "Loja";
                    FormaEnvioCarne.Email = "";
                    checkLoja.Click();
                    if (PropPCPO.Produto != Configs.CodMoto)
                    {
                        if (checkEmail.Selected)
                            checkEmail.Click();
                    }
                    break;
                //case "TODOS": //implementar futuramente
                //    if (string.IsNullOrEmpty(PropCli.InfoEmail))
                //    {
                //        inputEmail.Text.Should().ContainAll(PropCli.InfoEmail);
                //        FormaEnvioCarne.Email = PropCli.InfoEmail;
                //    }
                //    checkCorreios.Click();
                //    checkLoja.Click();
                //    break;
                default:
                    break;
            }

        }

        public void SelecionarDadosLiberacaoCredito()
        {

            MoveToElement(sessaoDadosLiberacao);
            cboTipoPessoa.Click();
            cboFormaLiberacao.Click();
            inputCnpjProprietario.ClickSendKeysOnChange(Lib.CGCCPFProprietarioBem);
            inputBeneficiario.ClickSendKeysOnChange(Lib.Beneficiario);
            cboBancoBenef.Click();
            inputAgenciaBenef.ClickSendKeysOnChange(Lib.Agencia);
            cboTipoContaBancariaBenef.Click();
            inputContaBenef.ClickSendKeysOnChange(Lib.Conta);            

            txtValorLiberacao.GetAttribute("value").Replace(".", "").Should().Contain(Convert.ToString(PropPCPO.ValorOperacao));
            ReportSteps.AddScreenshot();           

            Lib.ValorLiberacao = Convert.ToDecimal(PropPCPO.ValorOperacao);
            Lib.CGCCPFProprietarioBem = Lib.CGCCPFProprietarioBem.Replace(".", "").Replace("-", "");
        }

        public void ValidarDadosPreenchidosLibCredito()
        {
            MoveToElement(sessaoDadosLiberacao);
            Lib.FormaLib = txtCboFormaLiberacao.GetAttribute("value");
            Lib.CGCCPFProprietarioBem = txtCnpjProprietario.GetAttribute("value").Replace(".", "").Replace("-", "").Trim();
            Lib.Beneficiario = txtBeneficiario.GetAttribute("value");
            Lib.Banco = txtCboBancoBenef.GetAttribute("value");
            Lib.Agencia = txtAgenciaBenef.GetAttribute("value");
            Lib.TipoContaBancaria= txtCboTipoContaBancariaBenef.GetAttribute("value");
            Lib.Conta = txtContaBenef.GetAttribute("value");            
            Lib.ValorLiberacao = Convert.ToDecimal(PropPCPO.ValorOperacao);            

            Lib.Should().ObjectIsEmpty();
        }

        public void EfetivarProposta()
        {
            MoveToElement(btnEfetivar);            
            btnEfetivar.Click();
            ReportSteps.AddScreenshot();
        }

    }
}
