﻿using Framework.Tests.Steps;
using OpenQA.Selenium;
using Portosis.UITests.Utils;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace UiTestSiteCP._PageObjects

{
    public class LoginPO : BasePagePO
    {
        public LoginPO(IWebDriver driver) : base(driver)
        {
        }

        /*SESSÃO LOGIN*/
        public IWebElement inputUserName => FindElement(By.Id("ContentPlaceHolder1_usuario"));
        public IWebElement inputPassword => FindElement(By.Id("ContentPlaceHolder1_senha"));
        public IWebElement btnEntrar => FindElement(By.Id("ContentPlaceHolder1_entrar"));
        public IWebElement btnSair => FindElement(By.Id("lkbSair"));
        public IWebElement txtUsuarioLogado => FindElement(By.Id("liUsuarioLogado"));
        public IWebElement btnSim => FindElement(By.XPath("//*[@class='ui-button-text'][contains(text(),'Sim')]"));

        public void RealizarLogin()
        {
            //var o = Repositories.InicialRepositoryParceirosMock.TesteConexaoBaseMockEnel();
            inputUserName.SendKeys(Configs.AcessoUserAutomacao.User);
            inputPassword.SendKeys(Configs.AcessoUserAutomacao.Pass);
            btnEntrar.Click();

            try
            {
               driver.FindElement(By.Id("ui-id-1"));
                driver.FindElement(By.XPath("//*[@class='ui-button-text'][contains(text(),'Sim')]")).Click();
               // btnSim.Click();
            }
            catch { }
            ReportSteps.AddScreenshot();         
        }

        public void RealizarLogoff()
        {
            btnSair.Click();
        }

        public void AjustarApontamentoAmbienteRegressivo()
        {
            //Apontamento da URL do Site CP para o gate Enel regressivo 
            Repositories.ClientesRepositorySicred.AtualizaAmbSiteCPEnelRegressivo(Configs.AmbRegressivoSiteCPENEL);

            //Apontamento do Mock para o ambiente parceiros
            Repositories.InicialRepositoryParceirosMock.AtualizaAmbRegressivoParceiros(Configs.AmbRegressivoMock);
        }

        public void InserirClienteMockEnelRegressivo(string cpfcgc, string regiao, string numeroCliente, int dvNumeroCliente, string lote)
        {
            //Cliente - Inserir Cliente no Mock Enel Regressivo
            Repositories.InicialRepositoryEnelMock.IncluirClienteMockRegressivo(cpfcgc, regiao, numeroCliente, dvNumeroCliente, lote);
        }

        public void InserirClienteParceirosEnelRegressivo(string cpfcgc, string regiao, string numeroCliente, int dvNumeroCliente, string lote)
        {
            // Cliente - Inserir Cliente na base Parceiros
            Repositories.InicialRepositoryParceirosMock.IncluirClienteAmbParceiros(cpfcgc, regiao, numeroCliente, dvNumeroCliente, lote);
        }

        public void InserirFaturamentoParceirosEnelRegressivo(string regiao, string lote)
        {
            // Calendario de Faturamento  - Inserir as regiões na base Parceiros
            Repositories.InicialRepositoryParceirosMock.IncluirCalendarioFaturamentoAmbParceiros(regiao, lote);            
        }

        public void LimparDadosBaseMockEnelEParceiros(string cpfcgc, string regiao, string lote)
        {
            Repositories.InicialRepositoryEnelMock.RemoverClienteMockRegressivo(cpfcgc);
            Repositories.InicialRepositoryParceirosMock.RemoverClienteAmbParceiros(cpfcgc);
            Repositories.InicialRepositoryParceirosMock.RemoverCalendarioFaturamentoAmbParceiros(regiao, lote);
        }
    }
}