﻿using OpenQA.Selenium;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;
using Framework.Tests.Steps;
using Portosis.UITests.Utils;
using FluentAssertions;
using System;
using System.Linq;
using System.Threading;
using System.Text.RegularExpressions;

namespace UiTestSiteCP._PageObjects
{
    class PreAnalisePO : BasePagePO
    {
        public PreAnalisePO(IWebDriver driver) : base(driver) { }

        /*PRÉ ANALISE - SESSÃO DADOS DO CLIENTE*/
        public IWebElement txtCPF => FindElement(By.Id("ContentPlaceHolder1_lblCPFCli"));
        public IWebElement inputCep => FindElement(By.Id("ContentPlaceHolder1_txtCep"));
        public IWebElement btnVerificarCEPPreAnalise => FindElement(By.Id("ContentPlaceHolder1_btVerificarCEP"));

        public IWebElement btnOKCep => FindElement(By.ClassName("ui-dialog-buttonset"));
        public IWebElement inputCidade => FindElement(By.Id("ContentPlaceHolder1_txtCidade"));
        public IWebElement dropUF => FindElement(By.Id("ContentPlaceHolder1_dropUf"));
        public IWebElement inputLogradouro => FindElement(By.Id("ContentPlaceHolder1_txtEndereco"));
        public IWebElement inputNumero => FindElement(By.Id("ContentPlaceHolder1_txtNumeroEnd"));
        public IWebElement inputBairro => FindElement(By.Id("ContentPlaceHolder1_txtBairro"));
        public IWebElement dropClasseProf => FindElement(By.CssSelector("[id='ContentPlaceHolder1_PickListAtividade1_cmbClProf']>[value='" + PropCli.CodClasseProfissional + "']"));
        public IWebElement inputAtividade => FindElement(By.Id("ContentPlaceHolder1_PickListAtividade1_InputAtividade"));
        public IWebElement dropEstadoCivil => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dropEstadoCivil']>[value='" + PropCli.EstadoCivil + "']"));

        public IWebElement dropTipoContaBancaria => FindElement(By.XPath("//*[(@id ='ContentPlaceHolder1_dropTipoContaBancaria11' or @id ='ContentPlaceHolder1_dropTipoContaBancaria1')] /option[@value='" + _siglaContaBancariaElement + "']"));
        public IWebElement cboBanco => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_dropBanco1' or @id = 'ContentPlaceHolder1_dropBanco11']"));
        public IWebElement cboInputBanco => FindElement(By.XPath("//*[(@id ='ContentPlaceHolder1_dropBanco11' or @id ='ContentPlaceHolder1_dropBanco1')] /option[@value='" + _codBancoElement + "']"));


        /*PRÉ ANÁLISE - REFINANCIAMENTO*/
        public IWebElement BtnRefinanciamento => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_BtnRenegociacao"));
        public IWebElement BtnSelecionarTodos => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_rptContrato_selecionarTudo_0"));
        public IWebElement BtnSelecionarRenegociacoes => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_btnGravarRenegociacao"));
        public IWebElement ModalRefinanciamento => FindElement(By.Id("ModalRenegociacao"));
        public IWebElement txtContratoInicial => FindElement(By.XPath("//*[@class='contratoRenegociacao collapse']/ul/li[contains(text(),'Contrato')]/strong"));
        public IWebElement txtContrato => FindElement(By.XPath("//*[@class='contratoRenegociacao collapse']/ul/li/strong[contains(text(),'" + ContratoRenegociado.CONTRATO + "')]"));
        public IWebElement txtNumParcelas => FindElement(By.XPath("//*[@class='contratoRenegociacao collapse']/ul/li/strong[contains(text(),'" + ContratoRenegociado.QTDPARCELAS + "')]"));
        public IWebElement txtEmissao => FindElement(By.XPath("//*[@class='contratoRenegociacao collapse']/ul/li/strong[contains(text(),'" + ContratoRenegociado.EMISSAO + "')]"));
        public IWebElement txtVencimento => FindElement(By.XPath("//*[@class='contratoRenegociacao collapse']/ul/li/strong[contains(text(),'" + ContratoRenegociado.VENCIMENTO + "')]"));
        public IWebElement txtValorFinanciado => FindElement(By.XPath("//*[@class='contratoRenegociacao collapse']/ul/li/strong[contains(text(),'" + ContratoRenegociado.VALORFINANCIADO + "')]"));
        public IWebElement txtValorParcelas => FindElement(By.XPath("//*[@class='contratoRenegociacao collapse']/ul/li/strong[contains(text(),'" + ContratoRenegociado.VALORPARCELAS + "')]"));
        public IWebElement txtQtdParcelas => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_rptContrato_rptParcelas_0_lblqtdparcelasi_0"));
        public IWebElement txtTotalRenegociar => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblValorTotal"));
        public IWebElement txtParcelasRenegociar => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblParcelasRenegociar"));
        public IWebElement txtContratoRenegociar => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblContratosRenegociar"));
        // public IWebElement btnOKMSg => FindElement(By.ClassName("ui-button-text"));
        public IWebElement btnCancelarRenegociacao => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_btnCancelarRenegociacao"));
        public IWebElement btnSimularModal => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_BtnRenegociacao"));


        /*SESSÃO PROPOSTA CADASTRADA */
        //public IWebElement txtValorOperacao => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblValorResultSimula2"));
        //public IWebElement txtValorLiquidarRefin => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblValorLiquidar"));
        //public IWebElement txtValorParcela => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_Simulacao1_lblParcelaResultSimula' or @id='ContentPlaceHolder1_Simulacao1_lblParcelaResultSimula3']"));
        /*SESSÃO PROPOSTA CADASTRADA - Mari */
        public IWebElement txtValorOperacao => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_Simulacao1_lblValorResultSimula2' or @id ='ContentPlaceHolder1_Simulacao1_lblValorResultSimula' or @id='ContentPlaceHolder1_Simulacao1_lblValorResultSimula2']"));
        public IWebElement txtValorLiquidarRefin => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lblValorLiquidar"));
        public IWebElement txtValorParcela => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_Simulacao1_lblParcelaResultSimula' or @id='ContentPlaceHolder1_Simulacao1_lblParcelaResultSimula3']"));
        public IWebElement txtProduto => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_Simulacao1_lblSimulaProduto']"));


        /* BOTÕES */
        public IWebElement BtnGravarPreAnalise => FindElement(By.Id("BtnGravar"));
        public IWebElement BtnAtualizar => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_BtnAtualizar' or @id='BtnAtualizar']"));                

        public IWebElement modalRefinanciamento => FindElement(By.Id("ModalRenegociacao"));
        public IWebElement sessaoPropostaCadastrada => FindElement(By.ClassName("PropostaCadastrada"));
        public IWebElement BtnOKMsg => FindElement(By.ClassName("ui-dialog-buttonset"));
        public IWebElement BtnConfirmar => FindElement(By.XPath("//*[@class='ui-button-text'][contains(text(),'Confirmar')]"));
        public IWebElement inputAltValorOperacao => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_txtValorSolicitado"));
        public IWebElement btnNovaSimularModal => FindElement(By.Id("BtnSimulacao"));
        public IWebElement sessaoRefBancarias => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_CadastroCliente1_upRefBancarias' or @id='ContentPlaceHolder1_pnlReferenciasBancariasFamiliaDebito']"));

        public static string _codBancoElement { get; set; }
        public static string _siglaContaBancariaElement { get; set; }
        public static dynamic ContratoRenegociado { get; set; }
        public static decimal somatorioPMT { get; set; }

        public void PreencherDadosComplementares()
        {
            inputCep.ClickSendKeysOnChange(PropCli.Cep);
            btnVerificarCEPPreAnalise.Click();
            btnOKCep.Click();
            inputNumero.ClickSendKeysOnChange(PropCli.Numero.ToString());
            dropClasseProf.Click();
            inputAtividade.ClickSendKeysOnChange(PropCli.CodAtividade.ToString(), false);
            dropEstadoCivil.Click();
        }

        public void PreencherDadosReferenciasBancarias(string codBanco)
        {
            _codBancoElement = codBanco;
            PropCli.CodBanco = _codBancoElement;

            cboBanco.Click();
            cboInputBanco.Click();
            
            AtualizarElementoTipoContaBancariaSigla();
            dropTipoContaBancaria.Click();
            //MoveToElement(txtProduto);
            var prodtela = Regex.Replace((txtProduto.Text), "[^0-9]+", string.Empty) ;
            //MoveToElement(dropTipoContaBancaria);
            if (PropPCPO.Produto == Configs.CodDBC || PropPCPO.Produto == Configs.CodDBCRefin || prodtela == Configs.CodDBC || prodtela == Configs.CodDBCRefin)
            {
                new InclusaoDePropostaPO(Configs.Driver).PreencherReferenciasBancarias();
            }
        }


        public void GravarPreAnalise()
        {
            PageDownRodape();
            ReportSteps.AddScreenshot();
            BtnGravarPreAnalise.Click();
        }

        public void GravarPreAnaliseRefin()
        {
            GravarPreAnalise();
            PageUpCabecalho();
            Thread.Sleep(500);
            try
            {
                PageUpCabecalho();
                driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'O Valor Solicitado')]"));
                Thread.Sleep(500);
                driver.FindElement(By.XPath("//*[@class='ui-button-text'][contains(text(),'Confirmar')]")).Click();// BtnConfirmar.Click();
                //var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
                //BtnOKMsg.Click();
            }
            catch { }
            Thread.Sleep(500);
            try
            {
                driver.FindElement(By.ClassName("ui-dialog-buttonset")).Click();
            }
            catch { driver.FindElement(By.ClassName("ui-button-text")).Click(); }
            //try { driver.FindElement(By.ClassName("ui-button-text")).Click(); }
            //catch { }
            PageDownRodape();
            BtnAtualizar.Click();
        }

        public void AcessarModalNovaSimulacao()
        {
            PageDownRodape();
            btnNovaSimularModal.Click();
            new ModalNovaSimulacaoPO(Configs.Driver).ArmazenarValoresReCalcularPMT(txtValorOperacao, txtValorParcela);
        }

        public void AtualizarElementoTipoContaBancariaID()
        {
            if (PropPCPO.Produto == Configs.CodMoto || PropPCPO.Produto == Configs.CodCPFatura)
                PropCli.TipoContaBancaria = 3;
            else PropCli.TipoContaBancaria = 1;

            var IdContaBase = Repositories.ClientesRepositorySicred.BuscarTipoContaBancariaPorID(PropCli.TipoContaBancaria);
            _siglaContaBancariaElement = IdContaBase.ToString();
        }

        public void AtualizarElementoTipoContaBancariaSigla()
        {
            if (PropPCPO.Produto == Configs.CodMoto || PropPCPO.Produto == Configs.CodCPFatura)
                PropCli.TipoContaBancaria = 3;
            else PropCli.TipoContaBancaria = 1;

            var TpoContaBase = Repositories.ClientesRepositorySicred.BuscarTipoContaBancariaPorSigla(PropCli.TipoContaBancaria);
            _siglaContaBancariaElement = TpoContaBase.ToString();
        }


        public void ExibirModalRefinanciamento()
        {
            PageUpCabecalho();

            ContratoRenegociado = Repositories.ClientesRepositorySicred.BuscarInfoContratoRenegociado(txtContratoInicial.Text);
            txtContrato.Text.Should().Contain(ContratoRenegociado.CONTRATO);
            txtNumParcelas.Text.Should().Contain(Convert.ToString(ContratoRenegociado.QTDPARCELAS));
            txtEmissao.Text.Should().Contain(ContratoRenegociado.EMISSAO);
            txtVencimento.Text.Should().Contain(ContratoRenegociado.VENCIMENTO);
            txtValorFinanciado.Text.Should().Contain(Convert.ToString(ContratoRenegociado.VALORFINANCIADO));
            txtValorParcelas.Text.Should().Contain(Convert.ToString(ContratoRenegociado.VALORPARCELAS));
        }

        public void ExibirParcelasARenegociar()
        {
            MoveToElement(modalRefinanciamento);
            //   "//*[@class='GridPickList RenegParcelasGrid']/tbody/tr/td[4]"));   para dois contratos estava pegando a coluna de ambas tabelas                        
            var saldoPMT = FindElements(By.XPath("//*[@id='ContentPlaceHolder1_Simulacao1_rptContrato_upParcelasReneg_0']/table[@class='GridPickList RenegParcelasGrid']/tbody/tr/td[4]"));
            somatorioPMT = saldoPMT.Sum(x => Convert.ToDecimal(x.Text.Substring(2)));
            var qtdParcelasRenegociar = saldoPMT.Count();

            txtTotalRenegociar.Text.ToString().Replace("R$", "").Replace(".", "").Should().Contain(Convert.ToString(somatorioPMT));
            Convert.ToString(txtParcelasRenegociar.Text).Should().Contain(Convert.ToString(qtdParcelasRenegociar));
            //txtContratoRenegociar arrumar depois           
            PageDownRodape();
            ReportSteps.AddScreenshot();
        }

        public void SelecionarRefinanciamento()
        {
            PageDownRodape();
            ReportSteps.AddScreenshot();
            BtnRefinanciamento.Click();
        }

        public void SelecionarParcelas()
        {
            BtnSelecionarTodos.Click();
        }

        public void SelecionarRenegociacoes()
        {
            BtnSelecionarRenegociacoes.Click();
        }

        public void ValidarRenegociacao()
        {
            MoveToElement(sessaoPropostaCadastrada);

            txtValorLiquidarRefin.Text.ToString().Replace("R$", "").Replace(".", "").Should().Contain(Convert.ToString(somatorioPMT));
            txtValorOperacao.Text.ToString().Replace("R$", "").Replace(".", "").Should().Contain(Convert.ToString(PropPCPO.ValorOperacao));
            PageDownRodape();
            ReportSteps.AddScreenshot();
        }      
    }
}


//public void GravarPreAnaliseRefin()
//{
//    GravarPreAnalise();

//    try
//    {
//        PageUpCabecalho();
//        var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
//        BtnOKMsg.Click();                
//    }
//    catch            
//    {
//        try
//        {
//            var msgRefBancariaConta1 = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Tipo Conta Bancária (Conta 1) é obrigatório.')]"));
//            MoveToElement(btnOKMSg);
//            btnOKMSg.Click();
//            MoveToElement(sessaoRefBancarias);

//            /*AS VEZES NÃO VEM ATUALIZADA NA COMBO O TIPO DE CONTA*/
//            AtualizarElementoTipoContaBancariaSigla();
//            var dropTipoContaBancaria = FindElement(By.XPath("//*[(@id ='ContentPlaceHolder1_dropTipoContaBancaria11' or @id ='ContentPlaceHolder1_dropTipoContaBancaria1') or @id='ContentPlaceHolder1_CadastroCliente1_dropTipoContaBancaria1'] /option[@value='" + _siglaContaBancariaElement + "']"));
//            dropTipoContaBancaria.Click();
//            PageDownRodape();
//            BtnGravarPreAnalise.Click();

//            var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
//            BtnOKMsg.Click();
//        }
//        catch
//        {
//            try
//            {
//                PageUpCabecalho();
//                var msgPreAprovacaoCliente = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'O Valor da Parcela solicitada')]"));
//                BtnConfirmar.Click();
//                var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
//                BtnOKMsg.Click();
//            }
//            catch
//            {
//                try
//                {
//                    PageUpCabecalho();
//                    var msgPreAprovacaoCliente = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'O Valor Solicitado')]"));
//                    BtnConfirmar.Click();
//                    var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
//                    BtnOKMsg.Click();
//                }
//                catch
//                {
//                    PageUpCabecalho();
//                    var msgPreAprovacaoCliente = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'A quantidade de parcelas solicitadas')]"));
//                    BtnConfirmar.Click();
//                    var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
//                    BtnOKMsg.Click();
//                }
//            }
//        }
//    }
//    PageDownRodape();
//    BtnAtualizar.Click();            
//}
