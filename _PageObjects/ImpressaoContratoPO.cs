﻿using FluentAssertions;
using OpenQA.Selenium;
using System;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace UiTestSiteCP._PageObjects

{
    public class ImpressaoContratoPO : BasePagePO
    {
        public ImpressaoContratoPO(IWebDriver driver) : base(driver)
        {
        }

        /*APÓS EFETIVAÇÃO  - DADOS OPERAÇÃO*/
        public IWebElement txtResultValorOperacao => FindElement(By.Id("ContentPlaceHolder1_lblValorResultSimula"));
        public IWebElement txtResultValorParcela => FindElement(By.Id("ContentPlaceHolder1_lblParcelaResultSimula"));


        public void ValidarBaseParcContrato()
        {            
            PropContr.ResultValorOperacao = Convert.ToDecimal(txtResultValorOperacao.Text);
            PropContr.ResultValorParcela = Convert.ToDecimal(txtResultValorParcela.Text);
                                                                                                               
            var CtrBase = Repositories.ClientesRepositorySicred.BuscarParcelasDoContrato(PropPCPO.NroProposta, PropPCPO.Prazo);
            (CtrBase.CompareObject(PropContr)).Should().BeTrue();
        }
    }
}