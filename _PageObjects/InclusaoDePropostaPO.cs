﻿using FluentAssertions;
using Framework.Tests.Steps;
using OpenQA.Selenium;
using Portosis.UITests.Utils;
using System;
using System.Text.RegularExpressions;
using System.Threading;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace UiTestSiteCP._PageObjects
{
    public class InclusaoDePropostaPO : BasePagePO
    {
        public InclusaoDePropostaPO(IWebDriver driver) : base(driver)
        { }

        /*INCLUSÃO DE PROPOSTA - Identificação do Cliente*/
        public IWebElement inputNomeMae => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtMae"));
        public IWebElement inputCidade => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtCidade"));
        public IWebElement dropUF => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_dropUf"));

        public IWebElement dropNacionalidade => FindElement(By.CssSelector("[id='ContentPlaceHolder1_CadastroCliente1_dropNacionalidade']>[value='" + PropCli.CodNacionalidade + "']"));

        public IWebElement dropEstadoCivil => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_dropEstadoCivil"));
        public IWebElement dropGrauInstrucao => FindElement(By.CssSelector("[id='ContentPlaceHolder1_CadastroCliente1_dropGrauInstrucao']>[value='" + PropCli.CodGrauInstrucao + "']"));
        public IWebElement dropTipoIdentidade => FindElement(By.CssSelector("[id='ContentPlaceHolder1_CadastroCliente1_dropTipoIdentidade']>[value='" + PropCli.TipoIdentidade + "']"));
        public IWebElement inputIdentidade => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtIdentidade"));
        public IWebElement inputDataEmissao => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtEmissao"));
        public IWebElement dropEmissor => FindElement(By.CssSelector("[id='ContentPlaceHolder1_CadastroCliente1_dropEmissor']>[value='" + PropCli.CodEmissor + "']"));
        public IWebElement dropUfEmissor => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_dropUfEmissor"));


        /*INCLUSÃO DE PROPOSTA - RESIDÊNCIA*/
        public IWebElement dropTipoTelefone => FindElement(By.CssSelector("[id='ContentPlaceHolder1_CadastroCliente1_dropTipoFone1']>[value='" + PropCli.TipoTelefone + "']"));

        /*INCLUSÃO DE PROPOSTA - DADOS PROFISSIONAIS*/
        public IWebElement inputNomeEmpProf => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtFonte"));
        public IWebElement inputCepProf => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtCepProf"));
        public IWebElement inputNroCepProf => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtDadosProfissionaisNumeroEnd"));
        public IWebElement btnVerificarCEPIncProposta => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_Button1"));
        public IWebElement inputDataAdmissao => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtDataAdmissao"));
        public IWebElement inputDddProf => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtDddProf"));
        public IWebElement inputTelefoneProf => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtFoneProf"));
        public IWebElement inputAtividadeProfissional => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_PickListAtividade1_InputAtividade"));
        public IWebElement cboClasseProfissional => FindElement(By.CssSelector("[id='ContentPlaceHolder1_CadastroCliente1_PickListAtividade1_cmbClProf']>[selected='selected']"));

        /*INCLUSÃO DE PROPOSTA - REFERENCIAS PESSOAIS*/
        public IWebElement inputNomeRef1 => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtNomeRef1"));
        public IWebElement inputRelacRef1 => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtRelRef1"));
        public IWebElement inputDDDRef1 => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtDDDRef1"));
        public IWebElement inputTelefNomeRef1 => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtFoneRef1"));
        public IWebElement inputNomeRef2 => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtNomeRef2"));
        public IWebElement inputRelacRef2 => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtRelRef2"));
        public IWebElement inputDDDRef2 => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtDDDRef2"));
        public IWebElement inputTelefNomeRef2 => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_txtFoneRef2"));

        /*INCLUSÃO DE PROPOSTA E PRÉ ANÁLISE - REFERENCIAS BANCÁRIAS*/
        public IWebElement inputAgencia => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_CadastroCliente1_txtAgencia1' or @id= 'ContentPlaceHolder1_txtAgencia1']"));
        public IWebElement inputConta => FindElement(By.XPath("//*[@id= 'txtConta1Cli' or @id = 'txtConta1']"));
        public IWebElement inputContaDesde => FindElement(By.XPath("//*[@id= 'ContentPlaceHolder1_CadastroCliente1_txtContaDesde1' or @id = 'ContentPlaceHolder1_txtContaDesde1']"));
        public IWebElement btnGravarProp => FindElement(By.Id("ContentPlaceHolder1_BtnGravarp"));

        /*INCLUSÃO DE PROPOSTA  - TIPOS EFETIVAÇÃO*/
        public IWebElement cboTipoEfet => FindElement(By.Id("ContentPlaceHolder1_ddlEfetivacao"));
        public IWebElement cboInputTipoEfet => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_ddlEfetivacao']/option[contains(text(),'" + _tpoEfetivacao + "')]"));

        /*BOTÕES*/
        public IWebElement BtnGravarIncProposta => FindElement(By.Id("ContentPlaceHolder1_BtnGravarp"));
        public IWebElement BtnAvancarIncProposta => FindElement(By.Id("ContentPlaceHolder1_BtnAvancardas"));
        public IWebElement btnOKCep => FindElement(By.ClassName("ui-dialog-buttonset"));
        public IWebElement btnContinuar => FindElement(By.Id("ContentPlaceHolder1_btnContinua"));

        public IWebElement sessaoRefBancarias => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_upRefBancarias"));
        public IWebElement sessaoDadosCliente => FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_upRefBancarias"));
        public IWebElement btnOKMSg => FindElement(By.ClassName("ui-button-text"));
        public IWebElement BtnAtualizar => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_BtnAtualizar' or @id='BtnAtualizar']"));
        public IWebElement BtnConfirmar => FindElement(By.XPath("//*[@class='ui-button-text'][contains(text(),'Confirmar')]"));
        /*CAMPOS NÃO PREENCHIDOS NO REFINANCIAMENTO*/
        //public IWebElement cboBanco => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_dropBanco1' or @id = 'ContentPlaceHolder1_dropBanco11' or @id='ContentPlaceHolder1_CadastroCliente1_dropBanco1']"));
        public IWebElement cboBanco => FindElement(By.CssSelector("[id='ContentPlaceHolder1_CadastroCliente1_dropBanco1']>[selected='selected'],[id='ContentPlaceHolder1_dropBanco1']>[selected='selected']"));
        

        /*INCLUSAO DE PROPOSTA  - SESSÃO GARANTIAS*/
        public IWebElement cboZeroKm => FindElement(By.CssSelector("[id='ContentPlaceHolder1_ddlZero']>[value='" + PropGar.ZeroKm + "']"));
        public IWebElement cboMarca => FindElement(By.CssSelector("[id='ContentPlaceHolder1_ddlMarcas']>[value='" + PropGar.Marca + "']"));
        public IWebElement cboDescricao => FindElement(By.CssSelector("[id='ContentPlaceHolder1_ddlVeiculos']>[value='" + PropGar.Descricao + "']"));
        public IWebElement inputCombustivel => FindElement(By.Id("ContentPlaceHolder1_txtCombustivel"));
        public IWebElement inputPlaca => FindElement(By.Id("ContentPlaceHolder1_txtPlaca"));
        public IWebElement inputAno => FindElement(By.Id("ContentPlaceHolder1_txtAno"));
        public IWebElement inputModelo => FindElement(By.Id("ContentPlaceHolder1_txtModelo"));
        public IWebElement inputCor => FindElement(By.Id("txtCor"));
        public IWebElement inputRenavam => FindElement(By.Id("txtRenavam"));
        public IWebElement inputValor => FindElement(By.Id("ContentPlaceHolder1_txtValorGarantia"));
        public IWebElement inputChassi => FindElement(By.Id("ContentPlaceHolder1_txtChassi"));
        public IWebElement sessaoGarantia => FindElement(By.Id("ContentPlaceHolder1_pnlGarantias"));


        public static string _tpoEfetivacao { get; set; }
        public static string _siglaContaBancariaElement { get; set; }

        public void PreencherDadosIdentificacaoCliente()
        {
            inputNomeMae.ClickSendKeysOnChange(PropCli.NomeMae);
            inputCidade.ClickSendKeysOnChange(PropCli.Cidade);
            dropUF.ClickSendKeysOnChange(PropCli.UF);
            dropNacionalidade.Click();
            dropGrauInstrucao.Click();
            dropTipoIdentidade.Click();
            dropEmissor.Click();
            inputIdentidade.ClickSendKeysOnChange(PropCli.Identidade);
            dropUfEmissor.ClickSendKeysOnChange(PropCli.UfEmissor);
            inputDataEmissao.ClickSendKeysOnChange(PropPCPO.DataEmissao.ToString("ddMMyyyy"), false);
            ReportSteps.AddScreenshot();
        }

        public void PreencherDadosResidencia()
        {
            dropTipoTelefone.ClickSendKeysOnChange(PropCli.TipoTelefone);
        }
        public void PreencherDadosProfissionais()
        {
            if (String.IsNullOrEmpty(cboClasseProfissional.Text))
            {
                inputNomeEmpProf.ClickSendKeysOnChange(PropCli.EmpresaProf);
                inputCepProf.ClickSendKeysOnChange(PropCli.CepProf);
                btnVerificarCEPIncProposta.Click();
                btnOKCep.Click();
                inputNroCepProf.ClickSendKeysOnChange(PropCli.NroCepProf);
                inputDddProf.ClickSendKeysOnChange(PropCli.DddProf);
                inputTelefoneProf.ClickSendKeysOnChange(PropCli.TelefoneProf);
                inputDataAdmissao.ClickSendKeysOnChange(PropCli.DataAdmissaoProf.ToString("ddMMyyyy"), false);
            }

            if (String.IsNullOrEmpty(inputAtividadeProfissional.Text))
            {
                inputAtividadeProfissional.ClickSendKeysOnChange(PropCli.CodAtividade.ToString(), false);
            }
            ReportSteps.AddScreenshot();
         
        }

        public void PreencherDadosReferenciasProfissionais()
        {
            inputNomeRef1.ClickSendKeysOnChange(PropCli.NomeRef1);
            inputRelacRef1.ClickSendKeysOnChange(PropCli.RelacioRef1);
            inputDDDRef1.ClickSendKeysOnChange(PropCli.DddRef1);
            inputTelefNomeRef1.ClickSendKeysOnChange(PropCli.TelefoneRef1);
            inputNomeRef2.ClickSendKeysOnChange(PropCli.NomeRef2);
            inputRelacRef2.ClickSendKeysOnChange(PropCli.RelacioRef2);
            inputDDDRef2.ClickSendKeysOnChange(PropCli.DddRef2);
            inputTelefNomeRef2.ClickSendKeysOnChange(PropCli.TelefoneRef2);
        }

        public void PreencherReferenciasBancarias()
        {

            if ((cboBanco.Text).Contains("SELECIONE"))
            {

                if (String.IsNullOrEmpty(PropCli.CodBanco))
                {
                    PropCli.CodBanco = "0001";
                }

                switch (PropCli.CodBanco)
                {
                    case "0001": //Banco do Brasil
                        inputAgencia.ClickSendKeysOnChange("3229");
                        inputConta.ClickSendKeysOnChange("11362545");
                        break;
                    case "0104": //Caixa 
                        inputAgencia.ClickSendKeysOnChange(("0482"), false);
                        inputConta.ClickSendKeysOnChange(("0119378306"), false);
                        break;
                    case "0237": //Bradesco
                        inputAgencia.ClickSendKeysOnChange("0256");
                        inputConta.ClickSendKeysOnChange("11092357");
                        break;
                    case "341": //Itau
                        inputAgencia.ClickSendKeysOnChange("8760");
                        inputConta.ClickSendKeysOnChange("006760");
                        break;
                    default:
                        break;
                }
                PropCli.Agencia = inputAgencia.GetAttribute("value");
                PropCli.Conta = inputConta.GetAttribute("value");
                inputContaDesde.SendKeys(PropCli.ContaDesde);
            }
            else
            {
                PropCli.CodBanco = "0" + (Regex.Replace(cboBanco.Text, "[^0-9]+", string.Empty));
                var i = PropCli.CodBanco;
            }

            ReportSteps.AddScreenshot();
        }

        public void PreencherDadosGarantia()
        {
            MoveToElement(sessaoGarantia);
            cboZeroKm.Click();
            cboMarca.Click();
            cboDescricao.Click();
            inputCombustivel.ClickSendKeysOnChange(PropGar.Combustivel);
            inputPlaca.ClickSendKeysOnChange(PropGar.Placa);
            inputAno.ClickSendKeysOnChange(PropGar.Ano);
            inputModelo.ClickSendKeysOnChange(PropGar.Modelo);
            inputCor.ClickSendKeysOnChange(PropGar.Cor);
            inputRenavam.ClickSendKeysOnChange(PropGar.Renavam);
            inputValor.ClickSendKeysOnChange(PropGar.Valor);
            inputChassi.ClickSendKeysOnChange(PropGar.Chassi);
            ReportSteps.AddScreenshot();
        }

        public void AlterarContaBancariaRefin(string banco)
        {
            switch (banco)
            {
                case "0001": //Banco do Brasil
                             // inputAgencia.ClickSendKeysOnChange("3229");
                    inputConta.ClickSendKeysOnChange("11362545");
                    break;
                case "0104": //Caixa 
                    //inputAgencia.ClickSendKeysOnChange(("0482"), false);
                    inputConta.ClickSendKeysOnChange("0119378306");
                    break;
                case "0237": //Bradesco
                    //inputAgencia.ClickSendKeysOnChange("0256");
                    inputConta.ClickSendKeysOnChange("11092357");
                    break;
                case "0341": //Itaú
                    //inputAgencia.ClickSendKeysOnChange("0256");
                    inputConta.ClickSendKeysOnChange("313604");
                    break;
                default:
                    break;
            }
        }

        public void GravarInclusaoProposta()
        {
            BtnGravarIncProposta.Click();
        }

        public void GravarInclusaoPropostaRefin()
        {
            BtnGravarIncProposta.Click();
            Thread.Sleep(500);
            try
            {
                var msgRefBancariaConta1 = driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Tipo de conta (Conta')]")); // 1) é obrigatório.')]"));
                MoveToElement(msgRefBancariaConta1);
                driver.FindElement(By.ClassName("ui-button-text")).Click();
                MoveToElement(driver.FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_upRefBancarias")));
                /*AS VEZES NÃO VEM ATUALIZADA NA COMBO O TIPO DE CONTA*/
                AtualizarElementoTipoContaBancariaID();
                driver.FindElement(By.XPath("//*[(@id ='ContentPlaceHolder1_dropTipoContaBancaria11' or @id ='ContentPlaceHolder1_dropTipoContaBancaria1') or @id='ContentPlaceHolder1_CadastroCliente1_dropTipoContaBancaria1'] /option[@value='" + _siglaContaBancariaElement + "']")).Click();
                // PageDownRodape();
                //BtnAtualizar.Click();
                driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_BtnAtualizar' or @id='BtnAtualizar']")).Click();

            }
            catch { }
            try
            {
                PageUpCabecalho();
                driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'O Valor da Parcela solicitada')]"));
                //BtnConfirmar.Click();
                driver.FindElement(By.XPath("//*[@class='ui-button-text'][contains(text(),'Confirmar')]")).Click();
                driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
                driver.FindElement(By.ClassName("ui-button-text")).Click();
            }
            catch { }
            try
            {
                PageUpCabecalho();
                driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'O Valor Solicitado')]"));
                driver.FindElement(By.XPath("//*[@class='ui-button-text'][contains(text(),'Confirmar')]")).Click();
                driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
                driver.FindElement(By.ClassName("ui-button-text")).Click();
            }
            catch { }
            try
            {
                PageUpCabecalho();

                // var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
                //btnOKMSg.Click();
                driver.FindElement(By.ClassName("ui-button-text")).Click();
                driver.FindElement(By.XPath("//*[@id='ContentPlaceHolder1_BtnAtualizar' or @id='BtnAtualizar']")).Click(); //BtnAtualizar.Click();
            }
            catch { }

        }

        //public void GravarInclusaoPropostaRefin()
        //{
        //    BtnGravarIncProposta.Click();


        //    try
        //    {
        //         PageUpCabecalho();
        //        var msgRefBancariaConta1 = driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Tipo de conta (Conta 1) é obrigatório.')]"));
        //        // var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
        //        //btnOKMSg.Click();
        //        driver.FindElement(By.ClassName("ui-button-text")).Click();
        //        BtnAtualizar.Click();
        //    }
        //    catch
        //    {
        //        try
        //        {
        //            var msgNacionalidade = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Nacionalidade obrigatório')]"));
        //            MoveToElement(btnOKMSg);
        //            btnOKMSg.Click();
        //            MoveToElement(sessaoDadosCliente);
        //        }
        //        catch
        //        {
        //            try
        //            {
        //                var msgRefBancariaConta1 = driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Tipo de conta (Conta 1) é obrigatório.')]"));
        //                MoveToElement(btnOKMSg);
        //                btnOKMSg.Click();
        //                MoveToElement(sessaoRefBancarias);

        //                /*AS VEZES NÃO VEM ATUALIZADA NA COMBO O TIPO DE CONTA*/
        //                AtualizarElementoTipoContaBancariaID();
        //                var dropTipoContaBancaria = FindElement(By.XPath("//*[(@id ='ContentPlaceHolder1_dropTipoContaBancaria11' or @id ='ContentPlaceHolder1_dropTipoContaBancaria1') or @id='ContentPlaceHolder1_CadastroCliente1_dropTipoContaBancaria1'] /option[@value='" + _siglaContaBancariaElement + "']"));

        //                dropTipoContaBancaria.Click();
        //                // PageDownRodape();
        //                BtnAtualizar.Click();
        //            }
        //            catch
        //            {
        //                try
        //                {  /*O sistema exige o codigo segurança cnh, foi colocado um fixo*/
        //                    var msgCNH = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'O número de segurança da CNH')]"));
        //                    btnOKMSg.Click();
        //                     PageUpCabecalho();
        //                    var inputCNH = FindElement(By.Id("ContentPlaceHolder1_CadastroCliente1_cvCSCNH"));
        //                    inputCNH.ClickSendKeysOnChange("39023963324");
        //                    BtnGravarIncProposta.Click();
        //                }
        //                catch { }
        //                {
        //                    try
        //                    {
        //                        var msgContaBancaria = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Para cadastrar corretamente uma conta da Caixa Econômica')]"));
        //                        btnOKMSg.Click();
        //                        AlterarContaBancariaRefin(cboBanco.Text);
        //                        BtnGravarIncProposta.Click();

        //                    }
        //                    catch
        //                    {
        //                        try
        //                        {
        //                             PageUpCabecalho();
        //                            var msgPreAprovacaoCliente = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'O Valor da Parcela solicitada')]"));
        //                            BtnConfirmar.Click();
        //                            var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
        //                            btnOKMSg.Click();
        //                        }
        //                        catch
        //                        {
        //                             PageUpCabecalho();
        //                            var msgPreAprovacaoCliente = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'O Valor Solicitado')]"));
        //                            BtnConfirmar.Click();
        //                            var msgPropostaGravada = FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Proposta gravada com sucesso!')]"));
        //                            btnOKMSg.Click();
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        public void AvancarInclusaoProposta()
        {
            PageDownRodape();
            BtnAvancarIncProposta.Click();
            try
            {
                Thread.Sleep(500);
                driver.FindElement(By.XPath("//*[@class='box_mensagem'][contains(text(),'Para avançar é necessário que a proposta esteja em situação 21 - PRONTO PARA CONTRATO.')]"));
                driver.FindElement(By.ClassName("ui-button-text")).Click();
                if (SharedPO.EtapaAtualBD == 2)
                {
                    Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, 31);
                }
                else
                {
                    Repositories.InicialRepositorySicred.AtualizarSituacaoPropostaBase(PropPCPO.NroProposta, 21);
                }
                BtnAvancarIncProposta.Click();
            }
            catch { }

        }

        public void SelecionarTipoEfetivacao(string tipoEfetivacao)
        {
            _tpoEfetivacao = tipoEfetivacao;
            cboTipoEfet.Click();
            cboInputTipoEfet.Click();
            ReportSteps.AddScreenshot();
            btnContinuar.Click();
            AtribuirTipoEfetivacaoProposta(tipoEfetivacao);
        }

        public void AtribuirTipoEfetivacaoProposta(string tipoEfetivacao)
        {
            // verificar tipo se ativo em TIPOSEFETIVACAO para futuro teste
            switch (tipoEfetivacao.ToUpper())
            {
                case "ACEITE POR TELEFONE":
                    PropPCPO.TipoEfetivacao = 1;
                    break;
                case "LOJA":
                    PropPCPO.TipoEfetivacao = 2;
                    break;
                case "DELIVERY":
                    PropPCPO.TipoEfetivacao = 3;
                    break;
                default:
                    throw new NotImplementedException(" tipo Efetivacao não reconhecida");
            }
        }

        public void AtualizarElementoTipoContaBancariaID()
        {
            var IdContaBase = Repositories.ClientesRepositorySicred.BuscarTipoContaBancariaPorID(PropCli.TipoContaBancaria);
            _siglaContaBancariaElement = IdContaBase.ToString();
        }

    }
}
