﻿using FluentAssertions;
using Framework.Tests.Steps;
using NUnit.Framework;
using OpenQA.Selenium;
using Portosis.UITests.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace UiTestSiteCP._PageObjects
{
    public class PaginaInicialPO : BasePagePO
    {
        public PaginaInicialPO(IWebDriver driver) : base(driver)
        { }
        public IWebElement btnNovaProposta => FindElement(By.Id("ContentPlaceHolder1_NovaProposta"));
        public IWebElement btnPropostaExistente => FindElement(By.Id("ContentPlaceHolder1_PropostaExistente"));
        public IWebElement btnConsultar => FindElement(By.Id("ContentPlaceHolder1_Consultar")); 
        public IWebElement btnMaisOpcoes => FindElement(By.ClassName("LinkMostraTodos"));
        public IWebElement btnHistoricoConsolidadoCliente => FindElement(By.Id("ContentPlaceHolder1_HistoricoCliente"));
        public IWebElement btnRelatorios => FindElement(By.Id("ContentPlaceHolder1_Relatorios"));
        public IWebElement btnPropostaPorSituacao => FindElement(By.Id("ContentPlaceHolder1_lbRelPropostaSituacao"));
        public IWebElement btnLiberacoesDoDia => FindElement(By.Id("ContentPlaceHolder1_lbRelLiberacaoDia"));
        public IWebElement btnProducaoDiaria => FindElement(By.Id("ContentPlaceHolder1_lbRelProducaoDiaria"));
        public IWebElement btnProsseguirPropostaEmAberto => FindElement(By.Id("ContentPlaceHolder1_ConsultarProposta"));
        
        public IWebElement txtUsuarioLogado => FindElement(By.Id("liUsuarioLogado"));
        public IWebElement inputCPF => FindElement(By.Id("ContentPlaceHolder1_PickListCliente1_txtCPF"));
        public IWebElement inputProposta => FindElement(By.Id("ContentPlaceHolder1_txProposta"));
        public IWebElement txtContrato => FindElement(By.Id("ContentPlaceHolder1_lblContrato"));

        public static dynamic ClienteBase { get; set; }

        public enum ColunasGrid
        {
            Proposta = 1,
            Tipo = 2,
            Lojista = 3,
            Loja = 4,
            Data = 5,
            Situacao = 6,
            Valor = 7,
            Digitador = 8
        }

        public void ValidarUsuarioLogado()
        {
            Assert.AreEqual(Configs.AcessoUserAutomacao.User, txtUsuarioLogado.Text);
            ReportSteps.AddScreenshot();
        }

        public void IncluirProposta()
        {
            btnNovaProposta.Click();
        }

        public void AcessarConsultaPropostaExistente()
        {
            btnPropostaExistente.Click();
        }

        public void InformarCPFComPropostaExistente(string situacao, string produto)
        {
            var sit = Regex.Replace(situacao, "[^0-9]+", string.Empty);
            var prod = Regex.Replace(produto, "[^0-9]+", string.Empty);
            ClienteBase = null;
            ClienteBase = Repositories.InicialRepositorySicred.BuscaDadosPropostaExistente(Convert.ToInt32(sit), prod, 30);
            string CPF = ClienteBase.CLIENTE.Substring(0, 11);
            inputCPF.ClickSendKeysOnChange(CPF);

        }

        public void InformarNumeroPropostaExistente(string situacao, string produto, string etapa)
        {
            var sit = Regex.Replace(situacao, "[^0-9]+", string.Empty);
            var prod = Regex.Replace(produto, "[^0-9]+", string.Empty);
            var etp = Regex.Replace(etapa, "[^0-9]+", string.Empty);
            ClienteBase = null;

            if (etapa == null)
            {
                ClienteBase = Repositories.InicialRepositorySicred.BuscaDadosPropostaExistente(Convert.ToInt32(sit), prod, 30);
            }
            else{
                if (sit == "10" || sit == "21")
                {
                    ClienteBase = Repositories.InicialRepositorySicred.BuscaDadosPropostaExistenteStatusPendente(Convert.ToInt32(sit), prod, Convert.ToInt32(etp));
                }
                else if (sit == "31")
                {
                    ClienteBase = Repositories.InicialRepositorySicred.BuscaDadosPropostaExistenteStatusComplementarCadastro(Convert.ToInt32(sit), prod, Convert.ToInt32(etp));
                }
                else if (sit == "40")
                {
                    ClienteBase = Repositories.InicialRepositorySicred.BuscaDadosPropostaExistenteStatusPreEfetivado(Convert.ToInt32(sit), prod, Convert.ToInt32(etp));
                }
                else
                {
                    ClienteBase = Repositories.InicialRepositorySicred.BuscaDadosPropostaExistenteEmSimulacao(Convert.ToInt32(sit), prod, Convert.ToInt32(etp));
                }
                
            }
            
            string Proposta = ClienteBase.PROPOSTA.ToString();
            inputProposta.ClickSendKeysOnChange(Proposta);

        }

        public void SelecionarConsultar()
        {
            btnConsultar.Click();
        }

          public void ValidarExibicaoDadosDaPropostaNaGrid()
        {
            //Confome regra no código o digitador é exibido apenas quando proposta <> de 20 
            bool isFound = false;
            Thread.Sleep(1000);
            List<IWebElement> rows = new List<IWebElement>(driver.FindElement(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")).FindElements(By.CssSelector("tr:nth-child(n)")));
            var DadosOperacao = Repositories.PlanosRepositorySicred.ObterDadosValoresPropostaOperacao(ClienteBase.PROPOSTA.ToString());
            var ValorOperacao = ((($"{Convert.ToDecimal(DadosOperacao.VALORCOMPRA):C}").Replace("R$", "")).Trim());    
            string dadosCliente = (ClienteBase.PROPOSTA.ToString() + "Contrato" + ClienteBase.LOJISTA.ToString() + ClienteBase.LOJA.ToString() + ClienteBase.PRODUTO.ToString() + string.Format("{0:d}", ClienteBase.DATAINCLUSAO) + "CONTRATO EFETIVADO" + ValorOperacao).ToString().Replace(" ", "");
            for (int i = 0; i < rows.Count; i++)
            {
                var textRow = ((rows[i].Text).Replace(" ", ""));
                if (textRow.Equals(dadosCliente))
                {
                    isFound = true;
                    break;
                }
            }

            isFound.Should().BeTrue();
        }

        public void AcessarPropostaPesquisadaViaGrid()
        {
            List<IWebElement> colluns = new List<IWebElement>();
            colluns.AddRange(FindElement(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")).FindElements(By.TagName("td:nth-child(" + (int)ColunasGrid.Proposta + ")")));
            foreach (var col in colluns)
            {
                if (col.Text.Equals(ClienteBase.PROPOSTA.ToString()))
                {
                    col.Click();
                    break;
                }

            }

        }

        public void AcessarMenuMaisOpcoes()
        {
            btnMaisOpcoes.Click();
        }

        public void AcessarHistoricoConsolidadeCliente()
        {
            btnHistoricoConsolidadoCliente.Click();
        }
        public void AcessarRelatorioPorSituacao()
        {
            btnRelatorios.Click();
            btnPropostaPorSituacao.Click();
        }

       
        public void AcessarRelatorioLiberacoesDoDia()
        {
            btnRelatorios.Click();
            btnLiberacoesDoDia.Click();
        }

        public void AcessarRelatorioProducaoDiaria()
        {
            btnRelatorios.Click();
            btnProducaoDiaria.Click();
        }

        public void SelecionarSeguirComPropstaExistente()
        {
            
            btnProsseguirPropostaEmAberto.Click();
        }

        


        //public void ValidarExibicaoDadosDaPropostaNaGrid()  
        //{
        //    List<IWebElement> listaAplicacoes = new List<IWebElement>(driver.FindElements(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")));
        //    for (int i = 0; i < listaAplicacoes.Count; i++)
        //    {
        //        // var t = listaAplicacoes[i].FindElement(By.
        //        ///  .FindElementsByTagName("Text").ElementAt((int)ColunasAplicacoes.DataAplicacao).Text);
        //        //Dim varDia, varMes, VarAno As String
        //        break;
        //    }

        //}



    }
}

//public bool ValidarExibicaoDadosDaPropostaNaGrid()
//{
//    bool isFound = false;
//    List<IWebElement> rows = new List<IWebElement>(driver.FindElement(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")).FindElements(By.TagName("th")));
//    var propostas = new List<string>();
//    foreach (IWebElement row in rows)
//    {
//        if (row.Text.Equals("Proposta"))
//        {
//            List<IWebElement> colluns = new List<IWebElement>();
//            colluns.AddRange(driver.FindElement(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")).FindElements(By.TagName("td:nth-child("+(int)ColunasGrid.Proposta+")")));                   
//            foreach (IWebElement col in colluns)
//            {                       
//                propostas.Add(col.Text);
//            }
//            propostas.Should().ContainEquivalentOf(ClienteBase.PROPOSTA.ToString());
//        }

//        if (row.Text.Equals("Tipo"))
//        {
//            List<IWebElement> colluns = new List<IWebElement>();
//            colluns.AddRange(driver.FindElement(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")).FindElements(By.TagName("td:nth-child(" + (int)ColunasGrid.Tipo + ")")));
//            foreach (IWebElement col in colluns)
//            {
//                propostas.Add(col.Text);
//            }
//            propostas.Should().ContainEquivalentOf("Contrato");
//        }

//        if (row.Text.Equals("Lojista"))
//        {
//            List<IWebElement> colluns = new List<IWebElement>();
//            colluns.AddRange(driver.FindElement(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")).FindElements(By.TagName("td:nth-child(" + (int)ColunasGrid.Lojista + ")")));
//            foreach (IWebElement col in colluns)
//            {
//                propostas.Add(col.Text);
//            }
//            propostas.Should().ContainEquivalentOf(ClienteBase.LOGISTA.ToString());
//        }

//        if (row.Text.Equals("Loja"))
//        {
//            List<IWebElement> colluns = new List<IWebElement>();
//            colluns.AddRange(driver.FindElement(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")).FindElements(By.TagName("td:nth-child(" + (int)ColunasGrid.Loja + ")")));
//            foreach (IWebElement col in colluns)
//            {
//                propostas.Add(col.Text);
//            }
//            propostas.Should().ContainEquivalentOf(ClienteBase.LOJA.ToString());
//        }

//        if (row.Text.Equals("Situação"))
//        {
//            List<IWebElement> colluns = new List<IWebElement>();
//            colluns.AddRange(driver.FindElement(By.Id("ContentPlaceHolder1_ListContrPropCliente_GridContrPropoCliente")).FindElements(By.TagName("td:nth-child(" + (int)ColunasGrid.Situacao + ")")));
//            foreach (IWebElement col in colluns)
//            {
//                propostas.Add(col.Text);
//            }
//            propostas.Should().ContainEquivalentOf("CONTRATO EFETIVADO");
//        }
//    }
//    return isFound;
//}
