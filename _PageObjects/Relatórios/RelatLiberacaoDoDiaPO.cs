﻿using FluentAssertions;
using Framework.Tests.Steps;
using OpenQA.Selenium;
using SpecFlowTeste.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace SpecFlowTeste._PageObjects.Relatórios
{
    class RelatLiberacaoDoDiaPO : BasePagePO
    {
        public RelatLiberacaoDoDiaPO(IWebDriver driver) : base(driver)
        { }

        public IWebElement cboinputFormaliberacao => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_dropFormaLiberacao']/option[contains(text(),'" + _formaLiberacaoElement + "')]"));
        public IWebElement inputDataMovimentoDe => FindElement(By.Id("ContentPlaceHolder1_inputDataMovimento"));

        public static string _formaLiberacaoElement { get; set; }
        public static dynamic _DadosBaseRelatLibDia { get; set; }


        public void SelecionarFormaDeLiberacao(string Formaliberacao)
        {
            _formaLiberacaoElement = Formaliberacao;
            cboinputFormaliberacao.Click();
            _formaLiberacaoElement = null;
        }

        public void InformarDataMovimentoDe(string Formaliberacao)
        {
            //FORMALIB = CASE FORMA
            //WHEN '1' THEN 'Cheque'
            //WHEN '2' THEN 'DOC'
            //WHEN '4' THEN 'Outros'
            //WHEN '5' THEN 'OrdemPagamento'
            var forma = 0;
            switch ((Formaliberacao.ToUpper()).Replace(" ", ""))
            {
                
                case "CHEQUE":
                    forma = 1;
                    break;
                case "DOC":
                    forma = 2;
                    break;
                case "OUTROS":
                    forma = 4;
                    break;
                case "ORDEMPAGAMENTO":
                    forma = 5;
                    break;
                default:
                    break;
            }
            _DadosBaseRelatLibDia = Repositories.PlanosRepositorySicred.ObterDataValidaRelatLiberacoesDoDia(1);
            string Data = (_DadosBaseRelatLibDia.LIBERACAO).ToString("ddMMyyyy");
            inputDataMovimentoDe.Clear();
            inputDataMovimentoDe.ClickSendKeysOnChange(Data, false);
            inputDataMovimentoDe.SendKeys(Keys.Tab);
            Thread.Sleep(500);
        }

        public void ValidarDadosRelatorioLiberacaoDia()
        {
            WaitForElementNotVisible();
            var URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(URLPdfAba);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            var dadosContrato = Repositories.PlanosRepositorySicred.ObterDadosRelatorioLiberacoesDoDia(_DadosBaseRelatLibDia.LIBERACAO,_DadosBaseRelatLibDia.CONTRATO);
            //Comparar valores de umdynamic com texto
            foreach (var collun in dadosContrato)
            {
                text.Should().ContainAll((collun.Value).Replace(".", ","));
            }
        }









    }
    }
