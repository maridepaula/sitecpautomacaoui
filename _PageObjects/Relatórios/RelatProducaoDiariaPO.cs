﻿using FluentAssertions;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace SpecFlowTeste._PageObjects.Relatórios
{
    class RelatProducaoDiariaPO : BasePagePO
    {
        public RelatProducaoDiariaPO(IWebDriver driver) : base(driver)
        { }

        public IWebElement inputDataInicial => FindElement(By.Id("ContentPlaceHolder1_dataInicial"));
        public IWebElement inputIDataFinal => FindElement(By.Id("ContentPlaceHolder1_dataFinal"));

        public static dynamic _DadosBaseRelatProdDia { get; set; }

        public void InformarPeriodoProducaoDiaria(int intervaloDias, string situacaoProposta)
        {
            int sit = Convert.ToInt32(Regex.Replace(situacaoProposta, "[^0-9]+", string.Empty));

            var Datainicial = (DateTime.Today).ToString("ddMMyyyy");
            var DataFinal = (DateTime.Today).ToString("ddMMyyyy");
            IFormatProvider culture = new CultureInfo("en-US", true);
            string dataMovimento = DateTime.ParseExact(Repositories.InicialRepositorySicred.ObterDataMovimento(), "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(-intervaloDias).ToString("yyyy-MM-dd");
            _DadosBaseRelatProdDia = Repositories.PlanosRepositorySicred.ObterDadosFiltroRelatorioProducaoDiaria(dataMovimento, sit);

            if (_DadosBaseRelatProdDia != null)
            {
                Datainicial = (_DadosBaseRelatProdDia.PERIODO).Replace("/", "");
                DataFinal = (_DadosBaseRelatProdDia.PERIODO).Replace("/", "");
            }

            inputDataInicial.Clear();
            inputDataInicial.ClickSendKeysOnChange(Datainicial, false);
            inputIDataFinal.Clear();
            inputIDataFinal.ClickSendKeysOnChange(DataFinal, false);
        }

        public void ValidarExibicaoContratoGrid()
        {
            var isFound = false;
            List<IWebElement> rows = new List<IWebElement>(driver.FindElement(By.Id("ContentPlaceHolder1_GridProdDiaria")).FindElements(By.CssSelector("tr:nth-child(n)")));
            var d = _DadosBaseRelatProdDia;
            string dadosCliente = (d.PROPOSTA + d.NOME + d.CPF + d.ATENDENTE + d.ANALISTA + d.VALOR + d.PZ + d.OPERADOR + d.LOJISTA + d.LOJA + d.SITUACAO).ToString().Replace(" ", "");
            for (int i = 0; i < rows.Count; i++)
            {
                var textRow = ((rows[i].Text).Replace(" ", ""));
                if (textRow.Equals(dadosCliente))
                {
                    isFound = true;
                    break;
                }
            }
            isFound.Should().BeTrue();

        }


    }
}
