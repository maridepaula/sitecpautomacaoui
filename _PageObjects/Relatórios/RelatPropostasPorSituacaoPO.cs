﻿using FluentAssertions;
using Framework.Tests.Steps;
using OpenQA.Selenium;
using SpecFlowTeste.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace SpecFlowTeste._PageObjects.Relatórios
{
    class RelatPropostasPorSituacaoPO : BasePagePO
    {
        public RelatPropostasPorSituacaoPO(IWebDriver driver) : base(driver)
        { }

        public IWebElement inputDataInicial => FindElement(By.Id("ContentPlaceHolder1_dataInicial"));
        public IWebElement inputIDataFinal => FindElement(By.Id("ContentPlaceHolder1_dataFinal"));
        public IWebElement inputLojista => FindElement(By.Id("ContentPlaceHolder1_PickListLojista1_InputLojista")); 
        public IWebElement inputLoja => FindElement(By.Id("ContentPlaceHolder1_PickListLoja1_InputLoja"));
        public IWebElement cboinputSituacao => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_DropDownSituacoes']/option[contains(text(),'" + _situacaoElement + "')]"));
        public IWebElement checkAnalitico => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_rbTipo']//*[contains(@value,'"+_tpoRelatorioElement+"')]"));
         
        public static string _situacaoElement { get; set; }
        public static string _tpoRelatorioElement { get; set; }
        
        public static dynamic _DadosConsultaPorSituacao { get; set; }

        public void InformarIntervaloDeDatas(int IntervalodDias, string situacaoProposta)
        {      
            int sit = Convert.ToInt32(Regex.Replace(situacaoProposta, "[^0-9]+", string.Empty));
            var Datainicial =(DateTime.Today).ToString("ddMMyyyy");
            var DataFinal= (DateTime.Today).ToString("ddMMyyyy");

            _DadosConsultaPorSituacao = Repositories.PlanosRepositorySicred.ObterDadosFiltroRelatorioPorSituacao(IntervalodDias, sit);
            if (_DadosConsultaPorSituacao != null)
            {
                // _DadosConsultaPorSituacao = Repositories.PlanosRepositorySicred.ObterDadosFiltroRelatorioPorSituacao(IntervalodDias, sit);
                Datainicial = (_DadosConsultaPorSituacao.DATAINICIO).Replace("/", "");
                DataFinal = (_DadosConsultaPorSituacao.DATAFIM).Replace("/", "");
            }
            //var prod = Regex.Replace(produto, "[^0-9]+", string.Empty);

            inputDataInicial.Clear();
            inputDataInicial.ClickSendKeysOnChange(Datainicial,false);
            inputIDataFinal.Clear();
            inputIDataFinal.ClickSendKeysOnChange(DataFinal, false);

        }

        public void InformarLojista(string lojista)
        {
            string nrLojista = lojista;
            if (_DadosConsultaPorSituacao != null && lojista == "0")
            {
                nrLojista = _DadosConsultaPorSituacao.LOJISTA.ToString();
            }
            if (RelatProducaoDiariaPO._DadosBaseRelatProdDia != null && lojista == "0")
            {
                nrLojista = RelatProducaoDiariaPO._DadosBaseRelatProdDia.LOJISTA.ToString();
            }

            inputLojista.ClickSendKeysOnChange(nrLojista, false);
        }

        public void InformarLoja(string loja)
        {
            string nrLoja = loja;
            if (_DadosConsultaPorSituacao != null && loja == "0")
            {
                nrLoja = _DadosConsultaPorSituacao.LOJA.ToString();
            }
            if (RelatProducaoDiariaPO._DadosBaseRelatProdDia != null && loja == "0")
            {
                nrLoja = RelatProducaoDiariaPO._DadosBaseRelatProdDia.LOJA.ToString();
            }
            inputLoja.ClickSendKeysOnChange(nrLoja, false);

        }

        public void SelecionarOpcaoSituacao(string situacaoProposta)
        {
            _situacaoElement = situacaoProposta;
            cboinputSituacao.Click();
            _situacaoElement = null;
        }

        public void SelenionarRelatorioTipoAnalitico()
        {
            _tpoRelatorioElement = "Analítico";
            if (!checkAnalitico.Selected)
                checkAnalitico.Click();
        }

        public void ValidarDadosRelatorioPorSituacao()
        {
            WaitForElementNotVisible();
            var URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(URLPdfAba);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            var dadosContrato = Repositories.PlanosRepositorySicred.ObterDadosPorContratoRelatorioPropostaPorSituacao(_DadosConsultaPorSituacao.CONTRATO);
            //Comparar valores de umdynamic com texto
            foreach (var collun in dadosContrato)
            {
              text.Should().ContainAll((collun.Value).Replace(".",","));
            }

        }

    }
}
