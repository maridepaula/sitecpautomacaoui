﻿using FluentAssertions;
using Framework.Tests.Steps;
using JQSelenium;
using OpenQA.Selenium;
using Portosis.UITests.Utils;
using SpecFlowTeste.Core.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using UiTestSiteCP.Core.Utils;
using UiTestSiteCP.DataBase;

namespace UiTestSiteCP._PageObjects
{
    public class EfetivaPropostaPO : BasePagePO
    {
        public EfetivaPropostaPO(IWebDriver driver) : base(driver)
        { }

        /*APÓS EFETIVAÇÃO  DBC - DADOS OPERAÇÃO*/
        public IWebElement txtResultValorOperacaoDBC => FindElement(By.Id("ContentPlaceHolder1_lbDetalhesValorOperacao"));
        public IWebElement txtResultValorParcelaDBC => FindElement(By.Id("ContentPlaceHolder1_lbDetalhesValorParcela"));

        /* Impressao Docs */
        public IWebElement linkImprimirContratoCCB => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lnkContratoUnificado']"));
        public IWebElement linkImprimirTermoAdesao => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lnkTermoAdesaoSeguro']"));
        public IWebElement linkImprimirCheckList => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_LbkCheckList']"));
        public IWebElement linkImprimirBilheteDeSeguro => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lnkBilheteSeguro']"));
        public IWebElement linkImprimirDemonstrativoCET => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbkDemonstrativoCet']"));
        public IWebElement linkImprimirCadastroDoCliente => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbkCadastro']"));
        public IWebElement linkImprimirAutorizacaoRefin => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbkRenegociacao']"));
        public IWebElement linkImprimirAutorizacaoCPFatura => FindElement(By.Id("ContentPlaceHolder1_lnkEpFaturaAutor"));
        public IWebElement linkImprimirReciborenegociacao => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbkReciboRenegociacao']"));
        public IWebElement linkImprimirCarne => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbkCarne']"));
        public IWebElement linkImprimirPropotocoloCarne => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbkProtocoloCarne']"));
        public IWebElement BtnImprimirModalProtocoloCarne => FindElement(By.CssSelector("[id='botoes2'] >[class='print GreenButton btImprimirJq']"));
        public IWebElement BtnSairModalProtocoloCarne => FindElement(By.CssSelector("[id='botoes'] >[class='print BlueButton fecharModal']"));
        public IWebElement TxtDescricaoProposta => FindElement(By.Id("ContentPlaceHolder1_lbDescricaoProposta"));

        public static string URLPdfAba { get; set; }
        string CPF => Regex.Replace(FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbDadosClienteCpf' or @id='ContentPlaceHolder1_lblCPF']")).Text, "[^0-9a-zA-Z]+", "");
        string NomeCliente => (FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbDadosClienteNome' or @id='ContentPlaceHolder1_lblCliente']")).Text);
        string ValorOperacao => ((FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbDetalhesValorOperacao' or @id='ContentPlaceHolder1_lblValorResultSimula']"))).Text).Replace(".", string.Empty);


        /*EFETIVAÇÃO  - DOCUMENTOS OBRIGATÓRIOS */
        // XPATH public IList<IWebElement> GridDocsObrigatorios => new List<IWebElement>(FindElements(By.Xpath("//*[@id='ContentPlaceHolder1_uppChecklist'] //input[@type = 'checkbox']")));
        public IList<IWebElement> GridDocsObrigatorios => new List<IWebElement>(FindElements(By.CssSelector("[id='ContentPlaceHolder1_uppChecklist'] [type='checkbox']")));

        public void MarcarDctosObrigatorios()
        {
            PageDownRodape();

            for (var i = 0; i < GridDocsObrigatorios.Count; i++)
            {
                // WaitForElementNotVisible();
                FindElements(By.CssSelector("[id='ContentPlaceHolder1_uppChecklist'] [type='checkbox']")).ElementAt(i).Click();
            }
            ReportSteps.AddScreenshot();
        }

        public void ValidarBaseCliente()
        {
            var CliBase = Repositories.ClientesRepositorySicred.BuscarCliente(PropCli.CGCCPF);
            var t = PropCli;
            (CliBase.CompareObject(PropCli)).Should().BeTrue();
        }

        public void ValidarFormaDeEnvioBase()
        {
            var FormaEnvioBase = Repositories.ClientesRepositorySicred.BuscarFormaEnvio(PropPCPO.NroProposta);
            (FormaEnvioBase.CompareObject(FormaEnvioCarne)).Should().BeTrue();
        }

        public void ValidarBasePropostas()
        {
            var PropBase = Repositories.ClientesRepositorySicred.BuscarDetalhesDaProposta(PropPCPO.CGCCPF);
            var t = PropPCPO;
            (PropBase.CompareObject(PropPCPO)).Should().BeTrue();
        }

        public void ValidarBaseCheque()
        {
            var CheqBase = Repositories.ClientesRepositorySicred.BuscarCheque(PropPCPO.NroProposta, PropPCPO.Prazo, PropCheq.CMC7);
            (CheqBase.CompareObject(PropCheq)).Should().BeTrue();
        }

        public void ValidarBasePEParcContrato()
        {            
            //Resultado dos Dados da Operação na etapa Efetivação, pois a proposta DBC está em 46
            PropContr.ResultValorOperacao = Convert.ToDecimal(txtResultValorOperacaoDBC.Text);
            PropContr.ResultValorParcela = Convert.ToDecimal(txtResultValorParcelaDBC.Text);
                                                                                                                 
            var CtrBase = Repositories.ClientesRepositorySicred.BuscarPEParcelasDoContrato(PropPCPO.NroProposta, PropPCPO.Prazo);
            var t = PropContr;
            (CtrBase.CompareObject(PropContr)).Should().BeTrue();

        }

        public void ValidarBaseGarantia()
        {
            var PropBens = Repositories.ClientesRepositorySicred.BuscarBens(PropPCPO.NroProposta);
            (PropBens.CompareObject(PropGar)).Should().BeTrue();
        }


        public void ValidarBaseLiberacaoCredito()
        {
            var Liberacao = Repositories.ClientesRepositorySicred.BuscarLiberacao(PropPCPO.NroProposta);
            (Liberacao.CompareObject(Lib)).Should().BeTrue();
        }

        public void RealizarUploadDoctoBase()
        {
            /*ATUALIZAÇÃO DO VÍNCULO CPF SAFEDOC X CPF DO USUÁRIO LOGADO NO SITE CP*/
            Repositories.ClientesRepositorySicred.VincularUsuarioSafeDocSiteCP(Configs.AcessoUserAutomacao.User, Configs.AcessoUserSafeDoc.User);
            /*VINCULAÇÃO DE PRODUTO X DOCUMENTO X ENVIO SAFEDOC  */
            Repositories.ClientesRepositorySicred.AtualizarDctoEnviadoSafeDoc(Configs.AcessoUserAutomacao.User, PropPCPO.NroProposta);
            driver.Navigate().Refresh();

            //driver.execute_script("location.reload()")

            /*UTILIZAR CASO SEJA NECESSÁRIO DESFAZER O VÍNCULO PARA O DOCUMENTO ENVIADO SAFEDOC*/
            //Repositories.ClientesRepositorySicred.RemoverDoctosEnviadoSafeDOc(PropPCPO.NroProposta);
        }

        public void ValidarBaseEnvioDoctosObrigatoriosSafeDoc()
        {
            //Será ajustado na sequência quando na próx. sprint for considerado de fato o envio e a remoção para o SAFE DOC
            Repositories.ClientesRepositorySicred.BuscarDoctoObrigatoriosEnviados(Configs.AcessoUserAutomacao.User, PropPCPO.NroProposta);
            Lib.Should().ObjectIsEmpty();
        }

        public void ValidarBasePropostasConcessionarias()
        {
            var PropostaConcess = Repositories.ClientesRepositorySicred.BuscarPropostaConcessionaria(PropPCPO.NroProposta);
            (PropostaConcess.CompareObject(PropConcess)).Should().BeTrue();
        }


        public void ImprimirContratoCCB()
        {
            PageDownRodape();
            linkImprimirContratoCCB.Click();
            WaitForElementNotVisible();
        }
        public void ImprimirCheckList()
        {
            PageDownRodape();
            linkImprimirCheckList.Click();
            WaitForElementNotVisible();

        }
        public void ImprimirTermoDeAdesao()
        {
            PageDownRodape();
            linkImprimirTermoAdesao.Click();
            WaitForElementNotVisible();
        }
        public void ImprimirBilheteDeSeguro()
        {
            PageDownRodape();
            linkImprimirBilheteDeSeguro.Click();
            WaitForElementNotVisible();
        }

        public void ImprimirDemonstrativoCET()
        {
            PageDownRodape();
            linkImprimirDemonstrativoCET.Click();
            WaitForElementNotVisible();
        }

        public void ImprimirCadastroDoCliente()
        {
            PageDownRodape();
            linkImprimirCadastroDoCliente.Click();
            Thread.Sleep(2000);
        }

        public void ImprimirReciboRenegociacao()
        {
            PageDownRodape();
            linkImprimirReciborenegociacao.Click();
            WaitForElementNotVisible();
        }
        public void ImprimirCarne()
        {
            PageDownRodape();
            linkImprimirCarne.Click();
            WaitForElementNotVisible();
        }

        public void ImprimirPropotocoloCarne()
        {
            PageDownRodape();
            linkImprimirPropotocoloCarne.Click();
            WaitForElementNotVisible();
            // - abra um amodal do chrome Print que trava o driver - verificar + adiante
            // Btn na janela Protocolo Carnê
            //PageDownRodape();
            //var bt = driver.FindElement(By.CssSelector("[id='botoes2'] >[class='print GreenButton btImprimirJq']"));
            //MoveToElement(bt);
            //try
            //{
            //    //Configs.Driver.Scripts().ExecuteScript($"return document.querySelector('#botoes2')");
            //    //driver.FindElement(By.CssSelector("[id='botoes2'] >[class='print GreenButton btImprimirJq']")).Click();
            //    bt.Click();
            //}
            //catch { }
            //driver.SwitchTo().Window(driver.WindowHandles[1]);
            //string btnCancelChromePrint = "document.querySelector('body>print-preview-app').shadowRoot.querySelector('#sidebar').shadowRoot.querySelector('print-preview-button-strip').shadowRoot.querySelector('cr-button.cancel-button')";
            //Configs.Driver.Scripts().ExecuteScript($"return {btnCancelChromePrint}.click()");
            //driver.SwitchTo().Window(driver.WindowHandles[0]);

        }

        public void ImprimirAutorizacaoRefin()
        {
            PageDownRodape();
            linkImprimirAutorizacaoRefin.Click();
            Thread.Sleep(1000);
        }

        public void ImprimirAutorizacaoCPFatura()
        {
            PageDownRodape();
            linkImprimirAutorizacaoCPFatura.Click();
            WaitForElementNotVisible();
        }

        public void ValidarDadosPDFContratoCCB()
        {
            //URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            //URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(driver.SwitchTo().Window(driver.WindowHandles.Last()).Url);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            text.Should().ContainAll("Nº. Cédula de Crédito Bancário - " + PropPCPO.NroProposta);
            text.Should().ContainAll("CPF: " + CPF);
            text.Should().ContainAll($"Nome Completo: {NomeCliente}");
            if ((PropPCPO.Produto != Configs.CodDBCRefin) && (PropPCPO.Produto != Configs.CodCarneRefin) && (PropPCPO.Produto != Configs.CodChequeRefin))
            {
                text.Should().ContainAll($"Valor: {Convert.ToDecimal(PropPCPO.ValorOperacao):C}");// + (String.Format("{0:C}", Convert.ToDecimal(PropPCPO.ValorOperacao))));
            }
            else //if  (PropPCPO.Produto == "001025")
            {
                text.Should().ContainAll((($"{Convert.ToDecimal(PropPCPO.ValorOperacao):C}").Replace("R$", "")).Trim());
                //text.Should().ContainAll(($"{Convert.ToDecimal(PropPCPO.ValorOperacao):C}").Replace("R$",""));
                //text.Should().ContainAll($"Valor: {Convert.ToDecimal(ValorOperacao):C}");
                // validar valores para refin
            }

            text.Should().ContainAll(PropPCPO.Produto + " -");
            //valida a geração da primeira página do contrato
            text.Should().ContainAll("1ª Via Negociável - Credor/ Demais vias não negociáveis - Emitente");
            
            if (PropPCPO.Produto != Configs.CodCPFatura) 
            //valida a geração da segunda página do contrato --> para ENEL gera apenas 1 página
               text.Should().ContainAll("Termo de Autorização para Débito em Conta (TADC)");
        }

        public void ValidarDdosPDFTermoDeAdesao()
        {
            URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(URLPdfAba);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            text.Should().ContainAll("TERMO DE AUTORIZAÇÃO DE COBRANÇA DE PRÊMIO DE SEGURO");
            text.Should().ContainAll(string.Format(@"{0:000\.###\.###-##}", Convert.ToInt64(CPF)));
            text.Should().ContainAll(NomeCliente);

        }

        public void ValidarDdosPDFCheckList()
        {
            List<string> myCollection = new List<string>();
            URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(URLPdfAba);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            text.Should().ContainAll($"Nome do Cliente: {NomeCliente}");
            text.Should().ContainAll("Número do contrato: " + PropPCPO.NroProposta);
            var listElementChecklist = driver.FindElements(By.XPath("//*[contains(@id, 'ContentPlaceHolder1_rptCheckList_lblDocumentos')]"));
            foreach (var element in listElementChecklist)
            {
                text.Should().ContainAll(element.Text);
            }
        }

        public void ValidarDdosPDFBilheteDeSeguro()
        {
            URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(URLPdfAba);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            text.Should().ContainAll($"Nome: {NomeCliente}");
            text.Should().ContainAll($"Nº Bilhete: {PropPCPO.NroProposta}");
            // var u = ("CPF: " + (string.Format(@"{0:000\.###\.###-##}", Convert.ToInt64(PropCli.CGCCPF))));
            text.Should().ContainAll("CPF: " + (string.Format(@"{0:000\.###\.###-##}", Convert.ToInt64(CPF))));
        }

        public void ValidarDdosPDFDemonstrativoCET()
        {

            URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(URLPdfAba);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            TxtDescricaoProposta.Text.Substring(34, 2);
            if (TxtDescricaoProposta.Text.Substring(34, 2) == "20") //PropPCPO.Produto == Configs.CodChequeRefin && (
            {
                text.Should().ContainAll("Demonstrativo do Custo Efetivo Total (CET)");
            }else
            {
                text.Should().ContainAll("Simulação do Custo Efetivo Total (CET)");
            }
            text.Should().ContainAll($"{PropPCPO.NroProposta}");
            text.Should().ContainAll((string.Format(@"{0:000\.###\.###-##}", Convert.ToInt64(CPF))) + " - " + NomeCliente);
            text.Should().ContainAll("Assinatura do consumidor:");
        }

        public void ValidarPDFCadastroCliente()
        {            
            ReportSteps.AddScreenshot();
            driver.SwitchTo().Window(driver.WindowHandles[1]);         
            string btnCancelChromePrint = "document.querySelector('body>print-preview-app').shadowRoot.querySelector('#sidebar').shadowRoot.querySelector('print-preview-button-strip').shadowRoot.querySelector('cr-button.cancel-button')";
            Configs.Driver.Scripts().ExecuteScript($"return {btnCancelChromePrint}.click()");
            driver.SwitchTo().Window(driver.WindowHandles[0]);
            var text = (Configs.Driver.Scripts().ExecuteScript($"return document.querySelector('#ContentPlaceHolder1_ucCadastro_dlGrupos').textContent")).ToString();
            //text.Should().ContainAll(PropPCPO.NroProposta); - não gera numero de conrtato neste doc
            text.Should().ContainAll(CPF);
            text.Should().ContainAll(NomeCliente);
            if (PropPCPO.Produto != Configs.CodDBCRefin)
            {
                text.Should().ContainAll(PropCli.Identidade);
            }
        }

        public void ValidarPDFAutorizacaoRefin()
        {
            ReportSteps.AddScreenshot();
            driver.SwitchTo().Window(driver.WindowHandles[1]);
            string btnCancelChromePrint = "document.querySelector('body>print-preview-app').shadowRoot.querySelector('#sidebar').shadowRoot.querySelector('print-preview-button-strip').shadowRoot.querySelector('cr-button.cancel-button')";
            Configs.Driver.Scripts().ExecuteScript($"return {btnCancelChromePrint}.click()");
            driver.SwitchTo().Window(driver.WindowHandles[0]);
            var text = (Configs.Driver.Scripts().ExecuteScript($"return document.querySelector('#ModalContrato').textContent")).ToString();
            Thread.Sleep(1000);
            //var CPF = Regex.Replace(FindElement(By.Id("ContentPlaceHolder1_lbDadosClienteCpf")).Text, "[^0-9a-zA-Z]+", "");
            //var NomeCliente = (FindElement(By.Id("ContentPlaceHolder1_lbDadosClienteNome")).Text);
            //var ValorOperacao = ((FindElement(By.Id("ContentPlaceHolder1_lbDetalhesValorOperacao"))).Text).Replace(".", string.Empty);
            text.Should().ContainAll(NomeCliente);
            text.Should().ContainAll(CPF);
            text.Should().ContainAll(PropPCPO.NroProposta);
            text.Should().ContainAll(ValorOperacao);

        }

        public void ValidarPDFAutorizacaoCPFatura()
        {
            URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(URLPdfAba);
            Thread.Sleep(700);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            text.Should().ContainAll("Autorizo a Portocred S.A., Crédito Financiamento e Investimento, inscrita sob o CNPJ sob n.º");

            text.Should().ContainAll($"{NomeCliente}");
            text.Should().ContainAll(CPF);
            text.Should().ContainAll(PropConcess.NumeroCLiente);
           
            text.Should().ContainAll((($"{Convert.ToDecimal(PropPCPO.ValorOperacao):C}").Replace("R$", "")).Trim());           
           
            //valida a geração da primeira página do contrato
            text.Should().ContainAll("Termo de Autorização para Débito em Conta de Energia Eletrica");
            //valida a geração da segunda página do contrato
            text.Should().ContainAll("Termo de Autorização para Débito em Conta (TADC)");

        }

        public void ValidarPDFReciboRenegociacao()
        {
            URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(URLPdfAba);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            text.Should().ContainAll("Recibo de Pagamento");
            text.Should().ContainAll(NomeCliente);
            text.Should().ContainAll(CPF);
            text.Should().ContainAll(PropPCPO.NroProposta);
            //text.Should().ContainAll(ValorOperacao); Não exibe, validar grid com parcelas quitadas           
        }

        public void ValidarPDFCarne()
        {
            var listParcelas = new List<dynamic>();
           // Thread.Sleep(1000);
            //URLPdfAba = driver.SwitchTo().Window(driver.WindowHandles.Last()).Url;
            //URLPdfAba.Should().NotBeNullOrEmpty();
            var text = new BaseReadPDF().extrairTextoPdf(driver.SwitchTo().Window(driver.WindowHandles.Last()).Url);
            ReportSteps.AddScreenshot();
            WaitChangeOriginalURL();
            //driver.Close();
            //driver.SwitchTo().Window(driver.WindowHandles.LastOrDefault());
            listParcelas.AddRange(Repositories.PlanosRepositorySicred.ObterDadosParcelasContrato(PropPCPO.NroProposta));
            foreach (var parcela in listParcelas)
            {
                text.Should().ContainAll(parcela.NOSSONUMERO);
                var valorParcela = (String.Format("{0:C}", Convert.ToDecimal(parcela.PMT))).Replace("R$", "");
                text.Should().ContainAll(valorParcela);
            }
            text.Should().ContainAll(NomeCliente);
            text.Should().ContainAll(CPF);

        }

        public void ValidarPDFPropotocoloCarne()
        {
            ReportSteps.AddScreenshot();
            MoveToElement(BtnSairModalProtocoloCarne);
            BtnSairModalProtocoloCarne.Click();
            var listParcelas = new List<dynamic>();
            var text = (Configs.Driver.Scripts().ExecuteScript($"return document.querySelector('#ModalContrato > div').textContent")).ToString();
            Thread.Sleep(500);
            listParcelas.AddRange(Repositories.PlanosRepositorySicred.ObterDadosParcelasContrato(PropPCPO.NroProposta));
            foreach (var parcela in listParcelas)
            {
                var valorParcela = (String.Format("{0:C}", Convert.ToDecimal(parcela.PMT))).Replace(" ", "");
                text.Should().ContainAll(valorParcela);
                break;
            }
            text.Should().ContainAll(NomeCliente);
            text.Should().ContainAll(CPF);

        }

        public void ValidarExibicaoPropoConsultadaNaEfetivacao()
        {
            WaitForElementNotVisible();
            TxtDescricaoProposta.Text.Should().ContainAll(PaginaInicialPO.ClienteBase.PROPOSTA.ToString());
            PaginaInicialPO.ClienteBase = null;
        }






    }
}


//string NewWindowHandle = string.Empty;
//ReadOnlyCollection<string> windowHandles = driver.WindowHandles;
//NewWindowHandle = windowHandles[windowHandles.Count - 1];
//            driver.SwitchTo().Window(NewWindowHandle);