﻿using OpenQA.Selenium;
using UiTestSiteCP.Core.Utils;
using System;
using UiTestSiteCP.DataBase;
using Framework.Tests.Steps;
using FluentAssertions;
using Portosis.UITests.Utils;
using NUnit.Framework;


namespace UiTestSiteCP._PageObjects
{
    public class SimulacaoPO : BasePagePO
    {

        public SimulacaoPO(IWebDriver driver) : base(driver)
        { }

        public IWebElement btnNovaProposta => FindElement(By.Id("ContentPlaceHolder1_NovaProposta"));
        /*SESSÃO DADOS DO CLIENTE*/
        public IWebElement inputCpf => FindElement(By.Id("ContentPlaceHolder1_PickListCliente1_txtCPF"));
        public IWebElement inputNome => FindElement(By.Id("ContentPlaceHolder1_txtNome"));
        public IWebElement inputEmail => FindElement(By.Id("ContentPlaceHolder1_txtEmail"));
        public IWebElement inputRendaBruta => FindElement(By.Id("ContentPlaceHolder1_txtRenda"));
        public IWebElement inputDDD => FindElement(By.Id("ContentPlaceHolder1_txDDD"));
        public IWebElement inputTelefone => FindElement(By.Id("ContentPlaceHolder1_txtTelefone"));
        public IWebElement inputNascimento => FindElement(By.Id("ContentPlaceHolder1_txtNascimento"));
        public IWebElement inputMidia => FindElement(By.Id("ContentPlaceHolder1_PickListMidia1_txtMidia"));


        /*SIMULAÇÃO - SESSÃO DADOS DA OPERAÇÕES*/
        public IWebElement inputValorOperacao => FindElement(By.Id("ContentPlaceHolder1_txtValorSolicitado"));
        public IWebElement inputLojista => FindElement(By.Id("ContentPlaceHolder1_PickListLojista1_InputLojista"));
        public IWebElement inputLoja => FindElement(By.Id("ContentPlaceHolder1_PickListLoja1_InputLoja"));
        public IWebElement cboTipoContrato => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dropModalidade']>[value='" + _tpContratoElement + "']"));
        public IWebElement cboProduto => FindElement(By.Id("ContentPlaceHolder1_PickBoxProduto1_upTxtProduto"));
        public IWebElement cboInputProduto => FindElement(By.CssSelector("[title='" + _codProdutoElement + "']"));
        public IWebElement cboPlano => FindElement(By.Id("ContentPlaceHolder1_PickBoxPlano1_txtPlano"));
        public IWebElement cboInputPlano => FindElement(By.CssSelector("[title='" + _codPlanoElement + "']"));
        public IWebElement cboInputTipoSeguro => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_uc_ddlTipoSeguro_ddlTipoSeguro']/option[contains(text(),'" + _tpSeguroElement + "')]"));

        public IWebElement inputPrimVcto => FindElement(By.Name("ctl00$ContentPlaceHolder1$txtPrimeiroVCTO"));
        public IWebElement inputValorPrestacao => FindElement(By.Id("ContentPlaceHolder1_txtValorPrestacao"));


        public IWebElement cboPrazo => FindElement(By.Id("ContentPlaceHolder1_dropPrazo"));
        public IWebElement cboInputPrazo => FindElement(By.CssSelector("[id='ContentPlaceHolder1_dropPrazo']>[value='" + _qtdPrazo + "']"));

        /* SIMULAÇÃO - LISTA COM PRAZOS PARA A SIMULAÇÃO */
        public IWebElement radPrimPrazo => FindElement(By.Id("ContentPlaceHolder1_RepeaterResultados_RdPrazo_0"));
        public IWebElement txtPrimPrazo => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_RepeaterResultados_pnlItemSimulacao_0']/span[1]/label/strong"));


        /*SIMULAÇÃO - CP FATURA - SESSÃO DADOS DA OPERAÇÕES*/
        public IWebElement cboConcessionaria => FindElement(By.Id("ContentPlaceHolder1_ddlConcessionaria"));
        public IWebElement cboRegiaoAtendimento => FindElement(By.CssSelector("[id='ContentPlaceHolder1_ddlRegiaoAtendimento']>[value='" + _codigoRegiao + "']"));
        public IWebElement inputCodigoCliente => FindElement(By.Id("ContentPlaceHolder1_txtCodConcessionaria"));


        /*SIMULAÇÃO - RESULTADO SIMULAÇÃO*/
        public IWebElement txtValorOperacao => FindElement(By.Id("ContentPlaceHolder1_lbUnVlrCreditoPrin"));
        public IWebElement txtValorParcela => FindElement(By.Id("ContentPlaceHolder1_lbUnVlrParcela"));
        public IWebElement txtPrazo => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbUnPrazoPrin' or @id='ContentPlaceHolder1_Simulacao1_lblSimulaPrazo']"));
        public IWebElement txtPrimVencimento => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbUnPrimVctoPrin' or @id='ContentPlaceHolder1_Simulacao1_lblSimula1vcto']"));
        public IWebElement txtDataEmissao => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbUnEmissaoPrin' or @id='ContentPlaceHolder1_Simulacao1_lblSimulaEmissao']"));
        public IWebElement txtProduto => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbUnProdutoPrin' or @id='ContentPlaceHolder1_Simulacao1_lblSimulaProduto']"));
        public IWebElement txtPlano => FindElement(By.XPath("//*[@id='ContentPlaceHolder1_lbUnPlanoPrin' or @id='ContentPlaceHolder1_Simulacao1_lblSimulaPlano']"));
        public IWebElement radioPrazo => FindElement(By.Id("ContentPlaceHolder1_RepeaterResultados_pnlItemSimulacao_0"));

        /*Botões*/
        public IWebElement btnSimular => FindElement(By.XPath("//*[@id='BtnSimular' or @id='BtnSimulacao' or @id='ContentPlaceHolder1_Simulacao1_BtnSimular']"));
        public IWebElement BtnGravarSimulacao => FindElement(By.Id("ContentPlaceHolder1_btnGravar"));
        public IWebElement btnAvancarSimulacao => FindElement(By.Id("ContentPlaceHolder1_btnAvancar"));


        /*PARA PRODUTO DÉBITO EM CONTA, 
        * SERÁ NECESSÁRIO CONFIRMAR ESTA MENSAGEM ANTES DE INFORMAR O PLANO       
        * SERÁ NECESSÁRIO CONFIRMAR MSG FAMILIA DEBITO
        */
        public IWebElement btnOKMSg => FindElement(By.ClassName("ui-button-text"));
        public IWebElement inputBeneficio => FindElement(By.Id("ContentPlaceHolder1_txtNumeroBeneficio"));        //ver regra com o Gui ou Apoana
        public IWebElement inputEspecieBeneficio => FindElement(By.Id("ContentPlaceHolder1_txtEspecieBeneficio")); //ver regra com o Gui ou Apoana 

        public IWebElement BtnNOKMsgConsignado => FindElement(By.XPath("//*[@class='ui-dialog-buttonset']/button/span[contains(text(),'Não')]"));


        public static string _codProdutoElement { get; set; }
        public static string _produtoDescricao { get; set; }
        public static string _tpContratoElement { get; set; }
        public static string _codPlanoElement { get; set; }
        public static string _planoDescricao { get; set; }
        public static string _tpSeguroElement { get; set; }
        public static string _qtdPrazo { get; set; }
        public static string _codigoRegiao { get; set; }
        public decimal _vlParcelaPMT { get; set; }
        public decimal _vlOperacaoPMT { get; set; }
        public static dynamic _Refin { get; set; }



        public void PreencherDadosIniciaisCliente(string cpfcgc)
        {
            Repositories.InicialRepositorySicred.LimparDadosBase(cpfcgc);
            PropPCPO.CGCCPF = cpfcgc.PadRight(12, '1'); // cod cliente tem 1 após cpf           
            PropCli.CGCCPF = cpfcgc.PadRight(12, '1');
            inputCpf.ClickSendKeysOnChange(PropCli.CGCCPF);
            // Configs.Driver.Scripts().ExecuteScript("window.scroll(0,100);");
            inputNome.ClickSendKeysOnChange(PropCli.Nome);
            // Configs.Driver.Scripts().ExecuteScript("window.scrollTo({0}, {1})");
            inputEmail.ClickSendKeysOnChange(PropCli.InfoEmail);
            inputRendaBruta.ClickSendKeysOnChange(PropCli.RendaBruta);

            inputDDD.ClickSendKeysOnChange(PropCli.DDD);
            inputTelefone.ClickSendKeysOnChange(PropCli.Telefone);
            inputNascimento.ClickSendKeysOnChange(PropCli.Nascimento.ToString("ddMMyyyy"), false);
            inputMidia.ClickSendKeysOnChange(PropPCPO.Midia, false);
        }

        public void BuscarClienteRefin(string produto)
        {
            var RefinCpf = Repositories.ClientesRepositorySicred.BuscarClienteRefinanciamento(produto.Substring(0, 6));
            PropPCPO.CGCCPF = RefinCpf.Substring(0, 11);
            PropCli.CGCCPF = RefinCpf.Substring(0, 11);

            Repositories.ClientesRepositorySicred.atualizaCancelamentoPropostasEmAndamento(RefinCpf);
            inputCpf.ClickSendKeysOnChange(PropPCPO.CGCCPF);

            inputMidia.ClickSendKeysOnChange(PropPCPO.Midia, false);
        }

        public void BuscarClienteRefinDBCCaixa(string produto, string codBanco, string optante) /*TipoOptante tipoOptante)*/
        {

            var RefinOptante = Repositories.ClientesRepositorySicred.BuscarClienteRefinanciamentoDBCOptantes(produto.Substring(0, 6), codBanco);

            var RefinCpf = Repositories.ClientesRepositorySicred.atualizaOptante(RefinOptante, optante);
            PropPCPO.CGCCPF = RefinCpf;
            Repositories.ClientesRepositorySicred.atualizaCancelamentoPropostasEmAndamento(PropPCPO.CGCCPF.PadRight(12, '1'));

            //método vagner var RefinCpf = Repositories.ClientesRepositorySicred.BuscarClienteRefinanciamentoDBC(produto.Substring(0, 6), produtoRefin.Substring(0, 6), codBanco, tipoOptante);            

            inputCpf.ClickSendKeysOnChange(PropPCPO.CGCCPF);

            inputMidia.ClickSendKeysOnChange(PropPCPO.Midia, false);
        }

        public void BuscarClienteRefinDBCBradesco(string produto, string codBanco)
        {
            var RefinCpf = Repositories.ClientesRepositorySicred.BuscarClienteRefinanciamentoDBCBradesco(produto.Substring(0, 6), codBanco);
            PropPCPO.CGCCPF = RefinCpf.Substring(0, 11);

            Repositories.ClientesRepositorySicred.atualizaCancelamentoPropostasEmAndamento(RefinCpf);
            inputCpf.ClickSendKeysOnChange(PropPCPO.CGCCPF);

            inputMidia.ClickSendKeysOnChange(PropPCPO.Midia, false);
        }

        public void InformarValorOperacaoValidoFamiliaDebito()
        {
            PageDownRodape();
            var ParametrosDBC = Repositories.PlanosRepositorySicred.BuscaParametrosDBC();
            var vlmax = Convert.ToDouble(ParametrosDBC.ValorMaximoFinanciamento);
            string resultValorMaxDBC = ((vlmax)- (vlmax * 0.30)).ToString();
            inputValorOperacao.Clear();
            inputValorOperacao.ClickSendKeysOnChange(resultValorMaxDBC, false);

        }
        public void PreencherDadosOperacaoRefin(string valorOperacao, string lojista, string loja, string tipoContrato, string produto, string plano, string seguro, string prazo)
        {
            _tpContratoElement = tipoContrato;
            _codProdutoElement = produto.Substring(0, 6);
            _codPlanoElement = plano.Substring(0, 4);
            _tpSeguroElement = seguro;
            _produtoDescricao = produto;
            _planoDescricao = plano;

            PropPCPO.ValorOperacao = Convert.ToDecimal(valorOperacao).ToString("F");
            PropProd.TipoContrato = Repositories.ClientesRepositorySicred.BuscarCCB(_tpContratoElement, produto.Substring(0, 6));

            PropPCPO.Produto = produto.Substring(0, 6);
            PropPCPO.Plano = _codPlanoElement;
            PropPCPO.Loja = loja;
            PropPCPO.Lojista = lojista;

            /*DADOS DA OPERACAO*/
            inputValorOperacao.ClickSendKeysOnChange(PropPCPO.ValorOperacao, false);
            inputLojista.ClickSendKeysOnChange(lojista, false);
            inputLoja.ClickSendKeysOnChange(loja, false);

            PageDownRodape();
            cboTipoContrato.Click();

            /*PRODUTO*/
            cboProduto.Click();
            cboInputProduto.Click();

            /*PLANO*/
            PageDownRodape();
            cboPlano.Click();
            cboInputPlano.Click();
            if (PropPCPO.Produto == Configs.CodDBCRefin) /*jeh*/
            {
                var txtMsgFamiliaDebito = FindElement(By.XPath("//p[contains(text(),'seguem abaixo os bancos com convênios ativos.')]"));
                btnOKMSg.Click();
            }

            //cboInputTipoSeguro.Click();
            new SharedPO(Configs.Driver).AtualizarSeguro(cboInputTipoSeguro.Text);

            if (PropPCPO.Produto == Configs.CodDBCRefin) /*jeh*/
            {
                PropPCPO.PrimVencimento = Convert.ToDateTime(driver.FindElement(By.Id("ContentPlaceHolder1_txtPrimeiroVCTO")).GetAttribute("value").ToString());
            }
            else
            {
                PropPCPO.PrimVencimento = ObterDataDeVencimentoValidaPorPlano(Convert.ToInt32(_codPlanoElement));
                inputPrimVcto.ClickSendKeysOnChange(PropPCPO.PrimVencimento.ToString("ddMMyyyy"), false);
            }

            PageDownRodape();

            /*PRAZO*/
            //SE PRAZO CONTIVER SELECIONE, ATUALIZAR PARA Selecione conforme no Front
            if (prazo.ToUpper().Contains("SELECIONE"))
                _qtdPrazo = "Selecione";
            else  //Atualizar a variável do elemento e o objeto com zeros à esquerda 
            {
                _qtdPrazo = prazo;
                PropPCPO.Prazo = prazo.PadLeft(3, '0'); //to do
                cboPrazo.Click();
                cboInputPrazo.Click();
            }
        }

        public void PreencherDadosOperacao(string valorOperacao, string lojista, string loja, string tipoContrato, string produto, string plano, string seguro, string prazo)
        {
            _tpContratoElement = tipoContrato;
            _codProdutoElement = produto.Substring(0, 6);
            _codPlanoElement = plano.Substring(0, 4);
            _tpSeguroElement = seguro;
            _produtoDescricao = produto;
            _planoDescricao = plano;

            PropPCPO.ValorOperacao = Convert.ToDecimal(valorOperacao).ToString("F");
            PropProd.TipoContrato = Repositories.ClientesRepositorySicred.BuscarCCB(_tpContratoElement, produto.Substring(0, 6));

            PropPCPO.Produto = produto.Substring(0, 6);
            PropPCPO.Plano = _codPlanoElement;
            PropPCPO.Loja = loja;
            PropPCPO.Lojista = lojista;

            /*DADOS DA OPERACAO*/
            inputValorOperacao.ClickSendKeysOnChange(PropPCPO.ValorOperacao, false);
            inputLojista.ClickSendKeysOnChange(lojista, false);
            inputLoja.ClickSendKeysOnChange(loja, false);

            PageDownRodape();
            cboTipoContrato.Click();

            /*PRODUTO*/
            cboProduto.Click();
            cboInputProduto.Click();

            if (PropPCPO.Produto == Configs.CodDBC) /*jeh*/
            {
                var txtMsgBeneficioEspecie = FindElement(By.XPath("//p[contains(text(),'número do Benefício e Renda')]"));
                btnOKMSg.Click();
                inputBeneficio.ClickSendKeysOnChange(PropCli.NumBeneficio);  //ver regra com o Gui
                inputEspecieBeneficio.ClickSendKeysOnChange(PropCli.EspecieBeneficio);   //ver regra com o Gui
                inputEspecieBeneficio.Click();
            }
            else
            {
                PropCli.NumBeneficio = "";
                PropCli.EspecieBeneficio = "";
            }

            /*PLANO*/
            PageDownRodape();
            cboPlano.Click();
            cboInputPlano.Click();
            if (PropPCPO.Produto == Configs.CodDBC)
            {
                var txtMsgFamiliaDebito = FindElement(By.XPath("//p[contains(text(),'seguem abaixo os bancos com convênios ativos.')]"));
                btnOKMSg.Click();
            }

            //cboInputTipoSeguro.Click();
            new SharedPO(Configs.Driver).AtualizarSeguro(cboInputTipoSeguro.Text);

            if (PropPCPO.Produto == Configs.CodDBC)
            {
                PropPCPO.PrimVencimento = Convert.ToDateTime(driver.FindElement(By.Id("ContentPlaceHolder1_txtPrimeiroVCTO")).GetAttribute("value").ToString());
            }
            else
            {
                PropPCPO.PrimVencimento = ObterDataDeVencimentoValidaPorPlano(Convert.ToInt32(_codPlanoElement));
                inputPrimVcto.ClickSendKeysOnChange(PropPCPO.PrimVencimento.ToString("ddMMyyyy"), false);
            }

            PageDownRodape();

            /*PRAZO*/
            //SE PRAZO CONTIVER SELECIONE, ATUALIZAR PARA Selecione conforme no Front
            if (prazo.ToUpper().Contains("SELECIONE"))
                _qtdPrazo = "Selecione";
            else  //Atualizar a variável do elemento e o objeto com zeros à esquerda 
            {
                _qtdPrazo = prazo;
                PropPCPO.Prazo = prazo.PadLeft(3, '0');
                cboPrazo.Click();
                cboInputPrazo.Click();
            }
        }

        public void PreencherDadosOperacaoCPFatura(string valorOperacao, string lojista, string loja, string tipoContrato, string produto, string plano, string seguro, string prazo, string concessionaria, string regiao, string numeroCliente, string dvNumeroCliente, string lote)
        {
            _tpContratoElement = tipoContrato;
            _codProdutoElement = produto.Substring(0, 6);
            _codPlanoElement = plano.Substring(0, 4);
            _tpSeguroElement = seguro;
            _produtoDescricao = produto;
            _planoDescricao = plano;
            _codigoRegiao = regiao.Substring(0, 4);

            PropPCPO.ValorOperacao = Convert.ToDecimal(valorOperacao).ToString("F");
            PropProd.TipoContrato = Repositories.ClientesRepositorySicred.BuscarCCB(_tpContratoElement, produto.Substring(0, 6));


            PropPCPO.Produto = produto.Substring(0, 6);
            PropPCPO.Plano = _codPlanoElement;
            PropPCPO.Loja = loja;
            PropPCPO.Lojista = lojista;


            PropConcess.cpf = PropPCPO.CGCCPF.Substring(0, 11);

            /*DADOS DA OPERACAO*/
            inputValorOperacao.ClickSendKeysOnChange(PropPCPO.ValorOperacao, false);
            inputLojista.ClickSendKeysOnChange(lojista, false);
            inputLoja.ClickSendKeysOnChange(loja, false);

            PageDownRodape();
            cboTipoContrato.Click();

            /*PRODUTO*/
            cboProduto.Click();
            cboInputProduto.Click();

            PropCli.NumBeneficio = "";
            PropCli.EspecieBeneficio = "";

            /*PLANO*/
            PageDownRodape();
            cboPlano.Click();
            cboInputPlano.Click();

            //cboInputTipoSeguro.Click();
            new SharedPO(Configs.Driver).AtualizarSeguro(cboInputTipoSeguro.Text);

            cboConcessionaria.ClickSendKeysOnChange(concessionaria);
            cboRegiaoAtendimento.Click();
            inputCodigoCliente.ClickSendKeysOnChange(numeroCliente);

            AtualizarConcessionaria(concessionaria);
            PropConcess.codigoRegiao = cboRegiaoAtendimento.GetAttribute("value");
            PropConcess.NumeroCLiente = numeroCliente;
            PropConcess.DvNumeroCliente = dvNumeroCliente;

            //Está sendo gravado no objeto, porém ainda não está consistido na base em função do bug de gravar lote NULL
            //Ver com o Carlos 
            //PropConcess.setor = lote;

            PropPCPO.PrimVencimento = Convert.ToDateTime(FindElement(By.Id("ContentPlaceHolder1_txtPrimeiroVCTO")).GetAttribute("value").ToString());

            PageDownRodape();

            /*PRAZO*/
            //SE PRAZO CONTIVER SELECIONE, ATUALIZAR PARA Selecione conforme no Front
            if (prazo.ToUpper().Contains("SELECIONE"))
                _qtdPrazo = "Selecione";
            else  //Atualizar a variável do elemento e o objeto com zeros à esquerda 
            {
                _qtdPrazo = prazo;
                PropPCPO.Prazo = prazo.PadLeft(3, '0');
                cboPrazo.Click();
                cboInputPrazo.Click();
            }
        }

        public void RealizarSimulacao()
        {
            PageUpCabecalho();
            ReportSteps.AddScreenshot();
            MoveToElement(btnSimular);
            ReportSteps.AddScreenshot();
            btnSimular.Click();
        }

        public void ValidarResultadoSimulacao()
        {
            PageDownRodape();
            string[] arrayString = { txtDataEmissao.Text, txtPrazo.Text, txtPrimVencimento.Text, txtProduto.Text, txtPlano.Text };
            var td = txtDataEmissao.Text;
            var t = PropPCPO;
            txtDataEmissao.Text.Should().ContainAll(Configs.DataMovimento);
            if (PropPCPO.CGCCPF != null)
            {
                (PropPCPO.Prazo.ToString()).Should().ContainAll(txtPrazo.Text);
                txtPrimVencimento.Text.Should().Be(PropPCPO.PrimVencimento.ToString("d"));
                txtProduto.Text.Should().StartWithEquivalent(PropPCPO.Produto);
                txtPlano.Text.Should().ContainEquivalentOf(PropPCPO.Plano);
            }
            ReportSteps.AddScreenshot();

        }

        public void GravarSimulacao()
        {
            PageDownRodape();
            BtnGravarSimulacao.Click();
        }

        public void AvancarSimulacao()
        {
            btnAvancarSimulacao.Click();
        }

        public void ValidarConjuntoDadosSimulacacao()
        {
            PageDownRodape();
            (FindElement(By.Id("ContentPlaceHolder1_upRepeaterResultados"))).Should().NotBeNull();
            ReportSteps.AddScreenshot();
        }

        public void SelecionarPrazo()
        {
            PageDownRodape();
            radPrimPrazo.Click();
            _qtdPrazo = txtPrimPrazo.Text;
            PropPCPO.Prazo = txtPrimPrazo.Text.PadLeft(3, '0');
            ReportSteps.AddScreenshot();
        }

        public void AlterarPMTPreenchendoValorParcela()
        {
            PageDownRodape();
            if (_vlOperacaoPMT == 0)
            {
                _vlOperacaoPMT = Convert.ToDecimal(txtValorOperacao.Text);
            }

            if (_vlParcelaPMT == 0)
            {
                _vlParcelaPMT = Convert.ToDecimal(txtValorParcela.Text);
            }
            inputValorPrestacao.ClickSendKeys(_vlParcelaPMT.ToString("F") + Keys.Tab);
            ReportSteps.AddScreenshot();
        }

        public void AlterarPMTPreenchendoValorOperacao()
        {
            PageDownRodape();
            if (_vlOperacaoPMT == 0)
            {
                _vlOperacaoPMT = Convert.ToDecimal(txtValorOperacao.Text);
            }

            if (_vlParcelaPMT == 0)
            {
                _vlParcelaPMT = Convert.ToDecimal(txtValorParcela.Text);
            }
            inputValorOperacao.ClickSendKeys(_vlOperacaoPMT.ToString("F"));
            ReportSteps.AddScreenshot();

        }

        public void ValidarValorParcelaPMT()
        {
            PageDownRodape();
            var operacaoMin = Math.Floor((_vlParcelaPMT - (_vlParcelaPMT * Configs.PercentualQuebraPMT)) * 100) / 100;
            var operacaoMax = Math.Round((_vlParcelaPMT + (_vlParcelaPMT * Configs.PercentualQuebraPMT)), 2);
            var valorParceGerada = decimal.Round(Convert.ToDecimal(txtValorParcela.Text), 2, MidpointRounding.AwayFromZero);
            ReportSteps.AddScreenshot();
            Assert.IsTrue(ValidarValorEntreMaxEMin(valorParceGerada, operacaoMin, operacaoMax));
        }

        public void ValidarValorOperacaoPMT()
        {
            PageDownRodape();
            var operacaoMin = Math.Floor((_vlOperacaoPMT - (_vlOperacaoPMT * Configs.PercentualQuebraPMT)) * 100) / 100;
            var operacaoMax = Math.Round((_vlOperacaoPMT + (_vlOperacaoPMT * Configs.PercentualQuebraPMT)), 2);
            var valorOperacaoGerado = decimal.Round(Convert.ToDecimal(txtValorOperacao.Text), 2, MidpointRounding.AwayFromZero);
            Assert.IsTrue(ValidarValorEntreMaxEMin(valorOperacaoGerado, operacaoMin, operacaoMax));
        }

        public void AtualizarConcessionaria(string concessionaria)
        {
            var idConcessionaria = Repositories.ClientesRepositorySicred.BuscarConcessionaria(concessionaria);
            PropConcess.idConcessionaria = idConcessionaria;
        }
    }
}
