﻿using Framework.Tests.Steps;
using OpenQA.Selenium;
using Portosis.UITests.Utils;
using System;
using System.Collections.Generic;
using UiTestSiteCP.Core.Utils;
using System.Linq;
using FluentAssertions;
using UiTestSiteCP.DataBase;

namespace UiTestSiteCP._PageObjects
{
    public class ModalNovaSimulacaoPO : BasePagePO
    {
        public ModalNovaSimulacaoPO(IWebDriver driver) : base(driver)
        { }
        public IWebElement txtValorAproximadoParcela => FindElement(By.ClassName("ContentPlaceHolder1_Simulacao1_lbResultValor"));
        //public IList<IWebElement> GridPrazoEValorOperacao => FindElements(By.CssSelector("[id='ContentPlaceHolder1_Simulacao1_upRepeaterResultados']>[class='BxSimulacao']"));
        public IList<IWebElement> GridPrazoEValorOperacao => new List<IWebElement>(FindElements(By.CssSelector("[id='ContentPlaceHolder1_Simulacao1_upRepeaterResultados']>[class='BxSimulacao']")));
        public IWebElement inputValorParcela => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_txtValorPrestacao"));
        public IWebElement btnSimular => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_BtnSimular"));
        public IWebElement btnGravar => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_BtnGravar"));
        public IWebElement txtValorParcela => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lbUnVlrParcela"));
        public IWebElement txtValorOperacao => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lbUnVlrCreditoPrin"));

        public IWebElement cboInputPrazo => FindElement(By.CssSelector("[id='ContentPlaceHolder1_Simulacao1_dropPrazo'] [value='" + _qtdPrazo + "']"));
        public IWebElement txtPrazo => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lbUnPrazoPrin"));
        public IWebElement txtPlano => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lbUnPlanoPrin"));
        public IWebElement txtProduto => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lbUnProdutoPrin"));
        public IWebElement txtSeguro => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_lbUnProdutoPrin"));
        public IWebElement cboPlano => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_PickBoxPlano1_upTxtPlano"));
        public IWebElement cboInputPlano => FindElement(By.CssSelector("[title='" + PropPCPO.Plano + "']"));
        public IWebElement cboProduto => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_PickBoxProduto1_txtProduto"));
        public IWebElement cboInputProduto => FindElement(By.CssSelector("[title*='" + PropPCPO.Produto + "']"));
        public IWebElement inputPrimVcto => FindElement(By.Name("ctl00$ContentPlaceHolder1$Simulacao1$txtPrimeiroVCTO"));
        public IWebElement inputBeneficio => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_txtNumeroBeneficio"));
        public IWebElement inputEspecieBeneficio => FindElement(By.Id("ContentPlaceHolder1_Simulacao1_txtEspecieBeneficio"));
        public IWebElement btnOKMSg => FindElement(By.ClassName("ui-button-text"));


        public static decimal _vlParcelaPMT { get; set; }
        public static decimal _vlOperacaoPMT { get; set; }
        public static string _qtdPrazo { get; set; }

        public void PreencherValorPrestacao()
        {
            PageDownRodape();
            inputValorParcela.ClickSendKeys(_vlParcelaPMT.ToString("F") + Keys.Tab);
            ReportSteps.AddScreenshot();
        }

        public void ArmazenarValoresReCalcularPMT(IWebElement valorOperacao, IWebElement valorParcela)
        {
            PageDownRodape();
            if (_vlOperacaoPMT == 0)
            {
                _vlOperacaoPMT = Convert.ToDecimal(valorOperacao.Text);
            }

            if (_vlParcelaPMT == 0)
            {
                _vlParcelaPMT = Convert.ToDecimal(valorParcela.Text);
            }
        }
        public void LimparValoresReCalcularPMT()
        {
            _vlOperacaoPMT = 0;
            _vlParcelaPMT = 0;
        }

        public void ClicarEmSimular()
        {
            Configs.Driver.Scripts().ExecuteScript("arguments[0].scrollIntoView(true);", btnSimular);
            btnSimular.Click();
        }

        public void SelecionarPrazoComNovoValorOperacaoGeradoNaGrid()
        {
            var operacaoMin = Math.Floor((_vlOperacaoPMT - (_vlOperacaoPMT * Configs.PercentualQuebraPMT)) * 100) / 100;
            var operacaoMax = Math.Round((_vlOperacaoPMT + (_vlOperacaoPMT * Configs.PercentualQuebraPMT)), 2);
            var o = GridPrazoEValorOperacao;
            foreach (var element in GridPrazoEValorOperacao.Where(x => x.FindElement(By.ClassName("rdPrazos")).Text.Contains(PropPCPO.Prazo.TrimStart('0'))))
            {

                Configs.Driver.Scripts().ExecuteScript("arguments[0].scrollIntoView(true);", btnGravar);
                var txtValorOperacao = ((element.FindElement(By.ClassName("ValorResultParcela")).Text).Replace("R$", ""));
                var valorOperacaoGerado = decimal.Round(Convert.ToDecimal(txtValorOperacao), 2, MidpointRounding.AwayFromZero);
                (ValidarValorEntreMaxEMin(valorOperacaoGerado, operacaoMin, operacaoMax)).Should().BeTrue();
                element.FindElement(By.TagName("input")).Click();
            }
            ReportSteps.AddScreenshot();
        }

        public void SelecionarGravar()
        {

            btnGravar.Click();

        }

        public void ValidarValorParcelaPMTModal()
        {
            Configs.Driver.Scripts().ExecuteScript("arguments[0].scrollIntoView(true);", btnGravar);
            if (ModalNovaSimulacaoPO._vlOperacaoPMT != 0)
                _vlParcelaPMT = ModalNovaSimulacaoPO._vlParcelaPMT;

            PageDownRodape();
            var operacaoMin = Math.Floor((_vlParcelaPMT - (_vlParcelaPMT * Configs.PercentualQuebraPMT)) * 100) / 100;
            var operacaoMax = Math.Round((_vlParcelaPMT + (_vlParcelaPMT * Configs.PercentualQuebraPMT)), 2);
            var valorParceGerada = decimal.Round(Convert.ToDecimal(txtValorParcela.Text), 2, MidpointRounding.AwayFromZero);
            ReportSteps.AddScreenshot();
            (ValidarValorEntreMaxEMin(valorParceGerada, operacaoMin, operacaoMax)).Should().BeTrue();
        }

        public void ValidarValorOperacaoPMTModal()
        {
            Configs.Driver.Scripts().ExecuteScript("arguments[0].scrollIntoView(true);", btnGravar);
            if (ModalNovaSimulacaoPO._vlOperacaoPMT != 0)
                _vlOperacaoPMT = ModalNovaSimulacaoPO._vlOperacaoPMT;

            PageDownRodape();
            var operacaoMin = Math.Floor((_vlOperacaoPMT - (_vlOperacaoPMT * Configs.PercentualQuebraPMT)) * 100) / 100;
            var operacaoMax = Math.Round((_vlOperacaoPMT + (_vlOperacaoPMT * Configs.PercentualQuebraPMT)), 2);
            var valorOperacaoGerado = decimal.Round(Convert.ToDecimal(txtValorOperacao.Text), 2, MidpointRounding.AwayFromZero);
            // ReportSteps.AddScreenshot();
            (ValidarValorEntreMaxEMin(valorOperacaoGerado, operacaoMin, operacaoMax)).Should().BeTrue();

        }

        public void AlterarPrazo(int qtDias)
        {           
            _qtdPrazo = (Convert.ToInt32(PropPCPO.Prazo) + qtDias).ToString();
            PropPCPO.Prazo = _qtdPrazo.PadLeft(3, '0');
            //PropPCPO.Prazo = (Convert.ToInt32(PropPCPO.Prazo) + 1).ToString();            
            cboInputPrazo.Click();
            ReportSteps.AddScreenshot();
        }

        public void AlterarPlano(string plano)
        {
            PropPCPO.Plano = plano.Substring(0, 4);
            cboPlano.Click();
            cboInputPlano.Click();
            if((plano.Substring(0, 4)) != "3663" )
            AlterarPrazo(0);// prazo deve ser novamente informado por Bug na tela                       
        }

        public void AlterarProduto(string produto)
        {
            PropPCPO.Produto = produto.Substring(0, 6);
            cboProduto.Click();
            cboInputProduto.Click();

            if (PropPCPO.Produto == Configs.CodDBC) /*jeh*/
            {
                try
                {
                    var txtMsgBeneficioEspecie = FindElement(By.XPath("//p[contains(text(),'número do Benefício e Renda')]"));
                    btnOKMSg.Click();
                    inputBeneficio.SendKeys("1111111111");  //ver regra com o Gui
                    inputEspecieBeneficio.Click();
                    inputEspecieBeneficio.ClickSendKeys("12");   //ver regra com o Gui
                }
                catch { }
                //     inputEspecieBeneficio.Click();

                PropCli.NumBeneficio = inputBeneficio.GetAttribute("value");
                PropCli.EspecieBeneficio = inputEspecieBeneficio.GetAttribute("value");
                MoveToElement(btnSimular);
            }
            else
            {
                PropCli.NumBeneficio = "";
                PropCli.EspecieBeneficio = "";
            }
        }


        public void ValidarPrazoAlteradoNaModal()
        {
            MoveToElement(txtPrazo);
            txtPrazo.Text.Should().Contain(Convert.ToString(Convert.ToInt32(PropPCPO.Prazo)));
            ReportSteps.AddScreenshot();
        }

        public void AlterarPrazoPorParêmtroFixo(int prazo)
        {
            _qtdPrazo = (Convert.ToInt32(prazo).ToString());
            PropPCPO.Prazo = _qtdPrazo.PadLeft(3, '0');
            cboInputPrazo.Click();

        }

        public void ValidarPlanoAlteradoNaModal()
        {
            MoveToElement(txtPlano);
            txtPlano.Text.Should().Contain(Convert.ToString(Convert.ToInt32(PropPCPO.Plano)));
            ReportSteps.AddScreenshot();
        }

        public void ValidarProdutoAlteradoNaModal()
        {
            MoveToElement(txtProduto);
            txtProduto.Text.Should().Contain(Convert.ToString(Convert.ToInt32(PropPCPO.Produto)));
            ReportSteps.AddScreenshot();
        }


        public void InformarPrimeiroVcto()
        {
            if (PropPCPO.Produto == Configs.CodDBC || PropPCPO.Produto == Configs.CodDBCRefin)
            {
                PropPCPO.PrimVencimento = Convert.ToDateTime(driver.FindElement(By.Id("ContentPlaceHolder1_Simulacao1_txtPrimeiroVCTO")).GetAttribute("value").ToString());
            }
            else
            {
                PropPCPO.PrimVencimento = ObterDataDeVencimentoValidaPorPlano(Convert.ToInt32(PropPCPO.Plano));
                inputPrimVcto.ClickSendKeysOnChange(PropPCPO.PrimVencimento.ToString("ddMMyyyy"), false);
            }
        }
    }
}

