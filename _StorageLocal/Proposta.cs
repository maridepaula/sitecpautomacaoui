﻿using Portosis.UITests.Utils;
using System;
using UiTestSiteCP.DataBase;

namespace UiTestSiteCP._Storage
{
    public class Proposta
    {
        //Objeto Cliente compõe as tabelas : CLIENTE, COMPLEMENTOS
        public class Cliente
        {
            public string CGCCPF { get; set; }
            public string Nome { get; set; }
            public string InfoEmail { get; set; }
            public decimal RendaBruta { get; set; }
            public DateTime Nascimento { get; set; }
            public string NomeMae { get; set; }
            public string Cidade { get; set; }
            public int DDD { get; set; }
            public string Telefone { get; set; }
            public string Cep { get; set; }
            public int Numero { get; set; }
            public string CodClasseProfissional { get; set; }
            public string CodAtividade { get; set; }
            public string EstadoCivil { get; set; }
            public string CodBanco { get; set; }
            public int TipoContaBancaria { get; set; }
            public string UF { get; set; }
            public string CodNacionalidade { get; set; }
            public string CodGrauInstrucao { get; set; }
            public string TipoIdentidade { get; set; }
            public string Identidade { get; set; }
            public string CodEmissor { get; set; }
            public string UfEmissor { get; set; }
            public string TipoTelefone { get; set; }
            public string EmpresaProf { get; set; }
            public string CepProf { get; set; }
            public int NroCepProf { get; set; }
            public DateTime DataAdmissaoProf { get; set; }
            public int DddProf { get; set; }
            public string TelefoneProf { get; set; }
            public string NomeRef1 { get; set; }
            public string RelacioRef1 { get; set; }
            public int DddRef1 { get; set; }
            public string TelefoneRef1 { get; set; }
            public string NomeRef2 { get; set; }
            public string RelacioRef2 { get; set; }
            public int DddRef2 { get; set; }
            public string TelefoneRef2 { get; set; }
            public string Agencia { get; set; }
            public string Conta { get; set; }
            public string ContaDesde { get; set; }
            public string NumBeneficio { get; set; }
            public string EspecieBeneficio { get; set; }
        }

        //Objeto PropostaPCPO compõe as tabelas : PROPOSTASCREDITO, PROPOSTASOPERACOES E PROPOSTASTIPOEFETIVACAO
        public class PropostaPCPO
        {
            public string NroProposta { get; set; }
            public string CGCCPF { get; set; }
            public int Midia { get; set; }
            public string ValorOperacao { get; set; }
            public string Lojista { get; set; }
            public string Loja { get; set; }
            public string TipoContrato { get; set; }
            public string Produto { get; set; }
            public string Plano { get; set; }
            public string Seguro { get; set; }
            public DateTime PrimVencimento { get; set; }
            public string Prazo { get; set; }
            public DateTime DataEmissao { get; set; }
            public int TipoEfetivacao { get; set; }

        }
        //Objeto Contrato compõe as tabelas : CONTRATOSCDC, PE_CONTRATOSCDC
        public class Contrato
        {
            public Decimal ResultValorOperacao { get; set; }
            public Decimal ResultValorParcela { get; set; }
        }
        //Objeto Cheque compõe as tabelas : CHEQUES, PE_CHEQUES
        public class Cheque
        {
            public string CMC7 { get; set; }
        }

        public class FormaEnvio
        {
            public string Forma { get; set; }
            public string Email { get; set; }
        }

        public class PropostaGarantia
        {
            public string ZeroKm { get; set; }
            public string Marca { get; set; }
            public string Descricao { get; set; }
            public string Combustivel { get; set; }
            public string Ano { get; set; }
            public string Modelo { get; set; }
            public string Cor { get; set; }         
            public Decimal Valor { get; set; }
            public string Chassi { get; set; }
            public string Placa { get; set; }
            public string Renavam { get; set; }
        }

        public class PropostaConcessionaria
        {
            public string cpf { get; set; }
            public string NumeroCLiente { get; set; }
            public string DvNumeroCliente { get; set; }
            public int idConcessionaria { get; set; }
            public string codigoRegiao { get; set; }
         /*   public string setor { get; set; }*/
        }

        public class Liberacao
        {
            public string TipoPessoa { get; set; }
            public string FormaLib { get; set; }
            public string CGCCPFProprietarioBem { get; set; }
            public string Beneficiario { get; set; }
            public string Banco { get; set; }
            public string Agencia { get; set; }
            public string TipoContaBancaria { get; set; }
            public string Conta { get; set; }
            public decimal ValorLiberacao { get; set; }
        }              
        public static Cliente ObtemDadosDefaultCliente()
        {
            var dtEmissao = Configs.DataMovimento;
            return new Cliente
            {                
                Nome = @"TESTE AUTOMACAO FAVORECIDO",
                RendaBruta = 15000.00m,
                // DDD = clienteOptions?.DDD ?? 51,
                DDD = 51,
                Telefone = "982896877",
                Nascimento = new DateTime(1975, 08, 22),
                Cep = "57081540",
                Numero = 21,
                CodClasseProfissional = "07", //Funcionários Públicos
                CodAtividade = "1111",        // Aeroviario/Empregado de Aeroporto
                EstadoCivil = "D",       //Divorciado
                TipoContaBancaria = 1,
                NomeMae = "TESTE MAE AUTOMACAO",
                Cidade = "SALVADOR",
                UF = "BA",
                CodNacionalidade = "01",      //Brasileiro Nato
                CodGrauInstrucao = "7",       //Superior Completo
                TipoIdentidade = "01",        //Célula de Identidade RG
                Identidade = "4098596854",
                CodEmissor = "01",
                UfEmissor = "RS",
                TipoTelefone = "2",           //Celular
                EmpresaProf = "CWI SOFTWARE",
                CepProf = "57081540",
                NroCepProf = 24,
                DddProf = 71,
                TelefoneProf = "991445626",
                DataAdmissaoProf = new DateTime(2019, 12, 01),
                NomeRef1 = "REFERENCIA 1 TESTE AUTOMACAO",
                RelacioRef1 = "PRIMO",
                DddRef1 = 54,
                TelefoneRef1 = "998866898",
                NomeRef2 = "REFERENCIA 2 TESTE AUTOMACAO",
                RelacioRef2 = "TIA",
                DddRef2 = 55,
                TelefoneRef2 = "998844898",
                //ContaDesde = new DateTime(2018, 12, 01),
                ContaDesde = "122018",
                InfoEmail = @"automacaocwi@cwi.com.br",
                NumBeneficio = "1111111111",
                EspecieBeneficio = "12"

            };
        }

        public class Produto
        {
            public string TipoContrato { get; set; }
        }

        public static PropostaPCPO ObtemDadosDefaultPropostaPCPO()
        {
            var dtEmissao = Configs.DataMovimento;
            return new PropostaPCPO
            {
                DataEmissao = Convert.ToDateTime(dtEmissao),  
                Midia = 91,
            };
        }

        public static PropostaGarantia ObtemDadosDefaultPropostaGarantia()
        {
            return new PropostaGarantia
            {
                ZeroKm = "N",
                Marca = "31",
                Descricao = "310000",
                Combustivel = "Gasolina",
                Placa = "MWS1353",
                Ano = "2015",
                Modelo = "2015",
                Cor = "Laranja",
                Renavam = "99973084013",                
                Valor = 35000.00m,
                Chassi = "9BG116GW04C400001"
            };    
        }

        public static Liberacao ObtemDadosDefaultLiberacao()
        {
            return new Liberacao
            {
                TipoPessoa = "F",
                FormaLib = "4",
                CGCCPFProprietarioBem = "156.163.310-07",
                Beneficiario = "TESTE AUTOMACAO PROPRIETARIO",
                Banco = "0001",
                Agencia = "2777",
                TipoContaBancaria = "CF",
                Conta = "2260190",
                ValorLiberacao = 18000.00m,
            };
        }
    }
}