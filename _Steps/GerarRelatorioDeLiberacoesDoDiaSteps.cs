﻿using Portosis.UITests.Utils;
using SpecFlowTeste._PageObjects.Relatórios;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowTeste._Steps
{
    [Binding]
    public class GerarRelatorioDeLiberacoesDoDiaSteps
    {
        private RelatLiberacaoDoDiaPO _RelatLiberacaoDoDiaPO = new RelatLiberacaoDoDiaPO(Configs.Driver);

        [Given(@"seleciono a Forma de Liberação (.*)")]
        public void DadoSelecionoAFormaDeLiberacao(string formaLiberacao)
        {
            _RelatLiberacaoDoDiaPO.SelecionarFormaDeLiberacao(formaLiberacao);
        }

        [Given(@"informo uma data válida com Liberação Disponível para a Forma de Liberação (.*)")]
        public void DadoInformoUmaDataValidaComLiberacaoDisponivelParaAFormaDeLiberacao(string formaLiberacao)
        {
            _RelatLiberacaoDoDiaPO.InformarDataMovimentoDe(formaLiberacao);
        }

        [Given(@"visualizo em PDF o Relatório de Liberação do Dia exibindo corretamente o número Contrato, Nome do Cliente, Data da Liberação e Forma de Pagamento conforme filtro")]
        [When(@"visualizo em PDF o Relatório de Liberação do Dia exibindo corretamente o número Contrato, Nome do Cliente, Data da Liberação e Forma de Pagamento conforme filtro")]
        [Then(@"visualizo em PDF o Relatório de Liberação do Dia exibindo corretamente o número Contrato, Nome do Cliente, Data da Liberação e Forma de Pagamento conforme filtro")]
        public void EntaoVisualizoEmPDFORelatorioDeLiberacaoDoDiaExibindoCorretamenteONumeroContratoNomeDoClienteDataDaLiberacaoEFormaDePagamentoConformeFiltro()
        {
            _RelatLiberacaoDoDiaPO.ValidarDadosRelatorioLiberacaoDia();
        }





    }
}
