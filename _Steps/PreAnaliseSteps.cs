﻿using Portosis.UITests.Utils;
using TechTalk.SpecFlow;
using UiTestSiteCP._PageObjects;

namespace SpecFlowTeste._Steps
{
    [Binding]
    public class PreAnaliseSteps
    {
        private PreAnalisePO _PreAnalisePO = new PreAnalisePO(Configs.Driver);
        private InclusaoDePropostaPO _InclusaoDePropostaPO = new InclusaoDePropostaPO(Configs.Driver);

        [Given(@"informo os dados complementares do cliente")]
        public void DadoInformoOsDadosComplementaresDoCliente()
        {
            _PreAnalisePO.PreencherDadosComplementares();
        }

        [Given(@"informo os dados de referências bancárias para o banco: (.*)")]
        public void DadoInformoOsDadosDeReferenciasBancariasParaOBanco(string codBanco)
        {
            _PreAnalisePO.PreencherDadosReferenciasBancarias(codBanco);
        }    

        [Given(@"seleciono para gravar a pré análise")]
        public void DadoSelecionoParaGravarAPreAnalise()
        {
            _PreAnalisePO.GravarPreAnalise();
        }

        [Given(@"seleciono para gravar a pré análise do refin")]
        public void DadoSelecionoParaGravarAPreAnaliseDoRefin()
        {
            _PreAnalisePO.GravarPreAnaliseRefin();
        }

        [Given(@"clico em nova simulação")]
        [When(@"clico em nova simulação")]
        [Then(@"clico em nova simulação")]
        public void DadoClicoEmNovaSimulacao()
        {
            _PreAnalisePO.AcessarModalNovaSimulacao();
        }


        [Given(@"na sessão proposta cadastrada seleciono Refinanciamento")]
        public void DadoNaSessaoPropostaCadastradaSelecionoRefinanciamento()
        {
            _PreAnalisePO.SelecionarRefinanciamento();
        }

        [Given(@"o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas")]
        public void DadoOSistemaExibeUmaModalParaASelecaoDasRenegociacoesDoContratoComNumeroDeParcelasEmissaoVencimentoValorFinanciadoValorParcelas()
        {
             _PreAnalisePO.ExibirModalRefinanciamento();
        }

        [Given(@"confirmo a seleção de todas parcelas")]
        public void DadoConfirmoASelecaoDeTodasParcelas()
        {
            _PreAnalisePO.SelecionarParcelas();
        }

        [Given(@"na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar")]
        public void DadoNaModalDeveConstarOsSeguintesSomatoriosTotalRenegociarParcelasRenegociarContratosRenegociar()
        {
            _PreAnalisePO.ExibirParcelasARenegociar();
        }

        [Given(@"confirmo em selecionar renegociações")]
        public void DadoConfirmoEmSelecionarTodasRenegociacoes()
        {
            _PreAnalisePO.SelecionarRenegociacoes();
        }

        [Given(@"na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar")]
        public void DadoNaSessaoPropostaCadastradaValidoOsValoresValorOperacaoValorALiberarParcelaValorALiquidar()
        {
            _PreAnalisePO.ValidarRenegociacao();
        }               
    }
}
