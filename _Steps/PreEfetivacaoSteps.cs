﻿using UiTestSiteCP._PageObjects;
using TechTalk.SpecFlow;
using Portosis.UITests.Utils;

namespace UiTestSiteCP._Steps
{
    [Binding]
    public class PreEfetivacaoSteps
    {
        private PreEfetivacaoPO _PreEfetivacaoPO = new PreEfetivacaoPO(Configs.Driver);

        [Given(@"informo um cheque pré-datado completando o restante")]
        public void DadoInformoUmChequePre_DatadoCompletandoORestante()
        {
            _PreEfetivacaoPO.PreencherChequesPreDatados();
        }

        [Given(@"informo um cheque pré-datado para o refin completando o restante")]
        public void DadoInformoUmChequePre_DatadoParaORefinCompletandoORestante()
        {
            _PreEfetivacaoPO.PreencherChequesPreDatadosRefin();
        }

        [Given(@"clico em Pré-Efetivar")]
        public void DadoClicoEmPre_Efetivar()
        {
            _PreEfetivacaoPO.PreEfetivarProposta();
        }    

        [When(@"clico em Efetivar")]
        public void QuandoClicoEmEfetivar()
        {
            _PreEfetivacaoPO.EfetivarProposta();
        }

        [Given(@"seleciono a forma de envio: (.*)")]
        public void DadoSelecionoAFormaDeEnvio(string tipoEnvio)
        {
            _PreEfetivacaoPO.SelecionarTipoEnvio(tipoEnvio);
        }

        [Given(@"para refin seleciono a forma de envio: (.*)")]
        public void DadoParaRefinSelecionoAFormaDeEnvio(string tipoEnvio)
        {
            _PreEfetivacaoPO.SelecionarTipoEnvioRefin(tipoEnvio);
        }

        [Given(@"seleciono os dados para liberação de crédito")]
        public void DadoSelecionoOsDadosParaLiberacaoDeCredito()
        {
            _PreEfetivacaoPO.SelecionarDadosLiberacaoCredito();
        }

        [Given(@"visualizado os dados do titular já preenchidos para liberação de crédito")]
        public void DadoVisualizadoOsDadosDoTitularJaPreenchidosParaLiberacaoDeCredito()
        {
            _PreEfetivacaoPO.ValidarDadosPreenchidosLibCredito();
        }

    }
}