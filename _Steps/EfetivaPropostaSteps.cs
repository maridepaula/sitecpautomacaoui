﻿using Portosis.UITests.Utils;
using TechTalk.SpecFlow;
using UiTestSiteCP._PageObjects;

namespace UiTestSiteCP._Steps
{
    [Binding]
    public class EfetivaPropostaSteps
    {
        private EfetivaPropostaPO _EfetivaPropostaPO = new EfetivaPropostaPO(Configs.Driver);
        private ImpressaoContratoPO _ImpressaoContratoPO = new ImpressaoContratoPO(Configs.Driver);

        [Given(@"realizo upload dos documentos via base que serão encaminhados para formalização")]
        public void DadoRealizoUploadDosDocumentosViaBaseQueSeraoEncaminhadosParaFormalizacao()
        {
            _EfetivaPropostaPO.RealizarUploadDoctoBase();
        }

        [Given(@"marco os documentos obrigatórios")]
        public void DadoMarcoOsDocumentosObrigatorios()
        {
            _EfetivaPropostaPO.MarcarDctosObrigatorios();
        }

        [Then(@"valido na base em Clientes, Complementos que os dados foram inseridos para o cliente")]
        public void EntaoValidoNaBaseEmClientesComplementosQueOsDadosForamInseridosParaOCliente()
        {
            _EfetivaPropostaPO.ValidarBaseCliente();
        }

        [Then(@"valido na base em FormaEnvio_Propostas os dados persistidos corretamente")]
        public void EntaoValidoNaBaseEmFormaEnvio_PropostasOsDadosPersistidosCorretamente()
        {
            _EfetivaPropostaPO.ValidarFormaDeEnvioBase();
        }

        [Then(@"valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta")]
        public void EntaoValidoNaBaseEmPropostaCreditoPropostasOperacoesPropostasTipoEfetivacaoQueOsDadosForamInseridosParaAProposta()
        {
           _EfetivaPropostaPO.ValidarBasePropostas();   
        }

        [Then(@"valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato")]
        public void EntaoValidoNaBaseEmContratoEParcelasQueOsDadosForamInseridosParaOContrato()
        {
            _ImpressaoContratoPO.ValidarBaseParcContrato();
        }

        [Then(@"valido na base em Cheques que os dados foram inseridos para a proposta")]
        public void EntaoValidoNaBaseEmChequesQueOsDadosForamInseridosParaAProposta()
        {
            _EfetivaPropostaPO.ValidarBaseCheque();
        }

        [Then(@"valido na base em PE_Contratoscdc e PE_Parcelas que os dados foram inseridos para o contrato")] //to do
        public void EntaoValidoNaBaseEmPE_ContratoscdcEPE_ParcelasQueOsDadosForamInseridosParaOContrato()
        {
            _EfetivaPropostaPO.ValidarBasePEParcContrato(); //to do
        }

        [Then(@"valido na base em PropostaBens que os dados foram inseridos para a proposta")]
        public void EntaoValidoNaBaseEmPropostaBensQueOsDadosForamInseridosParaAProposta()
        {
            _EfetivaPropostaPO.ValidarBaseGarantia();
        }

        [Then(@"valido na base em Liberacao que os dados foram inseridos para a proposta")]
        public void EntaoValidoNaBaseEmLiberacaoQueOsDadosForamInseridosParaAProposta()
        {
            _EfetivaPropostaPO.ValidarBaseLiberacaoCredito();
        }

        [Then(@"valido na base em DocumentosEnviados_Safedoc, CheckList_Propostas que os dados foram inseridos para a proposta")]
        public void EntaoValidoNaBaseEmDocumentosEnviados_SafedocCheckList_PropostasQueOsDadosForamInseridosParaAProposta()
        {
            _EfetivaPropostaPO.ValidarBaseEnvioDoctosObrigatoriosSafeDoc();
        }

        [Then(@"valido na base em PropostasConcessionarias que os dados foram inseridos para a proposta")]
        public void EntaoValidoNaBaseEmPropostasConcessionariasQueOsDadosForamInseridosParaAProposta()
        {
            _EfetivaPropostaPO.ValidarBasePropostasConcessionarias();
        }

        [Given(@"seleciono imprimir Contrato CCB")]
        [When(@"seleciono imprimir Contrato CCB")]
        [Then(@"seleciono imprimir Contrato CCB")]
        public void DadoSelecionoImprimirContratoCCB()
        {
            _EfetivaPropostaPO.ImprimirContratoCCB();
        }

        [Given(@"valido os dados gerados corretamente no PDF do Contrato CCB")]
        [When(@"valido os dados gerados corretamente no PDF do Contrato CCB")]
        [Then(@"valido os dados gerados corretamente no PDF do Contrato CCB")]
        public void DadoValidoOsDadosGeradosCorretamenteoNoPDFDoContratoCCB()
        {
            _EfetivaPropostaPO.ValidarDadosPDFContratoCCB();
        }

        [Given(@"seleciono imprimir Termo de Adesão")]
        [When(@"seleciono imprimir Termo de Adesão")]
        [Then(@"seleciono imprimir Termo de Adesão")]
        public void DadoSelecionoImprimirTermoDeAdesao()
        {
            _EfetivaPropostaPO.ImprimirTermoDeAdesao();
        }

        [Given(@"valido os dados gerados corretamente no PDF do Termo de Adesão")]
        [When(@"valido os dados gerados corretamente no PDF do Termo de Adesão")]
        [Then(@"valido os dados gerados corretamente no PDF do Termo de Adesão")]
        public void DadoValidoOsDadosGeradosCorretamenteNoPDFDoTermoDeAdesao()
        {
            _EfetivaPropostaPO.ValidarDdosPDFTermoDeAdesao();
        }

        [Given(@"seleciono imprimir Check List")]
        [When(@"seleciono imprimir Check List")]
        [Then(@"seleciono imprimir Check List")]
        public void DadoSelecionoImprimirCheckList()
        {
            _EfetivaPropostaPO.ImprimirCheckList();
        }

        [Given(@"valido os dados gerados corretamente no PDF do Check List")]
        [When(@"valido os dados gerados corretamente no PDF do Check List")]
        [Then(@"valido os dados gerados corretamente no PDF do Check List")]
        public void DadoValidoOsDadosGeradosCorretamenteNoPDFDoCheckList()
        {
            _EfetivaPropostaPO.ValidarDdosPDFCheckList();
        }

        [Given(@"seleciono imprimir Bilhete de Seguro")]
        [When(@"seleciono imprimir Bilhete de Seguro")]
        [Then(@"seleciono imprimir Bilhete de Seguro")]
        public void DadoSelecionoImprimirBilheteDeSeguro()
        {
            _EfetivaPropostaPO.ImprimirBilheteDeSeguro();
        }

        [Given(@"valido os dados gerados corretamente no PDF do Bilhete de Seguro")]
        [When(@"valido os dados gerados corretamente no PDF do Bilhete de Seguro")]
        [Then(@"valido os dados gerados corretamente no PDF do Bilhete de Seguro")]
        public void DadoValidoOsDadosGeradosCorretamenteNoPDFDoBilheteDeSeguro()
        {
            _EfetivaPropostaPO.ValidarDdosPDFBilheteDeSeguro();
        }

        [Given(@"seleciono imprimir Demonstrativo CET")]
        [When(@"seleciono imprimir Demonstrativo CET")]
        [Then(@"seleciono imprimir Demonstrativo CET")]
        public void DadoSelecionoImprimirDemonstrativoCET()
        {
            _EfetivaPropostaPO.ImprimirDemonstrativoCET();
        }

        [Given(@"valido os dados gerados corretamente no PDF do Demonstrativo CET")]
        [When(@"valido os dados gerados corretamente no PDF do Demonstrativo CET")]
        [Then(@"valido os dados gerados corretamente no PDF do Demonstrativo CET")]
        public void DadoValidoOsDadosGeradosCorretamenteNoPDFDoDemonstrativoCET()
        {
            _EfetivaPropostaPO.ValidarDdosPDFDemonstrativoCET();
        }

        [Given(@"seleciono imprimir Cadastro do Cliente")]
        [When(@"seleciono imprimir Cadastro do Cliente")]
        [Then(@"seleciono imprimir Cadastro do Cliente")]
        public void DadoSelecionoImprimirCadastroDoCliente()
        {
            _EfetivaPropostaPO.ImprimirCadastroDoCliente();
        }

        [Given(@"valido os dados gerados corretamente no PDF do Cadastro do Cliente")]
        [When(@"valido os dados gerados corretamente no PDF do Cadastro do Cliente")]
        [Then(@"valido os dados gerados corretamente no PDF do Cadastro do Cliente")]
        public void DadoValidoOsDadosGeradosCorretamenteNoPDFDoCadastroDoCliente()
        {
            _EfetivaPropostaPO.ValidarPDFCadastroCliente();
        }

        [Given(@"seleciono imprimir Ckeck List")]
        [When(@"seleciono imprimir Ckeck List")]
        [Then(@"seleciono imprimir Ckeck List")]
        public void DadoSelecionoImprimirCkeckList()
        {
            _EfetivaPropostaPO.ImprimirCheckList();
        }

        [Given(@"seleciono imprimir Autorização do Cliente para realizar refinanciamento")]
        [When(@"seleciono imprimir Autorização do Cliente para realizar refinanciamento")]
        [Then(@"seleciono imprimir Autorização do Cliente para realizar refinanciamento")]
        public void DadoSelecionoImprimirAutorizacaoDoClienteParaRealizarRefinanciamento()
        {
            _EfetivaPropostaPO.ImprimirAutorizacaoRefin();
        }

        [Given(@"valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente")]
        [When(@"valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente")]
        [Then(@"valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente")]
        public void DadoValidoOsDadosGeradosCorretamenteNoPDFDeAutorizacaoDeRefinanciamentoDoCliente()
        {
            _EfetivaPropostaPO.ValidarPDFAutorizacaoRefin();
        }

        [Given(@"seleciono imprimir Autorização do Cliente para desconto em fatura")]
        [When(@"seleciono imprimir Autorização do Cliente para desconto em fatura")]
        [Then(@"seleciono imprimir Autorização do Cliente para desconto em fatura")]
        public void DadoSelecionoImprimirAutorizacaoDoClienteParaDescontoEmFatura()
        {
            _EfetivaPropostaPO.ImprimirAutorizacaoCPFatura();
        }

        [Given(@"valido os dados gerados corretamente no PDF de Autorização para Débito na Fatura de Energia Elétrica")]
        [When(@"valido os dados gerados corretamente no PDF de Autorização para Débito na Fatura de Energia Elétrica")]
        [Then(@"valido os dados gerados corretamente no PDF de Autorização para Débito na Fatura de Energia Elétrica")]
        public void EntaoValidoOsDadosGeradosCorretamenteNoPDFDeAutorizacaoParaDebitoNaFaturaDeEnergiaEletrica()
        {
            _EfetivaPropostaPO.ValidarPDFAutorizacaoCPFatura();
        }


        [Given(@"seleciono imprimir Recibo de Renegociação")]
        [When(@"seleciono imprimir Recibo de Renegociação")]
        [Then(@"seleciono imprimir Recibo de Renegociação")]
        public void DadoSelecionoImprimirReciboDeRenegociacao()
        {
            _EfetivaPropostaPO.ImprimirReciboRenegociacao();
        }

        [Given(@"valido os dados gerados corretamente no PDF de Recibo de Renegociação")]
        [When(@"valido os dados gerados corretamente no PDF de Recibo de Renegociação")]
        [Then(@"valido os dados gerados corretamente no PDF de Recibo de Renegociação")]
        public void DadoValidoOsDadosGeradosCorretamenteNoPDFDeReciboDeRenegociacao()
        {
            _EfetivaPropostaPO.ValidarPDFReciboRenegociacao();
        }

        [Given(@"seleciono imprimir Carnê")]
        [When(@"seleciono imprimir Carnê")]
        [Then(@"seleciono imprimir Carnê")]
        public void DadoSelecionoImprimirCarne()
        {
            _EfetivaPropostaPO.ImprimirCarne();
        }

        [Given(@"valido os dados gerados corretamento no PDF do Carnê")]
        [When(@"valido os dados gerados corretamento no PDF do Carnê")]
        [Then(@"valido os dados gerados corretamento no PDF do Carnê")]
        public void DadoValidoOsDadosGeradosCorretamentoNoPDFDoCarne()
        {
            _EfetivaPropostaPO.ValidarPDFCarne();
        }

        [Given(@"seleciono imprimir Protocolo Carnê")]
        [When(@"seleciono imprimir Protocolo Carnê")]
        [Then(@"seleciono imprimir Protocolo Carnê")]
        public void DadoSelecionoImprimirProtocoloCarne()
        {
            _EfetivaPropostaPO.ImprimirPropotocoloCarne();
        }

        [Given(@"valido os dados gerados corretamente no PDF do Protocolo Carnê")]
        [When(@"valido os dados gerados corretamente no PDF do Protocolo Carnê")]
        [Then(@"valido os dados gerados corretamente no PDF do Protocolo Carnê")]
        public void DadoValidoOsDadosGeradosCorretamenteNoPDFDoProtocoloCarne()
        {
            _EfetivaPropostaPO.ValidarPDFPropotocoloCarne();
        }

        [Given(@"visualizo os dados da proposta na Etapa de Efetivação")]
        [When(@"visualizo os dados da proposta na Etapa de Efetivação")]
        [Then(@"visualizo os dados da proposta na Etapa de Efetivação")]
        public void EntaoVisualizoOsDadosDaPropostaNaEtapaDeEfetivacao()
        {
            _EfetivaPropostaPO.ValidarExibicaoPropoConsultadaNaEfetivacao();
        }











    }
}
