﻿using UiTestSiteCP._PageObjects;
using TechTalk.SpecFlow;
using FluentAssertions;
using Portosis.UITests.Utils;
using Framework.Tests.Steps;

namespace UiTestSiteCP._Steps
{
    [Binding]
    public class SharedSteps
    {
        private SharedPO _SharedPO = new SharedPO(Configs.Driver);
        private ModalNovaSimulacaoPO _ModalNovaSimulacaoPO = new ModalNovaSimulacaoPO(Configs.Driver);       

        [Given(@"o usuário deve ser redirecionado para etapa '(.*)'")]
        [Then(@"o usuário deve ser redirecionado para etapa '(.*)'")]
        [When(@"o usuário deve ser redirecionado para etapa '(.*)'")]
        public void DadoOUsuarioDeveSerRedirecionadoParaEtapa(string etapa)
        {
            _SharedPO.ValidaRedirecionamentoPagina(etapa);
        }       


        [Given(@"a proposta deve estar na situação '(.*)'")]
        [When(@"a proposta deve estar na situação '(.*)'")]
        [Then(@"a proposta deve estar na situação '(.*)'")]
        public void DadoAPropostaDeveEstarNaSituacao(string situacao)
        {
            _SharedPO.ValidarSituacaoProposta(situacao);           
        }

        [Given(@"altero a simulação informando o valor de parcela da simulação anterior zerando o valor de operação")]
        [When(@"altero a simulação informando o valor de parcela da simulação anterior zerando o valor de operação")]
        [Then(@"altero a simulação informando o valor de parcela da simulação anterior zerando o valor de operação")]
        public void DadoAlteroASimulacaoInformandoOValorDeParcelaDaSimulacaoAnteriorZerandoOValorDeOperacao()
        {
            _SharedPO.AlterarPMTPreenchendoValorParcela();
        }

        [Given(@"confirmo a mensagem '(.*)'")]
        [When(@"confirmo a mensagem '(.*)'")]
        [Then(@"confirmo a mensagem '(.*)'")]
        public void DadoConfirmoAMensagem(string msg)
        {
            _SharedPO.ValidarMensagem(msg);
        }

        [Given(@"o número da proposta é gerado")]
        [When(@"o número da proposta é gerado")]
        [Then(@"o número da proposta é gerado")]
        public void DadoONumeroDaPropostaEGerado()
        {
            _SharedPO.ArmazenarNumeroProposta();
        }

        [Given(@"que o nome do cenário é (.*)")]
        [When(@"que o nome do cenário é (.*)")]
        [Then(@"que o nome do cenário é (.*)")]
        public void DadoQueONomeDoCenarioE(string scenarioName)
        {
            ReportSteps.AddScenarioTitle(scenarioName);
        }

        [Given(@"a proposta é atualizada via base para a situação '(.*)' na etapa de '(.*)'")]
        public void DadoAPropostaEAtualizadaViaBaseParaASituacaoNaEtapaDe(int situacao, string etapa)
        {
            _SharedPO.AtualizarStatusProposta(situacao, etapa);//.Should().BeTrue();
        }

        [Given(@"confirmo na base a situação da Proposta em '(.*)'")]
        public void DadoConfirmoNaBaseASituacaoDaPropostaEm(int situacao)
        {
            _SharedPO.AtualizarStatusNaBaseProposta(situacao);
        }


        [Given(@"seleciono Cancelar Proposta")]
        public void DadoSelecionoCancelarProposta()
        {
            _SharedPO.ClicarCancelarProposta();
        }

        [Given(@"visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior")]
        [When(@"visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior")]
        [Then(@"visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior")]
        public void QuandoVisualizoOValorDaOperacaoEParcelaGeradosCorretamenteConformeSimulacaoAnterior()
        {
            _SharedPO.ValidarValorParcelaPMT();
            _SharedPO.ValidarValorOperacaoPMT();
            _ModalNovaSimulacaoPO.LimparValoresReCalcularPMT();
        }

        [Given(@"valido na base em PROPOSTASCREDITO a situação alterada corretamente para '(.*)'")]
        [When(@"valido na base em PROPOSTASCREDITO a situação alterada corretamente para '(.*)'")]
        [Then(@"valido na base em PROPOSTASCREDITO a situação alterada corretamente para '(.*)'")]
        public void EntaoValidoNaBaseEmPROPOSTASCREDITOASituacaoAlteradaCorretamentePara(string situacao)
        {
            (_SharedPO.ValidarSituacaoPropostaBase(situacao)).Should().Be(true);
        }
        

        [Given(@"na sessão proposta cadastrada da etapa '(.*)', visualizo o prazo atualizado corretamente")]
        public void DadoNaSessaoPropostaCadastradaDaEtapaVisualizoOPrazoAtualizadoCorretamente(string etapa)
        {
            //Método criado no SharedPO, por ser utilizando na Pré Análise e inclusão (ambos possuem mesmos mapeamentos dos elem).     
            _SharedPO.ValidarPrazoAlterado();       
            
        }

        [Given(@"na sessão proposta cadastrada da etapa '(.*)', visualizo o plano atualizado corretamente")]
        public void DadoNaSessaoPropostaCadastradaDaEtapaVisualizoOPlanoAtualizadoCorretamente(string etapa)
        {
            _SharedPO.ValidarPlanoAlterado();
        }

        [Given(@"na sessão proposta cadastrada da etapa '(.*)', visualizo o produto atualizado corretamente")]
        public void DadoNaSessaoPropostaCadastradaDaEtapaVisualizoOProdutoAtualizadoCorretamente(string etapa)
        {
            _SharedPO.ValidarProdutoAlterado();
        }

        [Given(@"na sessão proposta cadastrada da etapa '(.*)', visualizo o seguro atualizado corretamente")]
        public void DadoNaSessaoPropostaCadastradaDaEtapaVisualizoOSeguroAtualizadoCorretamente(string etapa)
        {
            _SharedPO.ValidarSeguroAlterado();
        }

        [Given(@"realizo Refresh/Reaload da página atual")]
        [When(@"realizo Refresh/Reaload da página atual")]
        [Then(@"realizo Refresh/Reaload da página atual")]
        public void DadoRealizoRefreshRealoadDaPaginaAtual()
        {
            _SharedPO.RealizarRefreshPagina();
        }

    }
}
