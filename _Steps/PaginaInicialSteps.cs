﻿using UiTestSiteCP._PageObjects;
using TechTalk.SpecFlow;
using Portosis.UITests.Utils;

namespace UiTestSiteCP._Steps
{
    [Binding]
    public class PaginaInicialSteps
    {
        private PaginaInicialPO _PaginaInicialPO = new PaginaInicialPO(Configs.Driver);

        [Given(@"visualizo o usuário logado na página inicial")]
        public void DadoVisualizoOUsuarioLogadoNaPaginaInicial()
        {
            _PaginaInicialPO.ValidarUsuarioLogado();
        }

        [Given(@"seleciono inclusão de nova proposta")]
        public void DadoSelecionoInclusaoDeNovaProposta()
        {
            _PaginaInicialPO.IncluirProposta();
        }


        [Given(@"acesso Consultar Proposta Existente")]
        [When(@"acesso Consultar Proposta Existente")]
        [Then(@"acesso Consultar Proposta Existente")]
        public void DadoAcessoConsultarPropostaExistente()
        {
            _PaginaInicialPO.AcessarConsultaPropostaExistente();
        }

        [Given(@"seleciono consultar")]
        [When(@"seleciono consultar")]
        [Then(@"seleciono consultar")]
        public void QuandoSelecionoConsultar()
        {
            _PaginaInicialPO.SelecionarConsultar();
        }

        [Given(@"visualizo em tela a grid contendo as informações de número da Proposta, Tipo, Lojista, Loja, Produto,\tData, Situação, Valor e\tDigitador")]
        [When(@"visualizo em tela a grid contendo as informações de número da Proposta, Tipo, Lojista, Loja, Produto,\tData, Situação, Valor e\tDigitador")]
        [Then(@"visualizo em tela a grid contendo as informações de número da Proposta, Tipo, Lojista, Loja, Produto,\tData, Situação, Valor e\tDigitador")]
        public void QuandoVisualizoEmTelaAGridContendoAsInformacoesDeNumeroDaPropostaTipoLojistaLojaProdutoDataSituacaoValorEDigitador()
        {
            _PaginaInicialPO.ValidarExibicaoDadosDaPropostaNaGrid();
        }


        [Given(@"acesso a proposta pesquisada")]
        [When(@"acesso a proposta pesquisada")]
        [Then(@"acesso a proposta pesquisada")]
        public void QuandoAcessoAPropostaPesquisada()
        {
            _PaginaInicialPO.AcessarPropostaPesquisadaViaGrid();
        }

        [Given(@"informo um CPF com proposta já cadastrada e na situação (.*) e para o produto (.*)")]
        [When(@"informo um CPF com proposta já cadastrada e na situação (.*) e para o produto (.*)")]
        [Then(@"informo um CPF com proposta já cadastrada e na situação (.*) e para o produto (.*)")]
        public void DadoInformoUmCPFComPropostaJaCadastradaENaSituacaoEParaOProduto(string situacao, string produto)
        {
            _PaginaInicialPO.InformarCPFComPropostaExistente(situacao, produto);
        }

        [Given(@"informo o número de uma proposta com a situação (.*) e para o produto (.*)")]
        [When(@"informo o número de uma proposta com a situação (.*) e para o produto (.*)")]
        [Then(@"informo o número de uma proposta com a situação (.*) e para o produto (.*)")]
        public void DadoInformoONumeroDeUmaPropostaComASituacaoEParaOProduto(string situacao, string produto)
        {
            _PaginaInicialPO.InformarNumeroPropostaExistente(situacao, produto, null);
        }

        [Given(@"acesso Mais Opções")]
        [When(@"acesso Mais Opções")]
        [Then(@"acesso Mais Opções")]
        public void DadoAcessoMaisOpcoes()
        {
            _PaginaInicialPO.AcessarMenuMaisOpcoes();
        }

        [Given(@"seleciono Histórico Consolidado do Cliente")]
        [When(@"seleciono Histórico Consolidado do Cliente")]
        [Then(@"seleciono Histórico Consolidado do Cliente")]
        public void DadoSelecionoHistoricoConsolidadoDoCliente()
        {
            _PaginaInicialPO.AcessarHistoricoConsolidadeCliente();
        }

        [Given(@"acesso Relatórios > Propostas Por Situação")]
        [When(@"acesso Relatórios > Propostas Por Situação")]
        [Then(@"acesso Relatórios > Propostas Por Situação")]
        public void DadoAcessoRelatoriosPropostasPorSituacao()
        {
            _PaginaInicialPO.AcessarRelatorioPorSituacao();
        }

        [Given(@"acesso Relatórios > Liberações do Dia")]
        [Given(@"acesso Relatórios > Liberações do Dia")]
        [Given(@"acesso Relatórios > Liberações do Dia")]
        public void DadoAcessoRelatoriosLiberacoesDoDia()
        {
            _PaginaInicialPO.AcessarRelatorioLiberacoesDoDia();
        }

        [Given(@"acesso Relatórios > Produção Diária")]
        [When(@"acesso Relatórios > Produção Diária")]
        [Then(@"acesso Relatórios > Produção Diária")]
        public void DadoAcessoRelatoriosProducaoDiaria()
        {
            _PaginaInicialPO.AcessarRelatorioProducaoDiaria();
        }

        //[Given(@"informo o número de uma proposta com a situação '(.*)', produto '(.*)', etapa '(.*)' e data de vencimento maior que a da emissão atual")]
        //public void DadoInformoONumeroDeUmaPropostaComASituacaoProdutoEtapaEDataDeVencimentoMaiorQueADaEmissaoAtual(string situacao, string produto, string etapa)
        //{
        //    _PaginaInicialPO.InformarNumeroPropostaExistente(situacao, produto, etapa);
        //}

        [Given(@"informo o número de uma proposta com a situação (.*), produto (.*), etapa (.*) e data de vencimento maior que a da emissão atual")]
        public void DadoInformoONumeroDeUmaPropostaComASituacaoProdutoEtapaEDataDeVencimentoMaiorQueADaEmissaoAtual(string situacao, string produto, string etapa)
        {
            _PaginaInicialPO.InformarNumeroPropostaExistente(situacao, produto, etapa);
        }

        [Given(@"seleciono Prosseguir com a proposta em Aberto")]
        public void DadoSelecionoProsseguirComAPropostaEmAberto()
        {
            _PaginaInicialPO.SelecionarSeguirComPropstaExistente();
        }








    }
}
