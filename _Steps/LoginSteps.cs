﻿using UiTestSiteCP._PageObjects;
using TechTalk.SpecFlow;
using Portosis.UITests.Utils;

namespace UiTestSiteCP._Steps
{
    [Binding]
    class LoginSteps
    {
        private LoginPO _LoginPO = new LoginPO(Configs.Driver);

        [Given(@"que efetuo o login no sistema")]
        [When(@"que efetuo o login no sistema")]
        [Then(@"que efetuo o login no sistema")]
        public void DadoQueEfetuoOLoginNoSistema()
        {
            _LoginPO.RealizarLogin(); //3810029404    
        }

        [Given(@"valido o apontamento da base Sicred e Parceiros para o ambiente regressivo para os dados: CPF: (.*), Região: (.*), Número do Cliente: (.*), DV do Cliente: (.*) e Lote: (.*)")]
        public void DadoValidoOApontamentoDaBaseSicredEParceirosParaOAmbienteRegressivoParaOsDadosCPFRegiaoNumeroDoClienteDVDoClienteELote(string cpfcgc, string regiao, string numeroCliente, int dvNumeroCliente, string lote)
        {
            /*Ajuste de Apontamentos do Site CP e do Mock*/
            _LoginPO.AjustarApontamentoAmbienteRegressivo();

            /*Remoção do cliente no mock e ambiente parceiros*/
            _LoginPO.LimparDadosBaseMockEnelEParceiros(cpfcgc, regiao, lote);

            /*Inclusão do cliente no mock e ambiente parceiros*/
            _LoginPO.InserirClienteMockEnelRegressivo(cpfcgc, regiao, numeroCliente, dvNumeroCliente, lote);

           // _LoginPO.InserirClienteParceirosEnelRegressivo(cpfcgc, regiao, numeroCliente, dvNumeroCliente, lote);

            _LoginPO.InserirFaturamentoParceirosEnelRegressivo(regiao, lote);            
        }

    }
}

    