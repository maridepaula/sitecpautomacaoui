﻿using Portosis.UITests.Utils;
using SpecFlowTeste.Core.FixedValues;
using TechTalk.SpecFlow;
using UiTestSiteCP._PageObjects;

namespace SpecFlowTeste._Steps
{
    [Binding]
    public class SimulacaoSteps
    {
        private SimulacaoPO _SimulacaoPO = new SimulacaoPO(Configs.Driver);

        [Given(@"cadastro os dados iniciais do cliente para o CPF: (.*)")]
        public void DadoCadastroOsDadosIniciaisDoClienteParaOCPF(string cpfcgc)
        {
            _SimulacaoPO.PreencherDadosIniciaisCliente(cpfcgc);
        }

        [Given(@"informo um cliente apto para refinanciamento para o produto: (.*)")]
        public void DadoInformoUmClienteAptoParaRefinanciamentoParaOProdutoBanco(string produto)
        {
            _SimulacaoPO.BuscarClienteRefin(produto);
        }
     
 
        [Given(@"informo um cliente '(.*)' para refinanciamento para o produto: (.*) e banco: (.*)")]
        public void DadoInformoUmClienteParaRefinanciamentoParaOProdutoProdutoEBanco(string optante, string produto, string codBanco)
        {
  
            _SimulacaoPO.BuscarClienteRefinDBCCaixa(produto, codBanco, optante);
        }

        [Given(@"informo um cliente válido para refinanciamento para o produto: (.*) e banco '(.*)'")]
        public void DadoInformoUmClienteValidoAptoParaRefinanciamentoParaOProdutoEBanco(string produto, string codBanco)
        {         
            _SimulacaoPO.BuscarClienteRefinDBCBradesco(produto, codBanco);
        }


        [Given(@"na sessão Dados da Operação, informo o Valor Operação: (.*), Lojista: (.*), Loja: (.*), Tipo de Contrato: (.*), Produto: (.*), Plano: (.*), Seguro: (.*), Prazo: (.*)")]
        public void DadoNaSessaoDadosDaOperacaoInformoOValorOperacaoLojistaLojaTipoDeContratoProdutoPlanoSeguroPrazo(string valorOperacao, string lojista, string loja, string tipoContrato, string produto, string plano, string seguro, string prazo)
        {
            _SimulacaoPO.PreencherDadosOperacao(valorOperacao, lojista, loja, tipoContrato, produto, plano, seguro, prazo);
        }

        [Given(@"na sessão Dados da Operação, informo o Valor Operação: (.*), Lojista: (.*), Loja: (.*), Tipo de Contrato: (.*), Produto Refin: (.*), Plano: (.*), Seguro: (.*), Prazo: (.*)")]
        public void DadoNaSessaoDadosDaOperacaoInformoOValorOperacaoLojistaLojaTipoDeContratoProdutoRefinPlanoSeguroPrazo(string valorOperacao, string lojista, string loja, string tipoContrato, string produtoRefin, string plano, string seguro, string prazo)
        {
            _SimulacaoPO.PreencherDadosOperacaoRefin(valorOperacao, lojista, loja, tipoContrato, produtoRefin, plano, seguro, prazo);
        }


        [Given(@"em Dados da Operação, informo o Valor Operação: (.*), Lojista: (.*), Loja: (.*), Tipo de Contrato: (.*), Produto: (.*), Plano: (.*), Seguro: (.*), Prazo: (.*), Concessionária: (.*), Região: (.*), Número do Cliente: (.*), DV do Cliente: (.*) e Lote: (.*)")]
        public void DadoEmDadosDaOperacaoInformoOValorOperacaoLojistaLojaTipoDeContratoProdutoPlanoSeguroPrazoConcessionariaRegiaoNumeroDoClienteDVDoCliente(string valorOperacao, string lojista, string loja, string tipoContrato, string produto, string plano, string seguro, string prazo, string concessionaria, string regiao, string numeroCliente, string dvNumeroCliente, string lote)
        {
            _SimulacaoPO.PreencherDadosOperacaoCPFatura(valorOperacao, lojista, loja, tipoContrato, produto, plano, seguro, prazo, concessionaria, regiao, numeroCliente, dvNumeroCliente, lote);
        }
        

        [Given(@"clico em simular")]
        [When(@"clico em simular")]
        [Then(@"clico em simular")]
        public void DadoClicoEmSimular()
        {
            _SimulacaoPO.RealizarSimulacao();
        }


        [Given(@"o resultado da simulação é apresentado")]
        public void DadoOResultadoDaSimulacaoEApresentado()
        {
            _SimulacaoPO.ValidarResultadoSimulacao();
        }

        [Given(@"seleciono para gravar a simulação")]
        public void DadoSelecionoParaGravarASimulacao()
        {

            _SimulacaoPO.GravarSimulacao();
        }

        [Given(@"clico para avançar a simulação")]
        public void DadoSelecionoAvancar()
        {
            _SimulacaoPO.AvancarSimulacao();
        }

        [Given(@"o sistema deve retornar um conjunto com os prazos e prestações")]
        public void DadoOSistemaDeveRetornarUmConjuntoComOsPrazosEPrestacoes()
        {
            _SimulacaoPO.ValidarConjuntoDadosSimulacacao();
        }

        [Given(@"seleciono um prazo")]
        public void DadoSelecionoUmPrazo()
        {
            _SimulacaoPO.SelecionarPrazo();
        }

        [Given(@"informo um valor de operação abaixo do valor máximo financiado para família Débito")]
        public void DadoInformoUmValorDeOperacaoAbaixoDoValorMaximoFinanciadoParaFamiliaDebito()
        {
            _SimulacaoPO.InformarValorOperacaoValidoFamiliaDebito();
        }

        //[Given(@"altero a simulação informando o valor de parcela da simulação anterior zerando o valor de operação")]
        //public void DadoAlteroASimulacaoInformandoOValorDeParcelaDaSimulacaoAnteriorZerandoOValorDeOperacao()
        //{
        //    _SimulacaoPO.AlterarPMTPreenchendoValorParcela();
        //}

        //[Given(@"visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior")]
        //[When(@"visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior")]
        //[Then(@"visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior")]
        //public void QuandoVisualizoOValorDaOperacaoEParcelaGeradosCorretamenteConformeSimulacaoAnterior()
        //{
        //    _SimulacaoPO.ValidarValorParcelaPMT();
        //    _SimulacaoPO.ValidarValorOperacaoPMT();
        //}

    }
}

