﻿using Portosis.UITests.Utils;
using SpecFlowTeste._PageObjects.Relatórios;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowTeste._Steps
{
    [Binding]
    public class GerarRelatorioProducaoDiariaSteps
    {
        private RelatProducaoDiariaPO _RelatProducaoDiariaPO = new RelatProducaoDiariaPO(Configs.Driver);

        [Given(@"informo uma data válida com Produção Diária dentro do intervalo de (.*) dias com proposta na situação de (.*)")]
        public void DadoInformoUmaDataValidaComProducaoDiariaDentroDoIntervaloDeDiasComPropostaNaSituacaoDe(int intDias, string sitProposta)
        {
            _RelatProducaoDiariaPO.InformarPeriodoProducaoDiaria(intDias, sitProposta );
        }


        [Then(@"visualizo em tela a Grid contendo as informações de Número da Proposta, Nome do Cliente, CPF do Cliente, Nome do Agente, Analista, Valor Operação, Prazo, Operador, Lojista, Loja e Situação")]
        public void EntaoVisualizoEmTelaAGridContendoAsInformacoesDeNumeroDaPropostaNomeDoClienteCPFDoClienteNomeDoAgenteAnalistaValorOperacaoPrazoOperadorLojistaLojaESituacao()
        {
            _RelatProducaoDiariaPO.ValidarExibicaoContratoGrid();
        }



    }
}
