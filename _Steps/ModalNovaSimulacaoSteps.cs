﻿using Portosis.UITests.Utils;
using TechTalk.SpecFlow;
using UiTestSiteCP._PageObjects;

namespace UiTestSiteCP._Steps
{
    [Binding]
    public class ModalNovaSimulacaoSteps
    {
        private ModalNovaSimulacaoPO _ModalNovaSimulacaoPO = new ModalNovaSimulacaoPO(Configs.Driver);
        private PreAnalisePO _PreAnalisePO = new PreAnalisePO(Configs.Driver);
        private SharedPO _SharedPO = new SharedPO(Configs.Driver);

        [Given(@"na modal de nova simulação informao o valor de parcela da simulação anterior zerando o valor de operação")]
        public void DadoNaModalDeNovaSimulacaoInformaoOValorDeParcelaDaSimulacaoAnteriorZerandoOValorDeOperacao()
        {
            _ModalNovaSimulacaoPO.PreencherValorPrestacao();
        }

        [Given(@"na modal de nova simulação clico em simular")]
        public void DadoNaModalDeNovaSimulacaoClicoEmSimular()
        {
            _ModalNovaSimulacaoPO.ClicarEmSimular();
        }

        [Given(@"seleciono na modal de Nova Simulação o prazo com o valor de operação gerado corretamente")]
        public void DadoSelecionoNaModalDeNovaSimulacaoOPrazoComOValorDeOperacaoGeradoCorretamente()
        {
            _ModalNovaSimulacaoPO.SelecionarPrazoComNovoValorOperacaoGeradoNaGrid();
        }  

        [Given(@"seleciono na modal de Nova Simulação Gravar")]
        [When(@"seleciono na modal de Nova Simulação Gravar")]        
        public void QuandoSelecionoNaModalDeNovaSimulacaoGravar()
        {
            _ModalNovaSimulacaoPO.SelecionarGravar();
        }

        [Given(@"na modal de nova simulação visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior")]
        public void DadoNaModalDeNovaSimulacaoVisualizoOValorDaOperacaoEParcelaGeradosCorretamenteConformeSimulacaoAnterior()
        {
            _ModalNovaSimulacaoPO.ValidarValorOperacaoPMTModal();
            _ModalNovaSimulacaoPO.ValidarValorParcelaPMTModal();
        }

        [Given(@"na modal de Nova Simulação realizo a alteração do prazo")]
        public void DadoNaModalDeNovaSimulacaoRealizoAAlteracaoDoPrazo()
        {
            _ModalNovaSimulacaoPO.AlterarPrazo(1);

        }

        [Given(@"na modal de Nova Simulação realizo a alteração do plano '(.*)'")]
        public void DadoNaModalDeNovaSimulacaoRealizoAAlteracaoDoPlano(string plano)
        {
            _ModalNovaSimulacaoPO.AlterarPlano(plano);
        }

        [Given(@"na modal de Nova Simulacao informo um novo prazo (.*)")]
        public void DadoNaModalDeNovaSimulacaoInformoUmNovoPrazo(int prazo)
        {
            _ModalNovaSimulacaoPO.AlterarPrazoPorParêmtroFixo(prazo);
        }


        [Given(@"na modal de Nova Simulação realizo a alteração do produto '(.*)'")]
        public void DadoNaModalDeNovaSimulacaoRealizoAAlteracaoDoProduto(string produto)
        {
            _ModalNovaSimulacaoPO.AlterarProduto(produto);
        }

        [Given(@"na modal de Nova Simulação realizo a alteração do seguro para: (.*)")]
        public void DadoNaModalDeNovaSimulacaoRealizoAAlteracaoDoSeguroParaBASICO(string seguro)
        {
            _SharedPO.AtualizarSeguro(seguro);
            _ModalNovaSimulacaoPO.AlterarPrazo(0); 
        } 

        [Given(@"na modal de nova simulação visualizo o prazo gerado corretamente conforme simulação anterior")]
        public void DadoNaModalDeNovaSimulacaoVisualizoOPrazoGeradoCorretamenteConformeSimulacaoAnterior()
        {
            _ModalNovaSimulacaoPO.ValidarPrazoAlteradoNaModal();
        }

        [Given(@"na modal de nova simulação visualizo o plano gerado corretamente conforme simulação anterior")]
        public void DadoNaModalDeNovaSimulacaoVisualizoOPlanoGeradoCorretamenteConformeSimulacaoAnterior()
        {
            _ModalNovaSimulacaoPO.ValidarPlanoAlteradoNaModal();
        }

        [Given(@"na modal de nova simulação visualizo o produto gerado corretamente conforme simulação anterior")]
        public void DadoNaModalDeNovaSimulacaoVisualizoOProdutoGeradoCorretamenteConformeSimulacaoAnterior()
        {
            _ModalNovaSimulacaoPO.ValidarProdutoAlteradoNaModal();
        }   

        [Given(@"seleciono a data de Primeiro Vencimento")]
        public void DadoSelecionoADataDeºVencimento()
        {
            _ModalNovaSimulacaoPO.InformarPrimeiroVcto();
        }


    }
}
