﻿using Portosis.UITests.Utils;
using SpecFlowTeste._PageObjects.Relatórios;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowTeste._Steps
{
    [Binding]
    public class GerarRelatorioDePropostaPorSituacaoSteps
    {

        private RelatPropostasPorSituacaoPO _RelatPropostasPorSituacaoPO = new RelatPropostasPorSituacaoPO(Configs.Driver);

        [Given(@"informo um período com o intervalo de dias (.*) que contenha proposta na situação (.*)")]
        public void DadoInformoUmPeriodoComOIntervaloDeDiasQueContenhaPropostaNaSituacao(int intervaloDias, string situacao)
        {
            _RelatPropostasPorSituacaoPO.InformarIntervaloDeDatas(intervaloDias, situacao);
        }

        [Given(@"preencho a informa de Lojista (.*) e Loja (.*)")]
        public void DadoPreenchoAInformaDeLojistaELoja(string lojista, string loja)
        {
            _RelatPropostasPorSituacaoPO.InformarLojista(lojista);
            _RelatPropostasPorSituacaoPO.InformarLoja(loja );
        }

        [Given(@"seleciono a situção (.*)")]
        [When(@"seleciono a situção (.*)")]
        [Then(@"seleciono a situção (.*)")]
        public void DadoSelecionoASitucao(string situacaoProposta)
        {
            _RelatPropostasPorSituacaoPO.SelecionarOpcaoSituacao(situacaoProposta);
        }
        
        [Given(@"seleciono relatório tipo Analítico")]
        public void DadoSelecionoRelatorioTipoAnalitico()
        {
            _RelatPropostasPorSituacaoPO.SelenionarRelatorioTipoAnalitico();
        }
        
        [Then(@"visualizo em PDF o relatório por situação exibindo corretamente os dados do contrato")]
        public void EntaoVisualizoEmPDFORelatorioPorSituacaoExibindoCorretamenteOsDadosDoContrato()
        {
            _RelatPropostasPorSituacaoPO.ValidarDadosRelatorioPorSituacao();
        }
    }
}
