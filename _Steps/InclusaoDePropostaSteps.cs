﻿using Portosis.UITests.Utils;
using TechTalk.SpecFlow;
using UiTestSiteCP._PageObjects;

namespace UiTestSiteCP._Steps
{
    [Binding]
    public class InclusaoDePropostaSteps
    {
        private InclusaoDePropostaPO _InclusaoDePropostaPO = new InclusaoDePropostaPO(Configs.Driver);

        [Given(@"cadastro os dados de identificacao do cliente")]
        public void DadoCadastrosOsDadosDeIdentificacaoDoCliente()
        {
            _InclusaoDePropostaPO.PreencherDadosIdentificacaoCliente();
        }

        [Given(@"seleciono o tipo de telefone da residência")]
        public void DadoSelecionoOTipoDeTelefoneDaResidencia()
        {
            _InclusaoDePropostaPO.PreencherDadosResidencia();
        }

        [Given(@"informo os dados profissionais")]
        public void DadoInformoOsDadosProfissionais()
        {
            _InclusaoDePropostaPO.PreencherDadosProfissionais();
        }

        [Given(@"informo as referências profissionais")]
        public void DadoInformoAsReferenciasProfissionais()
        {
            _InclusaoDePropostaPO.PreencherDadosReferenciasProfissionais();
        }

        [Given(@"informo as referências bancárias")]
        public void DadoInformoAsReferenciasBancarias()
        {
            _InclusaoDePropostaPO.PreencherReferenciasBancarias();
        }

        [Given(@"informo os dados de garantia")]
        public void DadoInformoOsDadosDeGarantia()
        {
            _InclusaoDePropostaPO.PreencherDadosGarantia();
        }


        [Given(@"seleciono para gravar a inclusão da proposta")]
        public void DadoSelecionoParaGravarAInclusaoDaProposta()
        {
            _InclusaoDePropostaPO.GravarInclusaoProposta();
        }

        [Given(@"seleciono para gravar a inclusão da proposta do refin")]
        public void DadoSelecionoParaGravarAInclusaoDaPropostaDoRefin()
        {
            _InclusaoDePropostaPO.GravarInclusaoPropostaRefin();
        }

        [Given(@"clico para avançar a inclusão da proposta")]
        public void DadoSelecionoAvancarAInclusaoDaProposta()
        {
            _InclusaoDePropostaPO.AvancarInclusaoProposta();
        }

        [Given(@"seleciono um tipo de efetivação: (.*)")]
        public void DadoSelecionoUmTipoDeEfetivacao(string tipoEfetivacao)
        {
            _InclusaoDePropostaPO.SelecionarTipoEfetivacao(tipoEfetivacao);
        }
    }
}