﻿using Portosis.UITests.Utils;
using SpecFlowTeste._PageObjects;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowTeste._Steps
{
    [Binding]
    public class ConsultarHistoricoConsolidadoDoClienteSteps
    {

        private ConsolidadoDoClientePO _ConsolidadoDoClientePO = new ConsolidadoDoClientePO(Configs.Driver);

        [Given(@"informo o CPF do cliente sem contrato efetivado para o produto '(.*)'")]
        public void DadoInformoOCPFDoClienteSemContratoEfetivadoParaOProduto(string produto)
        {
            _ConsolidadoDoClientePO.InformarCPFConsolidadoSemContratoEfetivado(produto);
        }

        [Given(@"informo o CPF do cliente com contrato efetivado para o produto '(.*)'")]
        public void DadoInformoOCPFDoClienteComContratoEfetivadoParaOProduto(string produto)
        {
            _ConsolidadoDoClientePO.InformarCPFConsolidadoComContratoEfetivado(produto);
        }

        [Then(@"visualizo em tela as informações do Nome do Cliente, CPF, Número do Contrato, Valor Operação, Valor Liberado, Parcela, Prazo, Produto, Lojista, Loja e Data de Emissâo")]
        public void EntaoVisualizoEmTelaAsInformacoesDoNomeDoClienteCPFNumeroDoContratoValorOperacaoValorLiberadoParcelaPrazoProdutoLojistaLojaEDataDeEmissao()
        {
            _ConsolidadoDoClientePO.ValidarDadosDoContratoConsultadoNaGripPrincipal();
        }



    }
}
