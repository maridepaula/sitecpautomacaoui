﻿# language: pt-br
Funcionalidade: Refinanciamento proposta

Esquema do Cenário: Refinanciar Proposta EP CHEQUE REFIN com plano <plano>, COM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente apto para refinanciamento para o produto: <produto>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação	
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'	
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'	
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado para o refin completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	#E valido na base tipo de efetivação e cheques pré-datados da proposta cadastrada
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	#E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	#E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	#E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	#E valido na base em Cheques que os dados foram inseridos para a proposta
	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | produtoRefin       | plano                                       | seguro     | prazo | codBanco | tipoEfetivacao |
		| 02740365683 | 5000          | 905152  | 5152 | CCB          | 000712 - EPCHEQUE | 000720 - EP1 REFIN | 9886 - CARTA NA MANGA TX 11 96 NOVO E REFIN | SEM SEGURO | 15    | 0237     | Loja           |

Esquema do Cenário: Refinanciar Proposta EP CHEQUE REFIN com plano <plano>, SEM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente apto para refinanciamento para o produto: <produto>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação	
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'	
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado para o refin completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'	
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	#E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	#E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	#E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	#E valido na base em Cheques que os dados foram inseridos para a proposta
	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | produtoRefin       | plano                                       | seguro   | prazo     | codBanco | tipoEfetivacao |
		| 02740365683 | 5000          | 905153  | 5153 | CCB          | 000712 - EPCHEQUE | 000720 - EP1 REFIN | 9851 - EP1 REFIN CLIENTE N 9X A 36X CCB     | BÁSICO   | Selecione | 0104     | Delivery       |
		| 02740365683 | 4999          | 905152  | 5152 | CCB          | 000712 - EPCHEQUE | 000720 - EP1 REFIN | 9886 - CARTA NA MANGA TX 11 96 NOVO E REFIN | COMPLETO | Selecione | 0237     | Loja           |

Esquema do Cenário: Refinanciar Proposta EP CARNE REFIN com plano <plano>, COM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'	
	E informo um cliente apto para refinanciamento para o produto: <produto>	
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação	
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'	
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'	
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'	
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	#E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	#E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	#E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	#E valido na base em Cheques que os dados foram inseridos para a proposta
	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | produtoRefin            | plano                           | seguro     | prazo | codBanco | tipoEfetivacao | formaEnvio |
		| 02740365683 | 5000          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 001025 - CPCARNEC REFIN | 9615 - CPCARNEC 05 A 24X TX1625 | SEM SEGURO | 15    | 0341     | Delivery       | Correios   |
        | 02740365683 | 4990          | 000020  | 0020 | CCB          | 001028 - CPCARNEC | 001025 - CPCARNEC REFIN | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO   | 10    | 0001     |Loja            | Loja       |

Esquema do Cenário: Refinanciar Proposta EP CARNE REFIN com plano <plano>, SEM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente apto para refinanciamento para o produto: <produto>	
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação	
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'	
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'	
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'	
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	#E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	#E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	#E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	#E valido na base em Cheques que os dados foram inseridos para a proposta
	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | produtoRefin            | plano                           | seguro | prazo     | codBanco | tipoEfetivacao | formaEnvio |
		| 02740365683 | 5000          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 001025 - CPCARNEC REFIN | 9615 - CPCARNEC 05 A 24X TX1625 | BÁSICO | Selecione | 0104     | Delivery       | Loja       |

Esquema do Cenário: CEF Refinanciar Proposta CP DEBITO REFIN para optante ativo com plano <plano>, COM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente 'optante ativo' para refinanciamento para o produto: <produto> e banco: <codBanco>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação	
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'	
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'	
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios	
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	#E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	#E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	#E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	#E valido na base em Cheques que os dados foram inseridos para a proposta
	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | produtoRefin             | plano            | seguro     | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 5000          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 001040 - CP DÉBITO REFIN | 3577 - EP DÉBITO | SEM SEGURO | 8     | 0104     | Loja           |

Esquema do Cenário: CEF Refinanciar Proposta CP DEBITO REFIN optante inativo com plano <plano>, COM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente 'não optante' para refinanciamento para o produto: <produto> e banco: <codBanco>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação	
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'	
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'	
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'	
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	Então a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'

	#E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	#E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	#E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	#E valido na base em Cheques que os dados foram inseridos para a proposta
	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | produtoRefin             | plano            | seguro   | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 5000          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 001040 - CP DÉBITO REFIN | 3577 - EP DÉBITO | COMPLETO | 8     | 0104     | Loja           |

Esquema do Cenário: BRADESCO Refinanciar Proposta CP DEBITO REFIN com plano <plano>, SEM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente válido para refinanciamento para o produto: <produto> e banco '0237'
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação	
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'	
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'	
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'	
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	Então a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'

	#E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	#E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	#E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	#E valido na base em Cheques que os dados foram inseridos para a proposta
	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | produtoRefin             | plano            | seguro     | prazo     | codBanco | tipoEfetivacao |
		| 01236833600 | 5000          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 001040 - CP DÉBITO REFIN | 3577 - EP DÉBITO | SEM SEGURO | Selecione | 0237     | Delivery       |