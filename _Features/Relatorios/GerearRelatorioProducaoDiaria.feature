﻿# language: pt-br
Funcionalidade: Gerar Relatório Produção Diária

Esquema do Cenário: Gerar Relatório Produção Diária por loja e situação <sitProposta>
	E que efetuo o login no sistema
	E acesso Mais Opções
	E acesso Relatórios > Produção Diária
	E informo uma data válida com Produção Diária dentro do intervalo de <intervaloDias> dias com proposta na situação de <sitProposta>
	E preencho a informa de Lojista <nrLojista> e Loja <nrLoja>
	E seleciono a situção <sitProposta>
	Quando seleciono consultar
	Então visualizo em tela a Grid contendo as informações de Número da Proposta, Nome do Cliente, CPF do Cliente, Nome do Agente, Analista, Valor Operação, Prazo, Operador, Lojista, Loja e Situação
	
	#algumas situação nao têm na base, ver scrip para inserir se necessário teste
	Exemplos:
		| intervaloDias | sitProposta             | nrLojista | nrLoja |
		| 30            | 20 - CONTRATO EFETIVADO | 0         | 0      |
	   #| 30            | 09 - CANCELADA          | 0         | 0      | 
		| 30            | 10 - PENDENTE           | 0         | 0      |