﻿# language: pt-br
Funcionalidade: Gerar Relatório de Proposta Por Situação

Esquema do Cenário: Gerar Relatório do Tipo Analítico de Proposta Por Situação
	E que efetuo o login no sistema
	E acesso Mais Opções
	E acesso Relatórios > Propostas Por Situação
	E informo um período com o intervalo de dias <interDias> que contenha proposta na situação <sitProposta>
	E preencho a informa de Lojista <nrLojista> e Loja <nrLoja>
	E seleciono a situção <sitProposta>
	E seleciono relatório tipo Analítico
	Quando seleciono consultar
	Então visualizo em PDF o relatório por situação exibindo corretamente os dados do contrato

	Exemplos:
		| interDias | sitProposta             | nrLojista | nrLoja |
		| 6         | 20 - CONTRATO EFETIVADO | 0         | 0      |