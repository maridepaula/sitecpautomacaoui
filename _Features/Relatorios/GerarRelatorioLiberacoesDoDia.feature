﻿# language: pt-br
Funcionalidade: Gerar Relatório De Liberações Do Dia

Esquema do Cenário: Gerar Relatório Do Dia Por Forma de Liberacao <formaLiberacao>
	E que efetuo o login no sistema
	E acesso Mais Opções
	E acesso Relatórios > Liberações do Dia
	E seleciono a Forma de Liberação <formaLib>
	E informo uma data válida com Liberação Disponível para a Forma de Liberação <formaLib>
	Quando seleciono consultar
	Então visualizo em PDF o Relatório de Liberação do Dia exibindo corretamente o número Contrato, Nome do Cliente, Data da Liberação e Forma de Pagamento conforme filtro

	Exemplos:
		| formaLib |
		| Cheque   |