﻿# language: pt-br
Funcionalidade: Cancelamento Proposta

Esquema do Cenário: Cancelar proposta EP CHEQUE na Etapa 2 - Pré-Simulação com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                  | seguro   | prazo     | codBanco | tipoEfetivacao |
		| 48596884904 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | Selecione | 0001     | Loja           |

Esquema do Cenário: Cancelar proposta EP CHEQUE na Etapa 3 - Inlusão de Proposta com situação 31 - COMPLEMENTAR CADASTRO
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque com plano <plano>, prazo selecionado <prazo> e seguro <seguro> com tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                  | seguro   | prazo | codBanco | tipoEfetivacao |
		| 67127909725 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 36    | 0001     | Loja           |

Esquema do Cenário: Cancelar proposta EP CHEQUE na Etapa 3 - Inlusão de Proposta com situação 10 - PENDENTE
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque com plano <plano>, prazo selecionado <prazo> e seguro <seguro> com tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                  | seguro   | prazo | codBanco | tipoEfetivacao |
		| 67127909725 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 36    | 0001     | Loja           |

Esquema do Cenário: Cancelar proposta EP CARNE na Etapa 2 - Pré-Simulação com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo     | codBanco | tipoEfetivacao | formaEnvio |
		| 17661580055 | 1000          | 905154  | 5154 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | Selecione | 0001     | Loja           | Email      |

Esquema do Cenário: Cancelar proposta EP CARNE na Etapa 3 - Inlusão de Proposta com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo     | codBanco | tipoEfetivacao | formaEnvio |
		| 17661580055 | 1000          | 000020  | 0020 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | Selecione | 0001     | Loja           | Email      |

Esquema do Cenário: Cancelar proposta DEB CC PUBLICO na Etapa 2 - Pré-Simulação com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro   | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 1000          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO | 10    | 0104     | Loja           |

Esquema do Cenário: Cancelar proposta DEB CC PUBLICO na Etapa 3 - Inlusão de Proposta com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro   | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 1000          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO | 10    | 0104     | Loja           |

Esquema do Cenário: Cancelar proposta CP DEBITO REFIN optante inativo e banco CEF na Etapa 2 - Pré-Simulação com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente 'não optante' para refinanciamento para o produto: <produto> e banco: <codBanco>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | produtoRefin             | plano            | seguro   | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 5000          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 001040 - CP DÉBITO REFIN | 3577 - EP DÉBITO | COMPLETO | 8     | 0104     | Loja           |

Esquema do Cenário: Cancelar proposta CP DEBITO REFIN optante inativo e banco CEF na Etapa 3 - Inlusão de Proposta com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente 'não optante' para refinanciamento para o produto: <produto> e banco: <codBanco>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | produtoRefin             | plano            | seguro   | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 5000          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 001040 - CP DÉBITO REFIN | 3577 - EP DÉBITO | COMPLETO | 8     | 0104     | Loja           |


Esquema do Cenário: Cancelar proposta CP FATURA na Etapa 2 - Pré-Simulação com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E valido o apontamento da base Sicred e Parceiros para o ambiente regressivo para os dados: CPF: <cpfcgc>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E em Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>, Concessionária: <concessionaria>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto            | plano                          | seguro     | prazo | codBanco | tipoEfetivacao | formaEnvio | concessionaria | regiao | numeroCliente | dvNumeroCliente | lote |
		| 12345678909 | 1800          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | SEM SEGURO | 4     | 0104     | Loja           | Loja       | ENEL           | 2003   | 192219        | 6               | 99   |						

Esquema do Cenário: Cancelar proposta CP FATURA na Etapa 3 - Inclusão de Proposta com situação 10 - PENDENTE
	Dado que efetuo o login no sistema
	E valido o apontamento da base Sicred e Parceiros para o ambiente regressivo para os dados: CPF: <cpfcgc>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E em Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>, Concessionária: <concessionaria>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta é atualizada via base para a situação '10' na etapa de '2 - Pré Análise'
	E seleciono Cancelar Proposta
	Quando confirmo a mensagem 'Confirma cancelar a proposta?'
	Então confirmo a mensagem 'Cancelamento da proposta realizado com sucesso!'
	E a proposta deve estar na situação '61 - Proposta Cancelada'
	E valido na base em PROPOSTASCREDITO a situação alterada corretamente para '61 - Cancelada'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto            | plano                          | seguro     | prazo | codBanco | tipoEfetivacao | formaEnvio | concessionaria | regiao | numeroCliente | dvNumeroCliente | lote |		
		| 12345678909 | 1500          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | SEM SEGURO | 12    | 0104     | Loja           | Loja       | ENEL           | CELG   | 202220        | 0               | 99   |