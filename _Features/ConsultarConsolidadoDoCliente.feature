﻿#language: pt-br
Funcionalidade: Consultar Histórico Consolidado do Cliente

Cenário: Consultar Histórico Consolidado do Cliente Sem Contrato Efetivado
	Dado que efetuo o login no sistema
	E acesso Mais Opções
	E seleciono Histórico Consolidado do Cliente
	E informo o CPF do cliente sem contrato efetivado para o produto '001028 - CPCARNEC'
	Quando seleciono consultar
	Então confirmo a mensagem 'Não existem dados para o cpf digitado!'

Cenário: Consultar Histórico Consolidado do Cliente Com Contrato Efetivado
	Dado que efetuo o login no sistema
	E acesso Mais Opções
	E seleciono Histórico Consolidado do Cliente
	E informo o CPF do cliente com contrato efetivado para o produto '001028 - CPCARNEC'
	Quando seleciono consultar
	Então visualizo em tela as informações do Nome do Cliente, CPF, Número do Contrato, Valor Operação, Valor Liberado, Parcela, Prazo, Produto, Lojista, Loja e Data de Emissâo