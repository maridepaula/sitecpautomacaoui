﻿# language: pt-br
Funcionalidade: CalculoPMT

Esquema do Cenário: Alterar cálculo PMT com DEB CC PUBLICO na etapa: Simulação
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E altero a simulação informando o valor de parcela da simulação anterior zerando o valor de operação
	Quando clico em simular
	Então visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro   | prazo | codBanco | tipoEfetivacao | etapa     |
		| 52282323033 | 3500          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO | 10    | 0104     | Loja           | Simulação |

Esquema do Cenário: Alterar cálculo PMT com DEB CC PUBLICO na etapa: Pré-Análise
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque com plano <plano>, prazo selecionado <prazo> e seguro <seguro> com tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E clico em nova simulação
	E na modal de nova simulação informao o valor de parcela da simulação anterior zerando o valor de operação
	E na modal de nova simulação clico em simular
	E seleciono na modal de Nova Simulação o prazo com o valor de operação gerado corretamente
	Quando seleciono na modal de Nova Simulação Gravar
	Então visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro   | prazo | codBanco | tipoEfetivacao | etapa     |
		| 52282323033 | 3500          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO | 10    | 0104     | Loja           | Simulação |

Esquema do Cenário: Alterar cálculo PMT com DEB CC PUBLICO na etapa: Inclusão de Proposta
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque com plano <plano>, prazo selecionado <prazo> e seguro <seguro> com tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E clico em nova simulação
	E na modal de nova simulação informao o valor de parcela da simulação anterior zerando o valor de operação
	E na modal de nova simulação clico em simular
	E na modal de nova simulação visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior
	Quando seleciono na modal de Nova Simulação Gravar
	Então visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro   | prazo | codBanco | tipoEfetivacao | etapa     |
		| 52282323033 | 3500          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO | 10    | 0104     | Loja           | Simulação |

Esquema do Cenário: Alterar cálculo PMT com EPCARNE na etapa: Simulação
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E altero a simulação informando o valor de parcela da simulação anterior zerando o valor de operação
	Quando clico em simular
	Entao visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo | codBanco | tipoEfetivacao | formaEnvio | etapa     |
		| 67127909725 | 1000          | 000020  | 0020 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | 15    | 0001     | Loja           | Email      | Simulação |

Esquema do Cenário: Alterar cálculo PMT com EP CHEQUE na etapa: Simulação
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E altero a simulação informando o valor de parcela da simulação anterior zerando o valor de operação
	Quando clico em simular
	Entao visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                  | seguro   | prazo | codBanco | tipoEfetivacao | etapa     |
		| 67127909725 | 2500          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 36    | 0001     | Loja           | Simulação |

Esquema do Cenário: Alterar cálculo PMT com REFIN EP CARNÊ na etapa: Simulação
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente apto para refinanciamento para o produto: <produto>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E altero a simulação informando o valor de parcela da simulação anterior zerando o valor de operação
	Quando clico em simular
	Entao visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | produtoRefin            | plano                           | seguro     | prazo | codBanco | tipoEfetivacao | formaEnvio | etapa     |
		| 02740365683 | 2000          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 001025 - CPCARNEC REFIN | 9615 - CPCARNEC 05 A 24X TX1625 | SEM SEGURO | 15    | 0341     | Delivery       | Correios   | Simulação |

Esquema do Cenário: Alterar cálculo PMT com REFIN DEB CC PUBLICO na etapa: Pré-Análise
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente 'não optante' para refinanciamento para o produto: <produto> e banco: <codBanco>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E clico em nova simulação
	E na modal de nova simulação informao o valor de parcela da simulação anterior zerando o valor de operação
	E na modal de nova simulação clico em simular
	E seleciono na modal de Nova Simulação o prazo com o valor de operação gerado corretamente
	Quando seleciono na modal de Nova Simulação Gravar
	E confirmo a mensagem 'Produto alterado, refinanciamento removido com sucesso!'
	Então visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | produtoRefin             | plano            | seguro   | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 5000          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 001040 - CP DÉBITO REFIN | 3577 - EP DÉBITO | COMPLETO | 8     | 0104     | Loja           |

Esquema do Cenário:  Alterar cálculo PMT com REFIN DEB CC PUBLICO na etapa: Inclusão de Proposta
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente 'optante ativo' para refinanciamento para o produto: <produto> e banco: <codBanco>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico em nova simulação
	E na modal de nova simulação informao o valor de parcela da simulação anterior zerando o valor de operação
	E na modal de nova simulação clico em simular
	E seleciono na modal de Nova Simulação o prazo com o valor de operação gerado corretamente
	Quando seleciono na modal de Nova Simulação Gravar
	E confirmo a mensagem 'Produto alterado, refinanciamento removido com sucesso!'
	Então visualizo o valor da operação e parcela gerados corretamente conforme simulação anterior

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | produtoRefin             | plano            | seguro     | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 5000          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 001040 - CP DÉBITO REFIN | 3577 - EP DÉBITO | SEM SEGURO | 8     | 0104     | Loja           |