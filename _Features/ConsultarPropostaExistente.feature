﻿#language: pt-br
Funcionalidade: Consultar Proposta Existente

Esquema do Cenário: Consultar Proposta Existente Através do Número de CPF do Cliente
	Dado que efetuo o login no sistema
	E acesso Consultar Proposta Existente
	E informo um CPF com proposta já cadastrada e na situação <situacaoProposta> e para o produto <produto>
	Quando seleciono consultar
	E visualizo em tela a grid contendo as informações de número da Proposta, Tipo, Lojista, Loja, Produto,	Data, Situação, Valor e	Digitador
	E acesso a proposta pesquisada
	Então visualizo os dados da proposta na Etapa de Efetivação

	Exemplos:
		| situacaoProposta        | produto           |
		| 20 - CONTRATO EFETIVADO | 001028 - CPCARNEC |

Esquema do Cenário: Consultar Proposta Existente Através do Número de Contrato
	Dado que efetuo o login no sistema
	E acesso Consultar Proposta Existente	
	E informo o número de uma proposta com a situação <situacaoProposta> e para o produto <produto>
	Quando seleciono consultar
	E visualizo em tela a grid contendo as informações de número da Proposta, Tipo, Lojista, Loja, Produto,	Data, Situação, Valor e	Digitador
	E acesso a proposta pesquisada
	Então visualizo os dados da proposta na Etapa de Efetivação

	Exemplos:
		| situacaoProposta        | produto           |
		| 20 - CONTRATO EFETIVADO | 001028 - CPCARNEC |