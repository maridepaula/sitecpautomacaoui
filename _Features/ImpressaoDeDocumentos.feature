﻿# language: pt-br
Funcionalidade: Impressão De Documento

Esquema do Cenário: Impressao documentos DEB CC PUBLICO COM SEGURO COMPLETO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e Situação 46 - Pendente Autorização Débito em Conta
	Dado que o nome do cenário é 'Impressao documentos DEB CC PUBLICO COM SEGURO COMPLETO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e Situação 46 - Pendente Autorização Débito em Conta'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Termo de Adesão
	E valido os dados gerados corretamente no PDF do Termo de Adesão
	E seleciono imprimir Check List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Bilhete de Seguro
	E valido os dados gerados corretamente no PDF do Bilhete de Seguro
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	E a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'
	Então seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Termo de Adesão
	E valido os dados gerados corretamente no PDF do Termo de Adesão
	E seleciono imprimir Check List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Bilhete de Seguro
	E valido os dados gerados corretamente no PDF do Bilhete de Seguro
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro   | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 1000          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO | 10    | 0104     | Loja           |

Esquema do Cenário: Impressao documentos CP DEBITO REFIN BRADESCO SEM SEGURO na Etapa de Efetivação com a Proposta na Situaçãoo 40 - Pré-Efetivado e 46 - Pendente Autorização Débito em Conta
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente válido para refinanciamento para o produto: <produto> e banco '0237'
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Check List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	E seleciono imprimir Autorização do Cliente para realizar refinanciamento
	E valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente
	E seleciono imprimir Recibo de Renegociação
	E valido os dados gerados corretamente no PDF de Recibo de Renegociação
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	E a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'
	Então seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Check List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	E seleciono imprimir Autorização do Cliente para realizar refinanciamento
	E valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente
	E seleciono imprimir Recibo de Renegociação
	E valido os dados gerados corretamente no PDF de Recibo de Renegociação

	Exemplos:
		| valorOperacao | lojista | loja | tipoContrato | produto                 | produtoRefin             | plano            | seguro     | prazo     | codBanco | tipoEfetivacao |
		| 5000          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 001040 - CP DÉBITO REFIN | 3577 - EP DÉBITO | SEM SEGURO | Selecione | 0237     | Delivery       |

Esquema do Cenário: Impressao documentos EP CARNÊ COM SEGURO COMPLETO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e na Etapa de Impressão de Contrato com a Situação 46 - Pendente Autorização Débito em Conta
	Dado que o nome do cenário é 'Impressao documentos EP CARNÊ COM SEGURO COMPLETO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e na Etapa de Impressão de Contrato com a Situação 46 - Pendente Autorização Débito em Conta'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Check List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	E seleciono imprimir Carnê
	E valido os dados gerados corretamento no PDF do Carnê
	E seleciono imprimir Protocolo Carnê
	E valido os dados gerados corretamente no PDF do Protocolo Carnê
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Check List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	#E seleciono imprimir Carnê
	#E valido os dados gerados corretamento no PDF do Carnê
	E seleciono imprimir Protocolo Carnê
	E valido os dados gerados corretamente no PDF do Protocolo Carnê

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo     | codBanco | tipoEfetivacao | formaEnvio |
		| 17661580055 | 1000          | 000020  | 0020 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | Selecione | 0001     | Loja           | Email      |

Esquema do Cenário: Impressao documentos EP CARNÊ REFIN COM SEGURO BASICO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e na Etapa de Impressão de Contrato com a Situação 46 - Pendente Autorização Débito em Conta
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente apto para refinanciamento para o produto: <produto>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Check List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	E seleciono imprimir Recibo de Renegociação
	E valido os dados gerados corretamente no PDF de Recibo de Renegociação
	E seleciono imprimir Bilhete de Seguro
	E valido os dados gerados corretamente no PDF do Bilhete de Seguro
	E seleciono imprimir Autorização do Cliente para realizar refinanciamento
	E valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente
	#E seleciono imprimir Carnê
	#E valido os dados gerados corretamento no PDF do Carnê
	E seleciono imprimir Protocolo Carnê
	E valido os dados gerados corretamente no PDF do Protocolo Carnê
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Check List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	#E seleciono imprimir Carnê
	#E valido os dados gerados corretamento no PDF do Carnê
	E seleciono imprimir Protocolo Carnê
	E valido os dados gerados corretamente no PDF do Protocolo Carnê
	E seleciono imprimir Recibo de Renegociação
	E valido os dados gerados corretamente no PDF de Recibo de Renegociação
	E seleciono imprimir Autorização do Cliente para realizar refinanciamento
	E valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | produtoRefin            | plano                           | seguro | prazo     | codBanco | tipoEfetivacao | formaEnvio |
		| 02740365683 | 5000          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 001025 - CPCARNEC REFIN | 9615 - CPCARNEC 05 A 24X TX1625 | BÁSICO | Selecione | 0104     | Delivery       | Loja       |

	Esquema do Cenário: Impressao documentos EP CHEQUE REFIN COM SEGURO BASICO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e na Etapa de Impressão de Contrato com a Situação 46 - Pendente Autorização Débito em Conta
	Dado que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente apto para refinanciamento para o produto: <produto>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E seleciono para gravar a pré análise do refin
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E seleciono para gravar a inclusão da proposta do refin
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado para o refin completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Ckeck List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Autorização do Cliente para realizar refinanciamento
	E valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente
	E seleciono imprimir Recibo de Renegociação
	E valido os dados gerados corretamente no PDF de Recibo de Renegociação
	E seleciono imprimir Termo de Adesão
	E valido os dados gerados corretamente no PDF do Termo de Adesão
	E seleciono imprimir Bilhete de Seguro
	E valido os dados gerados corretamente no PDF do Bilhete de Seguro
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E seleciono imprimir Contrato CCB
	E valido os dados gerados corretamente no PDF do Contrato CCB
	E seleciono imprimir Ckeck List
	E valido os dados gerados corretamente no PDF do Check List
	E seleciono imprimir Demonstrativo CET
	E valido os dados gerados corretamente no PDF do Demonstrativo CET
	E seleciono imprimir Autorização do Cliente para realizar refinanciamento
	E valido os dados gerados corretamente no PDF de Autorização de Refinanciamento do Cliente
	E seleciono imprimir Recibo de Renegociação
	E valido os dados gerados corretamente no PDF de Recibo de Renegociação
	E seleciono imprimir Termo de Adesão
	E valido os dados gerados corretamente no PDF do Termo de Adesão
	E seleciono imprimir Bilhete de Seguro
	E valido os dados gerados corretamente no PDF do Bilhete de Seguro
	E seleciono imprimir Cadastro do Cliente
	E valido os dados gerados corretamente no PDF do Cadastro do Cliente
	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | produtoRefin       | plano                                       | seguro   | prazo     | codBanco | tipoEfetivacao |
		| 02740365683 | 4999          | 905152  | 5152 | CCB          | 000712 - EPCHEQUE | 000720 - EP1 REFIN | 9886 - CARTA NA MANGA TX 11 96 NOVO E REFIN | COMPLETO | Selecione | 0237     | Loja           |


Esquema do Cenário: Impressao documentos EP CHEQUE COM SEGURO COMPLETO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e na Etapa de Impressão de Contrato com a Situação 46 - Pendente Autorização Débito em Conta
    Dado que o nome do cenário é 'Impressao documentos EP CHEQUE COM SEGURO COMPLETO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e na Etapa de Impressão de Contrato com a Situação 46 - Pendente Autorização Débito em Conta'
    E que efetuo o login no sistema
    E seleciono inclusão de nova proposta
    E o usuário deve ser redirecionado para etapa '1 - Simulação'
    E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
    E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
    E clico em simular
    E o número da proposta é gerado
    E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
    E o sistema deve retornar um conjunto com os prazos e prestações
    E seleciono um prazo
    E seleciono para gravar a simulação
    E clico para avançar a simulação
    E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
    E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
    E informo os dados complementares do cliente
    E informo os dados de referências bancárias para o banco: <codBanco>
    E seleciono para gravar a pré análise
    E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
    E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
    E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
    E cadastro os dados de identificacao do cliente
    E seleciono o tipo de telefone da residência
    E informo os dados profissionais
    E informo as referências profissionais
    E informo as referências bancárias
    E seleciono para gravar a inclusão da proposta
    E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
    E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
    E clico para avançar a inclusão da proposta
    E seleciono um tipo de efetivação: <tipoEfetivacao>
    E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
    E informo um cheque pré-datado completando o restante
    E clico em Pré-Efetivar
    E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
    E a proposta deve estar na situação '40 - Pré-Efetivado'
    E o usuário deve ser redirecionado para etapa '5 - Efetivação'
    E seleciono imprimir Contrato CCB
    E valido os dados gerados corretamente no PDF do Contrato CCB
    E seleciono imprimir Demonstrativo CET
    E valido os dados gerados corretamente no PDF do Demonstrativo CET
    E seleciono imprimir Check List
    E valido os dados gerados corretamente no PDF do Check List
    E seleciono imprimir Termo de Adesão
    E valido os dados gerados corretamente no PDF do Termo de Adesão
    E seleciono imprimir Bilhete de Seguro
    E valido os dados gerados corretamente no PDF do Bilhete de Seguro
    E seleciono imprimir Cadastro do Cliente
    E valido os dados gerados corretamente no PDF do Cadastro do Cliente
    E marco os documentos obrigatórios
    Quando clico em Efetivar
    E confirmo a mensagem 'Proposta efetivada com sucesso!'
    Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
    E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
    E seleciono imprimir Contrato CCB
    E valido os dados gerados corretamente no PDF do Contrato CCB
    E seleciono imprimir Demonstrativo CET
    E valido os dados gerados corretamente no PDF do Demonstrativo CET
    E seleciono imprimir Check List
    E valido os dados gerados corretamente no PDF do Check List
    E seleciono imprimir Termo de Adesão
    E valido os dados gerados corretamente no PDF do Termo de Adesão
    E seleciono imprimir Bilhete de Seguro
    E valido os dados gerados corretamente no PDF do Bilhete de Seguro
    E seleciono imprimir Cadastro do Cliente
    E valido os dados gerados corretamente no PDF do Cadastro do Cliente
    
	Exemplos:
        | cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                       | seguro     | prazo     | codBanco | tipoEfetivacao |
        | 48596884904 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB      | COMPLETO   | Selecione | 0001     | Loja           |

Esquema do Cenário: Impressao documentos CP FATURA COM SEGURO COMPLETO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e na Etapa de Impressão de Contrato com a Situação 46 - Pendente Autorização Débito em Conta
    Dado que o nome do cenário é 'Impressao documentos CP FATURA COM SEGURO COMPLETO na Etapa de Efetivação com a Proposta na Situação 40 - Pré-Efetivado e na Etapa de Impressão de Contrato com a Situação 46 - Pendente Autorização Débito em Conta'
    E que efetuo o login no sistema
	E valido o apontamento da base Sicred e Parceiros para o ambiente regressivo para os dados: CPF: <cpfcgc>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>    
	E seleciono inclusão de nova proposta
    E o usuário deve ser redirecionado para etapa '1 - Simulação'
    E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
    E em Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>, Concessionária: <concessionaria>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
    E clico em simular
    E o número da proposta é gerado
    E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
    E o sistema deve retornar um conjunto com os prazos e prestações
    E seleciono um prazo
    E seleciono para gravar a simulação
    E clico para avançar a simulação
    E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
    E o resultado da simulação é apresentado
    E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
    E informo os dados complementares do cliente
    E informo os dados de referências bancárias para o banco: <codBanco>
    E seleciono para gravar a pré análise
    E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
    E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
    E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
    E cadastro os dados de identificacao do cliente
    E seleciono o tipo de telefone da residência
    E informo os dados profissionais
    E informo as referências profissionais
    E informo as referências bancárias
    E seleciono para gravar a inclusão da proposta
    E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
    E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
    E clico para avançar a inclusão da proposta
    E seleciono um tipo de efetivação: <tipoEfetivacao>
    E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
    E visualizado os dados do titular já preenchidos para liberação de crédito
    E clico em Pré-Efetivar
    E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
    E a proposta deve estar na situação '40 - Pré-Efetivado'
    E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E realizo upload dos documentos via base que serão encaminhados para formalização
    E seleciono imprimir Contrato CCB
    E valido os dados gerados corretamente no PDF do Contrato CCB
    E seleciono imprimir Demonstrativo CET
    E valido os dados gerados corretamente no PDF do Demonstrativo CET
    E seleciono imprimir Check List
    E valido os dados gerados corretamente no PDF do Check List
    E seleciono imprimir Autorização do Cliente para desconto em fatura
    E valido os dados gerados corretamente no PDF de Autorização para Débito na Fatura de Energia Elétrica
    E seleciono imprimir Termo de Adesão
    E valido os dados gerados corretamente no PDF do Termo de Adesão
    E seleciono imprimir Bilhete de Seguro
    E valido os dados gerados corretamente no PDF do Bilhete de Seguro
    E seleciono imprimir Cadastro do Cliente
    E valido os dados gerados corretamente no PDF do Cadastro do Cliente 
    Quando clico em Efetivar
    E confirmo a mensagem 'Proposta efetivada com sucesso!'
    Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
    E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
    E seleciono imprimir Contrato CCB
    E valido os dados gerados corretamente no PDF do Contrato CCB
    E seleciono imprimir Demonstrativo CET
    E valido os dados gerados corretamente no PDF do Demonstrativo CET
    E seleciono imprimir Check List
    E valido os dados gerados corretamente no PDF do Check List
    E seleciono imprimir Autorização do Cliente para desconto em fatura
    E valido os dados gerados corretamente no PDF de Autorização para Débito na Fatura de Energia Elétrica
    E seleciono imprimir Termo de Adesão
    E valido os dados gerados corretamente no PDF do Termo de Adesão
    E seleciono imprimir Bilhete de Seguro
    E valido os dados gerados corretamente no PDF do Bilhete de Seguro
    E seleciono imprimir Cadastro do Cliente
    E valido os dados gerados corretamente no PDF do Cadastro do Cliente

    Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto            | plano                          | seguro     | prazo     | codBanco | tipoEfetivacao | formaEnvio | concessionaria | regiao | numeroCliente | dvNumeroCliente | lote |
		| 12345678909 | 1800          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | COMPLETO   | Selecione | 0104     | Loja           | Loja       | ENEL           | 2003   | 192219        | 6               | 99   |						