﻿# language: pt-br
Funcionalidade: Efetivação proposta

Esquema do Cenário: Efetivar Proposta EP Cheque com plano <plano>, COM PRAZO SELECIONADO e seguro <seguro> com tipo de efetivação <tipoEfet> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque com plano <plano>, COM PRAZO SELECIONADO e seguro <seguro> com tipo de efetivação <tipoEfet> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em Cheques que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                       | seguro     | prazo | codBanco | tipoEfetivacao |
		| 67127909725 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB      | COMPLETO   | 36    | 0001     | Loja           |
		| 02740365683 | 5000          | 905153  | 5153 | CCB          | 000712 - EPCHEQUE | 3743 - EP CHQ 2A OFT A                      | BÁSICO     | 12    | 0104     | Delivery       |
		| 73897299100 | 1120          | 905152  | 5152 | CCB          | 000712 - EPCHEQUE | 9886 - CARTA NA MANGA TX 11 96 NOVO E REFIN | SEM SEGURO | 15    | 0237     | Loja           |

Esquema do Cenário: Efetivar Proposta EP Cheque com plano <plano>, SEM PRAZO SELECIONADO e seguro <seguro> com tipo de efetivação <tipoEfet> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque com plano <plano>, SEM PRAZO SELECIONADO e seguro <seguro> com tipo de efetivação <tipoEfet> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em Cheques que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                       | seguro     | prazo     | codBanco | tipoEfetivacao |
		| 48596884904 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB      | COMPLETO   | Selecione | 0001     | Loja           |
		| 02615516639 | 1050          | 905153  | 5153 | CCB          | 000712 - EPCHEQUE | 3743 - EP CHQ 2A OFT A                      | BÁSICO     | Selecione | 0104     | Delivery       |
		| 62950583024 | 1120          | 905152  | 5152 | CCB          | 000712 - EPCHEQUE | 9886 - CARTA NA MANGA TX 11 96 NOVO E REFIN | SEM SEGURO | Selecione | 0237     | Loja           |

Esquema do Cenário: Efetivar Proposta EP CARNÊ com plano <plano>, COM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta EP CARNÊ com plano <plano>, COM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em FormaEnvio_Propostas os dados persistidos corretamente

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro     | prazo | codBanco | tipoEfetivacao | formaEnvio |
		| 67127909725 | 1000          | 905154  | 5154 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO   | 15    | 0001     | Loja           | Email      |
		| 02740365683 | 1050          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | SEM SEGURO | 15    | 0104     | Delivery       | Correios   |
		| 67127909725 | 1050          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | BÁSICO     | 15    | 0104     | Loja           | Loja       |

Esquema do Cenário: Efetivar Proposta EP CARNÊ com plano <plano>, SEM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta EP CARNÊ com plano <plano>, SEM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em FormaEnvio_Propostas os dados persistidos corretamente

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro     | prazo     | codBanco | tipoEfetivacao | formaEnvio |
		| 17661580055 | 1000          | 905154  | 5154 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO   | Selecione | 0001     | Loja           | Email      |
		| 07668420703 | 1050          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | SEM SEGURO | Selecione | 0104     | Delivery       | Correios   |
		| 95954290318 | 1050          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | BÁSICO     | Selecione | 0104     | Loja           | Loja       |

Esquema do Cenário: Efetivar Proposta DEB CC PUBLICO com plano <plano>, COM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta DEB CC PUBLICO com plano <plano>, COM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	Então a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em PE_Contratoscdc e PE_Parcelas que os dados foram inseridos para o contrato

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro     | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 1000          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO   | 10    | 0104     | Loja           |
		| 01236833600 | 1050          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 3577 - EP DÉBITO | SEM SEGURO | 8     | 0237     | Delivery       |
		| 02379486646 | 1050          | 905118  | 5159 | CCB          | 001035 - DEB CC PUBLICO | 3577 - EP DÉBITO | BÁSICO     | 12    | 0104     | Loja           |

Esquema do Cenário: Efetivar Proposta DEB CC PUBLICO com plano <plano>, SEM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta DEB CC PUBLICO com plano <plano>, SEM PRAZO SELECIONADO, seguro <seguro>, tipo de efetivação <tipoEfetivacao> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	Então a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em PE_Contratoscdc e PE_Parcelas que os dados foram inseridos para o contrato

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro     | prazo     | codBanco | tipoEfetivacao |
		| 52282323033 | 1000          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO   | Selecione | 0104     | Loja           |
		| 01236833600 | 1050          | 904283  | 4283 | CCB          | 001035 - DEB CC PUBLICO | 3577 - EP DÉBITO | SEM SEGURO | Selecione | 0237     | Delivery       |
		| 02379486646 | 1050          | 905118  | 5159 | CCB          | 001035 - DEB CC PUBLICO | 3577 - EP DÉBITO | BÁSICO     | Selecione | 0104     | Loja           |

Esquema do Cenário: Efetivar Proposta CP Moto com plano <plano>, COM PRAZO SELECIONADO, SEM seguro com tipo de efetivação <tipoEfet> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta CP Moto com plano <plano>, COM PRAZO SELECIONADO, SEM seguro com tipo de efetivação <tipoEfet> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E informo os dados de garantia
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E seleciono os dados para liberação de crédito
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em FormaEnvio_Propostas os dados persistidos corretamente
	E valido na base em PropostaBens que os dados foram inseridos para a proposta
	E valido na base em Liberacao que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto       | plano                        | seguro     | prazo | codBanco | tipoEfetivacao | formaEnvio |
		| 67127909725 | 18000         | 905197  | 5197 | CCB          | 001029 - MOTO | 3817 - MOTO RS 2012 A OKM R6 | SEM SEGURO | 36    | 0104     | Loja           | Correios   |

Esquema do Cenário: Efetivar Proposta CP Moto com plano <plano>, SEM PRAZO SELECIONADO, SEM seguro com tipo de efetivação <tipoEfet> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta CP Moto com plano <plano>, SEM PRAZO SELECIONADO, SEM seguro com tipo de efetivação <tipoEfet> e contrato <tipoContrato>'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E informo os dados de garantia
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E seleciono os dados para liberação de crédito
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em FormaEnvio_Propostas os dados persistidos corretamente
	E valido na base em PropostaBens que os dados foram inseridos para a proposta
	E valido na base em Liberacao que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto       | plano                         | seguro     | prazo     | codBanco | tipoEfetivacao | formaEnvio |
		| 73897299100 | 18000         | 905197  | 5197 | CCB          | 001029 - MOTO | 3815 - MOTO RS 2012 A OKM R24 | SEM SEGURO | Selecione | 0104     | Loja           | Loja       |

Esquema do Cenário: Efetivar Proposta CP Fatura com plano <plano>, SEM PRAZO SELECIONADO, e seguro <seguro> com tipo de efetivação <tipoEfet> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta CP Fatura com plano <plano>, SEM PRAZO SELECIONADO, e seguro <seguro> com tipo de efetivação <tipoEfet> e contrato <tipoContrato>'
	E valido o apontamento da base Sicred e Parceiros para o ambiente regressivo para os dados: CPF: <cpfcgc>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E em Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>, Concessionária: <concessionaria>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o sistema deve retornar um conjunto com os prazos e prestações
	E seleciono um prazo
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E o resultado da simulação é apresentado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E visualizado os dados do titular já preenchidos para liberação de crédito
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E realizo upload dos documentos via base que serão encaminhados para formalização
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em Liberacao que os dados foram inseridos para a proposta
	E valido na base em DocumentosEnviados_Safedoc, CheckList_Propostas que os dados foram inseridos para a proposta
	E valido na base em PropostasConcessionarias que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto            | plano                          | seguro     | prazo     | codBanco | tipoEfetivacao | formaEnvio | concessionaria | regiao | numeroCliente | dvNumeroCliente | lote |
		| 12345678909 | 1800          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | SEM SEGURO | Selecione | 0104     | Loja           | Loja       | ENEL           | 2003   | 192219        | 6               | 99   |
		| 12345678909 | 2000          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | COMPLETO   | Selecione | 0104     | Loja           | Loja       | ENEL           | 2005   | 172217        | 5               | 99   |
		| 12345678909 | 1500          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | SEM SEGURO | Selecione | 0104     | Loja           | Loja       | ENEL           | CELG   | 202220        | 0               | 99   |

Esquema do Cenário: Efetivar Proposta CP Fatura com plano <plano>, COM PRAZO SELECIONADO, e seguro <seguro> com tipo de efetivação <tipoEfet> e contrato <tipoContrato>
	Dado que o nome do cenário é 'Efetivar Proposta CP Fatura com plano <plano>, SEM PRAZO SELECIONADO, e seguro <seguro> com tipo de efetivação <tipoEfet> e contrato <tipoContrato>'
	E valido o apontamento da base Sicred e Parceiros para o ambiente regressivo para os dados: CPF: <cpfcgc>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E em Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>, Concessionária: <concessionaria>, Região: <regiao>, Número do Cliente: <numeroCliente>, DV do Cliente: <dvNumeroCliente> e Lote: <lote>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E o resultado da simulação é apresentado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E visualizado os dados do titular já preenchidos para liberação de crédito
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E realizo upload dos documentos via base que serão encaminhados para formalização
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em Liberacao que os dados foram inseridos para a proposta
	E valido na base em DocumentosEnviados_Safedoc, CheckList_Propostas que os dados foram inseridos para a proposta
	E valido na base em PropostasConcessionarias que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto            | plano                          | seguro     | prazo | codBanco | tipoEfetivacao | formaEnvio | concessionaria | regiao | numeroCliente | dvNumeroCliente | lote |
		| 12345678909 | 1800          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | SEM SEGURO | 4     | 0104     | Loja           | Loja       | ENEL           | 2003   | 192219        | 6               | 99   |
		| 12345678909 | 2000          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | COMPLETO   | 8     | 0104     | Loja           | Loja       | ENEL           | 2005   | 172217        | 5               | 99   |
		| 12345678909 | 1500          | 905134  | 5134 | CCB          | 001038 - CP FATURA | 3848 - EPFATURA PADRAO REGULAR | SEM SEGURO | 12    | 0104     | Loja           | Loja       | ENEL           | CELG   | 202220        | 0               | 99   |

Esquema do Cenário: Efetivar uma nova simulação EP CARNÊ alterando o prazo na Etapa 2 - Pré-Análise
	Dado que o nome do cenário é 'Efetivar uma nova simulação EP CARNÊ alterando o prazo na Etapa 2 - Pré-Análise'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do prazo
	E na modal de nova simulação clico em simular
	E na modal de nova simulação visualizo o prazo gerado corretamente conforme simulação anterior
	E seleciono na modal de Nova Simulação Gravar
	E informo os dados de referências bancárias para o banco: <codBanco>
	E na sessão proposta cadastrada da etapa '2 - Pré Análise', visualizo o prazo atualizado corretamente
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em FormaEnvio_Propostas os dados persistidos corretamente

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo | codBanco | tipoEfetivacao | formaEnvio |
		| 86564643705 | 1000          | 905154  | 5154 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | 15    | 0001     | Loja           | Email      |

Esquema do Cenário: Efetivar uma nova simulação EP Cheque alterando o plano na Etapa 2 - Pré-Análise
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque alterando o plano na Etapa 2 - Pré-Análise'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do plano '9886 - CARTA NA MANGA TX 11 96 NOVO E REFIN'
	E seleciono a data de Primeiro Vencimento
	E na modal de nova simulação clico em simular
	E na sessão proposta cadastrada da etapa '2 - Pré Análise', visualizo o plano atualizado corretamente
	E seleciono na modal de Nova Simulação Gravar
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em Cheques que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                  | seguro   | prazo | codBanco | tipoEfetivacao |
		| 68786861050 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 12    | 0001     | Loja           |

Esquema do Cenário: Efetivar uma nova simulação EP CARNÊ alterando o produto na Etapa 2 - Pré-Análise
	Dado que o nome do cenário é 'Efetivar uma nova simulação EP CARNÊ alterando o produto na Etapa 2 - Pré-Análise'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do produto '001035	DEB CC PUBLICO'
	E na modal de Nova Simulação realizo a alteração do plano '3577 - EP DÉBITO'
	E seleciono a data de Primeiro Vencimento
	E na modal de nova simulação clico em simular
	E na modal de nova simulação visualizo o produto gerado corretamente conforme simulação anterior
	E seleciono na modal de Nova Simulação Gravar
	E informo os dados de referências bancárias para o banco: <codBanco>
	E na sessão proposta cadastrada da etapa '2 - Pré Análise', visualizo o produto atualizado corretamente
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	Então a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em PE_Contratoscdc e PE_Parcelas que os dados foram inseridos para o contrato

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo | codBanco | tipoEfetivacao | formaEnvio |
		| 73383795306 | 1000          | 905154  | 5154 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | 10    | 0104     | Loja           | Email      |

Esquema do Cenário: Efetivar uma nova simulação EP CARNE REFIN alterando o produto na Etapa 2 - Pré-Análise
	Dado que o nome do cenário é 'Efetivar uma nova simulação EP CARNE REFIN alterando o produto na Etapa 2 - Pré-Análise'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E informo um cliente apto para refinanciamento para o produto: <produto>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto Refin: <produtoRefin>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E na sessão proposta cadastrada seleciono Refinanciamento
	E o sistema exibe uma modal para a seleção das renegociações do contrato, com número de parcelas, emissão, vencimento, valor financiado, valor parcelas
	E confirmo a seleção de todas parcelas
	E na modal deve constar os seguintes somatórios Total Renegociar, Parcelas Renegociar, Contratos Renegociar
	E confirmo em selecionar renegociações
	E na sessão proposta cadastrada valido os valores: Valor Operação, Valor a Liberar, Parcela, Valor a Liquidar
	E realizo Refresh/Reaload da página atual
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do produto '001035	DEB CC PUBLICO'
	E na modal de Nova Simulação realizo a alteração do plano '3663 - EP DÉBITO'
	E na modal de Nova Simulacao informo um novo prazo <prazoNovaSim>
	E seleciono a data de Primeiro Vencimento
	E na modal de nova simulação clico em simular
	E na modal de nova simulação visualizo o produto gerado corretamente conforme simulação anterior
	E seleciono na modal de Nova Simulação Gravar
	E confirmo a mensagem 'Produto alterado, refinanciamento removido com sucesso!'
	E na sessão proposta cadastrada da etapa '2 - Pré Análise', visualizo o produto atualizado corretamente
	E seleciono para gravar a pré análise  
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	Então a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'
	#E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	#E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	#E valido na base em PE_Contratoscdc e PE_Parcelas que os dados foram inseridos para o contrato

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | produtoRefin            | plano                           | seguro     | prazo | codBanco | tipoEfetivacao | formaEnvio | prazoNovaSim |
		| 02740365683 | 5000          | 905077  | 5084 | CCB          | 001028 - CPCARNEC | 001025 - CPCARNEC REFIN | 9615 - CPCARNEC 05 A 24X TX1625 | SEM SEGURO | 15    | 0341     | Loja           | Correios   | 12           |

Esquema do Cenário: Efetivar uma nova simulação EP CARNÊ alterando o seguro Completo para Básico na Etapa 2 - Pré-Análise
	Dado que o nome do cenário é 'Efetivar uma nova simulação EP CARNÊ alterando o seguro Completo para Básico na Etapa 2 - Pré-Análise'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do seguro para: <seguroAlterado>
	E na modal de nova simulação clico em simular
	E seleciono na modal de Nova Simulação Gravar
	E informo os dados de referências bancárias para o banco: <codBanco>
	E na sessão proposta cadastrada da etapa '2 - Pré Análise', visualizo o seguro atualizado corretamente
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em FormaEnvio_Propostas os dados persistidos corretamente

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo | codBanco | tipoEfetivacao | formaEnvio | seguroAlterado |
		| 86564643705 | 1000          | 905154  | 5154 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | 15    | 0001     | Loja           | Email      | BÁSICO         |

Esquema do Cenário: Efetivar uma nova simulação EP CARNÊ alterando o prazo na Etapa 3 - Inclusão de Proposta
	Dado que o nome do cenário é 'Efetivar uma nova simulação EP CARNÊ alterando o prazo na Etapa 3 - Inclusão de Proposta'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do prazo
	E na modal de nova simulação clico em simular
	E na modal de nova simulação visualizo o prazo gerado corretamente conforme simulação anterior
	E confirmo na base a situação da Proposta em '21'
	E seleciono na modal de Nova Simulação Gravar
	E na sessão proposta cadastrada da etapa '3 - Inclusão de Proposta', visualizo o prazo atualizado corretamente
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E seleciono a forma de envio: <formaEnvio>
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em FormaEnvio_Propostas os dados persistidos corretamente

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo | codBanco | tipoEfetivacao | formaEnvio |
		| 86564643705 | 1000          | 905154  | 5154 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | 10    | 0001     | Loja           | Email      |

Esquema do Cenário: Efetivar uma nova simulação EP Cheque alterando o plano na Etapa 3 - Inclusão de Proposta
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque alterando o plano na Etapa 3 - Inclusão de Proposta'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do plano '9886 - CARTA NA MANGA TX 11 96 NOVO E REFIN'
	E seleciono a data de Primeiro Vencimento
	E na modal de nova simulação clico em simular
	E na modal de nova simulação visualizo o plano gerado corretamente conforme simulação anterior
	E confirmo na base a situação da Proposta em '21'
	E seleciono na modal de Nova Simulação Gravar
	E na sessão proposta cadastrada da etapa '3 - Inclusão de Proposta', visualizo o plano atualizado corretamente
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em Cheques que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                  | seguro   | prazo | codBanco | tipoEfetivacao |
		| 68786861050 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 12    | 0001     | Loja           |

Esquema do Cenário: Efetivar uma nova simulação EP Cheque alterando o seguro na Etapa 3 - Inclusão de Proposta
	Dado que o nome do cenário é 'Efetivar Proposta EP Cheque alterando o seguro na Etapa 3 - Inclusão de Proposta'
	E que efetuo o login no sistema
	E visualizo o usuário logado na página inicial
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do seguro para: <seguroAlterado>
	E na modal de nova simulação clico em simular
	E confirmo na base a situação da Proposta em '21'
	E seleciono na modal de Nova Simulação Gravar
	E na sessão proposta cadastrada da etapa '3 - Inclusão de Proposta', visualizo o seguro atualizado corretamente
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em Contrato e Parcelas que os dados foram inseridos para o contrato
	E valido na base em Cheques que os dados foram inseridos para a proposta

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                                  | seguro   | prazo | codBanco | tipoEfetivacao | seguroAlterado |
		| 68786861050 | 1000          | 905154  | 5154 | CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 12    | 0001     | Loja           | SEM SEGURO     |

Esquema do Cenário: Efetivar uma nova simulação EP CARNÊ alterando o produto na Etapa 3 - Inclusão de Proposta
	Dado que o nome do cenário é 'Efetivar uma nova simulação EP CARNÊ alterando o produto na 3 - Inclusão de Proposta'
	E que efetuo o login no sistema
	E seleciono inclusão de nova proposta
	E o usuário deve ser redirecionado para etapa '1 - Simulação'
	E cadastro os dados iniciais do cliente para o CPF: <cpfcgc>
	E na sessão Dados da Operação, informo o Valor Operação: <valorOperacao>, Lojista: <lojista>, Loja: <loja>, Tipo de Contrato: <tipoContrato>, Produto: <produto>, Plano: <plano>, Seguro: <seguro>, Prazo: <prazo>
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico em nova simulação
	E na modal de Nova Simulação realizo a alteração do produto '001035	DEB CC PUBLICO'
	E na modal de Nova Simulação realizo a alteração do plano '3577 - EP DÉBITO'
	E seleciono a data de Primeiro Vencimento
	E na modal de nova simulação clico em simular
	E na modal de nova simulação visualizo o produto gerado corretamente conforme simulação anterior
	E confirmo na base a situação da Proposta em '21'
	E seleciono na modal de Nova Simulação Gravar
	E na sessão proposta cadastrada da etapa '3 - Inclusão de Proposta', visualizo o produto atualizado corretamente
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	Então a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'
	E valido na base em Clientes, Complementos que os dados foram inseridos para o cliente
	E valido na base em PropostaCredito, PropostasOperacoes, PropostasTipoEfetivacao que os dados foram inseridos para a proposta
	E valido na base em PE_Contratoscdc e PE_Parcelas que os dados foram inseridos para o contrato

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto           | plano                           | seguro   | prazo | codBanco | tipoEfetivacao | formaEnvio |
		| 73383795306 | 1000          | 905154  | 5154 | CCB          | 001028 - CPCARNEC | 9615 - CPCARNEC 05 A 24X TX1625 | COMPLETO | 10    | 0104     | Loja           | Email      |