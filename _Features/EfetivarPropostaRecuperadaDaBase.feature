﻿# language: pt-br
Funcionalidade: EfetivarPropostaRecuperadaDaBase

Esquema do Cenário: : Efetivar Proposta Recuperada Da Base na Etapa 01- SIMULAÇÃO e Situação 01 - PROPOSTA EM SIMULAÇÃO Produto 001035 - DEB CC PUBLICO
	Dado que efetuo o login no sistema
	E acesso Consultar Proposta Existente
	E informo o número de uma proposta com a situação ' 01 - PROPOSTA EM SIMULAÇÃO', produto '001035', etapa '01- SIMULAÇÃO' e data de vencimento maior que a da emissão atual
	E seleciono consultar
	E acesso a proposta pesquisada
	E confirmo a mensagem 'Esta proposta será atualizada para a data de movimento atual.'
	E confirmo a mensagem 'O Produto selecionado é débito em conta, seguem abaixo os bancos com convênios ativos.'
	E informo um valor de operação abaixo do valor máximo financiado para família Débito
	E clico em simular
	E o número da proposta é gerado
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E o resultado da simulação é apresentado
	E seleciono para gravar a simulação
	E clico para avançar a simulação
	E o usuário deve ser redirecionado para etapa '2 - Pré Análise'
	E a proposta deve estar na situação '01 - PROPOSTA EM SIMULAÇÃO'
	E informo os dados complementares do cliente
	#E informo os dados de referências bancárias para o banco: <codBanco>
	E informo as referências bancárias
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'A proposta será efetivada automaticamente após confirmação da autorização de Débito em Conta pelo cliente!'
	Então a proposta deve estar na situação '46 - Pendente Autorização Débito em Conta'

	Exemplos:
		| cpfcgc      | valorOperacao | lojista | loja | tipoContrato | produto                 | plano            | seguro   | prazo | codBanco | tipoEfetivacao |
		| 52282323033 | 1000          | 905140  | 5144 | CCB          | 001035 - DEB CC PUBLICO | 3663 - EP DÉBITO | COMPLETO | 10    | 0104     | Loja           |

Esquema do Cenário: Efetivar Proposta Recuperada Da Base na Etapa 02 - Pré Análise e Situação 10 - PENDENTE Produto 000712 - EPCHEQUE
	Dado que efetuo o login no sistema
	E acesso Consultar Proposta Existente
	E informo o número de uma proposta com a situação <situacaoProposta>, produto <produto>, etapa <etapa> e data de vencimento maior que a da emissão atual
	E seleciono consultar
	E seleciono Prosseguir com a proposta em Aberto
	E o número da proposta é gerado
	E informo os dados complementares do cliente
	E informo os dados de referências bancárias para o banco: <codBanco>
	E seleciono para gravar a pré análise
	E a proposta é atualizada via base para a situação '31' na etapa de '2 - Pré Análise'
	E o usuário deve ser redirecionado para etapa '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '31 - COMPLEMENTAR CADASTRO'
	E cadastro os dados de identificacao do cliente
	E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	Exemplos:
		| tipoContrato | produto           | plano                                  | seguro   | codBanco | tipoEfetivacao | etapa | situacaoProposta |
		| CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 0104     | Loja           | 02    | 10 - PENDENTE    |

Esquema do Cenário: Efetivar Proposta Recuperada Da Base na Etapa 03 e Situação 21 - PRONTO PARA CONTRATO  Produto 000712 - EPCHEQUE
	Dado que efetuo o login no sistema
	E acesso Consultar Proposta Existente
	E informo o número de uma proposta com a situação <situacaoProposta>, produto <produto>, etapa <etapa> e data de vencimento maior que a da emissão atual
	E seleciono consultar
	E seleciono Prosseguir com a proposta em Aberto
	E confirmo a mensagem 'Esta proposta será atualizada para a data de movimento atual.'
	E o número da proposta é gerado
	#E cadastro os dados de identificacao do cliente
	#E seleciono o tipo de telefone da residência
	#E informo os dados profissionais
	#E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	Exemplos:
		| tipoContrato | produto           | plano                                  | seguro   | codBanco | tipoEfetivacao | etapa | situacaoProposta          |
		| CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 0104     | Loja           | 03    | 21 - PRONTO PARA CONTRATO |

Esquema do Cenário: Efetivar Proposta Recuperada Da Base na Etapa 03 e Situação 31 - COMPLEMENTAR CADASTRO com  Produto 000712 - EPCHEQUE
	Dado que efetuo o login no sistema
	E acesso Consultar Proposta Existente
	E informo o número de uma proposta com a situação <situacaoProposta>, produto <produto>, etapa <etapa> e data de vencimento maior que a da emissão atual
	E seleciono consultar
	E seleciono Prosseguir com a proposta em Aberto
	E confirmo a mensagem 'Esta proposta será atualizada para a data de movimento atual.'
	E o número da proposta é gerado
	#E cadastro os dados de identificacao do cliente
	#E seleciono o tipo de telefone da residência
	E informo os dados profissionais
	E informo as referências profissionais
	E informo as referências bancárias
	E seleciono para gravar a inclusão da proposta
	E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	E clico para avançar a inclusão da proposta
	E seleciono um tipo de efetivação: <tipoEfetivacao>
	E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	E informo um cheque pré-datado completando o restante
	E clico em Pré-Efetivar
	E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	Exemplos:
		| tipoContrato | produto           | plano                                  | seguro   | codBanco | tipoEfetivacao | etapa | situacaoProposta           |
		| CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 0104     | Loja           | 03    | 31 - COMPLEMENTAR CADASTRO |

Esquema do Cenário: Efetivar Proposta Recuperada Da Base na Etapa 04 e Situação 40 - Pré-Efetivado com  Produto 000712 - EPCHEQUE
	Dado que efetuo o login no sistema
	E acesso Consultar Proposta Existente
	E informo o número de uma proposta com a situação <situacaoProposta>, produto <produto>, etapa <etapa> e data de vencimento maior que a da emissão atual
	E seleciono consultar
	E seleciono Prosseguir com a proposta em Aberto
	#E confirmo a mensagem 'Esta proposta será atualizada para a data de movimento atual.'
	E o número da proposta é gerado
	#E cadastro os dados de identificacao do cliente
	#E seleciono o tipo de telefone da residência
	#E informo os dados profissionais
	#E informo as referências profissionais
	#E informo as referências bancárias
	#E seleciono para gravar a inclusão da proposta
	#E a proposta é atualizada via base para a situação '21' na etapa de '3 - Inclusão de Proposta'
	#E a proposta deve estar na situação '21 - PRONTO PARA CONTRATO'
	#E clico para avançar a inclusão da proposta
	#E seleciono um tipo de efetivação: <tipoEfetivacao>
	#E o usuário deve ser redirecionado para etapa '4 - Pré-Efetivação'
	#E informo um cheque pré-datado completando o restante
	#E clico em Pré-Efetivar
	#E confirmo a mensagem 'Proposta pré-efetivada com sucesso!'
	E a proposta deve estar na situação '40 - Pré-Efetivado'
	E o usuário deve ser redirecionado para etapa '5 - Efetivação'
	E marco os documentos obrigatórios
	Quando clico em Efetivar
	E confirmo a mensagem 'Proposta efetivada com sucesso!'
	Então a proposta deve estar na situação '20 - CONTRATO EFETIVADO'
	E o usuário deve ser redirecionado para etapa '6 - Impressão Contrato'

	Exemplos:
		| tipoContrato | produto           | plano                                  | seguro   | codBanco | tipoEfetivacao | etapa | situacaoProposta  |
		| CCB          | 000712 - EPCHEQUE | 9848 - EPCHEQUE CLIENTE N 9X A 36X CCB | COMPLETO | 0104     | Loja           | 04    | 40 - Pré-Efetivado |