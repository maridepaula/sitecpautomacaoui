﻿namespace SpecFlowTeste.Core.FixedValues
{
    public enum TipoOptante
    {
        None = 0,
        Ativo = 1,
        Inativo = 2,
        NaoOptante = 3
    }
}
