﻿using System.Text;

namespace System
{
    public static class ExceptionExtension
    {
        public static string ToHtmlString(this Exception ex)
        {
            var retorno = new StringBuilder();

            retorno.Append("Message:<br>");
            retorno.Append(ex.Message.Replace("\n", "<br>"));
            retorno.Append("<br><br>");

            if (ex.InnerException != null)
            {
                retorno.Append("Innerexception:<br>");
                retorno.Append(ex.InnerException.ToHtmlString());
                retorno.Append("<hr><br><br>");
            }

            retorno.Append("Source:<br>");
            retorno.Append(ex.Source);
            retorno.Append("<br><br>");

            retorno.Append("Stacktrace:<br>");
            retorno.Append(ex.StackTrace);
            retorno.Append("<br><br>");

            return retorno.ToString();
        }

        public static string ToFormatedString(this Exception ex)
        {
            var retorno = new StringBuilder();

            retorno.AppendLine("Message:");
            retorno.AppendLine(ex.Message);
            retorno.AppendLine();

            if (ex.InnerException != null)
            {
                retorno.AppendLine("Innerexception:");
                retorno.AppendLine(ex.InnerException.ToFormatedString());
                retorno.AppendLine();
            }

            retorno.AppendLine("Source:");
            retorno.AppendLine(ex.Source);
            retorno.AppendLine();

            retorno.AppendLine("Stacktrace:");
            retorno.AppendLine(ex.StackTrace);
            retorno.AppendLine();

            return retorno.ToString();
        }
    }
}
