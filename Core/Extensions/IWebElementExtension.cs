﻿using Portosis.UITests.Utils;
using System.Threading;

namespace OpenQA.Selenium
{
    public static class IWebElementExtension
    {

        public static void ClickSendKeysOnChange<T>(this IWebElement element, T value, bool executeJS = true)
        {
            var e = element;
            var count = 0;
            if (executeJS)
                Configs.Driver.Scripts().ExecuteScript("$(arguments[0]).change();", element);
            do
            {
                try
                {
                    e.Click();

                    if(value.GetType() == typeof(decimal) )
                        e.SendKeys(  string.Format("{0:#.00}", value) );
                    else
                        e.SendKeys(value.ToString());
                    e.SendKeys(Keys.Tab);
                    count = 7;
                }
                catch
                {
                    Thread.Sleep(500);
                    count++;
                }
            } while (count <= 6);
        }


        public static IJavaScriptExecutor Scripts(this IWebDriver driver)
        {
            return (IJavaScriptExecutor)driver;
        }

        public static void ClickSendKeys<T>(this IWebElement element, T valor)
        {
            // var wait = new WebDriverWait(Configs.Driver,Configs.TimeoutElementDefault);
            // wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));

            Thread.Sleep(3000);
            element.Click();
            // Thread.Sleep(1000);
            element.SendKeys(valor.ToString());
            // Thread.Sleep(500);
            // element.SendKeys(keySequence);
            // for (var i = 0; i < keySequence.Length; i++)
            // {
            //     element.SendKeys(keySequence[i].ToString());
            // }
        }
    }
}