﻿using System.Reflection;

namespace System
{
    public static class ObjectExtension
    {
        public static bool CompareObject(this object obj1, object obj2)
        {
            foreach (PropertyInfo property in obj1.GetType().GetProperties())
            {
                object value1 = property.GetValue(obj1, null);
                object value2 = property.GetValue(obj2, null);
                if (value1 == null && value2 != null || value1 != null && !value1.Equals(value2))//((value1 == null && value2 != null)||(value1 == null && !value1.Equals(value2)))
                {
                    Console.WriteLine("Valor objeto 01:#"+ value1 + "#- " + property);
                    Console.WriteLine("Valor objeto 01:#"+ value2 + "#- " + property);
                    return false;
                }
            }
            return true;
        }


        public static bool ObjectIsEmpty(this object obj1)
        {
            if (obj1 != null)
            {
                foreach (PropertyInfo property in obj1.GetType().GetProperties())
                {
                    object value1 = property.GetValue(obj1, null);

                    if (!value1.Equals(null))
                    {
                        return false;
                    }
                }
            }

            return true;
        }


    }
}
