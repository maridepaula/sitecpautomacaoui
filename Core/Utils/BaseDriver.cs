﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace UiTestSiteCP.Core.Utils
{
    public class BaseDriver
    {
        public readonly IWebDriver driver;
        public readonly WebDriverWait Wait;
        public readonly Actions actions;   
        IJavaScriptExecutor js;

        public BaseDriver(IWebDriver driver)
        {
            this.driver = driver;
            this.js = driver as IJavaScriptExecutor;
            this.actions = new Actions(driver);

        }
    }
}
