﻿using System;
using OpenQA.Selenium;
using UiTestSiteCP.DataBase;
using static UiTestSiteCP._Storage.Proposta;
using Portosis.UITests.Utils;
using System.Threading;
using System.Linq;

namespace UiTestSiteCP.Core.Utils
{
    public class BasePagePO : BasePageElements
    {
        public BasePagePO(IWebDriver driver) : base(driver)
        { }
        public IWebElement rodapePagina => FindElement(By.ClassName("footer"));
        public IWebElement cabecalho => FindElement(By.ClassName("header"));

        //public static Cliente PropCli = ObtemDadosDefaultCliente(new ClienteOptions
        //{
        //    Nome = "Mari"
        //});

        public static Cliente PropCli; //= ObtemDadosDefaultCliente();
                                       //   public static Cliente PropCliDBC = ObtemDadosDefaultClienteDebitoEmConta();
        public static PropostaPCPO PropPCPO;//= ObtemDadosDefaultPropostaPCPO();
        public static PropostaGarantia PropGar;// = ObtemDadosDefaultPropostaGarantia();
        public static PropostaConcessionaria PropConcess;// = new PropostaConcessionaria();
        public static Liberacao Lib;// = ObtemDadosDefaultLiberacao();
        public static Cheque PropCheq;// = new Cheque();
        public static Contrato PropContr;//= new Contrato();
        public static Produto PropProd;//= new Produto();
        public static FormaEnvio FormaEnvioCarne;// = new FormaEnvio();
        
        public DateTime ObterDataDeVencimentoValidaPorPlano(int CodigoPlano)
        {
            var carenciaMinima = Repositories.PlanosRepositorySicred.ObterCarenciaMinimaPlano(CodigoPlano);
            var dataMovimento = Convert.ToDateTime(Configs.DataMovimento);
            var dataValida = dataMovimento.AddDays(Convert.ToDouble(carenciaMinima));
            int prazo = 1;
            do
            {
                var isFeriado = Repositories.InicialRepositorySicred.BuscaFeriado(dataValida);
                if (!((dataValida.DayOfWeek.Equals(DayOfWeek.Saturday)) || (dataValida.DayOfWeek.Equals(DayOfWeek.Sunday)) || (isFeriado)))
                {
                    return dataValida;
                }
                else {                   
                    dataValida = dataValida.AddDays(prazo);
                    prazo++;
                }
            }while ( prazo < 10);

            throw new Exception("DATA INVÁLIDA");
        }

        public bool ValidarValorEntreMaxEMin(decimal value, decimal min, decimal max)
        {
            return (value >= min && value <= max);
        }

        public void PageDownRodape()
        {
            MoveToElement(rodapePagina);
        }

        public void PageUpCabecalho()
        {
            MoveToElement(cabecalho);
        }

        public void WaitChangeOriginalURL()
        {
            driver.Close();
            Thread.Sleep(500);
            //WebDriverWait wait = new WebDriverWait(Configs.Driver, TimeSpan.FromSeconds(30));
            //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.UrlContains(Configs.UrlRoot));
            driver.SwitchTo().Window(driver.WindowHandles.LastOrDefault());
        }


    }
}
