﻿using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using Portosis.UITests.Utils;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.ObjectModel;

namespace UiTestSiteCP.Core.Utils
{
    public class BasePageElements : BaseDriver
    {
        public BasePageElements(IWebDriver driver) : base(driver)
        { }

        public void MoveToElement(IWebElement element)
        {
            var actions = new Actions(driver); //jeh inserido
            actions.MoveToElement(element);
            actions.Perform();
            Thread.Sleep(1000);
        }
        public IWebElement FindElement(By by)
        {
        var wait = new WebDriverWait(Configs.Driver, Configs.TimeoutElementDefault);

            if (by != null)
            {
                WaitForElementNotVisible();
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
                try
                {
                    wait = new WebDriverWait(Configs.Driver, TimeSpan.FromSeconds(20));
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(by));
                }
                catch (Exception ex) { Console.WriteLine(ex); }
                return driver.FindElement(by);
            }

            return null;
        }


        public IWebElement WaitElementBeVisibleText(By by, string text)
        {
            WaitForElementNotVisible();
            var wait = new WebDriverWait(Configs.Driver, Configs.TimeoutElementDefault);
            if (by != null)
            {
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.TextToBePresentInElementLocated(by, text));
            }
            return null;

        }


        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            var wait = new WebDriverWait(Configs.Driver, Configs.TimeoutElementDefault);

            if (by != null)
            {
                WaitForElementNotVisible();
                WaitForElementCalculandoNotVisible();
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
                try
                {
                    wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(by));
                }
                catch (Exception ex) { Console.WriteLine(ex); }
                return driver.FindElements(by);
            }

            return null;
        }


        public void WaitForElementNotVisible()
        {
            var wait = new WebDriverWait(Configs.Driver, Configs.TimeoutLoaderDefault);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div/p[.='Carregando...']")));
        }

        public void WaitForElementCalculandoNotVisible()
        {
            var wait = new WebDriverWait(Configs.Driver, Configs.TimeoutLoaderDefault);
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//div/p[.='Calculando...']")));
        }       
    }
}
