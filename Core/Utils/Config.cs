﻿using Framework.Tests.Steps;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using UiTestSiteCP.DataBase;
using UiTestSiteCP.Utils;

namespace Portosis.UITests.Utils
{
    public static class Configs
    {
        public const int Timeout = 60;
        public static string CodDBC { get; set; } = "001035";
        public static string CodDBCRefin { get; set; } = "001040";
        public static string CodChequeRefin { get; set; } = "000720";
        public static string CodCarneRefin { get; set; } = "001025";
        public static string CodMoto { get; set; } = "001029";
        public static string CodCPFatura { get; set; } = "001038";
        public static string OutputHtml => $"{DateTime.Now:yyyyMMddHHmmss}.PortoSis.QA.html";
        public static string OutputPdf => $"{DateTime.Now:yyyyMMddHHmmss}.PortoSis.QA.pdf";
        public static string OutputJson => $"{DateTime.Now:yyyyMMddHHmmss}.PortoSis.QA.json";
        public static string OutputFolderExtent => $"{DateTime.Now:yyyyMMddHHmmss}";
        public static string DataMovimento { get; set; }
        public static decimal PercentualQuebraPMT { get; set; } = 0.00004m;        

        public static ReportType ReportType = ReportType.PDF;

        public const int ImgWidth = 400;
        public const int ImgHeight = 300;

        public static IWebDriver Driver;
        public static WebDriverWait Wait;
        public readonly static TimeSpan TimeoutElementDefault = TimeSpan.FromSeconds(20);
        public readonly static TimeSpan TimeoutLoaderDefaulToVisibility = TimeSpan.FromSeconds(1);
        public readonly static TimeSpan TimeoutLoaderDefault = TimeSpan.FromSeconds(40); //30
        public readonly static TimeSpan TimeoutNeurotech = TimeSpan.FromSeconds(20);

        /************************************************/
        /*       AMBIENTE TEST04 Sicred/Portosis        */
        /************************************************/
        //public const string UrlRoot = "http://mtz-test04/SiteCP/";
        //public static string ConnectionStringSicred = "Server=10.60.75.35;Database=db_sicred_test04; User Id=portosis; Password=ps&portosis;connect timeout=30;";
        //public static string ConnectionStringPortosis = "Server=10.60.75.35;Database=db_portosis_test04; User Id=portosis; Password=ps&portosis;connect timeout=30;";


        /************************************************/
        /*       AMBIENTE REGRESSIVO Sicred/Portosis    */
        /************************************************/
        public const string UrlRoot = "http://mtz-test03/SiteCP/";
       // public const string UrlRoot = "http://localhost:53426/";
        
        public static string ConnectionStringSicred = "Server=10.60.75.35;Database=db_sicred_regressivo; User Id=portosis; Password=ps&portosis;connect timeout=30;";
        public static string ConnectionStringPortosis = "Server=10.60.75.35;Database=db_portosis_regressivo; User Id=portosis; Password=ps&portosis;connect timeout=30;";

        /************************************************/
        /*    AMBIENTE REGRESSIVO ENEL  MOCK/PARCEIROS  */
        /************************************************/
        public static string AmbRegressivoSiteCPENEL = "http://api-gate-enel-regressivo.psd.portocred.com.br";
        public static string AmbRegressivoMock = "http://10.60.75.43/enelmock/api/";
        public static string ConnectionStringMockRegressivoMySql = "Server=10.60.75.43;Database=mock-enel; Uid=app_mock_enel; Pwd=WxovqEwbM9U78oU26h24;";
        public static string ConnectionStringParceirosMySql = "Server=localhost;Port=6665;Database=db-epfatura-parceiro-enel-regressivo;Uid=app_epfatura_parceiro_enel;Pwd=i5ipOAZQxRWg3CJVWaWv;";
        //"Server=db-epfatura01-parceiro-hom.psd.portocred.com.br;Port=3306;Database=db-epfatura-parceiro-enel-regressivo;Uid=app_epfatura_parceiro_enel;Pwd=i5ipOAZQxRWg3CJVWaWv;";


        /************************************************/
        /*                USUÁRIO SITE CP               */
        /************************************************/
        //public static Usuarios AcessoUserAutomacao = new Usuarios("AUTOMACAO", "123456");
        // public static Usuarios AcessoUserAutomacao = new Usuarios("JESSICACWI", "123456");
        //  public static Usuarios AcessoUserAutomacao = new Usuarios("DATUMG", "123456"); 
        public static Usuarios AcessoUserAutomacao = new Usuarios("CWIMARCIO", "123456");

        /************************************************/
        /*       USUÁRIO SITE CP - ENEL - SAFE DOC      */
        /************************************************/
        public static Usuarios AcessoUserSafeDoc = new Usuarios("13171352370", "@ce55o@teste");
    }
}
