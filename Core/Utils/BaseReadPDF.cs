﻿using java.lang;
using java.net;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using System.IO;

namespace SpecFlowTeste.Core.Utils
{
    public class BaseReadPDF
    {
        public string extrairTextoPdf(string path)
        {
            PDDocument pdf = null;
            try
            {
                pdf = PDDocument.load(new URL(path));
                PDFTextStripper stripper = new PDFTextStripper();
                string texto = stripper.getText(pdf);
                return texto;
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            finally
            {
                if (pdf != null)
                    try
                    {
                        pdf.close();
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
            }
        }

    }
}
