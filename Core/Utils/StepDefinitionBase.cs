using System.IO;
using System.Reflection;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using UiTestSiteCP._PageObjects;
using UiTestSiteCP.DataBase;
using System.Collections.Generic;
using System.Linq;
using Portosis.UITests.Utils;
using UiTestSiteCP.Core.Utils;
using static UiTestSiteCP._Storage.Proposta;

//using UiSiteCP._PageObjects;


namespace UiTestSiteCP.Utils
{

    [Binding]
    public static class StepDefinitionBase
    {

        public static string Url = $"{Configs.UrlRoot}";



        //public static void LimpaBaseParaTeste()
        //{

        //    var CpfList = new List<string>() { "67127909725", "02740365683", "73897299100", "48596884904", "02615516639", "62950583024",
        //                                       "17661580055", "07668420703", "95954290318", "52282323033", "01236833600", "02379486646" };
        //    //O CLIENTE COMENTADO SER� UTILIZADO NO REFINANCIAMENTO, POR ISSO N�O EST� SENDO APAGADO DA BASE
        //    CpfList.ToList().ForEach(item => Repositories.InicialRepositorySicred.LimparDadosBase(item + "1"));

        //}


        [BeforeScenario]
        public static void BeforeScenario()
        {
            // Repositories.InicialRepositorySicred.LimparDadosBase(PropostaEpCheque.ObtemDadosDefault().CGCCPF+"1");          
            Repositories.InicialRepositorySicred.CriarUsuario(Configs.AcessoUserAutomacao.User);
            //LimpaBaseParaTeste();
            //ChromeOptions options = new ChromeOptions();
            //options.AddArguments("--disable-print-preview");
            Configs.Driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            Configs.Driver.Navigate().GoToUrl(Url);
            Configs.Driver.Manage().Window.Maximize();
            Configs.DataMovimento = Repositories.InicialRepositorySicred.ObterDataMovimento();

            BasePagePO.PropCli = ObtemDadosDefaultCliente();
            BasePagePO.PropPCPO = ObtemDadosDefaultPropostaPCPO();
            BasePagePO.PropGar = ObtemDadosDefaultPropostaGarantia();
            BasePagePO.PropConcess = new PropostaConcessionaria();
            BasePagePO.Lib = ObtemDadosDefaultLiberacao();
            BasePagePO.PropCheq = new Cheque();
            BasePagePO.PropContr = new Contrato();
            BasePagePO.PropProd = new Produto();
            BasePagePO.FormaEnvioCarne = new FormaEnvio();

        }

        [AfterScenario]
        public static void AfterScenario()
        {            
            /*jeh**/
           Repositories.InicialRepositorySicred.LimparDadosBase(BasePagePO.PropPCPO.CGCCPF);

            if(BasePagePO.PropPCPO.Produto == Configs.CodCPFatura)
            {
                new LoginPO(Configs.Driver).LimparDadosBaseMockEnelEParceiros(BasePagePO.PropConcess.cpf,
                                                                              BasePagePO.PropConcess.codigoRegiao,
                                                                              "99");
                                                                             /* BasePagePO.PropConcess.setor*/
            }

            new LoginPO(Configs.Driver).RealizarLogoff();
            Configs.Driver.Quit();
            Configs.Driver.Dispose();

            BasePagePO.PropCli = new Cliente();
            BasePagePO.PropPCPO = new PropostaPCPO();
            BasePagePO.PropGar = new PropostaGarantia();
            BasePagePO.PropConcess = new PropostaConcessionaria();
            BasePagePO.Lib = new Liberacao();
            BasePagePO.PropCheq = new Cheque();
            BasePagePO.PropContr = new Contrato();
            BasePagePO.PropProd = new Produto();
            BasePagePO.FormaEnvioCarne = new FormaEnvio();

        }

    }
}


//// foreach(CpfList, item => Repositories.InicialRepositorySicred.LimparDadosBase(item + "1"));
//// each(CpfList, i => Console.WriteLine(i));
//CpfList.ToList().ForEach(item => Repositories.InicialRepositorySicred.LimparDadosBase(item + "1"));
////foreach (var item in CpfList)
////{
////    Repositories.InicialRepositorySicred.LimparDadosBase(item + "1");
////}