namespace UiTestSiteCP.Utils
{
    public class Usuarios
    {
       public Usuarios(string user, string pass)
        {
            User = user;
            Pass = pass;
        }
        public string User { get; private set; }
        public string Pass { get; private set; }
        
    }
}