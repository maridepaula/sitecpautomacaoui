﻿using System;
using System.IO;
using System.Linq;

namespace Portosis.UITests.Implementacao
{
    public static class AppHelper
    {
        private class FileDate
        {
            public string FileName;
            public DateTime LastWriteTime;
            public FileDate(string fileName, DateTime lastWriteTime)
            {
                FileName = fileName;
                LastWriteTime = lastWriteTime;
            }
        }

        public static string GetFilePath(string exeName)
        {
            var pathWithEnv = @"%USERPROFILE%\AppData\Local\Apps\2.0";
            var filePath = Environment.ExpandEnvironmentVariables(pathWithEnv);
            var files = Directory.GetFiles(filePath, exeName, SearchOption.AllDirectories);

            if (files != null && files.Count() > 0)
            {
                var dates = files.Select(x => new FileDate(x, File.GetLastWriteTime(x)));
                var maxDate = dates.Max(x => x.LastWriteTime);
                return dates.FirstOrDefault(x => x.LastWriteTime == maxDate).FileName;
            }
            else
            {
                throw new FileNotFoundException(exeName);
            }
        }


        public static void CriarDiretorioPadrao()
        {
            var _folder = @"C:\Temp\Evidencia"; //nome do diretorio a ser criado
            try
            {
                if (!Directory.Exists(_folder))
                {
                    Directory.CreateDirectory(_folder);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Arquivo para anexo TestTxtAutomacao não encontrado!!");
            }

        }
   }
}
