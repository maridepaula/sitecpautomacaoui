﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using Framework.Tests.QA.Base;
using Portosis.UITests.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow.Bindings;

namespace Framework.Tests.QA
{
    internal class ExtentQA : ReportQABase
    {
        private static ExtentReports _extentReports;
        private static string _pathReport;

        public static void Init()
        {
            _pathReport = Path.Combine(WorkDir, Configs.OutputFolderExtent);
            var extentHtmlReporter = new ExtentHtmlReporter(_pathReport);
            extentHtmlReporter.Config.EnableTimeline = false;
            extentHtmlReporter.Config.ReportName = "Portosis Test Report";
            extentHtmlReporter.Config.DocumentTitle = extentHtmlReporter.Config.ReportName;
            extentHtmlReporter.Start();

            _extentReports = new ExtentReports();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetReferencedAssemblies());
            var filter = new string[] { "nunit", "appium", "WebDriver", "Dapper", "Specflow", "ExtentReports" };

            var assembliesFiltered = assemblies.Where(x => filter.Any(y => x.Name.ToLower().Contains(y.ToLower())))
                .GroupBy(x => x.Name, x => x, (key, g) => g.FirstOrDefault())
                .OrderBy(x => x.Name);
            foreach (var assembly in assembliesFiltered)
            {
                _extentReports.AddSystemInfo(assembly.Name, $"v{assembly.Version}");
            }

            _extentReports.AddSystemInfo("Selenium", "v3.0.1");
            _extentReports.AttachReporter(extentHtmlReporter);
        }

        public ExtentQA()
        { }

        public ExtentQA(List<FeatureQA> features) : base(features)
        { }

        public void ParserExtent()
        {
            if (_extentReports == null)
                throw new Exception("ExtentReports not initialized");

            foreach (var feature in Features)
            {
                var _feature = _extentReports.CreateTest<Feature>(feature.FeatureInfo.Title, feature.FeatureInfo.Description);
                _feature.AssignCategory(feature.FeatureInfo.Tags);

                foreach (var scenario in feature.Scenarios)
                {
                    var _scenario = _feature.CreateNode<Scenario>(scenario.FormatedTitle);
                    _scenario.AssignCategory(scenario.ScenarioInfo.Tags);

                    foreach (var step in scenario.Steps)
                    {
                        var _step = CreateScenario(_scenario, step.StepInfo.StepDefinitionType, step.FormatedText);

                        if (step.Error != null)
                        {
                            //_step.Error(step.Error.ToFormatedString());
                            _step = _step.Fail(step.Error.Message);
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(step.StepInfo.Table)))
                            CreateScenario(_step, step.StepInfo.StepDefinitionType, step.StepInfo.Table.ToString());

                        //int imgNumber = 0;
                        //foreach (var screenshot in step.ScreenShotsQA)
                        //{
                        //    _step.AddScreenCaptureFromBase64String(screenshot.Screenshot.AsBase64EncodedString);

                        //    var imgFile = Path.Combine(pathReport, $"img_{++imgNumber}.jpg");
                        //    var ms = new MemoryStream(screenshot.Screenshot.AsByteArray);
                        //    var i = Image.FromStream(ms);
                        //    i.Save(imgFile);

                        //    _step.AddScreenCaptureFromPath(imgFile);
                        //}
                        _step.Model.StartTime = step.StartTime;
                        _step.Model.EndTime = step.EndTime;
                    }

                    _scenario.Model.StartTime = scenario.StartTime = scenario.Steps.Min(x => x.StartTime);
                    _scenario.Model.EndTime = scenario.EndTime = scenario.Steps.Max(x => x.EndTime);
                }

                _feature.Model.StartTime = feature.StartTime = feature.Scenarios.Min(x => x.StartTime);
                _feature.Model.EndTime = feature.EndTime = feature.Scenarios.Max(x => x.EndTime);
            }

            _extentReports.Flush();
        }

        private static ExtentTest CreateScenario(ExtentTest extent, StepDefinitionType stepDefinitionType, string text)
        {
            switch (stepDefinitionType)
            {
                case StepDefinitionType.Given:
                    return extent.CreateNode<Given>(text); // cria o nodo para Given

                case StepDefinitionType.Then:
                    return extent.CreateNode<Then>(text); // cria o nodo para Then

                case StepDefinitionType.When:
                    return extent.CreateNode<When>(text); // cria o nodo para When
                default:
                    throw new ArgumentOutOfRangeException(nameof(stepDefinitionType), stepDefinitionType, null);
            }
        }

        public override void WriteReport()
        {
            ParserExtent();
        }
    }
}
