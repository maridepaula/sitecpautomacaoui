﻿using Framework.Tests.QA.Base;
using Portosis.UITests.Utils;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Framework.Tests.QA
{
    internal class HtmlQA : ReportQABase
    {
        private string fileNameLogPath = string.Format(@"{0}\{1}", WorkDir, Configs.OutputHtml);

        public override void WriteReport()
        {
            if (File.Exists(fileNameLogPath))
                File.Delete(fileNameLogPath);

            StringBuilder content = new StringBuilder();

            content.AppendLine("<!DOCTYPE html>");
            content.AppendLine("<html>");
            content.AppendLine("<link rel=\"stylesheet\" type=\"text/css\" href=\"md.css\">");
            content.AppendLine("<body class=\"markdown-body\">");
            content.AppendLine("<h2>Funcionalidades</h2>");

            foreach (var feature in Features)
            {
                content.AppendLine("<details>");
                content.AppendLine($"<summary><strong>{feature.FeatureInfo.Title}</strong></summary>");
                content.AppendLine($"<pre>{feature.FeatureInfo.Description}</pre>");
                content.AppendLine("<br>");

                foreach (var scenario in feature.Scenarios)
                {
                    content.AppendLine("<details>");
                    content.AppendLine($"<summary><strong>Cenário: {scenario.FormatedTitle}</strong></summary>");
                    content.AppendLine("<pre>");

                    foreach (var step in scenario.Steps)
                    {
                        content.AppendLine(step.FormatedText);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(step.StepInfo.Table)))
                            content.AppendLine($"{step.StepInfo.Table}");

                        if (step.ScreenShotsQA.Any())
                        {
                            foreach (var screenshot in step.ScreenShotsQA)
                            {
                                content.AppendLine($"<img src=\"data: image / png; base64, {screenshot.Screenshot.ToString()}\" width=\"{Configs.ImgWidth}\" height=\"{Configs.ImgHeight}\"/><br><br>");
                            }
                        }

                        if (step.Error != null)
                        {
                            content.AppendLine("<xmp>");
                            content.AppendLine(step.Error.ToHtmlString());
                            content.AppendLine("</xmp>");
                        }
                    }
                    content.AppendLine("</pre><hr />");
                    content.AppendLine("</details>");
                }
                content.AppendLine("</pre><hr />");
                content.AppendLine("</details>");
            }
            content.AppendLine("</body></html>");

            WriteContent(content.ToString());
        }

        private void WriteContent(string content)
        {
            using (var writer = new StreamWriter(new MemoryStream(), Encoding.UTF8))
            {
                writer.Write(content);
                writer.Flush();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                File.WriteAllBytes(fileNameLogPath, (writer.BaseStream as MemoryStream).ToArray());
            }
        }
    }
}
