﻿using Newtonsoft.Json;
using System;

namespace Portosis.UITests.QA.Json
{
    [Serializable]
    [JsonObject(Title = "Step")]
    internal class StepJson
    {
        public bool HasError { get; set; }
        public string Text { get; set; }
        public string Table { get; set; }
        public string ImageAsBase64EncodedString { get; set; }
        public string Error { get; set; }
    }
}
