﻿using Framework.Tests.QA.Base;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Portosis.UITests.QA.Json
{

    [Serializable]
    [JsonObject(Title = "ReportQA")]
    internal class ExportQAJson
    {
        [JsonProperty(PropertyName = "Features")]
        internal List<FeatureJson> FeaturesJson { get; set; }

        internal ExportQAJson()
        { }

        internal ExportQAJson(IList<FeatureQA> features)
        {
            FeaturesJson = new List<FeatureJson>();

            foreach (var feature in features)
            {
                var featureJson = new FeatureJson();
                featureJson.Title = feature.FeatureInfo.Title;
                featureJson.Description = feature.FeatureInfo.Description;

                foreach (var scenario in feature.Scenarios)
                {
                    var scenarioJson = new ScenarioJson();
                    scenarioJson.Title = scenario.FormatedTitle;
                    scenarioJson.HasError = scenario.HasError;

                    foreach (var step in scenario.Steps)
                    {
                        var stepJson = new StepJson();
                        stepJson.Text = step.FormatedText;
                        stepJson.HasError = step.HasError;

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(step.StepInfo.Table)))
                            stepJson.Table = step.StepInfo.Table.ToString();

                        foreach (var screenshot in step.ScreenShotsQA)
                        {
                            stepJson.ImageAsBase64EncodedString = screenshot.Screenshot.AsBase64EncodedString;
                        }

                        if (step.Error != null)
                        {
                            stepJson.Error = step.Error.ToFormatedString();
                        }

                        scenarioJson.StepsJson.Add(stepJson);
                    }

                    featureJson.ScenariosJson.Add(scenarioJson);
                }

                FeaturesJson.Add(featureJson);
            }
        }
    }
}
