﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Portosis.UITests.QA.Json
{
    [Serializable]
    [JsonObject(Title = "Feature")]
    internal class FeatureJson
    {
        [JsonProperty(PropertyName = "Scenarios")]
        public List<ScenarioJson> ScenariosJson { get; set; } = new List<ScenarioJson>();
        public string Description { get; set; }
        public string Title { get; set; }
    }
}
