﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Portosis.UITests.QA.Json
{
    [Serializable]
    [JsonObject(Title = "Scenario")]
    internal class ScenarioJson
    {
        [JsonProperty(PropertyName = "Steps")]
        public List<StepJson> StepsJson { get; set; } = new List<StepJson>();
        public bool HasError { get; set; }
        public string Title { get; set; }
    }
}
