﻿using Framework.Tests.QA.Base;
using Newtonsoft.Json;
using Portosis.UITests.QA.Json;
using Portosis.UITests.Utils;
using System.Collections.Generic;

namespace Framework.Tests.QA
{
    internal class JsonQA : ReportQABase
    {
        public JsonQA()
        { }

        public JsonQA(List<FeatureQA> features) : base(features)
        { }

        private string fileNameLogPath = string.Format(@"{0}\{1}", WorkDir, Configs.OutputJson);

        public override void WriteReport()
        {
            var jsonObject = new ExportQAJson(Features);
            var result = JsonConvert.SerializeObject(jsonObject);
            WriteContent(fileNameLogPath, result);
        }
    }
}
