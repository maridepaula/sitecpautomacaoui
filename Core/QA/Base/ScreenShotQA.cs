﻿using OpenQA.Selenium;

namespace Framework.Tests.QA.Base
{
    internal class ScreenShotQA
    {
        internal ScreenShotQA(Screenshot screenshot)
        {
            Screenshot = screenshot;
        }

        internal Screenshot Screenshot { get; set; }
    }
}
