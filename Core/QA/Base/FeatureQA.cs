﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace Framework.Tests.QA.Base
{
    internal class FeatureQA
    {
        internal FeatureQA(FeatureInfo featureInfo)
        {
            Scenarios = new List<ScenarioQA>();
            FeatureInfo = featureInfo;
        }
        internal FeatureInfo FeatureInfo { get; set; }

        internal List<ScenarioQA> Scenarios { get; set; }

        internal DateTime StartTime { get; set; }
        internal DateTime EndTime { get; set; }

        internal bool HasError
        {
            get
            {
                return Scenarios.Any(x => x.HasError);
            }
        }

        public override string ToString()
        {
            return FeatureInfo.Title;
        }
    }
}
