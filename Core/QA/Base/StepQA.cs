﻿using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace Framework.Tests.QA.Base
{
    internal class StepQA
    {
        internal DateTime StartTime { get; set; }
        internal DateTime EndTime { get; set; }

        internal StepInfo StepInfo;
        internal ScenarioQA ParentScenario { get; private set; }

        internal List<ScreenShotQA> ScreenShotsQA { get; set; }

        internal Exception Error { get; set; }

        internal StepQA(StepInfo stepInfo, ScenarioQA parentScenario)
        {
            StepInfo = stepInfo;
            ParentScenario = parentScenario;
            ScreenShotsQA = new List<ScreenShotQA>();
            StartTime = DateTime.Now;
        }

        internal void SetEndTime()
        {
            EndTime = DateTime.Now;
        }

        internal bool HasError
        {
            get
            {
                return Error != null;
            }
        }

        public string FormatedText
        {
            get
            {
                string stepDefinitionType = GetStepDefinition(StepInfo);
                return $"{stepDefinitionType} {StepInfo.Text}";
            }
        }


        public override string ToString()
        {
            return FormatedText;
        }

        private string GetStepDefinition(StepInfo stepDefinition)
        {
            string stepDefinitionType = string.Empty;
            switch (stepDefinition.StepDefinitionType)
            {
                case TechTalk.SpecFlow.Bindings.StepDefinitionType.Given:
                    if ((ParentScenario.LastStepDefinition == "Dado" ) || (ParentScenario.LastStepDefinition == "E"))
                    {
                        stepDefinitionType = "E";
                    }
                    else
                    {
                        stepDefinitionType = "Dado";
                    }
                    break;
                case TechTalk.SpecFlow.Bindings.StepDefinitionType.When:
                    if (ParentScenario.LastStepDefinition == "Quando") //|| ParentScenario.LastStepDefinition == "E"
                    {
                        stepDefinitionType = "E";
                    }
                    else
                    {
                        stepDefinitionType = "Quando";
                    }
                    break;
                case TechTalk.SpecFlow.Bindings.StepDefinitionType.Then:
                    if ((ParentScenario.LastStepDefinition == "Então") || (ParentScenario.LastStepDefinition == "E"))
                    {
                        stepDefinitionType = "E";
                    }
                    else
                    {
                        stepDefinitionType = "Então";
                    }
                    break;
                default:
                    break;
            }

            ParentScenario.LastStepDefinition = stepDefinitionType;
            return stepDefinitionType;
        }
    }
}
