﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace Framework.Tests.QA.Base
{
    internal class ScenarioQA
    {
        internal ScenarioQA(ScenarioInfo scenarioInfo)
        {
            Steps = new List<StepQA>();
            ScenarioInfo = scenarioInfo;
            
        }

        internal string Title { get; set; }
        internal ScenarioInfo ScenarioInfo { get; set; }
        internal List<StepQA> Steps { get; set; }

        internal DateTime StartTime { get; set; }
        internal DateTime EndTime { get; set; }

        internal string LastStepDefinition { get; set; }

        internal bool HasError
        {
            get
            {
                return Steps.Any(x => x.HasError);
            }
        }

        public string FormatedTitle
        {
            get
            {
                var formatedTitle = Title ?? ScenarioInfo.Title;
                formatedTitle = HasError ? $"{formatedTitle} [FALHOU]" : formatedTitle;
                return formatedTitle;
            }
        }

        public override string ToString()
        {
            return FormatedTitle; 
        }

        public StepQA CurrentStep
        {
            get
            {
                return Steps.LastOrDefault();
            }
        }
    }
}
