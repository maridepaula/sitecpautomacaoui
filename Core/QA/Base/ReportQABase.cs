﻿using OpenQA.Selenium;
using Portosis.UITests.Utils;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using TechTalk.SpecFlow;
//using System.Windows.Forms;

namespace Framework.Tests.QA.Base
{
    internal abstract class ReportQABase
    {
        protected static string WorkDir => Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_WorkDir");
        
        private List<FeatureQA> _features { get; set; }

        internal ReportQABase(List<FeatureQA> features)
        {
            _features = features;
        }

        internal ReportQABase()
        {
            if (!Directory.Exists(WorkDir))
                Directory.CreateDirectory(WorkDir);

            _features = new List<FeatureQA>();
        }

        internal bool HasError
        {
            get
            {
                return _features.Any(x => x.HasError);
            }
        }

        private FeatureQA CurrentFeature
        {
            get
            {
                return _features.LastOrDefault();
            }
        }

        private ScenarioQA CurrentScenario
        {
            get
            {
                return CurrentFeature.Scenarios.LastOrDefault();
            }
        }

        private StepQA CurrentStep
        {
            get
            {
                return CurrentScenario.Steps.LastOrDefault();
            }
        }

        internal void AddFeature(FeatureInfo featureInfo)
        {
            _features.Add(new FeatureQA(featureInfo));
        }

        internal void AddScenario(ScenarioInfo scenarioInfo)
        {
            CurrentFeature.Scenarios.Add(new ScenarioQA(scenarioInfo));
        }

        internal void AddStep(StepInfo stepInfo)
        {
            CurrentScenario.Steps.Add(new StepQA(stepInfo, CurrentScenario));
        }

        internal void SetStepEndTime()
        {
            CurrentScenario.CurrentStep.SetEndTime();
        }

        internal void AddScreenshot()
        {
            Screenshot screenshot = ((ITakesScreenshot)Configs.Driver).GetScreenshot();
            CurrentStep.ScreenShotsQA.Add(new ScreenShotQA(screenshot));
        }

        //screenShot do window, para funcinar add via references, nao baixa por nugget
        //internal void AddWindowsScreenshot()
        //{
        //    var bounds = Screen.GetBounds(Point.Empty);
        //    using (var bitmap = new Bitmap(bounds.Width, bounds.Height))
        //    {
        //        using (var g = Graphics.FromImage(bitmap))
        //        {
        //            g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
        //        }

        //        var converter = new ImageConverter();
        //        var bytes = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));

        //        var screenshot = new Screenshot(Convert.ToBase64String(bytes));
        //        CurrentStep.ScreenShotsQA.Add(new ScreenShotQA(screenshot));
        //    }
        //}


        internal void AddScenarioTitle(string scenarioTitle)
        {
            CurrentScenario.Title = scenarioTitle;
        }

        internal void AddReportError()
        {
            if (ScenarioContext.Current.TestError != null)
            {
                AddScreenshot();
                CurrentStep.Error = ScenarioContext.Current.TestError;
            }
        }

        public List<FeatureQA> Features
        {
            get
            {
                return _features
                        .GroupBy(x => x.FeatureInfo.Title, x => x, (key, g) => new FeatureQA(g.FirstOrDefault().FeatureInfo)
                        {
                            Scenarios = g.SelectMany(y => y.Scenarios).ToList()
                        })
                        .OrderBy(x => x.FeatureInfo.Title)
                        .ToList();
            }
        }

        public abstract void WriteReport();

        protected void WriteContent(string path, string content)
        {
            var directoryName = Path.GetDirectoryName(path);
            if (Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            using (var writer = new StreamWriter(new MemoryStream(), Encoding.UTF8))
            {
                writer.Write(content);
                writer.Flush();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                File.WriteAllBytes(path, (writer.BaseStream as MemoryStream).ToArray());
            }
        }
    }
}
