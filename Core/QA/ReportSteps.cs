﻿using Framework.Tests.QA;
using Framework.Tests.QA.Base;
using Portosis.UITests.Utils;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace Framework.Tests.Steps
{
    [Binding]
    public static class ReportSteps
    {
        private static ReportQABase _reportQA;

        private static ReportQABase ReportQA
        {
            get
            {
                if (_reportQA == null)
                {
                    switch (Configs.ReportType)
                    {
                        case ReportType.Html:
                            _reportQA = new HtmlQA();
                            break;
                        case ReportType.PDF:
                            _reportQA = new PdfQA();
                            break;
                        case ReportType.Json:
                            _reportQA = new JsonQA();
                            break;
                        case ReportType.Extent:
                            _reportQA = new ExtentQA();
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }
                return _reportQA;
            }
        }

        [BeforeFeature]
        public static void BeforeFeature()
        {
            ReportQA.AddFeature(FeatureContext.Current.FeatureInfo);
        }

        [BeforeScenario]
        public static void BeforeScenario()
        {
           
            ReportQA.AddScenario(ScenarioContext.Current.ScenarioInfo);
        }

        [BeforeStep]
        public static void BeforeStep()
        {
            ReportQA.AddStep(ScenarioStepContext.Current.StepInfo);
        }

        [AfterStep]
        public static void AfterStep()
        {
            if (ScenarioContext.Current.TestError != null)
            {
                ReportQA.AddReportError();
            }
            ReportQA.SetStepEndTime();
        }

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            ExtentQA.Init();
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            ReportQA.WriteReport();

            if ((Configs.ReportType != ReportType.Extent))
                new ExtentQA(ReportQA.Features).WriteReport();

            //if ((Configs.ReportType != ReportType.Json))
            //    new JsonQA(ReportQA.Features).WriteReport();
        }

        public static void AddScreenshot()
        {
            Thread.Sleep(100);
            ReportQA.AddScreenshot();
        }
        //public static void AddWindowsScreenshot()
        //{
        //    ReportQA.AddWindowsScreenshot();
        //}

        public static void AddScenarioTitle(string scenarioTitle)
        {
            ReportQA.AddScenarioTitle(scenarioTitle);
        }
    }
}
