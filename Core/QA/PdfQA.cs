﻿using Framework.Tests.QA.Base;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Portosis.UITests.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Framework.Tests.QA
{
    internal class PdfQA : ReportQABase
    {
        private enum TipoRelatorio
        {
            Todos,
            SomenteSucessos,
            SomenteErros
        }

        private Section _currentSection;

        public override void WriteReport()
        {
            WriteReport(Features.Where(x => x.Scenarios.Any(y => !y.HasError)), $@"{WorkDir}\Evidencia_Sucesso_{Configs.OutputPdf}", TipoRelatorio.SomenteSucessos);
            WriteReport(Features.Where(x => x.Scenarios.Any(y => y.HasError)), $@"{WorkDir}\Evidencia_Erro_{Configs.OutputPdf}", TipoRelatorio.SomenteErros);
        }

        private void WriteReport(IEnumerable<FeatureQA> featuresFilter, string fileNameLogPathFilter, TipoRelatorio tipoRelatorio)
        {
            if (!featuresFilter.Any())
                return;

            var doc = new Document(PageSize.A4);
            
            if (File.Exists(fileNameLogPathFilter))
                File.Delete(fileNameLogPathFilter);

            doc.SetMargins(30, 30, 30, 30);
            doc.AddCreationDate();
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(fileNameLogPathFilter, FileMode.Create));

            doc.Open();

            int i = 1;
            foreach (var feature in featuresFilter)
            {
                Chapter chapter = CreateChapter(feature.FeatureInfo.Title, feature.FeatureInfo.Description, i++);

                foreach (var scenario in feature.Scenarios.Where(x =>
                    (tipoRelatorio == TipoRelatorio.SomenteErros && x.HasError)
                    || (tipoRelatorio == TipoRelatorio.SomenteSucessos && !x.HasError)
                    || tipoRelatorio == TipoRelatorio.Todos
                ))
                {
                    var header = CreateHeader(scenario.FormatedTitle, scenario.HasError);
                    _currentSection = chapter.AddSection(20f, header, 2);

                    foreach (var step in scenario.Steps)
                    {
                        AddText(step.FormatedText);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(step.StepInfo.Table)))
                            AddText(step.StepInfo.Table.ToString());

                        if (step.ScreenShotsQA.Any())
                        {
                            foreach (var screenshot in step.ScreenShotsQA)
                            {
                                AddText(" ", 8);
                                AddImage(screenshot);
                            }
                            _currentSection.Add(Chunk.NEWLINE);
                        }

                        if (step.Error != null)
                        {
                            _currentSection.Add(Chunk.NEWLINE);
                            _currentSection.Add(Chunk.NEWLINE);
                            AddText(step.Error.ToFormatedString(), 8);
                        }
                    }
                    _currentSection.Add(Chunk.NEWLINE);
                }
                doc.Add(chapter);
                doc.Add(Chunk.NEWLINE);
                doc.Add(Chunk.NEWLINE);
            }

            doc.Close();
        }

        private void AddImage(ScreenShotQA screenShotQA)
        {
            Image pic = Image.GetInstance(screenShotQA.Screenshot.AsByteArray);

            var percentage = pic.Height > pic.Width ? Configs.ImgHeight / pic.Height : Configs.ImgWidth / pic.Width;
            var scale = percentage * 100;
            pic.ScalePercent(scale);

            pic.Border = Rectangle.BOX;
            pic.BorderColor = BaseColor.BLACK;
            pic.BorderWidth = 3f;

            PdfPTable table = new PdfPTable(1);
            PdfPCell cell = new PdfPCell(pic);
            cell.Border = Rectangle.NO_BORDER;

            table.AddCell(cell);

            _currentSection.Add(table);
        }

        private Paragraph AddText(string text, float? size = null)
        {
            Paragraph paragrafo = new Paragraph();
            paragrafo.Alignment = Element.ALIGN_LEFT;
            paragrafo.Font = new Font(Font.NORMAL, size ?? 10, (int)System.Drawing.FontStyle.Regular);
            paragrafo.Add(text);
            _currentSection.Add(paragrafo);
            return paragrafo;
        }

        private Chapter CreateChapter(string title, string description, int number)
        {
            Chunk chapTitle = new Chunk(title);
            Chapter chapter = new Chapter(new Paragraph(chapTitle), number);
            chapTitle.SetLocalDestination(chapter.GetBookmarkTitle().Content);

            if (!string.IsNullOrWhiteSpace(description))
            {
                Paragraph paragrafo = new Paragraph();
                paragrafo.Alignment = Element.ALIGN_LEFT;
                paragrafo.Font = new Font(Font.NORMAL, 10, (int)System.Drawing.FontStyle.Regular);
                paragrafo.Add(description);

                chapter.Add(paragrafo);
            }
            chapter.Add(Chunk.NEWLINE);

            return chapter;
        }

        private Paragraph CreateHeader(string text, bool hasError)
        {
            Paragraph paragrafo = new Paragraph();
            paragrafo.Alignment = Element.ALIGN_LEFT;
            paragrafo.Font = new Font(Font.NORMAL, 12, (int)System.Drawing.FontStyle.Bold, hasError ? new BaseColor(255, 0, 0) : new BaseColor(0, 128, 0));
            paragrafo.Add(text);
            return paragrafo;
        }
    }
}