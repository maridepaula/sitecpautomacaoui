﻿namespace Framework.Tests.Steps
{
    public enum ReportType
    {
        Html = 1,
        PDF,
        Json,
        Extent
    }
}
