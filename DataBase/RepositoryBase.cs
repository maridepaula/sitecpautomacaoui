using System.Collections.Generic;
using System.Data;
using System.Dynamic;

namespace UiTestSiteCP.DataBase
{
 public class RepositoryBase
    {
        protected IDbConnection Connection { get; set; }

        public RepositoryBase(IDbConnection connection)
        {
            Connection = connection;
        }

        // para retornar uma lista de dynamic
        public static dynamic ToExpandoObject(object value)
        {
            IDictionary<string, object> dapperRowProperties = value as IDictionary<string, object>;

            IDictionary<string, object> expando = new ExpandoObject();

            foreach (KeyValuePair<string, object> property in dapperRowProperties)
                expando.Add(property.Key, property.Value);

            return expando as ExpandoObject;
        }



        //protected SqlConnection Connection { get; set; }

        //public RepositoryBase(SqlConnection connection)
        //{
        //    Connection = connection;
        //}

    }
}