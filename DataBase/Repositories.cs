using System.Data.SqlClient;
using Portosis.UITests.Utils;
using MySql.Data.MySqlClient;

namespace UiTestSiteCP.DataBase
{
    class Repositories
    {
        private static string ConectionStringSicred = Configs.ConnectionStringSicred;
        private static string ConnectionStringPortosis = Configs.ConnectionStringPortosis;
        private static string ConnectionStringEnelMockMySQL = Configs.ConnectionStringMockRegressivoMySql;
        //private static string ConnectionStringEnelMockMySQL = Configs.ConnectionStringMockRegressivoMySql;
        public static InicialRepositorySicred InicialRepositorySicred => new InicialRepositorySicred(new SqlConnection(ConectionStringSicred));
        public static PlanosRepositorySicred PlanosRepositorySicred => new PlanosRepositorySicred(new SqlConnection(ConectionStringSicred));
        public static ClientesRepositorySicred ClientesRepositorySicred => new ClientesRepositorySicred(new SqlConnection(ConectionStringSicred));
        public static InicialRepositoryPortosis InicialRepositoryPortosis => new InicialRepositoryPortosis(new SqlConnection(ConnectionStringPortosis));
        public static InicialRepositoryEnelMock InicialRepositoryEnelMock => new InicialRepositoryEnelMock(new MySqlConnection(ConnectionStringEnelMockMySQL));
        public static InicialRepositoryParceirosMock InicialRepositoryParceirosMock  => new InicialRepositoryParceirosMock(new MySqlConnection(Configs.ConnectionStringParceirosMySql));

    }
}