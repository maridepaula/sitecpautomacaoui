﻿using Dapper;
using MySql.Data.MySqlClient;

namespace UiTestSiteCP.DataBase
{
    public class InicialRepositoryParceirosMock : RepositoryBase
    {

        public InicialRepositoryParceirosMock(MySqlConnection connection) : base(connection)
        { }

        //public void TesteConexaoBaseMockEnelUpdate()
        //{
        //    Connection.Execute
        //         (@"update cliente set nome = 'automacao teste2' where id = 1");
        //}

        public dynamic TesteConexaoBaseMockEnel()
        {
            var t = Connection.Query<dynamic>
                 (@"select *from cliente where cpf = '12345678909' and numero_cliente = '192219'");
            return t;
        }

        //public string AtualizaAmbRegressivoMock(string url)
        //{
        //    /*após resolver incompatibilidade, atualizar para update*/
        //    //UPDATE configuracao_gate_enel
        //    //SET VALOR = @url
        //    //where chave = 'URL_API';
        //  //  return Connection.Query<string>
        //  //   (@"SELECT	valor FROM	configuracao_gate_enel WHERE chave ='URL_API' and valor = @URL");          
        //}

        public void AtualizaAmbRegressivoParceiros(string url)
        {
            Connection.Query
               (@"UPDATE configuracao_gate_enel 
                   SET VALOR = @URL
                  WHERE chave ='URL_API';"
           , new { url });
        }

        public void IncluirClienteAmbParceiros(string cpfcgc, string regiao, string numeroCliente, int dvNumeroCliente, string lote)
        {
            if (regiao == "CELG")
            {
                Connection.Query
                (@"INSERT INTO cliente (api, nome, numero_cliente, regiao_atendimento, data_nascimento, cep, ddd, telefone, cpf, data_ultimo_faturamento, estado_cliente, estado_faturamento, estado_fornecimento, data_hora_ultima_atualizacao, lote_faturamento, media_consumo)
                        values (1, 'TESTE AUTOMACAO ENEL', @numeroCliente, @regiao, '1952-03-11', '24440451', '', '', @cpfcgc, '2019-11-23', '0', 'N', '0', '2019-11-24 13:51:15', @lote, 0.00);"
                , new { cpfcgc, regiao, numeroCliente, dvNumeroCliente, lote });
            }
            else
            {
                Connection.Query
                (@"INSERT INTO cliente (api, nome, numero_cliente, regiao_atendimento, dv_numero_cliente, data_nascimento, cep, ddd, telefone, cpf, data_ultimo_faturamento, estado_cliente, estado_faturamento, estado_fornecimento, data_hora_ultima_atualizacao, lote_faturamento, media_consumo)
                        values (1, 'TESTE AUTOMACAO ENEL', @numeroCliente, @regiao, @dvNumeroCliente, '1952-03-11', '24440451', '', '', @cpfcgc, '2019-11-23', '0', 'N', '0', '2019-11-24 13:51:15', @lote, 0.00);"
                , new { cpfcgc, regiao, numeroCliente, dvNumeroCliente, lote });
            }

        }

        public void RemoverClienteAmbParceiros(string cpfcgc)
        {
            Connection.Query
                (@"DELETE FROM cliente 
                   WHERE cpf = @cpfcgc ;"
            , new { cpfcgc});
        }
        //numero_cliente = @numerocliente
        //           AND    regiao_atendimento = @regiao
        //           AND    cpf = @cpfcgc
        //           AND    lote_faturamento = @lote
        public void IncluirCalendarioFaturamentoAmbParceiros(string regiao, string lote)
        {
            Connection.Query
                (@"INSERT INTO calendario_faturamento (regiao_concessionaria, data_referencia, setor, data_pre_faturamento, data_leitura, data_faturamento, data_vencimento_convencional, data_vencimento_imediato)
                   VALUES (@REGIAO,
                           DATE_ADD(LAST_DAY(NOW() - INTERVAL 1 MONTH), INTERVAL 1 DAY),
 	                       @LOTE, 
 	                       DATE_ADD(CURDATE(), INTERVAL -15 DAY),
 	                       DATE_ADD(CURDATE(), INTERVAL -14 DAY),
 	                       DATE_ADD(CURDATE(), INTERVAL -13 DAY),
 	                       DATE_ADD(CURDATE(), INTERVAL 10 DAY),
 	                       DATE_ADD(CURDATE(), INTERVAL 10 DAY));"
             , new { regiao, lote });
        }

        public void RemoverCalendarioFaturamentoAmbParceiros(string regiao, string lote)
        {
            Connection.Query
                (@"DELETE FROM	calendario_faturamento  
                   WHERE  regiao_concessionaria = @regiao
                   AND    setor = @lote"
                  /* AND    data_referencia = DATE_ADD(LAST_DAY(NOW() - INTERVAL 1 MONTH), INTERVAL 1 DAY) /*1º DIA DO MÊS*/
             , new { regiao, lote });
        }
    }
}