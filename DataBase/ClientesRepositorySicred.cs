﻿using System.Data.SqlClient;
using Dapper;
using System.Linq;
using static UiTestSiteCP._Storage.Proposta;
using System.Text;
using System;
using SpecFlowTeste.Core.FixedValues;

namespace UiTestSiteCP.DataBase
{
    public class ClientesRepositorySicred : RepositoryBase
    {
        public ClientesRepositorySicred(SqlConnection connection) : base(connection)
        { }

        public Cliente BuscarCliente(string CGCCPF)
        {
            return Connection.Query<Cliente>
               (@"SELECT TRIM(CLI.CICCGC) AS CGCCPF, CLI.NOME, CLI.RENDAAPROVADA AS RENDABRUTA, CLI.NASCIMENTO, CLI.MAE AS NOMEMAE, CLI.NATURALIDADE AS CIDADE,
                  COMP.RESDDD AS DDD, TRIM(COMP.RESFONE) AS TELEFONE, COMP.RESCEP AS CEP, COMP.RESNUMERO AS NUMERO, COMP.CLASSEPROFISSIONAL AS CODCLASSEPROFISSIONAL, 
                  COMP.ATIVSETOR AS CODATIVIDADE, COMP.ESTCIVIL AS ESTADOCIVIL, COMP.REFBANCO1 AS CODBANCO, COMP.REFBANCTIPOCONTA1 AS TIPOCONTABANCARIA, COMP.UFNASCIMENTO AS UF, 
                  COMP.NACIONALIDADE AS CODNACIONALIDADE, COMP.GRAUINSTRUCAO AS CODGRAUINSTRUCAO, COMP.TIPODOCIDENT AS TIPOIDENTIDADE, TRIM(COMP.NRODOCIDENT) AS IDENTIDADE, COMP.ORGAOEMISSOR AS CODEMISSOR, 
                  COMP.UFEMIDOCIDENT AS UFEMISSOR, COMP.RESTIPOFONE1 AS TIPOTELEFONE, COMP.ATIVEMPRESA AS EMPRESAPROF, COMP.COMCEP AS CEPPROF, COMP.COMENDNUMERO AS NROCEPPROF, 
                  COMP.ATIVDATAADMISSAO AS DATAADMISSAOPROF, COMP.COMDDD AS DDDPROF, COMP.COMFONE AS TELEFONEPROF, COMP.REFPESSNOME1 AS NOMEREF1, COMP.REFPESSREL1 AS RELACIOREF1,
                  COMP.REFPESSDDD1 AS DDDREF1, COMP.REFPESSFONE1 AS TELEFONEREF1, COMP.REFPESSNOME2 AS NOMEREF2, COMP.REFPESSREL2 AS RELACIOREF2, COMP.REFPESSDDD2 AS DDDREF2, 
                  COMP.REFPESSFONE2 AS TELEFONEREF2, COMP.REFAGENCIA1 AS AGENCIA, COMP.REFCONTACORRENTE1 AS CONTA, COMP.REFTEMPOCONTA1 AS CONTADESDE, COMP.INFOEMAIL,
                  COMP.AtivNumBeneficio AS NUMBENEFICIO, COMP.AtivEspecieBeneficio AS ESPECIEBENEFICIO
                  FROM CLIENTES CLI
                  INNER JOIN COMPLEMENTOS COMP ON CLI.CLIENTE = COMP.CLIENTE
                  WHERE CLI.CICCGC = @CGCCPF
                  AND   CLI.RENDAAPROVADA = COMP.ATIVRENDA"
            , new { CGCCPF }
                ).FirstOrDefault();
        }

        //public string BuscarClienteRefinanciamento(string produto, string produtoRefin, string codBanco)
        //{
        //    return Connection.Query<string>
        //         (@"declare @CLIENTE varchar(15) = 
        //             (
        //                SELECT TOP 1 PROPC.cliente as CPF  
        //                FROM    CONTRATOSCDC CDC
        //                INNER JOIN COMPROMISSOS CMP ON  CDC.CONTRATO = CMP.CONTRATO
        //                 AND CDC.EMPRESA = CMP.EMPRESA 
        //                 AND CDC.AGENCIA = CMP.AGENCIA
        //                INNER JOIN PARCELAS PAR	ON PAR.CONTRATO = CDC.CONTRATO
        //                INNER JOIN CLIENTES C	ON CMP.CLIENTE = C.CLIENTE
        //                INNER JOIN PROPOSTASCREDITO PROPC ON C.CLIENTE = PROPC.CLIENTE
        //                INNER JOIN COMPLEMENTOS COMP ON C.CLIENTE = COMP.CLIENTE
        //                WHERE PAR.CONTRATO = CDC.CONTRATO
        //                AND   PAR.EMPRESA = CDC.EMPRESA
        //                AND   PAR.AGENCIA = CDC.AGENCIA
        //                AND   PAR.SITUACAO = 'L'
        //                AND   CDC.DIASATRASO = 0
        //                AND   CDC.SITUACAO  = 'A' 
        //                AND   CDC.EMPRESA = '01'
        //                AND   CDC.AGENCIA = '0001'                   
        //                AND   CDC.PRINCIPAL <= 2500.00
        //                AND   COMP.REFBANCO1 = @codBanco
        //                AND   COMP.REFBANCTIPOCONTA1 <> 0
        //                AND CMP.CLIENTE NOT IN (SELECT	CMP.CLIENTE
        //                      FROM    CONTRATOSCDC CDC 
        //                      INNER JOIN COMPROMISSOS CMP ON  CDC.CONTRATO = CMP.CONTRATO
        //                       AND CDC.EMPRESA = CMP.EMPRESA
        //                       AND CDC.AGENCIA = CMP.AGENCIA
        //                      WHERE	CDC.DIASATRASO > 0
        //                      AND CDC.SITUACAO = 'A'
        //                      AND CDC.EMPRESA = '01'
        //                      AND CDC.AGENCIA = '0001'
        //                      )
        //                AND C.NASCIMENTO >= '1965-01-01'
        //                AND CDC.QTDPAGAS >= 3
        //                AND CDC.PRODUTO =  @PRODUTO
        //                AND CDC.SALDO <= (CDC.TOTALPARCELAS * .5)
        //                AND EXISTS (SELECT  1
        //                   FROM PROPOSTASCREDITO PC 
        //                   WHERE PC.SITUACAO = 20  
        //	AND PC.SITUACAO NOT IN (01,10,17,21,31,40,46)
        //                   AND PC.PROPOSTA = PROPC.PROPOSTA 
        //                   AND CDC.QTDPAGAS >= 6 
        //                   GROUP BY CLIENTE 
        //                   HAVING COUNT(CONTRATO) = 1 
        //                   AND Count(CONTRATO) < 2 
        //	 )
        //                ORDER BY NEWID()
        //            );
        //            select @cliente",
        //          new { produto, produtoRefin, codBanco}).FirstOrDefault();
        //}        

        //        public string BuscarClienteRefinanciamento(string produto, string produtoRefin)
        //        { //arthur + mari
        //            return Connection.Query<string>
        //                 (@"DECLARE @CLIENTE VARCHAR(15) =
        //	                (
        //		                select top 1
        //    TRIM(c.cliente) 
        //from clientes                   c
        //    INNER JOIN PROPOSTASCREDITO PROPC
        //        ON C.CLIENTE = PROPC.CLIENTE
        //           and c.empresa = propc.empresa
        //    inner join contratoscdc     concdc
        //        on propc.proposta = concdc.contrato
        //           and propc.empresa = concdc.empresa
        //           and propc.agencia = concdc.agencia
        //    INNER JOIN COMPLEMENTOS     COMP
        //        ON C.CLIENTE = COMP.CLIENTE
        //where PROPC.situacao = '20'
        //      AND NOT EXISTS (SELECT 1 FROM PROPOSTASCREDITO P2 WHERE P2.PRODUTO = PROPC.PRODUTO AND PRODUTO <> @PRODUTO) --000712, 001028, 001035
        //     /* and PROPC.produto = @PRODUTO*/
        //      and concdc.qtdpagas >= 3
        //      AND concdc.DIASATRASO = 0
        //      AND concdc.SITUACAO = 'A'
        //      AND concdc.EMPRESA = '01'
        //      AND concdc.AGENCIA = '0001'
        //      AND concdc.PRINCIPAL <= 2500.00
        //      AND c.CLIENTE NOT IN (
        //                               SELECT CMP.CLIENTE
        //                               FROM CONTRATOSCDC           CDC
        //                                   INNER JOIN COMPROMISSOS CMP
        //                                       ON CDC.CONTRATO = CMP.CONTRATO
        //                                          AND CDC.EMPRESA = CMP.EMPRESA
        //                                          AND CDC.AGENCIA = CMP.AGENCIA
        //                               WHERE CDC.DIASATRASO > 0
        //                                     AND CDC.SITUACAO = 'A'
        //                                     AND CDC.EMPRESA = '01'
        //                                     AND CDC.AGENCIA = '0001'
        //                           )
        //      AND C.NASCIMENTO >= '1965-01-01'
        //      AND COMP.REFBANCTIPOCONTA1 <> 0
        //      and c.cliente not in (
        //                               select c2.cliente
        //                               from clientes                   c2
        //                                   INNER JOIN PROPOSTASCREDITO PROPC2
        //                                       ON C2.CLIENTE = PROPC2.CLIENTE
        //                                          and c2.empresa = propc2.empresa
        //                                   INNER JOIN COMPLEMENTOS     COMP2
        //                                       ON C2.CLIENTE = COMP2.CLIENTE
        //                               where propc2.situacao <> '20'
        //                           )

        //				   ORDER BY NEWID()
        //		            )		
        //		            UPDATE  COMPLEMENTOS
        //		            SET RESDDD = '51', RESFONE = '982896877', RESCEP = '57081540', RESNUMERO = '21', CLASSEPROFISSIONAL = '07', ATIVSETOR = '1111',
        //                        ESTCIVIL = 'D', REFBANCO1 = '0104', REFBANCTIPOCONTA1  = 1, UFNASCIMENTO = 'BA', NACIONALIDADE = '01', GRAUINSTRUCAO = 7,
        //                        TIPODOCIDENT = '01', NRODOCIDENT = '4098596854', ORGAOEMISSOR = '01', UFEMIDOCIDENT = 'RS', RESTIPOFONE1 = 2, ATIVEMPRESA = 'CWI SOFTWARE',    
        //                        COMCEP = '57081540', COMENDNUMERO = '24', ATIVDATAADMISSAO = '2019-12-01 00:00:00.000', COMDDD = '71', COMFONE = '991445626', REFPESSNOME1 = 'REFERENCIA 1 TESTE AUTOMACAO',
        //			            REFPESSREL1 = 'PRIMO', REFPESSDDD1 = '54', REFPESSFONE1 = '998866898', REFPESSNOME2 = 'REFERENCIA 2 TESTE AUTOMACAO', REFPESSREL2  = 'TIA', REFPESSDDD2  = '55',
        //			            REFPESSFONE2 =	'998844898', REFAGENCIA1  = '0482',  REFCONTACORRENTE1 = '0119378306', REFTEMPOCONTA1 = 	'122018', INFOEMAIL= 'automacaocwi@cwi.com.br'
        //			            -- COMP.AtivNumBeneficio COMP.AtivEspecieBeneficio 
        //	                WHERE CLIENTE = @CLIENTE
        //                    SELECT @CLIENTE",
        //                  new { produto, produtoRefin }).FirstOrDefault();
        //        }

        public string BuscarClienteRefinanciamento(string produto)
        {
            return Connection.Query<string>
                 (@"DECLARE @CLIENTE VARCHAR(15) =
	                (
		                select top 1
                        TRIM(c.cliente) 
                    from clientes                   c
                        INNER JOIN PROPOSTASCREDITO PROPC
                            ON C.CLIENTE = PROPC.CLIENTE
                               and c.empresa = propc.empresa
                        inner join contratoscdc     concdc
                            on propc.proposta = concdc.contrato
                               and propc.empresa = concdc.empresa
                               and propc.agencia = concdc.agencia
                        INNER JOIN COMPLEMENTOS     COMP
                            ON C.CLIENTE = COMP.CLIENTE
                    where concdc.DIASATRASO = 0
                          AND concdc.SITUACAO = 'A'
                          AND concdc.EMPRESA = '01'
                          AND concdc.AGENCIA = '0001'
                          AND concdc.PRINCIPAL <= 5000.00
                          AND LEN(c.CLIENTE) >=12
                          AND c.CLIENTE !=''
                          AND c.CLIENTE NOT IN (
                                                   SELECT CMP.CLIENTE
                                                   FROM CONTRATOSCDC           CDC
                                                       INNER JOIN COMPROMISSOS CMP
                                                           ON CDC.CONTRATO = CMP.CONTRATO
                                                              AND CDC.EMPRESA = CMP.EMPRESA
                                                              AND CDC.AGENCIA = CMP.AGENCIA
                                                   WHERE CDC.DIASATRASO > 0
                                                         AND CDC.SITUACAO = 'A'
                                                         AND CDC.EMPRESA = '01'
                                                         AND CDC.AGENCIA = '0001'
                                               )
                          AND C.NASCIMENTO >= '1965-01-01'                          
                          AND COMP.REFBANCTIPOCONTA1 <> 0   
                          And c.cliente not in (
                                                   select c2.cliente
                                                   from clientes                   c2
                                                       INNER JOIN PROPOSTASCREDITO PROPC2
                                                           ON C2.CLIENTE = PROPC2.CLIENTE
                                                              and c2.empresa = propc2.empresa
                                                       INNER JOIN COMPLEMENTOS     COMP2
                                                           ON C2.CLIENTE = COMP2.CLIENTE
                                                   where propc2.situacao IN (30, 10, 40, 01, 21, 46)
                                                )
                          AND EXISTS (SELECT 1 
			                                            FROM PROPOSTASCREDITO PC 
			                                            WHERE PC.SITUACAO = 20 
			                                            AND PC.PROPOSTA=concdc.CONTRATO 
			                                            AND concdc.QTDPAGAS >= 6 
                                                        AND concdc.produto = @produto
			                                            GROUP BY CLIENTE 
			                                            HAVING COUNT(CONTRATO) = 1 
			                                            AND Count(CONTRATO) < 2  
                                               )
				   ORDER BY NEWID()
		            )		
                    SELECT @CLIENTE
                    
                    UPDATE CLIENTES 
                    SET CNHCliente = '06223461929', CODIGOSEGURANCACNH = '06223461929'
                    WHERE CLIENTE = @CLIENTE

		            UPDATE COMPLEMENTOS
		            SET RESDDD = '51', RESFONE = '982896877', RESCEP = '57081540', RESNUMERO = '21', CLASSEPROFISSIONAL = '07', ATIVSETOR = '1111',
                        ESTCIVIL = 'D', REFBANCO1 = '0104', REFBANCTIPOCONTA1  = 1, UFNASCIMENTO = 'BA', NACIONALIDADE = '01', GRAUINSTRUCAO = 7,
                        TIPODOCIDENT = '01', NRODOCIDENT = '4098596854', ORGAOEMISSOR = '01', UFEMIDOCIDENT = 'RS', RESTIPOFONE1 = 2, ATIVEMPRESA = 'CWI SOFTWARE',    
                        COMCEP = '57081540', COMENDNUMERO = '24', ATIVDATAADMISSAO = '2019-12-01 00:00:00.000', COMDDD = '71', COMFONE = '991445626', REFPESSNOME1 = 'REFERENCIA 1 TESTE AUTOMACAO',
			            REFPESSREL1 = 'PRIMO', REFPESSDDD1 = '54', REFPESSFONE1 = '998866898', REFPESSNOME2 = 'REFERENCIA 2 TESTE AUTOMACAO', REFPESSREL2  = 'TIA', REFPESSDDD2  = '55',
			            REFPESSFONE2 =	'998844898', REFAGENCIA1  = '0482',  REFCONTACORRENTE1 = '0119378306', REFTEMPOCONTA1 = 	'122018', INFOEMAIL= 'automacaocwi@cwi.com.br',
                        REFCHEQUEESPECIAL1	= 0.00, REFCHEQUELIMITE1 = 0.00, REFCHEQUEESPECIAL2 = 0.00, REFCHEQUELIMITE2 = 0.00, REFLIMITECHEQUE1 = 0.00, REFLIMITECHEQUE2 = 0.00, REFLIMITECHEQUE3 = 0.00
			            -- COMP.AtivNumBeneficio COMP.AtivEspecieBeneficio 
	                WHERE CLIENTE = @CLIENTE",
                  new { produto }).FirstOrDefault();
        }

        public string BuscarClienteRefinanciamentoDBCBradesco(string produto, string codBanco)
        {
            return Connection.Query<string>
                 (@"DECLARE @CLIENTE VARCHAR(15) =
	                (
		                select top 1
                        TRIM(c.cliente) 
                    from clientes                   c
                        INNER JOIN PROPOSTASCREDITO PROPC
                            ON C.CLIENTE = PROPC.CLIENTE
                               and c.empresa = propc.empresa
                        inner join contratoscdc     concdc
                            on propc.proposta = concdc.contrato
                               and propc.empresa = concdc.empresa
                               and propc.agencia = concdc.agencia
                        INNER JOIN COMPLEMENTOS     COMP
                            ON C.CLIENTE = COMP.CLIENTE
                    where concdc.DIASATRASO = 0
                          AND concdc.SITUACAO = 'A'
                          AND concdc.EMPRESA = '01'
                          AND concdc.AGENCIA = '0001'
                          AND concdc.PRINCIPAL <= 5000.00
                          AND LEN(c.CLIENTE) >=12
                          AND c.CLIENTE !=''
                          AND c.CLIENTE NOT IN (
                                                   SELECT CMP.CLIENTE
                                                   FROM CONTRATOSCDC           CDC
                                                       INNER JOIN COMPROMISSOS CMP
                                                           ON CDC.CONTRATO = CMP.CONTRATO
                                                              AND CDC.EMPRESA = CMP.EMPRESA
                                                              AND CDC.AGENCIA = CMP.AGENCIA
                                                   WHERE CDC.DIASATRASO > 0
                                                         AND CDC.SITUACAO = 'A'
                                                         AND CDC.EMPRESA = '01'
                                                         AND CDC.AGENCIA = '0001'
                                               )
                          AND C.NASCIMENTO >= '1965-01-01'
                          AND COMP.REFBANCTIPOCONTA1 <> 0   
                          AND COMP.REFBANCO1 = @codBanco
                        /*  And c.cliente not in (
                                                   select c2.cliente
                                                   from clientes                   c2
                                                       INNER JOIN PROPOSTASCREDITO PROPC2
                                                           ON C2.CLIENTE = PROPC2.CLIENTE
                                                              and c2.empresa = propc2.empresa
                                                       INNER JOIN COMPLEMENTOS     COMP2
                                                           ON C2.CLIENTE = COMP2.CLIENTE
                                                   where propc2.situacao IN (30, 10, 40, 01, 21, 46)
                                                )*/
                          AND EXISTS (SELECT 1 
			                                            FROM PROPOSTASCREDITO PC 
			                                            WHERE PC.SITUACAO = 20 
			                                            AND PC.PROPOSTA=concdc.CONTRATO 
			                                            AND concdc.QTDPAGAS >= 6 
                                                        AND concdc.produto = @produto
			                                            GROUP BY CLIENTE 
			                                            HAVING COUNT(CONTRATO) = 1 
			                                            AND Count(CONTRATO) < 2  
                                               )
				   ORDER BY NEWID()
		            )
                    SELECT @CLIENTE
                    
                    UPDATE CLIENTES 
                    SET CNHCliente = '06223461929', CODIGOSEGURANCACNH = '06223461929'
                    WHERE CLIENTE = @CLIENTE	
	
		            UPDATE COMPLEMENTOS
		            SET RESDDD = '51', RESFONE = '982896877', RESCEP = '57081540', RESNUMERO = '21', CLASSEPROFISSIONAL = '07', ATIVSETOR = '1111',
                        ESTCIVIL = 'D', REFBANCO1 = '0237', REFBANCTIPOCONTA1  = 1, UFNASCIMENTO = 'BA', NACIONALIDADE = '01', GRAUINSTRUCAO = 7,
                        TIPODOCIDENT = '01', NRODOCIDENT = '4098596854', ORGAOEMISSOR = '01', UFEMIDOCIDENT = 'RS', RESTIPOFONE1 = 2, ATIVEMPRESA = 'CWI SOFTWARE',    
                        COMCEP = '57081540', COMENDNUMERO = '24', ATIVDATAADMISSAO = '2019-12-01 00:00:00.000', COMDDD = '71', COMFONE = '991445626', REFPESSNOME1 = 'REFERENCIA 1 TESTE AUTOMACAO',
			            REFPESSREL1 = 'PRIMO', REFPESSDDD1 = '54', REFPESSFONE1 = '998866898', REFPESSNOME2 = 'REFERENCIA 2 TESTE AUTOMACAO', REFPESSREL2  = 'TIA', REFPESSDDD2  = '55',
			            REFPESSFONE2 =	'998844898', REFAGENCIA1  = '5389',  REFCONTACORRENTE1 = '12553862', REFTEMPOCONTA1 = 	'122018', INFOEMAIL= 'automacaocwi@cwi.com.br',
			            AtivNumBeneficio = '1111111111',  AtivEspecieBeneficio = '12', REFCHEQUEESPECIAL1	= 0.00, REFCHEQUELIMITE1 = 0.00, REFCHEQUEESPECIAL2 = 0.00, REFCHEQUELIMITE2 = 0.00,
                        REFLIMITECHEQUE1 = 0.00, REFLIMITECHEQUE2 = 0.00, REFLIMITECHEQUE3 = 0.00
	                WHERE CLIENTE = @CLIENTE"
                    ,
                  new { produto, codBanco }).FirstOrDefault();
        }



        public int BuscarClienteRefinanciamentoDBCOptantes(string produto, string codBanco)
        {
            return Connection.Query<int>
                  (@"DECLARE @DebitoContaOptanteID INT = 
                    (                        
                        SELECT TOP 1 dco.DebitoContaOptanteID	                   
                        FROM clientes                   c
                            INNER JOIN PROPOSTASCREDITO PROPC
                                ON C.CLIENTE = PROPC.CLIENTE
                                   and c.empresa = propc.empresa
                            INNER JOIN contratoscdc     concdc
                                on propc.proposta = concdc.contrato
                                   and propc.empresa = concdc.empresa
                                   and propc.agencia = concdc.agencia
                            INNER JOIN COMPLEMENTOS     COMP
                                ON C.CLIENTE = COMP.CLIENTE
                            INNER JOIN dbo.SYN_PORTOSIS_DebitoContaOptante DCO 
                                ON DCO.Cliente = COMP.CLIENTE
                    		        AND DCO.NumeroBanco = COMP.REFBANCO1 
                    			    AND DCO.NumeroAgencia = COMP.REFAGENCIA1
								    AND OPTANTEATIVO = 1										
                    			    AND	CONCAT(SUBSTRING(NumeroConta, 1, 3), [dbo].[SYN_PORTOSIS_FillLeft](dbo.SYN_PORTOSIS_fnRemoveZeros(RIGHT(NumeroConta,LEN(NumeroConta)-3), 0), 7, '0')) = REFCONTACORRENTE1		   	
                        where concdc.DIASATRASO = 0
                              AND concdc.SITUACAO = 'A'
                              AND concdc.EMPRESA = '01'
                              AND concdc.AGENCIA = '0001'
                              AND concdc.PRINCIPAL <= 5000.00
                              AND LEN(c.CLIENTE) >=12
                              AND c.CLIENTE !=''
                              AND concdc.SALDO <= (concdc.TOTALPARCELAS * .5)
                              AND c.CLIENTE NOT IN (
                                                       SELECT CMP.CLIENTE
                                                       FROM CONTRATOSCDC           CDC
                                                           INNER JOIN COMPROMISSOS CMP
                                                               ON CDC.CONTRATO = CMP.CONTRATO
                                                                  AND CDC.EMPRESA = CMP.EMPRESA
                                                                  AND CDC.AGENCIA = CMP.AGENCIA
                                                       WHERE CDC.DIASATRASO > 0
                                                             AND CDC.SITUACAO = 'A'
                                                             AND CDC.EMPRESA = '01'
                                                             AND CDC.AGENCIA = '0001'
                                                   )
                              AND C.NASCIMENTO >= '1965-01-01'
                              AND COMP.REFBANCTIPOCONTA1 <> 0
                              AND COMP.REFBANCO1 = @CodBanco
                           /*   And c.cliente not in (
                                                   select c2.cliente
                                                   from clientes                   c2
                                                       INNER JOIN PROPOSTASCREDITO PROPC2
                                                           ON C2.CLIENTE = PROPC2.CLIENTE
                                                              and c2.empresa = propc2.empresa
                                                       INNER JOIN COMPLEMENTOS     COMP2
                                                           ON C2.CLIENTE = COMP2.CLIENTE
                                                   where propc2.situacao IN (30, 10, 40, 01, 21, 46)
                                                )*/
                             AND EXISTS (SELECT 1 
			                            FROM PROPOSTASCREDITO PC 
			                            WHERE PC.SITUACAO = 20 
			                            AND PC.PROPOSTA=concdc.CONTRATO 
			                            AND concdc.QTDPAGAS >= 6 
                                        AND COMP.REFBANCO1 = @CodBanco
                                        AND concdc.produto = @produto
			                            GROUP BY CLIENTE 
			                            HAVING COUNT(CONTRATO) = 1 
			                            AND Count(CONTRATO) < 2  
                                        )
                   );                                           
                    SELECT @debitoContaOptanteID",
            new { produto, codBanco }).FirstOrDefault();
        }

        public string ManipularClienteRefin(string produto, string cliente)
        {
            return Connection.Query<string>
            (@"declare @contrato varchar(15) = 
               (SELECT CDC.CONTRATO
                   FROM  CONTRATOSCDC CDC
                   INNER JOIN PROPOSTASCREDITO PROPC ON CDC.CONTRATO = PROPC.PROPOSTA
                   INNER JOIN CLIENTES C	ON PROPC.CLIENTE = C.CLIENTE
                   WHERE PROPC.SITUACAO ='20'
                   AND   C.CLIENTE = @CLIENTE  
                   AND   CDC.DIASATRASO = 0
                   AND   CDC.SITUACAO  = 'A' 
                   AND   CDC.EMPRESA = '01'
                   AND   CDC.AGENCIA = '0001'     
                   AND   C.CLIENTE = @CLIENTE              
                   AND C.NASCIMENTO >= '1965-01-01'
                   AND CDC.PRODUTO =  @PRODUTO)                                      					  
			           DECLARE @SALDO DECIMAL(10,2) = (SELECT SUM (SALDO) FROM dbo.PARCELAS WHERE CONTRATO = @contrato AND PARCELA <= '006')				   					 	                
				       IF (@SALDO) > '0.00'
                       BEGIN 
                           UPDATE CONTRATOSCDC 
	                       SET QTDPAGAS = 6, SALDO = @SALDO 
	                       WHERE CONTRATO = @contrato 
 	         
	                       UPDATE PARCELAS 
	                       SET SITUACAO = 'L', PRINCIPAL = '0.00', RECEITA = '0.00', SALDO = '0.00', SALDORENDA = '0.00'
	                       WHERE CONTRATO = @contrato 
	                       AND PARCELA <= '006'                                          
                       END
                   SELECT TOP 1 cliente as CPF FROM PROPOSTASCREDITO WHERE proposta = @contrato"
                , new { produto, cliente }
            ).FirstOrDefault();
        }

        public dynamic BuscarClienteRefinanciamentoDBC(string produto, string produtoRefin, string codBanco, TipoOptante tipoOptante)
        { //método do vagner
            var query = ObtemQueryOptante(tipoOptante, produto, produtoRefin, codBanco);
            var clienteOptante = Connection.QueryFirstOrDefault<dynamic>(query.ToString());

            if (clienteOptante == null)
            {
                query = ObtemQueryOptante(TipoOptante.None, produto, produtoRefin, codBanco);
                clienteOptante = Connection.QueryFirstOrDefault<dynamic>(query.ToString());

                var update = new StringBuilder(@"
DECLARE @diasLimiteRenovacao INT = (SELECT CONTEUDO FROM PARAMETROS_GERAIS WHERE NOME = 'DIAS_LIMITE_RENOVACAO_OPTANTES')
DECLARE @DataDentroCriterio DATETIME = CAST((GETDATE() + @diasLimiteRenovacao) AS DATETIME)
DECLARE @DataForaCriterio DATETIME = CAST((GETDATE() -1 - @diasLimiteRenovacao) AS DATETIME)");

                switch (tipoOptante)
                {
                    case TipoOptante.Ativo:
                        update.AppendLine(@"
    update SYN_PORTOSIS_DebitoContaOptante
	SET OPTANTEATIVO = 1,
	DATAULTIMOPROCESSAMENTO = @DataDentroCriterio
	WHERE DebitoContaOptanteID = @DebitoContaOptanteID;");
                        break;

                    case TipoOptante.Inativo:
                        update.AppendLine(@"
    update SYN_PORTOSIS_DebitoContaOptante
	SET OPTANTEATIVO = 1,
	DATAULTIMOPROCESSAMENTO = @DataForaCriterio
	WHERE DebitoContaOptanteID = @DebitoContaOptanteID;");
                        break;

                    case TipoOptante.NaoOptante:
                        update.AppendLine(@"
    update SYN_PORTOSIS_DebitoContaOptante
	SET OPTANTEATIVO = 0,
	WHERE Cliente = @CPF;");
                        break;

                    case TipoOptante.None:
                    default:
                        break;
                }

                Connection.Execute(update.ToString(),
                    new
                    {
                        CPF = clienteOptante.CPF,
                        DebitoContaOptanteID = clienteOptante.DebitoContaOptanteID
                    });
            }
            return clienteOptante;
        }

        private static StringBuilder ObtemQueryOptante(TipoOptante tipoOptante, string produto, string produtoRefin, string codBanco)
        { //método do vagner
            var query = new StringBuilder();

            #region SQL
            query.AppendLine(@"DECLARE @diasLimiteRenovacao INT = (SELECT CONTEUDO FROM PARAMETROS_GERAIS WHERE NOME = 'DIAS_LIMITE_RENOVACAO_OPTANTES')
DECLARE @DataDentroCriterio DATETIME = CAST((GETDATE() + @diasLimiteRenovacao) AS DATETIME)
DECLARE @DataForaCriterio DATETIME = CAST((GETDATE() -1 - @diasLimiteRenovacao) AS DATETIME)

SELECT TOP 1 PROPC.cliente as CPF, DebitoContaOptanteID
FROM    CONTRATOSCDC CDC
INNER JOIN COMPROMISSOS CMP ON  CDC.CONTRATO = CMP.CONTRATO
	AND CDC.EMPRESA = CMP.EMPRESA 
	AND CDC.AGENCIA = CMP.AGENCIA
INNER JOIN PARCELAS PAR	ON PAR.CONTRATO = CDC.CONTRATO
INNER JOIN CLIENTES C	ON CMP.CLIENTE = C.CLIENTE
INNER JOIN PROPOSTASCREDITO PROPC ON C.CLIENTE = PROPC.CLIENTE
INNER JOIN COMPLEMENTOS COMP ON C.CLIENTE = COMP.CLIENTE
INNER JOIN dbo.SYN_PORTOSIS_DebitoContaOptante DCO 
	ON DCO.Cliente = COMP.CLIENTE
	AND DCO.NumeroBanco = COMP.REFBANCO1 
	AND DCO.NumeroAgencia = COMP.REFAGENCIA1
	AND	CONCAT(SUBSTRING(NumeroConta, 1, 3), [dbo].[SYN_PORTOSIS_FillLeft](dbo.SYN_PORTOSIS_fnRemoveZeros(RIGHT(NumeroConta,LEN(NumeroConta)-3), 0), 7, '0')) = REFCONTACORRENTE1
WHERE 1=1
AND PAR.CONTRATO = CDC.CONTRATO
AND   PAR.EMPRESA = CDC.EMPRESA
AND   PAR.AGENCIA = CDC.AGENCIA
AND   PAR.SITUACAO = 'L'
AND   CDC.DIASATRASO = 0
AND   CDC.SITUACAO  = 'A' 
AND   CDC.EMPRESA = '01'
AND   CDC.AGENCIA = '0001'                   
AND   CDC.PRINCIPAL <= 2500.00
AND   COMP.REFBANCO1 = @codBanco
AND CMP.CLIENTE NOT IN (SELECT	CMP.CLIENTE
						FROM    CONTRATOSCDC CDC 
						INNER JOIN COMPROMISSOS CMP ON  CDC.CONTRATO = CMP.CONTRATO
							AND CDC.EMPRESA = CMP.EMPRESA
							AND CDC.AGENCIA = CMP.AGENCIA
						WHERE	CDC.DIASATRASO > 0
						AND CDC.SITUACAO = 'A'
						AND CDC.EMPRESA = '01'
						AND CDC.AGENCIA = '0001'
						)
AND C.NASCIMENTO >= '1965-01-01'
AND CDC.QTDPAGAS >= 6
AND CDC.PRODUTO =  @codProduto
AND CDC.SALDO <= (CDC.TOTALPARCELAS * .5)                       
AND EXISTS (SELECT 1 
			FROM PROPOSTASCREDITO PC 
			WHERE PC.SITUACAO = 20 
			AND PC.PROPOSTA=CDC.CONTRATO 
			AND CDC.QTDPAGAS >= 6 
			GROUP BY CLIENTE 
			HAVING COUNT(CONTRATO) = 1 
			AND Count(CONTRATO) < 2  )");
            #endregion

            switch (tipoOptante)
            {
                case TipoOptante.Ativo:
                    query.AppendLine("AND GETDATE() <= CAST((DataUltimoProcessamento + @DiasLimiteRenovacao) AS DATE)");
                    break;

                case TipoOptante.Inativo:
                    query.AppendLine("AND GETDATE() > CAST((DataUltimoProcessamento + @DiasLimiteRenovacao) AS DATE)");
                    break;

                case TipoOptante.NaoOptante:
                    query.AppendLine("AND DEBITOCONTAOPTANTEID IS NULL");
                    break;

                case TipoOptante.None:
                default:
                    break;
            }

            return query;
        }

        public void atualizaCancelamentoPropostasEmAndamento(string cliente)
        { //por enquanto está sendo utilizando apenas para os optantes
            Connection.Query
                (@"IF (SELECT COUNT(PROPOSTA) 
                        FROM PROPOSTASCREDITO  
                        WHERE CLIENTE = @CLIENTE
                        AND SITUACAO IN (02, 30, 10, 40, 01, 21, 46, 31)) > 0 	
                    BEGIN
                        UPDATE PROPOSTASCREDITO   /*CANCELAR AS PROPOSTAS EM ANDAMENTO PARA O CLIENTE*/
                        SET SITUACAO = '61'
                        WHERE CLIENTE = @CLIENTE
                        AND SITUACAO IN (02, 30, 10, 40, 01, 21, 46, 31);                        
                    END"
            , new { cliente });
        }

        public string atualizaOptante(int debitocontaoptanteid, string optante) /* TipoOptante tipoOptante)*/
        {
            return Connection.Query<string>
             (@"DECLARE @diasLimiteRenovacao INT = (SELECT CONTEUDO FROM PARAMETROS_GERAIS WHERE NOME = 'DIAS_LIMITE_RENOVACAO_OPTANTES')
                DECLARE @DataDentroCriterio DATETIME = CAST((GETDATE() + @diasLimiteRenovacao) AS DATETIME)
                DECLARE @DataForaCriterio DATETIME = CAST((GETDATE() -1 - @diasLimiteRenovacao) AS DATETIME)
                DECLARE @CPF varchar(15)= (SELECT TOP 1 CLIENTE AS CPF FROM SYN_PORTOSIS_DebitoContaOptante WHERE DebitoContaOptanteID = @DebitoContaOptanteID)
                IF (@optante) = 'optante ativo'        -- 'optante ativo dentro do criterio'
	                update SYN_PORTOSIS_DebitoContaOptante 
	                SET OPTANTEATIVO = 1,
	                DATAULTIMOPROCESSAMENTO = @DataDentroCriterio
	                WHERE DebitoContaOptanteID = @DebitoContaOptanteID
                ELSE  -- 'optante ativo fora do criterio'
	                update SYN_PORTOSIS_DebitoContaOptante 
	                SET OPTANTEATIVO = 1,
	                DATAULTIMOPROCESSAMENTO = @DataForaCriterio
	                WHERE DebitoContaOptanteID = @DebitoContaOptanteID                                        
                SELECT @CPF
                
                UPDATE CLIENTES 
                SET CNHCliente = '06223461929', CODIGOSEGURANCACNH = '06223461929'                    
                WHERE CLIENTE = @CPF

                UPDATE COMPLEMENTOS
		        SET AtivNumBeneficio = '1111111111',  AtivEspecieBeneficio = '12', REFCHEQUEESPECIAL1	= 0.00, REFCHEQUELIMITE1 = 0.00,
                    REFCHEQUEESPECIAL2 = 0.00, REFCHEQUELIMITE2 = 0.00, REFLIMITECHEQUE1 = 0.00, REFLIMITECHEQUE2 = 0.00, REFLIMITECHEQUE3 = 0.00
	            WHERE CLIENTE = @CPF"
            , new { debitocontaoptanteid, optante }
                ).FirstOrDefault();
        }
        public FormaEnvio BuscarFormaEnvio(string nrProposta)
        {
            return Connection.Query<FormaEnvio>
               (@"select top  1  Forma, Email from formaenvio_propostas where proposta = @nrProposta"
            , new { nrProposta }
                ).FirstOrDefault();
        }
        public string BuscarTipoContaBancariaPorSigla(int idConta)
        {
            return Connection.Query<string>
             (@"SELECT SIGLA FROM TIPOCONTABANCARIA WHERE ID = @idConta"
            , new { idConta }
                ).FirstOrDefault();
        }
        public int BuscarTipoContaBancariaPorID(int idConta)
        {
            return Connection.Query<int>
             (@"SELECT ID FROM TIPOCONTABANCARIA WHERE ID = @idConta"
            , new { idConta }
                ).FirstOrDefault();
        }
        public PropostaPCPO BuscarDetalhesDaProposta(string CGCCPF)
        {
            return Connection.Query<PropostaPCPO>
               (@"SELECT TRIM(PROPC.CLIENTE) AS CGCCPF, PROPO.FINDTEMISSAO AS DATAEMISSAO, PROPC.LOJA, PROPC.LOJISTA, PROPC.MIDIA, PROPC.PROPOSTA AS NROPROPOSTA, PROPO.IDPRODUTOSEGURO AS SEGURO,
                  PROPO.PLANO, PROPO.PRAZO, PROPO.FINDTPRIMVENCIM AS PRIMVENCIMENTO, PROPC.PRODUTO, PROPEFET.IDTIPOEFETIVACAO AS TIPOEFETIVACAO, PROPO.VALORCOMPRA AS VALOROPERACAO
                  FROM PROPOSTASCREDITO PROPC
                  INNER JOIN PROPOSTASOPERACOES PROPO ON PROPC.PROPOSTA = PROPO.PROPOSTA
                  INNER JOIN PROPOSTASTIPOEFETIVACAO PROPEFET ON PROPO.PROPOSTA = PROPEFET.PROPOSTA	                  
                  WHERE PROPC.CLIENTE = @CGCCPF
                  AND PROPO.VALORCOMPRA = PROPO.FINPRINCIPAL
                  AND PROPO.VALORCOMPRA = PROPO.VALORLIBERACAO
                  AND PROPC.LOJISTA = PROPO.LOJISTA
                  AND PROPC.LOJA =  PROPO.LOJA"
            , new { CGCCPF }
                ).FirstOrDefault();
        }
        public int BuscarSeguro(string proposta, string descSeguro)
        {
            return Connection.Query<int>
             (@"SELECT PO.IDPRODUTOSEGURO AS SEGURO
                FROM PROPOSTASOPERACOES PO
                JOIN PRODUTOSCDC PROD ON PO.PRODUTO = PROD.PRODUTO
                JOIN SEGURADORAS SEG ON PO.SEGURADORA = SEG.SEGURADORA
                JOIN PRODUTOSSEGURO PRODSEG ON PO.IDPRODUTOSEGURO = PRODSEG.IDPRODUTOSEGURO	             
                WHERE  PO.CONTRATO = @PROPOSTA 
                AND    PRODSEG.TIPOSEGURO = @descSeguro"
            , new { proposta, descSeguro }
                ).FirstOrDefault();

            //(@" SELECT CDC.IDPRODUTOSEGURO AS SEGURO
            //  FROM CONTRATOSCDC CDC
            //  JOIN PRODUTOSCDC PROD ON CDC.PRODUTO = PROD.PRODUTO
            //  JOIN SEGURADORAS SEG ON CDC.SEGURADORA = SEG.SEGURADORA
            //  JOIN PRODUTOSSEGURO PRODSEG ON CDC.IDPRODUTOSEGURO = PRODSEG.IDPRODUTOSEGURO	             
            //  WHERE  CDC.CONTRATO = @PROPOSTA 
            //  AND    PRODSEG.TIPOSEGURO = @descSeguro"

        }
        public Contrato BuscarParcelasDoContrato(string proposta, string prazo)
        {
            return Connection.Query<Contrato>
               (@"IF (SELECT COUNT(PARCELA) FROM PARCELAS P WHERE P.CONTRATO = @PROPOSTA) = @PRAZO 
                    BEGIN
	                    SELECT TOP 1 CDC.VALORCOMPRA AS ResultValorOperacao, PAR.PMT AS ResultValorParcela
	                    FROM PARCELAS PAR 
	                    JOIN CONTRATOSCDC CDC ON PAR.CONTRATO = CDC.CONTRATO
	                    JOIN PRODUTOSCDC PROD ON CDC.PRODUTO = PROD.PRODUTO	                   
	                    JOIN PROPOSTASOPERACOES PROPOP ON CDC.CONTRATO = PROPOP.CONTRATO
	                    WHERE CDC.CONTRATO = @PROPOSTA 	                    
                        AND CDC.IDPRODUTOSEGURO = PROPOP.IDPRODUTOSEGURO
	                    AND CDC.PRAZO = @PRAZO
	                    AND CDC.PLANO = PROPOP.PLANO
	                    AND CDC.PRODUTO = PROPOP.PRODUTO
	                    AND CDC.LOJA    = PROPOP.LOJA
	                    AND CDC.LOJISTA = PROPOP.LOJISTA
	                    AND CDC.VALORCOMPRA = PROPOP.FINPRINCIPAL                        
	                    AND CDC.PRESTACAO = PAR.PMT
                    END"
            , new { proposta, prazo }
                ).FirstOrDefault();
        }
        public Contrato BuscarPEParcelasDoContrato(string proposta, string prazo)
        {
            return Connection.Query<Contrato>
               (@"IF (SELECT COUNT(PARCELA) FROM PE_PARCELAS P WHERE P.CONTRATO = @PROPOSTA) = @PRAZO 
                    BEGIN
	                    SELECT TOP 1 CDC.VALORCOMPRA AS ResultValorOperacao, PAR.PMT AS ResultValorParcela
	                    FROM PE_PARCELAS PAR 
	                    JOIN PE_CONTRATOSCDC CDC ON PAR.CONTRATO = CDC.CONTRATO
	                    JOIN PRODUTOSCDC PROD ON CDC.PRODUTO = PROD.PRODUTO	                   
	                    JOIN PROPOSTASOPERACOES PROPOP ON CDC.CONTRATO = PROPOP.CONTRATO
	                    WHERE CDC.CONTRATO = @PROPOSTA 	                     
                        AND CDC.IDPRODUTOSEGURO = PROPOP.IDPRODUTOSEGURO
	                    AND CDC.PRAZO = @PRAZO
	                    AND CDC.PLANO = PROPOP.PLANO
	                    AND CDC.PRODUTO = PROPOP.PRODUTO
	                    AND CDC.LOJA    = PROPOP.LOJA
	                    AND CDC.LOJISTA = PROPOP.LOJISTA
	                    AND CDC.VALORCOMPRA = PROPOP.FINPRINCIPAL
	                    AND CDC.PRESTACAO = PAR.PMT
                    END"
            , new { proposta, prazo }
                ).FirstOrDefault();
        }
        public Cheque BuscarCheque(string proposta, string prazo, string cmc7)
        {
            return Connection.Query<Cheque>
               (@"IF (SELECT COUNT(PARCELA) FROM CHEQUES  WHERE CONTRATO = @PROPOSTA) = @PRAZO
                      SELECT CMC7 
                      FROM CHEQUES C
                      WHERE C.CONTRATO = @proposta
                      AND   C.CMC7 = @CMC7"
            , new { proposta, prazo, cmc7 }
                ).FirstOrDefault();
        }
        public string BuscarCCB(string tipocontrato, string codProduto)
        {
            return Connection.Query<string>
             (@"IF @TIPOCONTRATO = 'CCB'
	                SELECT MODELOCCB 
	                FROM  PRODUTOSCDC
	                WHERE PRODUTO = @CODPRODUTO
	                AND   MODELOCCB IN (SELECT CODIGO
						                FROM   CCBMODELOS
						                WHERE  CODIGO = MODELOCCB)	
                ELSE IF @TIPOCONTRATO = 'ACEITETELEFONE'
	                SELECT MODELOACEITEFONE 
	                FROM  PRODUTOSCDC
	                WHERE PRODUTO = @CODPRODUTO
	                AND   MODELOACEITEFONE IN (SELECT CODIGO
						                       FROM	  ACEITETELEFONEBMODELOS
							                   WHERE  CODIGO = MODELOACEITEFONE)              
                ELSE
                    SELECT MODELOCCB   
                    FROM  PRODUTOSCDC
                    WHERE PRODUTO = @CODPRODUTO
                    AND   MODELOACEITEFONE = MODELOCCB" //QUANDO AMBOS FOREM NULL => (MÚTUO)
            , new { tipocontrato, codProduto }
                ).FirstOrDefault();
        }
        public int BuscarSeguro(string proposta, string descSeguro, string produto)
        {
            return Connection.Query<int>
             (@"IF @PRODUTO = '001035' OR @PRODUTO = '001040'  
                 SELECT CDC.IDPRODUTOSEGURO AS SEGURO
                 FROM PE_CONTRATOSCDC CDC
	             JOIN PRODUTOSCDC PROD ON CDC.PRODUTO = PROD.PRODUTO
	             JOIN SEGURADORAS SEG ON CDC.SEGURADORA = SEG.SEGURADORA
	             JOIN PRODUTOSSEGURO PRODSEG ON CDC.IDPRODUTOSEGURO = PRODSEG.IDPRODUTOSEGURO	             
	             WHERE  CDC.CONTRATO = @PROPOSTA                  
	             AND    PRODSEG.TIPOSEGURO = @descSeguro
                ELSE 
                 SELECT CDC.IDPRODUTOSEGURO AS SEGURO
                 FROM CONTRATOSCDC CDC
	             JOIN PRODUTOSCDC PROD ON CDC.PRODUTO = PROD.PRODUTO
	             JOIN SEGURADORAS SEG ON CDC.SEGURADORA = SEG.SEGURADORA
	             JOIN PRODUTOSSEGURO PRODSEG ON CDC.IDPRODUTOSEGURO = PRODSEG.IDPRODUTOSEGURO	             
	             WHERE  CDC.CONTRATO = @PROPOSTA                  
	             AND    PRODSEG.TIPOSEGURO = @descSeguro"
            , new { proposta, descSeguro, produto }
                ).FirstOrDefault();
        }
        public dynamic BuscarInfoContratoRenegociado(string contrato)
        {
            return Connection.Query<dynamic>
             (@"SELECT top 1 CDC.CONTRATO as CONTRATO
		        , QTDPARCELAS 
				 ,(
			        (SELECT CONVERT(VARCHAR(10), 
			  	        (SELECT  PO.FINDTEMISSAO 
			  		    FROM PROPOSTASOPERACOES PO 
			  		    WHERE PO.PROPOSTA = @CONTRATO
			  		    ), 103 )
			        )
                )  AS EMISSAO
		        ,CDC.PRINCIPAL AS VALORFINANCIADO
		        ,CDC.TOTALPARCELAS AS VALORPARCELAS	
		        ,(
			        (SELECT CONVERT(VARCHAR(10), 
			  	        (SELECT  PAR.VCTO 
			  		    FROM PARCELAS PAR 
			  		    WHERE PAR.CONTRATO = @CONTRATO
			  		    AND cdc.QTDPARCELAS = par.parcela
			  		    ), 103 )
			        )
                )  AS VENCIMENTO
                FROM    CONTRATOSCDC CDC
                INNER JOIN PARCELAS PAR 
                    ON PAR.CONTRATO = CDC.CONTRATO    	                    
                WHERE   CDC.DIASATRASO = 0
                        AND CDC.SITUACAO  = 'A' 
                        AND CDC.EMPRESA = '01'
                        AND CDC.AGENCIA = '0001'
		                AND CDC.CONTRATO = @CONTRATO"
                    , new { contrato }
                ).FirstOrDefault();
        }

        public PropostaGarantia BuscarBens(string nroProposta)
        {
            return Connection.Query<PropostaGarantia>
               (@"SELECT	PROPBENS.NOVOUSADO AS ZEROKM, MARC.MARCA, VEI.VEICULO as descricao, PROPBENS.COMPLEMENTO as combustivel, PROPBENS.PLACA,
		                    PROPBENS.ANO, PROPBENS.MODELO, PROPBENS.COR, PROPBENS.RENAVAM, PROPBENS.VALORORIGINAL as valor, PROPBENS.CHASSI
                    FROM	PROPOSTASBENS PROPBENS
                    INNER JOIN MARCAS MARC
	                    ON PROPBENS.MARCA = MARC.MARCA
                    INNER JOIN VEICULOS VEI
	                    ON MARC.MARCA = VEI.MARCA
                    WHERE	PROPOSTA = @nroProposta"
                    , new { nroProposta }
                ).FirstOrDefault();
        }

        public Liberacao BuscarLiberacao(string nroProposta)
        {
            return Connection.Query<Liberacao>
               (@"SELECT TIPOPESSOA, FORMA AS FORMALIB, TRIM(CGCCPF) AS CGCCPFProprietarioBem, BENEFICIARIO, BANCO, TRIM(AGENCIACONTA) AS AGENCIA, 
                         TIPOCONTABANCO AS TIPOCONTABANCARIA, CONTA, VALOR AS VALORLIBERACAO
                  FROM LIBERACAO  
                  WHERE CONTRATO = @nroProposta"
                    , new { nroProposta }
                ).FirstOrDefault();
        }

        public void VincularUsuarioSafeDocSiteCP(string usuarioLogado, string usuarioSafeDoc)
        {
            Connection.Query
                (@"DISABLE TRIGGER [dbo].TIU_USUARIOS ON [dbo].USUARIOS; 
                   UPDATE USUARIOS 
                   SET CPF = @usuarioSafeDoc
                   WHERE USUARIO = @usuarioLogado;
                   ENABLE TRIGGER [dbo].TIU_USUARIOS ON [dbo].USUARIOS;"
            , new { usuarioLogado, usuarioSafeDoc });
        }

        public void AtualizarDctoEnviadoSafeDoc(string usuarioLogado, string proposta)
        {
            /*PARAMETRIZAÇÃO DO MODELOCHECKLISTA 22 PARA O PRODUTO EP FATURA*/
            Connection.Query
                (@" DECLARE @MODELOCHECKLISTCCB varchar(2) = '22'
                    BEGIN
                        UPDATE PRODUTOSCDC 
	                    SET    MODELOCHECKLISTCCB = @MODELOCHECKLISTCCB
	                    WHERE  FAMILIAPRODUTO = 'EPFATURA'                    
​
                        /*ATUALIZAÇÃO DA OBRIGATORIEDADE DO APENAS UM DOCUMENTO: IDDOCUMENTO 224 IDCHECKLIST:22*/	
                        UPDATE DOCUMENTOS_CHECKLIST_PROPOSTAS
                        SET    OBRIGATORIO = 'S'  
                        where  IDCHECKLIST = @MODELOCHECKLISTCCB 
                        and    IDDOCUMENTO = 224
​
                        UPDATE DOCUMENTOS_CHECKLIST_PROPOSTAS
                        SET    OBRIGATORIO = 'N'  
                        where  IDCHECKLIST = @MODELOCHECKLISTCCB 
                        and    IDDOCUMENTO in (220,221,222,223)
​
                        DECLARE @PROX_ID_DOCTO_ENVIADO_SAFEDOC BIGINT = (SELECT TOP 1 (IdDocumentoSafeDoc + 1) FROM DocumentosEnviados_SafeDoc ORDER BY IdDocumentoSafeDoc DESC);
                        /*INSERIR UM NOVO REGISTRO PARA O DOCUMENTO QUE SERÁ ENVIADO PARA SAFEDOC*/
	                        INSERT INTO DocumentosEnviados_SafeDoc (IDDOCUMENTOSAFEDOC, DATAENVIOSAFEDOC, HAShFILE, NOMEARQUIVO, EXTENSAO)
	                        VALUES (@PROX_ID_DOCTO_ENVIADO_SAFEDOC, GETDATE(), 'ab50e5db156e34574e92a48c44e2ddac', 'Comprovante de residência - Conta de Luz', 'jpg')
​
                        /*ATUALIZAR NA PROPOSTA O DOCUMENTO CRIADO NA TABELA ACIMA DocumentosEnviados_SafeDoc*/
	                        UPDATE  CHECKLIST_PROPOSTAS
	                        SET		USUARIO = @usuarioLogado,
	     	                        PREENCHIDO = 'S', 
			                        DATAPREENCHIMENTO = GETDATE(),
			                        IdDocumentosEnviados_SafeDoc = (SELECT ID
											                        FROM	DocumentosEnviados_SafeDoc
											                        WHERE	ID = (SELECT IDENT_CURRENT ('DocumentosEnviados_SafeDoc') AS Current_Identity))
	                        where	proposta = @PROPOSTA 
	                        AND		IDCHECKLIST = @MODELOCHECKLISTCCB
	                        AND		IDDOCUMENTO = 224;
                    END"
            , new { usuarioLogado, proposta });
        }

        //public void AtualizarDctoEnviadoSafeDoc(string usuarioLogado, string proposta)
        //{
        //    /*PARAMETRIZAÇÃO DO MODELOCHECKLISTA 22 PARA O PRODUTO EP FATURA*/
        //    Connection.Query
        //        (@" DECLARE @MODELOCHECKLISTCCB INT = 22
        //            DECLARE @CODPRODUTO INT = 001038

        //            IF (select MODELOCHECKLISTCCB from PRODUTOSCDC where  produto = @CODPRODUTO) <> @MODELOCHECKLISTCCB
        //            BEGIN
        //             UPDATE PRODUTOSCDC 
        //             SET MODELOCHECKLISTCCB = @MODELOCHECKLISTCCB
        //             WHERE PRODUTO = @CODPRODUTO
        //            END

        //            /*ATUALIZAÇÃO DA OBRIGATORIEDADE DO APENAS UM DOCUMENTO: IDDOCUMENTO 224 IDCHECKLIST:22*/	
        //            UPDATE DOCUMENTOS_CHECKLIST_PROPOSTAS
        //            SET    OBRIGATORIO = 'S'  
        //            where  IDCHECKLIST = @MODELOCHECKLISTCCB 
        //            and    IDDOCUMENTO = 224

        //            UPDATE DOCUMENTOS_CHECKLIST_PROPOSTAS
        //            SET    OBRIGATORIO = 'N'  
        //            where  IDCHECKLIST = @MODELOCHECKLISTCCB 
        //            and    IDDOCUMENTO in (220,221,222,223,224)

        //            DECLARE @PROX_ID_DOCTO_ENVIADO_SAFEDOC BIGINT = (SELECT TOP 1 (IdDocumentoSafeDoc + 1) FROM DocumentosEnviados_SafeDoc ORDER BY IdDocumentoSafeDoc DESC);
        //            /*INSERIR UM NOVO REGISTRO PARA O DOCUMENTO QUE SERÁ ENVIADO PARA SAFEDOC*/
        //             INSERT INTO DocumentosEnviados_SafeDoc (IDDOCUMENTOSAFEDOC, DATAENVIOSAFEDOC, HAShFILE, NOMEARQUIVO, EXTENSAO)
        //             VALUES (@PROX_ID_DOCTO_ENVIADO_SAFEDOC, GETDATE(), 'ab50e5db156e34574e92a48c44e2ddac', 'Comprovante de residência - Conta de Luz', 'jpg')

        //            /*ATUALIZAR NA PROPOSTA O DOCUMENTO CRIADO NA TABELA ACIMA DocumentosEnviados_SafeDoc*/
        //             UPDATE  CHECKLIST_PROPOSTAS
        //             SET		USUARIO = @usuarioLogado,
        //                   PREENCHIDO = 'S', 
        //               DATAPREENCHIMENTO = GETDATE(),
        //               IdDocumentosEnviados_SafeDoc = (SELECT ID
        //			                    FROM	DocumentosEnviados_SafeDoc
        //			                    WHERE	ID = (SELECT IDENT_CURRENT ('DocumentosEnviados_SafeDoc') AS Current_Identity))
        //             where	proposta = @PROPOSTA 
        //             AND		IDCHECKLIST = @MODELOCHECKLISTCCB
        //             AND		IDDOCUMENTO = 224;"
        //    , new { usuarioLogado, proposta });
        //}

        public dynamic BuscarDoctoObrigatoriosEnviados(string usuarioLogado, string proposta)
        {
            return Connection.Query<dynamic>
                (@"declare @IDCHECKLIST int = 22
                   declare @IDDOCUMENTO int = 224
                   select  ckprop.idchecklist, ckprop.iddocumento, ckprop.proposta, ckprop.usuario, ckprop.preenchido, ckprop.datapreenchimento, 
		                   ckprop.iddocumentosenviados_safedoc, safedoc.iddocumentosafedoc, safedoc.dataenviosafedoc, safedoc.hashfile, safedoc.nomearquivo, safedoc.extensao
                   from	CHECKLIST_PROPOSTAS  ckprop
                   inner join DocumentosEnviados_SafeDoc safedoc
	                   on ckprop.IdDocumentosEnviados_SafeDoc = safedoc.id
                   where   ckprop.proposta = @proposta
                   AND	   ckprop.IDCHECKLIST = @IDCHECKLIST
                   AND	   ckprop.IDDOCUMENTO = @IDDOCUMENTO
                   AND     ckprop.USUARIO =  @usuarioLogado"
                    , new { usuarioLogado, proposta }
                ).FirstOrDefault();
        }


        public void RemoverDoctosEnviadoSafeDOc(string proposta)
        {
            /*PARAMETRIZAÇÃO DO MODELOCHECKLISTA 22 PARA O PRODUTO EP FATURA*/
            Connection.Query
                    (@"DECLARE @MODELOCHECKLISTCCB INT = 22
                   DECLARE @iddocumentoenviados_safedoc bigint = (select	IdDocumentosEnviados_SafeDoc
				   								                from	CHECKLIST_PROPOSTAS 
				   								                where	proposta = @proposta										        														
				   								                AND		IDCHECKLIST = @MODELOCHECKLISTCCB
				   								                AND		IDDOCUMENTO = 224)
                   /*1 REMOÇÃO DO DOCUMENTO ENVIADO SAFEDOC*/
                   DELETE DOCUMENTOSENVIADOS_SAFEDOC 
                   WHERE ID IN (SELECT	IdDocumentosEnviados_SafeDoc
			                       FROM	CHECKLIST_PROPOSTAS
			                       where	proposta = @proposta
			                    AND	IDCHECKLIST = @MODELOCHECKLISTCCB
			                    AND	IDDOCUMENTO = 224
			                    AND    IdDocumentosEnviados_SafeDoc = @iddocumentoenviados_safedoc)		

                   /*2 DESVINCULAR O DOCUMENTO À TABELA CHECKLIST_PROPOSTAS*/
                   UPDATE   CHECKLIST_PROPOSTAS
	               SET		USUARIO = '', 	
	     	                PREENCHIDO = '', 
			                DATAPREENCHIMENTO = '',
			                IdDocumentosEnviados_SafeDoc = @iddocumentoenviados_safedoc
	               WHERE	proposta = @proposta	
	               AND		IDCHECKLIST = @MODELOCHECKLISTCCB
	               AND	    IDDOCUMENTO = 224"
                , new { proposta });
        }

        public int BuscarConcessionaria(string NomeConcessionaria)
        {
            return Connection.Query<int>
             (@" SELECT IDCONCESSIONARIA
                 FROM CONCESSIONARIAS 
	             WHERE NOME = @NomeConcessionaria"
            , new { NomeConcessionaria }
                ).FirstOrDefault();
        }

        public dynamic BuscarDadosProposta(string nrProposta)
        {
            return Connection.Query<dynamic>
             (@"select *from PROPOSTASCREDITO WHERE PROPOSTA = @nrProposta"
                    , new { nrProposta }
              ).FirstOrDefault();
        }

        public PropostaConcessionaria BuscarPropostaConcessionaria(string nroProposta)
        {
            return Connection.Query<PropostaConcessionaria>
               (@"SELECT NUMEROCLIENTE, DVNUMEROCLIENTE, IDCONCESSIONARIA, CODIGOREGIAO, cpf
                  FROM   PROPOSTASCONCESSIONARIAS
                  WHERE  PROPOSTASICRED = @nroProposta
                  AND	 PROPOSTASICRED = CONTRATOSICRED"
            , new { nroProposta }
                ).FirstOrDefault();
        }

        public void AtualizaAmbSiteCPEnelRegressivo(string UrlAmbRegressivo)
        {
            Connection.Query
               (@"UPDATE PARAMETROS_GERAIS
	              SET CONTEUDO = @UrlAmbRegressivo
	              WHERE NOME = 'URL_GATE_ENEL'"
            , new { UrlAmbRegressivo });
        }

        public dynamic BuscaClienteSemContratoEfetivado(string Produto)
        {
          return Connection.Query<dynamic>
                (@"SELECT top 1 *FROM PROPOSTASCREDITO  WHERE SITUACAO !=20 
                    and produto = @Produto ORDER BY DATAINCLUSAO DESC;", new { Produto }).FirstOrDefault();
        }

        public string BuscaCPFClienteComContratoEfetivado(string Produto)
        {
            return Connection.Query<string>
                (@"SELECT TOP 1 cliente FROM PROPOSTASCREDITO C1 WHERE C1.situacao = 20
                   AND NOT EXISTS (SELECT 1 FROM PROPOSTASCREDITO C2 WHERE C2.produto != @Produto and C1.Cliente = C2.Cliente)
                   GROUP BY C1.cliente HAVING COUNT(C1.cliente) = 2 
                   ORDER BY MAX(DATAEFETIVACAO) DESC", new { Produto }).FirstOrDefault();

        }





    }
}

