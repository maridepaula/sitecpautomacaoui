﻿using Dapper;
using MySql.Data.MySqlClient;

namespace UiTestSiteCP.DataBase
{
    public class InicialRepositoryEnelMock : RepositoryBase
    {

        public InicialRepositoryEnelMock(MySqlConnection connection) : base(connection)
        { }


        public void IncluirClienteMockRegressivo(string cpfcgc, string regiao, string numeroCliente, int dvNumeroCliente, string lote)
        {
            if (regiao == "CELG")
            {
                Connection.Query
                (@"INSERT INTO cliente (api, nome, numero_cliente, regiao_atendimento, data_nascimento, cep, ddd, telefone, cpf, data_ultimo_faturamento, estado_cliente, estado_faturamento, estado_fornecimento, data_hora_ultima_atualizacao, lote_faturamento, media_consumo)
                        values (1, 'TESTE AUTOMACAO ENEL', @numeroCliente, @regiao, '1952-03-11', '24440451', '', '', @cpfcgc, '2019-11-23', '0', 'N', '0', '2019-11-24 13:51:15', @lote, 0.00);"
                , new { cpfcgc, regiao, numeroCliente, dvNumeroCliente, lote });
            }
            else
            {
                Connection.Query
                (@"INSERT INTO cliente (api, nome, numero_cliente, regiao_atendimento, dv_numero_cliente, data_nascimento, cep, ddd, telefone, cpf, data_ultimo_faturamento, estado_cliente, estado_faturamento, estado_fornecimento, data_hora_ultima_atualizacao, lote_faturamento, media_consumo)
                        values (1, 'TESTE AUTOMACAO ENEL', @numeroCliente, @regiao, @dvNumeroCliente, '1952-03-11', '24440451', '', '', @cpfcgc, '2019-11-23', '0', 'N', '0', '2019-11-24 13:51:15', @lote, 0.00);"
                , new { cpfcgc, regiao, numeroCliente, dvNumeroCliente, lote });
            }
        }

        public void RemoverClienteMockRegressivo(string cpfcgc)

        {
            Connection.Query
                (@"DELETE FROM cliente 
                   WHERE  cpf = @cpfcgc"
            , new { cpfcgc});

           // Connection.Query
           //    (@"DELETE FROM cliente 
           //        WHERE  numero_cliente = @numerocliente                  
           //        AND    regiao_atendimento = @regiao
           //        AND    cpf = @cpfcgc
           //        AND    lote_faturamento = @lote;"
           //, new { cpfcgc, regiao, numeroCliente, lote });
        }

        public dynamic TesteConexaoBaseMockEnel()
        {
            return Connection.Query<dynamic>
                 (@"select *from cliente where cpf = '12345678909'");
        }

    }
}

