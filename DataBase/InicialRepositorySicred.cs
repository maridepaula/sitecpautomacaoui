using System.Data.SqlClient;
using System.Linq;
using Dapper;
using System;

namespace UiTestSiteCP.DataBase
{
    class InicialRepositorySicred : RepositoryBase
    {
        public InicialRepositorySicred(SqlConnection connection) : base(connection)
        { }
        //public dynamic LimparDadosBase(string CPF)
        //{
        //    return Connection.Query<dynamic>
        //        (@"SELECT Login FROM SegUsuario WHERE Login LIKE '%AUTOMACAO.CWI%';").FirstOrDefault();
        //}



        public void CriarUsuario(string userLogin)
        {
            Connection.Query
                (@"DECLARE @USUARIO VARCHAR(10) = (SELECT USUARIO FROM USUARIOS WHERE USUARIO = @userLogin)
                   IF @USUARIO IS NULL
	                  INSERT INTO USUARIOS (EMPRESA, USUARIO, NOME, SENHA, NIVEL, SETOR, GRUPO, TIPOUSUARIO, DATASENHA, VALIDADE, BLOQUEIOSENHA, BLOQUEIOGRUPO, DATARENOVA, DIASRENOVA, ULTIMASENHA, DIFERENCIADO, USUARIOAGENCIA, AGENCIA, LOJISTA, LOJA, usuarioloja, PrimeiroAcessoSenha, NTENTATIVAS, CPF, EMAIL, PROMOTORA, FILIALPROMOTORA)
	                  VALUES ('01', @userLogin, 'Automacao', '687;8>',9, '0001', 'INFO', 'S', '2018-01-01', 9999, 0, NULL, '2018-01-01', 10, NULL, 0, 'S', '0001', '000000', '0000', NULL, 'N', 0, '02734834887', 'automacao@cwi.com.br', '', '');"
                , new { userLogin });
        }

        public void LimparDadosBase(string cpf)
        {
            try
            {
                Connection.Query
                    (@" DISABLE TRIGGER [dbo].TIU_PROPOSTASCREDITO ON [dbo].PROPOSTASCREDITO; 
                        DISABLE TRIGGER [dbo].TIUD_PROPOSTASCREDITO_2 ON [dbo].PROPOSTASCREDITO;
                        DISABLE TRIGGER [dbo].[TD_DELETE_PROPOSTASOPERACOES] ON [dbo].[PROPOSTASOPERACOES];

                        DECLARE @cliente VARCHAR(12) = @cpf + '1' 
                        DECLARE @contrato VARCHAR(12)
                        DECLARE @nrProposta INT = (SELECT COUNT(*) FROM PROPOSTASCREDITO WHERE CLIENTE = @cliente)
                        WHILE(@nrProposta > 0 )
                        BEGIN
                            SET @contrato = (select top 1 PROPOSTA FROM PROPOSTASCREDITO WHERE CLIENTE = @cliente)
                            DELETE PARCELAS WHERE CONTRATO = @contrato
                            DELETE CONTRATOSCDC WHERE CONTRATO = @contrato
                            DELETE CHEQUES where CONTRATO = @contrato;
                            DELETE PE_PARCELAS WHERE CONTRATO = @contrato; 
                            DELETE PE_CONTRATOSCDC WHERE CONTRATO = @contrato;
                            DELETE PE_CHEQUES where CONTRATO = @contrato;
                            DELETE PROPOSTASOPERACOES WHERE PROPOSTA = @contrato
                            DELETE PROPOSTASLOGSITUACAO  WHERE PROPOSTA = @contrato
                            SET @nrProposta -= 1
                        END
                        DELETE LOGANALISEPROPOSTA    WHERE PROPOSTA = @contrato; 	
                        DELETE CHECKLIST_PROPOSTAS	 WHERE PROPOSTA = @contrato; 
                        DELETE PROPOSTASCREDITO WHERE CLIENTE = @cliente;
                        DELETE COMPLEMENTOS WHERE CLIENTE = @cliente;
                        DELETE CLIENTES WHERE CLIENTE = @cliente; 
                        DELETE PROPOSTASTIPOEFETIVACAO WHERE PROPOSTA = @contrato;
                        DELETE LIBERACAO WHERE CONTRATO = @contrato;
                        DELETE PROPOSTASBENS WHERE PROPOSTA = @contrato;
                        DELETE PROPOSTASCONCESSIONARIAS WHERE cpf = @cpf;    
                        
                        ENABLE TRIGGER [dbo].TIU_PROPOSTASCREDITO ON [dbo].PROPOSTASCREDITO;
                        ENABLE TRIGGER [dbo].TIUD_PROPOSTASCREDITO_2 ON [dbo].PROPOSTASCREDITO;
                        ENABLE TRIGGER [dbo].[TD_DELETE_PROPOSTASOPERACOES] ON [dbo].[PROPOSTASOPERACOES];                        
                    ", new { cpf });
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        public void AtualizarSituacaoPropostaBase(string proposta, int situacao)
        {
            Connection.Query
                       (@"UPDATE PROPOSTASCREDITO 
                        SET SITUACAO = @situacao, APROVADOR = 'MASTER'
                        WHERE PROPOSTA = @proposta
                    ", new { proposta, situacao });
        }

        public string ObterDataMovimento()
        {   //obter regra no m�dulo Sicred
            return Connection.QueryFirst<string>
                (@"DECLARE @DATAMOVIMENTO AS DATETIME = (SELECT DATAMOVIMENTO FROM PARAMETROS)
                    IF((SELECT Count(*) FROM ENCERRAMENTODIARIODOCS WHERE EMPRESA = 01 AND DATAENCERRADA = (SELECT DATAMOVIMENTO FROM PARAMETROS)  AND REABERTO = 'N') = 1
                        OR((SELECT PROXIMADIARIA FROM PARAMETROS) != @DATAMOVIMENTO))
                            SELECT CONVERT(VARCHAR(10), (SELECT DATAMOVIMENTO FROM PARAMETROS) +1, 103);
                    ELSE
                        SELECT CONVERT(VARCHAR(10), (SELECT DATAMOVIMENTO FROM PARAMETROS), 103 ); ");
            //(@"DECLARE @DATAMOVIMENTO AS DATETIME = (SELECT DATAMOVIMENTO FROM PARAMETROS)
            //   IF (SELECT PROXIMADIARIA FROM PARAMETROS) = @DATAMOVIMENTO
            //    SELECT CONVERT(VARCHAR(10), (SELECT DATAMOVIMENTO FROM PARAMETROS), 103 );
            //   ELSE IF
            //   	SELECT DATAENCERRADA   
            //       FROM ENCERRAMENTODIARIODOCS WHERE EMPRESA = 01  AND DATAENCERRADA = (SELECT DATAMOVIMENTO FROM PARAMETROS)  AND REABERTO = 'N'
            //   ELSE

            //    SELECT CONVERT(VARCHAR(10), (SELECT DATAMOVIMENTO FROM PARAMETROS) + 1, 103);");

        }

        public bool BuscaFeriado(DateTime Data)
        {
            return Connection.Query<bool>
           (@"  IF(select COUNT(*) from feriados where empresa = 01 AND DATA = @Data) = 0 
                select 0
                ELSE
                select 1", new { Data }
            ).FirstOrDefault();

        }

        public dynamic BuscaDadosPropostaExistente(int Situacao, string Produto, int Dias)
        {
            return Connection.Query<dynamic>(@"DECLARE @DATAMOVIMENTO AS DATETIME = (SELECT DATAMOVIMENTO FROM PARAMETROS)
                                                SELECT TOP 1 *FROM PROPOSTASCREDITO PC1 
                                                WHERE Produto = @Produto 
                                                --AND DataEfetivacao between @DATAMOVIMENTO-@Dias and @DATAMOVIMENTO
                                                AND EXISTS (SELECT 1 FROM PROPOSTASCREDITO PC2 WHERE PC1.CLIENTE  = PC2.CLIENTE AND situacao = @Situacao
                                                            AND NOT EXISTS( SELECT 1 FROM PROPOSTASCREDITO PC3 WHERE PC2.CLIENTE = PC3.CLIENTE GROUP BY cliente HAVING COUNT(cliente) <=4 ) )
                                                --ORDER BY DataEfetivacao DESC	
		                                        AND DataEfetivacao between @DATAMOVIMENTO-@Dias and @DATAMOVIMENTO	                                    
                                                ",
                                        new { Produto, Situacao, Dias }).FirstOrDefault();
        }

        public dynamic BuscaDadosPropostaExistenteEmSimulacao(int Situacao, string Produto, int Etapa)
        {
            return Connection.Query<dynamic>(@" 
                                                DECLARE @ValorMaximoFinanciamento NUMERIC(9) = (SELECT TOP 1 isnull(ValorMaximoFinanciamento,0.0)
												                                                 FROM DebitoContaParametros 
												                                                 WHERE EMPRESA  = 01
												                                                 AND FamiliaProduto   = 'DEBITO'
												                                                 AND (SELECT DATAMOVIMENTO FROM PARAMETROS) BETWEEN VIGENCIAINICIAL AND VIGENCIAFINAL)
                                                DECLARE @ValorMinimoFinanciamento NUMERIC(9) = (SELECT TOP 1 isnull(ValorMinimoOperacao,0.0)  
												                                                 FROM DebitoContaParametros 
												                                                 WHERE EMPRESA  = 01
												                                                 AND FamiliaProduto   = 'DEBITO'
												                                                 AND (SELECT DATAMOVIMENTO FROM PARAMETROS) BETWEEN VIGENCIAINICIAL AND VIGENCIAFINAL)
                                                DECLARE @PROPOSTA DECIMAL = (
                                                                            SELECT TOP 1 PC1.PROPOSTA FROM PROPOSTASCREDITO PC1
                                                                            INNER JOIN PROPOSTASOPERACOES PO
                                                                            ON PC1.PROPOSTA = PO.PROPOSTA
                                                                            INNER JOIN COMPLEMENTOS COMP
                                                                            ON PC1.CLIENTE = COMP.CLIENTE
                                                                            WHERE PC1.etapa = @Etapa
                                                                            AND PC1.situacao = @Situacao
                                                                            AND PC1.produto = @Produto
                                                                            AND PO.FINDTPRIMVENCIM > (SELECT DATAMOVIMENTO FROM PARAMETROS)
                                                                            AND PO.FINDTEMISSAO < (SELECT DATAMOVIMENTO FROM PARAMETROS)
                                                                            AND PO.PRAZO <> 0
							                                                --AND PO.FINPRINCIPAL BETWEEN @ValorMinimoFinanciamento AND   @ValorMaximoFinanciamento
							                                                --AND PO.VALORCOMPRA BETWEEN @ValorMinimoFinanciamento AND @ValorMaximoFinanciamento
                                                                            AND NOT EXISTS (SELECT 1 FROM PropostasCredito PC2 WITH (NOLOCK)
                                                                                             WHERE pC2.Empresa = 01 
                                                                                             AND pC2.Situacao NOT IN('01','20','07','17','37','38','41','60','61','64','99') 
                                                                                             AND PC1.CLIENTE = PC2.CLIENTE
                                                                                                )
                                                                            ORDER BY PC1.datainclusao
                                                                            )
                                                --UPTADE VWCP_BuscarProposta SET ValorCompra = @ValorMaximoFinanciamento - 100 WHERE PROPOSTA = @PROPOSTA 
                                                UPDATE LOJISTAS SET SSO ='N' WHERE LOJISTA = (SELECT LOJISTA FROM PROPOSTASCREDITO WHERE PROPOSTA = @PROPOSTA)                                                  
                                                SELECT TOP 1 *FROM PROPOSTASCREDITO WHERE PROPOSTA = @PROPOSTA                                   
                                                ",
                                        new { Produto, Situacao, Etapa }).FirstOrDefault();
        }

        public dynamic BuscaDadosPropostaExistenteStatusPendente(int Situacao, string Produto, int Etapa)
        {
            return Connection.Query<dynamic>(@"
                                                            DECLARE @cliente VARCHAR(12) = (SELECT tOP 1 PC1.CLIENTE FROM PROPOSTASCREDITO PC1
								                                                             INNER JOIN PROPOSTASOPERACOES PO
								                                                             ON PC1.PROPOSTA = PO.PROPOSTA
								                                                             INNER JOIN COMPLEMENTOS CLP
								                                                             ON PC1.CLIENTE = CLP.CLIENTE
								                                                             WHERE PC1.situacao = @Situacao 
								                                                             AND CLP.ESTCIVIL IN('S','V','D')
								                                                             AND PO.PRAZO <> 0 
								                                                             AND PC1.produto = '000712'
								                                                             AND PC1.etapa = @Etapa
								                                                             AND PO.FINDTPRIMVENCIM > (SELECT DATAMOVIMENTO FROM PARAMETROS) 
								                                                             AND PO.FINDTEMISSAO < (SELECT DATAMOVIMENTO FROM PARAMETROS)
								                                                             AND LEN(CLP.REFPESSNOME1)>=2
								                                                             AND LEN(CLP.REFPESSREL1)>=2 
								                                                             AND LEN(CLP.REFPESSDDD1)>=2
								                                                             AND LEN(CLP.REFPESSFONE1)>=2 
								                                                             AND LEN(CLP.REFPESSNOME2)>=2
								                                                             AND LEN(CLP.REFPESSREL2)>=2
								                                                             AND LEN(CLP.REFPESSDDD2)>=2
								                                                             AND LEN(CLP.REFPESSFONE2)>=2
								                                                             AND NOT EXISTS (SELECT 1 FROM PROPOSTASCREDITO PC2 WHERE PC1.CLIENTE = PC2.CLIENTE  GROUP BY CLIENTE HAVING count (PC2.CLIENTE) >= 2)                                                           
								                                                             ORDER BY DATAINCLUSAO DESC
								                                                             ) 
                                                            DECLARE @PropostaQ decimal = (select top 1 PROPOSTA FROM PROPOSTASCREDITO WHERE CLIENTE = @Cliente AND SITUACAO = @Situacao  AND etapa = @Etapa);
                                                            DECLARE @DiasCarenciaPlano INT = (SELECT CarenciaMinima FROM Planos WHERE plano = (SELECT Plano FROM PROPOSTASOPERACOES WHERE PROPOSTA = @PropostaQ));
                                                            UPDATE PROPOSTASOPERACOES SET FINDTPRIMVENCIM = (SELECT DATAMOVIMENTO+@DiasCarenciaPlano FROM PARAMETROS) WHERE Proposta = @PropostaQ
                                                            UPDATE PROPOSTASOPERACOES SET PRIMVENCIMVRG = (SELECT DATAMOVIMENTO+@DiasCarenciaPlano FROM PARAMETROS) WHERE Proposta = @PropostaQ
                                                            SELECT TOP 1 * FROM PROPOSTASCREDITO WHERE CLIENTE = @Cliente AND PROPOSTA = @PropostaQ;                                      
                                                            ",
                                        new { Produto, Situacao, Etapa }).FirstOrDefault();
        }

        public dynamic BuscaDadosPropostaExistenteStatusComplementarCadastro(int Situacao, string Produto, int Etapa)
        {
            return Connection.Query<dynamic>(@" DECLARE @cliente VARCHAR(12) = (SELECT tOP 1 PC1.CLIENTE FROM PROPOSTASCREDITO PC1
								                                                             INNER JOIN PROPOSTASOPERACOES PO
								                                                             ON PC1.PROPOSTA = PO.PROPOSTA
								                                                             INNER JOIN COMPLEMENTOS CLP
								                                                             ON PC1.CLIENTE = CLP.CLIENTE
								                                                             WHERE PC1.situacao = @Situacao 
								                                                             AND CLP.ESTCIVIL IN('S','V','D')
								                                                             AND PO.PRAZO <> 0 
								                                                             AND PC1.produto = '000712'
								                                                             AND PC1.etapa = @Etapa
								                                                             AND PO.FINDTPRIMVENCIM > (SELECT DATAMOVIMENTO FROM PARAMETROS) 
								                                                             AND PO.FINDTEMISSAO < (SELECT DATAMOVIMENTO FROM PARAMETROS)								                                          
								                                                             AND NOT EXISTS (SELECT 1 FROM PROPOSTASCREDITO PC2 WHERE PC1.CLIENTE = PC2.CLIENTE  GROUP BY CLIENTE HAVING count (PC2.CLIENTE) >= 2)                                                           
								                                                             ORDER BY DATAINCLUSAO DESC
								                                                             ) 
                                                            DECLARE @PropostaQ decimal = (select top 1 PROPOSTA FROM PROPOSTASCREDITO WHERE CLIENTE = @Cliente AND SITUACAO = @Situacao  AND etapa = @Etapa);
                                                            DECLARE @DiasCarenciaPlano INT = (SELECT CarenciaMinima FROM Planos WHERE plano = (SELECT Plano FROM PROPOSTASOPERACOES WHERE PROPOSTA = @PropostaQ));
                                                            UPDATE PROPOSTASOPERACOES SET FINDTPRIMVENCIM = (SELECT DATAMOVIMENTO+@DiasCarenciaPlano FROM PARAMETROS) WHERE Proposta = @PropostaQ
                                                            UPDATE PROPOSTASOPERACOES SET PRIMVENCIMVRG = (SELECT DATAMOVIMENTO+@DiasCarenciaPlano FROM PARAMETROS) WHERE Proposta = @PropostaQ
                                                            UPDATE COMPLEMENTOS  
                                                                             SET 
                                                                             REFPESSNOME1= ''
                                                                            ,REFPESSREL1 = ''
                                                                            ,REFPESSDDD1 = ''
                                                                            ,REFPESSFONE1= ''
                                                                            ,REFPESSNOME2= ''
                                                                            ,REFPESSREL2 = ''
                                                                            ,REFPESSDDD2 = ''
                                                                            ,REFPESSFONE2= ''
                                                                            WHERE CLIENTE = @Cliente
                                                            SELECT TOP 1 * FROM PROPOSTASCREDITO WHERE CLIENTE = @Cliente AND PROPOSTA = @PropostaQ;                                      
                                                            ",
                                        new { Produto, Situacao, Etapa }).FirstOrDefault();
        }

        public dynamic BuscaDadosPropostaExistenteStatusPreEfetivado(int Situacao, string Produto, int Etapa)
        {
            return Connection.Query<dynamic>(@" DECLARE @cliente VARCHAR(12) = (SELECT tOP 1 PC1.CLIENTE FROM PROPOSTASCREDITO PC1
								                                                             INNER JOIN PROPOSTASOPERACOES PO
								                                                             ON PC1.PROPOSTA = PO.PROPOSTA
								                                                             INNER JOIN COMPLEMENTOS CLP
								                                                             ON PC1.CLIENTE = CLP.CLIENTE
								                                                             WHERE PC1.situacao = @Situacao 
								                                                             AND CLP.ESTCIVIL IN('S','V','D')
								                                                             AND PO.PRAZO <> 0 
								                                                             AND PC1.produto = '000712'
								                                                             AND PC1.etapa = @Etapa
								                                                             AND PO.FINDTPRIMVENCIM > (SELECT DATAMOVIMENTO FROM PARAMETROS) 
								                                                             AND PO.FINDTEMISSAO < (SELECT DATAMOVIMENTO FROM PARAMETROS)								                                          
								                                                             AND NOT EXISTS (SELECT 1 FROM PROPOSTASCREDITO PC2 WHERE PC1.CLIENTE = PC2.CLIENTE  GROUP BY CLIENTE HAVING count (PC2.CLIENTE) >= 2)                                                           
								                                                             ORDER BY DATAINCLUSAO DESC
								                                                             ) 
                                                            DECLARE @PropostaQ decimal = (select top 1 PROPOSTA FROM PROPOSTASCREDITO WHERE CLIENTE = @Cliente AND SITUACAO = @Situacao  AND etapa = @Etapa);
                                                            SELECT TOP 1 * FROM PROPOSTASCREDITO WHERE CLIENTE = @Cliente AND PROPOSTA = @PropostaQ;                                      
                                                            ",
                                        new { Produto, Situacao, Etapa }).FirstOrDefault();
        }




    }
}



//DECLARE @DiasCarenciaPlano INT = (SELECT CarenciaMinima FROM Planos WHERE plano = (SELECT Plano FROM PROPOSTASOPERACOES WHERE PROPOSTA = @PropostaQ));
//UPDATE PROPOSTASOPERACOES SET FINDTPRIMVENCIM = (SELECT DATAMOVIMENTO+@DiasCarenciaPlano FROM PARAMETROS) WHERE Proposta = @PropostaQ
//UPDATE PROPOSTASOPERACOES SET PRIMVENCIMVRG = (SELECT DATAMOVIMENTO+@DiasCarenciaPlano FROM PARAMETROS) WHERE Proposta = @PropostaQ
//UPDATE COMPLEMENTOS  
//                 SET 
//                 REFPESSNOME1= ''
//                ,REFPESSREL1 = ''
//                ,REFPESSDDD1 = ''
//                ,REFPESSFONE1= ''
//                ,REFPESSNOME2= ''
//                ,REFPESSREL2 = ''
//                ,REFPESSDDD2 = ''
//                ,REFPESSFONE2= ''
//                WHERE CLIENTE = @Cliente