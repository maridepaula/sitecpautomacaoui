﻿using System.Data.SqlClient;
using Dapper;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System;

namespace UiTestSiteCP.DataBase
{
    class PlanosRepositorySicred : RepositoryBase
    {
        public PlanosRepositorySicred(SqlConnection connection) : base(connection)
        { }

        public decimal ObterCarenciaMinimaPlano(int codigoPlano)
        {
            return Connection.QueryFirst<decimal>
                (@"SELECT CarenciaMinima FROM Planos WHERE plano = @CodigoPlano;", new { codigoPlano });
        }

        public dynamic ObterDadosParcelasContrato(string NrContrato)
        {
            return Connection.Query(@"SELECT *FROM PARCELAS WHERE CONTRATO = @NrContrato;", new { NrContrato }).Select(x => (ExpandoObject)ToExpandoObject(x));
        }

        public dynamic BuscaParametrosDBC()
        {
            return Connection.Query(@"SELECT TOP 1 *
												 FROM DebitoContaParametros 
												 WHERE Empresa  = 01
												 AND FamiliaProduto   = 'DEBITO'").FirstOrDefault();
        }
        

        public dynamic ObterDadosValoresPropostaOperacao(string NrContrato)
        {

            return Connection.Query<dynamic>(@"SELECT TOP 1 *FROM Propostasoperacoes WHERE proposta = @NrContrato;", new { NrContrato }).FirstOrDefault();

        }

        public dynamic ObterDadosDosContratosPorCPF(string CPF, int Situacao)
        {
            return Connection.Query(@"SELECT CL.NOME, 
	                                       SUBSTRING(CL.CLIENTE,1,11)AS CPF,
	                                       PC.PROPOSTA AS CONTRATO,
	                                       PO.VALORCOMPRA AS VALOROPERACAO,
	                                       PO.FINPRINCIPAL AS VALORLIBERADO,
	                                       PO.PRESTACAO AS PARCELA,
	                                       CAST(PO.PRAZO AS INT) AS PRAZO,
	                                       PRD.NOME AS PRODUTO,
	                                       (PC.LOJISTA +' - '+ LJTS.ABREVIATURA) AS LOJISTA,
	                                       (PC.LOJA +' - '+ LJS.ABREVIATURA) AS LOJA
                                        FROM PROPOSTASCREDITO PC
                                        INNER JOIN CLIENTES CL ON PC.cliente = CL.cliente
                                        INNER JOIN PROPOSTASOPERACOES PO ON PC.PROPOSTA = PO.PROPOSTA
                                        INNER JOIN PRODUTOSCDC PRD ON PC.PRODUTO = PRD.PRODUTO 
                                        INNER JOIN LOJISTAS LJTS ON PC.LOJISTA = LJTS.LOJISTA 
                                        INNER JOIN LOJAS LJS ON PC.LOJA = LJS.LOJA AND LJS.LOJISTA = PC.LOJISTA
                                        AND PC.cliente = @CPF
                                        AND PC.SITUACAO = @Situacao",
                                        new { CPF, Situacao }).Select(x => (ExpandoObject)ToExpandoObject(x));
        }

        public dynamic ObterDadosFiltroRelatorioPorSituacao(int IntervaldoDias, int situacao)
        {
            return Connection.Query<dynamic>(@"DECLARE @DATAMOVIMENTO AS DATETIME = (SELECT DATAMOVIMENTO FROM PARAMETROS)
                                                DECLARE @INTERVALODIAS INT = @IntervaldoDias
                                                SELECT TOP 1
                                                FORMAT(@DataMovimento-@INTERVALODIAS,'dd/MM/yyy') AS DATAINICIO, 
                                                FORMAT(@DataMovimento,'dd/MM/yyy') AS DATAFIM, 
                                                C1.LOJISTA,
                                                C1.LOJA,
                                                C1.CONTRATO
                                                FROM ContratosCDC C1
                                                INNER JOIN PROPOSTASCREDITO PC ON PC.PROPOSTA = C1.CONTRATO
                                                WHERE
                                                EXISTS (SELECT 1 FROM ContratosCDC C2 WHERE EMISSAO BETWEEN @DATAMOVIMENTO - @INTERVALODIAS AND @DATAMOVIMENTO GROUP BY Loja HAVING COUNT(Loja) = 1)
                                                AND PC.SITUACAO = @situacao
                                                ORDER BY EMISSAO DESC;",
                                                new { situacao }).FirstOrDefault();

        }

        public dynamic ObterDadosPorContratoRelatorioPropostaPorSituacao(string NrContrato)
        {
            return Connection.Query<dynamic>
                (@"SELECT CL.NOME, 
                            SUBSTRING(CL.CLIENTE, 1, 11)AS CPF,
                            RTRIM(PC.PROPOSTA) AS PROPOSTA,
                            PO.PRODUTO,
                            PO.PRAZO,
                            PO.PLANO,
                            FORMAT(PO.FINDTPRIMVENCIM, 'dd/MM/yyy') AS PRIMEIROVENCIMENTO,
                            RTRIM(PO.VALORCOMPRA) AS VALOROPERACAO,
                            RTRIM(PO.VALORLIBERACAO) AS VALORLIBERADO,
                            RTRIM(PO.PRESTACAO) AS PARCELA,
                            FORMAT(PC.DATAINCLUSAO, 'dd/MM/yyy') AS INCLUSAO,
                             PC.LOJA,
                            CASE PC.SITUACAO WHEN 20 THEN 'CONTRATO EFETIVADO'
                                ELSE 'SITUACAO NAO DEFINIDA' END,
                            FORMAT(CDC.EMISSAO, 'dd/MM/yyy') AS DATAEMISSAO,
                            FORMAT(PC.DATAEFETIVACAO, 'dd/MM/yyy') AS EFETIVACAO,
                            CASE CDC.SITUACAO WHEN 'A' THEN 'Aberto'
                                ELSE 'SITUACAO NAO DEFINIDA' END AS SITCONTRATO,
                            PC.LOJISTA,
                            RTRIM(PC.USUARIO)
                            FROM PROPOSTASCREDITO PC
                            INNER JOIN CLIENTES CL ON PC.cliente = CL.cliente
                            INNER JOIN PROPOSTASOPERACOES PO ON PC.PROPOSTA = PO.PROPOSTA
                            INNER JOIN PRODUTOSCDC PRD ON PC.PRODUTO = PRD.PRODUTO
                            INNER JOIN LOJISTAS LJTS ON PC.LOJISTA = LJTS.LOJISTA
                            INNER JOIN LOJAS LJS ON PC.LOJA = LJS.LOJA AND LJS.LOJISTA = PC.LOJISTA
                            INNER JOIN CONTRATOSCDC CDC ON PC.PROPOSTA = CDC.CONTRATO
                            AND PC.PROPOSTA = @NrContrato;", new { NrContrato }).FirstOrDefault();

        }

        public dynamic ObterDataValidaRelatLiberacoesDoDia(int codFormaLiberacao)
        {
            return Connection.Query<dynamic>(@"select top 1 
                                                    *
                                                    from     
                                                    liberacao lib WITH (INDEX (PK_LIBERACAO)) 
                                                    INNER JOIN contratosCdc co WITH (INDEX (PK_CONTRATOSCDC)) ON 
		                                                    lib.Empresa = co.Empresa    
		                                                    And Lib.Agencia = co.Agencia    
		                                                    And Lib.Contrato = co.Contrato    
                                                    INNER JOIN produtosCdc pc WITH (INDEX (PK_PRODUTOSCDC)) ON 
		                                                    pc.Empresa = co.Empresa    
		                                                    And pc.Produto = co.produto    
                                                    INNER JOIN Lojistas lo WITH (INDEX (PK_LOJISTAS)) ON 
		                                                    Lo.Empresa = co.Empresa    
		                                                    And Lo.Lojista = co.Lojista    
                                                    INNER JOIN Lojas lj WITH (INDEX (PK_LOJAS)) ON   
		                                                    Lj.Empresa = co.Empresa    
		                                                    And Lj.Lojista = co.Lojista    
		                                                    And Lj.Loja = co.Loja    
                                                    LEFT JOIN propostasCredito pt ON 
		                                                    Lib.Empresa = pt.Empresa  
		                                                    And Lib.Contrato = pt.Proposta  
                                                    Where     
		                                                    pc.Gerencial <> '12'
		                                                    and Lib.Forma = @codFormaLiberacao
		                                                    order by Lib.Liberacao Desc;
                    ",
                  new { codFormaLiberacao }).FirstOrDefault();

        }

        public dynamic ObterDadosRelatorioLiberacoesDoDia(DateTime DataLibercao, string nrContrato)
        {
            return Connection.Query<dynamic>
                (@"--DECLARE @Date DATE = (SELECT CONVERT(datetime, (FORMAT(22092003, '###/##/####')), 103))           
                        SELECT TOP 1
                        FORMAT(Lib.Liberacao, 'dd/MM/yyyy') AS DATALIBERACAO,
                        CASE Lib.FORMA
                        WHEN '1' THEN 'Cheque'
                        WHEN '2' THEN 'DOC'
                        WHEN '4' THEN 'Outros'
                        WHEN '5' THEN 'OrdemPagamento'
                        END AS FORMADEPAGAMENTO,
                        RTRIM(Cdc.Contrato) AS CONTRATO,
                        Cli.Nome
                        FROM contratosCdc Cdc
                        INNER JOIN Liberacao Lib
                        ON Lib.Contrato = Cdc.Contrato
                        INNER JOIN CLIENTES CLI
                        ON cli.CICCGC = Lib.CGCCPF
                        WHERE Cdc.CONTRATO = @nrContrato
                        AND Cdc.DataLIBERACAO = @DataLibercao  ",
          new { DataLibercao, nrContrato }).FirstOrDefault();



        }

        public dynamic ObterDadosFiltroRelatorioProducaoDiaria(string DataMinima,  int situacao)
        {
            return Connection.Query<dynamic>(@"DECLARE @EMPRESA CHAR(2) = '01'
                                                SELECT TOP 1
                                                LJ.AGENTECP, 
                                                FORMAT(PO.FINDTEMISSAO,'dd/MM/yyy') AS PERIODO, 
                                                -- DADOS GRID FRONT
                                                PO.PROPOSTA,
                                                C.NOME,
                                                C.CICCGC AS CPF, 
                                                P.USUARIO AS ATENDENTE,
                                                P.APROVADOR AS ANALISTA,
                                                PO.VALORCOMPRA AS VALOR,
                                                COALESCE(PO.PRAZO, P.PRAZOAPROVADO) AS PZ, --CLAUDIONEI - 31/03/2014
                                                (AG.AGENTE + ' - ' + AG.NOME) AS OPERADOR,
                                                LJ.LOJISTA,
                                                LJ.LOJA,
                                                PS.DESCRICAO AS SITUACAO
                                                FROM PROPOSTASOPERACOES PO WITH (NOLOCK)
                                                JOIN PROPOSTASCREDITO P WITH (NOLOCK) ON PO.EMPRESA = P.EMPRESA
                                                    AND PO.PROPOSTA = P.PROPOSTA
                                                    AND PO.LOJISTA = P.LOJISTA
                                                    AND PO.LOJA = P.LOJA
                                                    AND PO.FINDTEMISSAO >= @DataMinima
                                                JOIN PROPOSTASSITUACOES PS WITH (NOLOCK) ON PS.EMPRESA = P.EMPRESA
                                                    AND PS.CODSITUACAO = P.SITUACAO
                                                JOIN CLIENTES C WITH (NOLOCK) ON C.EMPRESA = P.EMPRESA
                                                    AND C.CLIENTE = P.CLIENTE
                                                JOIN LOJAS LJ WITH (NOLOCK) ON LJ.EMPRESA = P.EMPRESA
                                                    AND LJ.LOJISTA = P.LOJISTA
                                                    AND LJ.LOJA = P.LOJA
                                                    --AND LJ.AGENTECP = ISNULL(@AGENTECP, LJ.AGENTECP)
                                                JOIN AGENTES AG WITH (NOLOCK) ON AG.EMPRESA = P.EMPRESA
                                                    AND AG.AGENTE = LJ.AGENTECP
                                                WHERE P.SITUACAO = @situacao
                                                AND EXISTS (SELECT 1 FROM LOJAS LJ2 WHERE LJ.AGENTECP = LJ2.AGENTECP GROUP BY (LJ2.AGENTECP) HAVING LJ2.AGENTECP < 20) 
                                                OPTION (RECOMPILE);",
                                                new { DataMinima, situacao }).FirstOrDefault();

        }

    }
}
